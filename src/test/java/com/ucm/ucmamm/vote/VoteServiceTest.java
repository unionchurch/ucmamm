package com.ucm.ucmamm.vote;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import com.ucm.ucmamm.common.CacheService;
import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;
import java.util.function.UnaryOperator;

import static ch.qos.logback.classic.Level.DEBUG;
import static com.ucm.ucmamm.common.Caches.ELECTION;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.data.domain.Sort.Direction.DESC;

@RunWith(MockitoJUnitRunner.class)
public class VoteServiceTest {

    private static final String MOCK_ID = "1234";
    private static final String MOCK_NAME = "Mock UCM Election";
    private static final LocalDate MOCK_DATE = LocalDate.parse("2018-01-01");
    private static final LocalTime MOCK_TIME = LocalTime.parse("10:00:00");
    private static final Instant MOCK_TIME_INSTANT = Instant.parse("2018-01-01T00:00:00.000Z");
    private static final String MOCK_PROPOSAL_1 = "1";
    private static final String MOCK_PROPOSAL_2 = "2";
    private static final String MOCK_QUESTION_1 = "1";
    private static final String MOCK_QUESTION_2 = "2";
    private static final String MOCK_ANSWER_1 = "1";
    private static final String MOCK_ANSWER_2 = "2";
    private static final String MOCK_SEAT_1 = "1";
    private static final String MOCK_SEAT_2 = "2";
    private static final Integer MOCK_SEAT_COUNT = 14;
    private static final String MOCK_CANDIDATE_1 = "1";
    private static final String MOCK_CANDIDATE_2 = "2";
    private static final String MOCK_CANDIDATE_3 = "3";
    private static final String MOCK_CANDIDATE_4 = "4";

    private static final UnaryOperator<String> FIRST_NAME = suffix -> "FName" + suffix;
    private static final UnaryOperator<String> MIDDLE_NAME = suffix -> "MName" + suffix;
    private static final UnaryOperator<String> LAST_NAME = suffix -> "LName" + suffix;
    private static final Function<String, Long> MEMBER_ID = Long::parseLong;

    private static VoteInput voteInput;
    private static Vote vote;
    private static Vote savedVote;
    private static Election original;
    private static Election expectedModified;

    @InjectMocks
    private VoteService voteService;
    @Mock
    private VoteRepository voteRepository;
    @Mock
    private ElectionRepository electionRepository;
    @Mock
    private ConversionService conversionService;
    @Mock
    private DateService dateService;
    @Mock
    private CacheService cacheService;
    @Mock
    private Page<Vote> votePage;
    @Mock
    private Appender<ILoggingEvent> appender;
    @Captor
    private ArgumentCaptor<ILoggingEvent> argumentCaptor;

    @Before
    public void setUp() {
        Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logger.setLevel(DEBUG);
        logger.addAppender(appender);

        Map<String, Boolean> proposals = new HashMap<>();
        proposals.put(MOCK_PROPOSAL_1, TRUE);
        proposals.put(MOCK_PROPOSAL_2, Boolean.FALSE);
        Map<String, String> questions = new HashMap<>();
        questions.put(MOCK_QUESTION_1, MOCK_ANSWER_1);
        questions.put(MOCK_PROPOSAL_2, MOCK_ANSWER_2);
        List<SeatVote> seatVotes = Arrays.asList(
                SeatVote.builder()
                        .candidateId(MOCK_CANDIDATE_1)
                        .approved(TRUE)
                        .build(),
                SeatVote.builder()
                        .candidateId(MOCK_CANDIDATE_2)
                        .approved(Boolean.FALSE)
                        .build());
        Map<String, List<SeatVote>> seats = Collections.singletonMap("1", seatVotes);

        voteInput = VoteInput.builder()
                .memberId(1L)
                .electionId(MOCK_ID)
                .proposals(proposals)
                .questions(questions)
                .seats(seats)
                .build();
        vote = Vote.builder()
                .time(MOCK_TIME_INSTANT)
                .memberId(1L)
                .electionId(MOCK_ID)
                .proposals(proposals)
                .questions(questions)
                .seats(seats)
                .deleted(Boolean.FALSE)
                .build();
        savedVote = vote.withId("1");

        original = Election.builder()
                .id(MOCK_ID)
                .name(MOCK_NAME)
                .date(MOCK_DATE)
                .startTime(MOCK_TIME)
                .endTime(MOCK_TIME)
                .proposal(Proposal.builder()
                        .id(MOCK_PROPOSAL_1)
                        .content(MOCK_PROPOSAL_1)
                        .approved(0L)
                        .disapproved(0L)
                        .build())
                .proposal(Proposal.builder()
                        .id(MOCK_PROPOSAL_2)
                        .content(MOCK_PROPOSAL_2)
                        .approved(0L)
                        .disapproved(0L)
                        .build())
                .question(Question.builder()
                        .id(MOCK_QUESTION_1)
                        .content(MOCK_QUESTION_1)
                        .answer(Answer.builder()
                                .id(MOCK_ANSWER_1)
                                .content(MOCK_ANSWER_1)
                                .selected(0L)
                                .build())
                        .answer(Answer.builder()
                                .id(MOCK_ANSWER_2)
                                .content(MOCK_ANSWER_2)
                                .selected(0L)
                                .build())
                        .build())
                .question(Question.builder()
                        .id(MOCK_QUESTION_2)
                        .content(MOCK_QUESTION_2)
                        .answer(Answer.builder()
                                .id(MOCK_ANSWER_1)
                                .content(MOCK_ANSWER_1)
                                .selected(0L)
                                .build())
                        .answer(Answer.builder()
                                .id(MOCK_ANSWER_2)
                                .content(MOCK_ANSWER_2)
                                .selected(0L)
                                .build())
                        .build())
                .seat(Seat.builder()
                        .id(MOCK_SEAT_1)
                        .name(MOCK_SEAT_1)
                        .count(MOCK_SEAT_COUNT)
                        .candidate(Candidate.builder()
                                .id(MOCK_CANDIDATE_1)
                                .memberId(MEMBER_ID.apply(MOCK_CANDIDATE_1))
                                .firstName(FIRST_NAME.apply(MOCK_CANDIDATE_1))
                                .middleName(MIDDLE_NAME.apply(MOCK_CANDIDATE_1))
                                .lastName(LAST_NAME.apply(MOCK_CANDIDATE_1))
                                .approved(0L)
                                .disapproved(0L)
                                .build())
                        .candidate(Candidate.builder()
                                .id(MOCK_CANDIDATE_2)
                                .memberId(MEMBER_ID.apply(MOCK_CANDIDATE_2))
                                .firstName(FIRST_NAME.apply(MOCK_CANDIDATE_2))
                                .middleName(MIDDLE_NAME.apply(MOCK_CANDIDATE_2))
                                .lastName(LAST_NAME.apply(MOCK_CANDIDATE_2))
                                .approved(0L)
                                .disapproved(0L)
                                .build())
                        .build())
                .seat(Seat.builder()
                        .id(MOCK_SEAT_2)
                        .name(MOCK_SEAT_2)
                        .count(MOCK_SEAT_COUNT)
                        .candidate(Candidate.builder()
                                .id(MOCK_CANDIDATE_3)
                                .memberId(MEMBER_ID.apply(MOCK_CANDIDATE_3))
                                .firstName(FIRST_NAME.apply(MOCK_CANDIDATE_3))
                                .middleName(MIDDLE_NAME.apply(MOCK_CANDIDATE_3))
                                .lastName(LAST_NAME.apply(MOCK_CANDIDATE_3))
                                .approved(0L)
                                .disapproved(0L)
                                .build())
                        .candidate(Candidate.builder()
                                .id(MOCK_CANDIDATE_4)
                                .memberId(MEMBER_ID.apply(MOCK_CANDIDATE_4))
                                .firstName(FIRST_NAME.apply(MOCK_CANDIDATE_4))
                                .middleName(MIDDLE_NAME.apply(MOCK_CANDIDATE_4))
                                .lastName(LAST_NAME.apply(MOCK_CANDIDATE_4))
                                .approved(0L)
                                .disapproved(0L)
                                .build())
                        .build())
                .deleted(FALSE)
                .active(TRUE)
                .version(1)
                .createdBy("ADMIN")
                .createdAt(MOCK_TIME_INSTANT)
                .build();
        expectedModified = Election.builder()
                .id(MOCK_ID)
                .name(MOCK_NAME)
                .date(MOCK_DATE)
                .startTime(MOCK_TIME)
                .endTime(MOCK_TIME)
                .proxyVoted(0L)
                .proposal(Proposal.builder()
                        .id(MOCK_PROPOSAL_1)
                        .content(MOCK_PROPOSAL_1)
                        .approved(1L)
                        .disapproved(0L)
                        .build())
                .proposal(Proposal.builder()
                        .id(MOCK_PROPOSAL_2)
                        .content(MOCK_PROPOSAL_2)
                        .approved(0L)
                        .disapproved(1L)
                        .build())
                .question(Question.builder()
                        .id(MOCK_QUESTION_1)
                        .content(MOCK_QUESTION_1)
                        .answer(Answer.builder()
                                .id(MOCK_ANSWER_1)
                                .content(MOCK_ANSWER_1)
                                .selected(1L)
                                .build())
                        .answer(Answer.builder()
                                .id(MOCK_ANSWER_2)
                                .content(MOCK_ANSWER_2)
                                .selected(0L)
                                .build())
                        .build())
                .question(Question.builder()
                        .id(MOCK_QUESTION_2)
                        .content(MOCK_QUESTION_2)
                        .answer(Answer.builder()
                                .id(MOCK_ANSWER_1)
                                .content(MOCK_ANSWER_1)
                                .selected(0L)
                                .build())
                        .answer(Answer.builder()
                                .id(MOCK_ANSWER_2)
                                .content(MOCK_ANSWER_2)
                                .selected(1L)
                                .build())
                        .build())
                .seat(Seat.builder()
                        .id(MOCK_SEAT_1)
                        .name(MOCK_SEAT_1)
                        .count(MOCK_SEAT_COUNT)
                        .candidate(Candidate.builder()
                                .id(MOCK_CANDIDATE_1)
                                .memberId(MEMBER_ID.apply(MOCK_CANDIDATE_1))
                                .firstName(FIRST_NAME.apply(MOCK_CANDIDATE_1))
                                .middleName(MIDDLE_NAME.apply(MOCK_CANDIDATE_1))
                                .lastName(LAST_NAME.apply(MOCK_CANDIDATE_1))
                                .approved(1L)
                                .disapproved(0L)
                                .build())
                        .candidate(Candidate.builder()
                                .id(MOCK_CANDIDATE_2)
                                .memberId(MEMBER_ID.apply(MOCK_CANDIDATE_2))
                                .firstName(FIRST_NAME.apply(MOCK_CANDIDATE_2))
                                .middleName(MIDDLE_NAME.apply(MOCK_CANDIDATE_2))
                                .lastName(LAST_NAME.apply(MOCK_CANDIDATE_2))
                                .approved(0L)
                                .disapproved(1L)
                                .build())
                        .build())
                .seat(Seat.builder()
                        .id(MOCK_SEAT_2)
                        .name(MOCK_SEAT_2)
                        .count(MOCK_SEAT_COUNT)
                        .candidate(Candidate.builder()
                                .id(MOCK_CANDIDATE_3)
                                .memberId(MEMBER_ID.apply(MOCK_CANDIDATE_3))
                                .firstName(FIRST_NAME.apply(MOCK_CANDIDATE_3))
                                .middleName(MIDDLE_NAME.apply(MOCK_CANDIDATE_3))
                                .lastName(LAST_NAME.apply(MOCK_CANDIDATE_3))
                                .approved(0L)
                                .disapproved(0L)
                                .build())
                        .candidate(Candidate.builder()
                                .id(MOCK_CANDIDATE_4)
                                .memberId(MEMBER_ID.apply(MOCK_CANDIDATE_4))
                                .firstName(FIRST_NAME.apply(MOCK_CANDIDATE_4))
                                .middleName(MIDDLE_NAME.apply(MOCK_CANDIDATE_4))
                                .lastName(LAST_NAME.apply(MOCK_CANDIDATE_4))
                                .approved(0L)
                                .disapproved(0L)
                                .build())
                        .build())
                .deleted(FALSE)
                .active(TRUE)
                .version(2)
                .createdBy("ADMIN")
                .createdAt(MOCK_TIME_INSTANT)
                .updatedBy("SYSTEM")
                .updatedAt(MOCK_TIME_INSTANT)
                .build();
    }

    @Test
    public void givenVoteInput_whenSave_thenShouldReturnSavedVote() {
        when(conversionService.convert(voteInput, Vote.class)).thenReturn(vote);
        when(voteRepository.save(vote)).thenReturn(savedVote);

        Vote actual = voteService.saveVote(voteInput);

        assertEquals(savedVote, actual);

        verify(conversionService).convert(voteInput, Vote.class);
        verify(voteRepository).save(vote);
    }

    @Test
    public void givenSavedElection_whenTally_thenShouldUpdateElectionAccordingly() {
        when(electionRepository.findByActiveAndDeleted(TRUE, FALSE, Sort.by(DESC, "date")))
                .thenReturn(singletonList(original));
        when(electionRepository.save(any(Election.class))).thenReturn(original);
        when(voteRepository.findByTalliedAndDeleted(FALSE, FALSE, PageRequest.of(0, 10, DESC, "time")))
                .thenReturn(votePage);
        when(votePage.hasContent()).thenReturn(TRUE);
        when(votePage.getContent()).thenReturn(singletonList(vote));
        when(votePage.getTotalElements()).thenReturn(1L);
        when(dateService.getCurrentTime()).thenReturn(MOCK_TIME_INSTANT);

        voteService.tallyVotes();

        verify(appender, times(2)).doAppend(argumentCaptor.capture());
        List<ILoggingEvent> loggingEvents = argumentCaptor.getAllValues();
        assertEquals("Running thread for tallying of votes...", loggingEvents.get(0).getFormattedMessage());
        assertEquals("Votes tallied: 1", loggingEvents.get(1).getFormattedMessage());

        verify(electionRepository).findByActiveAndDeleted(TRUE, FALSE, Sort.by(DESC, "date"));
        verify(voteRepository).findByTalliedAndDeleted(FALSE, FALSE, PageRequest.of(0, 10, DESC, "time"));
        verify(voteRepository).save(any(Vote.class));
        verify(votePage).hasContent();
        verify(votePage).getContent();
        verify(electionRepository).save(expectedModified.withVoted(1L));
        verify(dateService).getCurrentTime();
        verify(votePage).getTotalElements();
        verify(cacheService).evictCache(ELECTION, "active_election");
        verify(cacheService).evictCache(ELECTION, "1234");
    }

    @Test
    public void givenNoActiveElection_whenVote_thenShouldNotUpdateElection() {
        when(electionRepository.findByActiveAndDeleted(TRUE, FALSE, Sort.by(DESC, "date")))
                .thenReturn(emptyList());

        voteService.tallyVotes();

        verify(appender, times(2)).doAppend(argumentCaptor.capture());
        List<ILoggingEvent> loggingEvents = argumentCaptor.getAllValues();
        assertEquals("Running thread for tallying of votes...", loggingEvents.get(0).getFormattedMessage());
        assertEquals("No active election was found.", loggingEvents.get(1).getFormattedMessage());

        verify(electionRepository).findByActiveAndDeleted(TRUE, FALSE, Sort.by(DESC, "date"));
        verify(voteRepository, never()).findByTalliedAndDeleted(FALSE, FALSE, PageRequest.of(0, 10, DESC, "time"));
        verify(votePage, never()).hasContent();
        verify(votePage, never()).getContent();
        verify(electionRepository, never()).save(expectedModified);
        verify(dateService, never()).getCurrentTime();
        verify(votePage, never()).getTotalElements();
    }

    @Test
    public void givenActiveElectionButNoVotes_whenVote_thenShouldNotUpdateElection() {
        when(electionRepository.findByActiveAndDeleted(TRUE, FALSE, Sort.by(DESC, "date")))
                .thenReturn(singletonList(original));
        when(voteRepository.findByTalliedAndDeleted(FALSE, FALSE, PageRequest.of(0, 10, DESC, "time")))
                .thenReturn(votePage);
        when(votePage.hasContent()).thenReturn(FALSE);

        voteService.tallyVotes();

        verify(appender, times(2)).doAppend(argumentCaptor.capture());
        List<ILoggingEvent> loggingEvents = argumentCaptor.getAllValues();
        assertEquals("Running thread for tallying of votes...", loggingEvents.get(0).getFormattedMessage());
        assertEquals("No votes need to tally.", loggingEvents.get(1).getFormattedMessage());

        verify(electionRepository).findByActiveAndDeleted(TRUE, FALSE, Sort.by(DESC, "date"));
        verify(voteRepository).findByTalliedAndDeleted(FALSE, FALSE, PageRequest.of(0, 10, DESC, "time"));
        verify(votePage).hasContent();
        verify(votePage, never()).getContent();
        verify(electionRepository, never()).save(expectedModified);
        verify(dateService, never()).getCurrentTime();
        verify(votePage, never()).getTotalElements();
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(voteRepository, electionRepository, conversionService, dateService, cacheService, votePage);
    }

}
