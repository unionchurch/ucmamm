package com.ucm.ucmamm.vote;

import com.ucm.ucmamm.AuthServerConfiguration;
import com.ucm.ucmamm.ResourceServerConfiguration;
import com.ucm.ucmamm.UcmAmmOauth2Properties;
import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.common.ResourceTestSupport;
import com.ucm.ucmamm.domain.*;
import com.ucm.ucmamm.election.ElectionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static java.util.Optional.of;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = VoteResourceTest.Main.class)
public class VoteResourceTest extends ResourceTestSupport {

    @SpringBootApplication(scanBasePackages = {"com.ucm.ucmamm.common", "com.ucm.ucmamm.vote"}, exclude = MongoAutoConfiguration.class)
    @Import({UcmAmmOauth2Properties.class, ResourceServerConfiguration.class, AuthServerConfiguration.class, ResourceTestSupport.SecurityConfiguration.class})
    public static class Main {

        public static void main(String[] args) {
            SpringApplication.run(VoteResourceTest.Main.class, args);
        }

    }

    @MockBean
    private VoteService voteService;
    @MockBean
    private DateService dateService;
    @MockBean
    private VoteRepository voteRepository;
    @MockBean
    private VoterRepository voterRepository;
    @MockBean
    private ElectionRepository electionRepository;
    @MockBean
    private MemberRepository memberRepository;

    @Override
    protected void initializeMocks() {
        when(dateService.getCurrentDateTime()).thenReturn(LocalDateTime.parse("2019-01-02T12:00:00"));
        when(dateService.mergeDateAndTime(LocalDate.parse("2019-01-02"), LocalTime.parse("10:00:00")))
                .thenReturn(LocalDateTime.parse("2019-01-02T10:00:00"));
        when(dateService.mergeDateAndTime(LocalDate.parse("2019-01-02"), LocalTime.parse("17:00:00")))
                .thenReturn(LocalDateTime.parse("2019-01-02T17:00:00"));
        when(electionRepository.findByIdAndDeleted(anyString(), anyBoolean()))
                .thenReturn(of(Election.builder()
                        .date(LocalDate.parse("2019-01-02"))
                        .startTime(LocalTime.parse("10:00:00"))
                        .endTime(LocalTime.parse("17:00:00"))
                        .build()));
        when(memberRepository.findByMemberIdAndDeleted(anyLong(), anyBoolean())).thenReturn(of(Member.builder().build()));
        when(voterRepository.findByMemberIdAndElectionId(anyLong(), anyString())).thenReturn(of(Voter.builder().build()));
    }

    @Test
    public void givenVoteInput_whenPost_thenShouldReturnNoContent() throws Exception {
        Map<String, Boolean> proposals = new HashMap<>();
        proposals.put("1", Boolean.TRUE);
        proposals.put("2", Boolean.FALSE);
        Map<String, String> questions = new HashMap<>();
        questions.put("1", "1");
        questions.put("2", "1");
        List<SeatVote> seatVotes = Arrays.asList(
                SeatVote.builder()
                        .candidateId("1")
                        .approved(Boolean.TRUE)
                        .build(),
                SeatVote.builder()
                        .candidateId("2")
                        .approved(Boolean.FALSE)
                        .build());
        Map<String, List<SeatVote>> seats = Collections.singletonMap("1", seatVotes);
        VoteInput voteInput = VoteInput.builder()
                .memberId(1L)
                .electionId("1234")
                .proposals(proposals)
                .questions(questions)
                .seats(seats)
                .build();

        mockMvc.perform(post("/api/v1/votes")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken())
                .content(toJson(voteInput)))
                .andExpect(status().isNoContent());
    }

}
