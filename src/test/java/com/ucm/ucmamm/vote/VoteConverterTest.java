package com.ucm.ucmamm.vote;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.SeatVote;
import com.ucm.ucmamm.domain.Vote;
import com.ucm.ucmamm.domain.Voter;
import com.ucm.ucmamm.domain.VoterRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;
import java.util.*;

import static java.lang.Boolean.FALSE;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class VoteConverterTest {

    private static final Instant MOCK_TIME = Instant.parse("2018-01-01T00:00:00.000Z");

    @InjectMocks
    private VoteConverter voteConverter;
    @Mock
    private DateService dateService;
    @Mock
    private VoterRepository voterRepository;

    @Before
    public void setUp() {
        when(dateService.getCurrentTime()).thenReturn(MOCK_TIME);
        when(voterRepository.findByMemberIdAndElectionId(anyLong(), anyString())).thenReturn(of(Voter.builder().build()));
    }

    @Test
    public void givenVoteInput_whenConvert_thenShouldReturnCorrectVote() {
        Map<String, Boolean> proposals = new HashMap<>();
        proposals.put("1", Boolean.TRUE);
        proposals.put("2", FALSE);

        Map<String, String> questions = new HashMap<>();
        questions.put("1", "1");
        questions.put("2", "1");

        List<SeatVote> seatVotes = Arrays.asList(
                SeatVote.builder()
                        .candidateId("1")
                        .approved(Boolean.TRUE)
                        .build(),
                SeatVote.builder()
                        .candidateId("2")
                        .approved(FALSE)
                        .build());
        Map<String, List<SeatVote>> seats = Collections.singletonMap("1", seatVotes);

        VoteInput voteInput = VoteInput.builder()
                .memberId(1L)
                .electionId("1234")
                .proposals(proposals)
                .questions(questions)
                .seats(seats)
                .build();
        Vote vote = voteConverter.convert(voteInput);

        assertEquals(Vote.builder()
                .time(MOCK_TIME)
                .memberId(1L)
                .electionId("1234")
                .proposals(proposals)
                .questions(questions)
                .seats(seats)
                .proxy(FALSE)
                .deleted(FALSE)
                .tallied(FALSE)
                .build(), vote);

        verify(dateService).getCurrentTime();
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(dateService);
    }

}
