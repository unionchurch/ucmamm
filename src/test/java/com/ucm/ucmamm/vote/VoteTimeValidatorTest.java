package com.ucm.ucmamm.vote;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.Election;
import com.ucm.ucmamm.domain.ElectionRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;

import static java.lang.Boolean.FALSE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class VoteTimeValidatorTest {

    @InjectMocks
    private VoteTimeValidator voteTimeValidator;
    @Mock
    private ElectionRepository electionRepository;
    @Mock
    private DateService dateService;
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(voteTimeValidator, "enabled", true);
    }

    @Test
    public void whenVoteIsDoneBeforeElectionTimeRangeThenShouldReturnFalse() {
        when(electionRepository.findByIdAndDeleted(anyString(), anyBoolean()))
                .thenReturn(Optional.of(Election.builder()
                        .id("1234")
                        .date(LocalDate.parse("2019-01-02"))
                        .startTime(LocalTime.parse("10:00:00"))
                        .endTime(LocalTime.parse("17:00:00"))
                        .build()));
        when(dateService.getCurrentDateTime()).thenReturn(LocalDateTime.parse("2019-01-02T08:00:00"));
        doCallRealMethod().when(dateService).mergeDateAndTime(any(LocalDate.class), any(LocalTime.class));

        assertFalse(voteTimeValidator.isValid("1234", constraintValidatorContext));

        verify(electionRepository).findByIdAndDeleted("1234", FALSE);
        verify(dateService).getCurrentDateTime();
        verify(dateService, times(2)).mergeDateAndTime(any(LocalDate.class), any(LocalTime.class));
    }

    @Test
    public void whenVoteIsDoneAfterElectionTimeRangeThenShouldReturnFalse() {
        when(electionRepository.findByIdAndDeleted(anyString(), anyBoolean()))
                .thenReturn(Optional.of(Election.builder()
                        .id("1234")
                        .date(LocalDate.parse("2019-01-02"))
                        .startTime(LocalTime.parse("10:00:00"))
                        .endTime(LocalTime.parse("17:00:00"))
                        .build()));
        when(dateService.getCurrentDateTime()).thenReturn(LocalDateTime.parse("2019-01-02T17:00:05"));
        doCallRealMethod().when(dateService).mergeDateAndTime(any(LocalDate.class), any(LocalTime.class));

        assertFalse(voteTimeValidator.isValid("1234", constraintValidatorContext));

        verify(electionRepository).findByIdAndDeleted("1234", FALSE);
        verify(dateService).getCurrentDateTime();
        verify(dateService, times(2)).mergeDateAndTime(any(LocalDate.class), any(LocalTime.class));
    }

    @Test
    public void whenVoteIsDoneWithinElectionTimeRangeThenShouldReturnTrue() {
        when(electionRepository.findByIdAndDeleted(anyString(), anyBoolean()))
                .thenReturn(Optional.of(Election.builder()
                        .id("1234")
                        .date(LocalDate.parse("2019-01-02"))
                        .startTime(LocalTime.parse("10:00:00"))
                        .endTime(LocalTime.parse("17:00:00"))
                        .build()));
        when(dateService.getCurrentDateTime()).thenReturn(LocalDateTime.parse("2019-01-02T12:00:00"));
        doCallRealMethod().when(dateService).mergeDateAndTime(any(LocalDate.class), any(LocalTime.class));

        assertTrue(voteTimeValidator.isValid("1234", constraintValidatorContext));

        verify(electionRepository).findByIdAndDeleted("1234", FALSE);
        verify(dateService).getCurrentDateTime();
        verify(dateService, times(2)).mergeDateAndTime(any(LocalDate.class), any(LocalTime.class));
    }

    @Test
    public void whenValidationIsDisabledThenShouldAlwaysReturnTrue() {
        ReflectionTestUtils.setField(voteTimeValidator, "enabled", false);

        assertTrue(voteTimeValidator.isValid("1234", constraintValidatorContext));

        verify(electionRepository, never()).findByIdAndDeleted("1234", FALSE);
        verify(dateService, never()).getCurrentDateTime();
        verify(dateService, never()).mergeDateAndTime(any(LocalDate.class), any(LocalTime.class));
    }

    @After
    public void teardown() {
        verifyNoMoreInteractions(electionRepository, dateService);
    }

}
