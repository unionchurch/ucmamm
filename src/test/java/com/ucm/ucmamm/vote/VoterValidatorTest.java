package com.ucm.ucmamm.vote;

import com.ucm.ucmamm.domain.Voter;
import com.ucm.ucmamm.domain.VoterRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class VoterValidatorTest {

    @InjectMocks
    private VoterValidator voterValidator;

    @Mock
    private VoterRepository voterRepository;
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Test
    public void givenRegisteredVoter_whenValidate_thenShouldReturnTrue() {
        when(voterRepository.findByMemberIdAndElectionId(anyLong(), anyString())).thenReturn(of(Voter.builder().build()));

        assertTrue(voterValidator.isValid(MemberIdAndElectionId.builder().electionId("1234").memberId(1234L).build(), constraintValidatorContext));

        verify(voterRepository).findByMemberIdAndElectionId(1234L, "1234");
    }

    @Test
    public void givenNonRegisteredVoter_whenValidate_thenShouldReturnFalse() {
        when(voterRepository.findByMemberIdAndElectionId(anyLong(), anyString())).thenReturn(empty());

        assertFalse(voterValidator.isValid(MemberIdAndElectionId.builder().electionId("1234").memberId(1234L).build(), constraintValidatorContext));

        verify(voterRepository).findByMemberIdAndElectionId(1234L, "1234");
    }

    @After
    public void teardown() {
        verifyNoMoreInteractions(voterRepository);
    }

}
