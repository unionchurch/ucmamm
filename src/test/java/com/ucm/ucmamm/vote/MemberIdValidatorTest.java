package com.ucm.ucmamm.vote;

import com.ucm.ucmamm.domain.Member;
import com.ucm.ucmamm.domain.MemberRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MemberIdValidatorTest {

    @InjectMocks
    private MemberIdValidator memberIdValidator;
    @Mock
    private MemberRepository memberRepository;
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Test
    public void givenMemberIdIsInvalid_whenValidate_thenShouldReturnFalse() {
        when(memberRepository.findByMemberIdAndDeleted(anyLong(), anyBoolean())).thenReturn(empty());

        assertFalse(memberIdValidator.isValid(1L, constraintValidatorContext));

        verify(memberRepository).findByMemberIdAndDeleted(1L, Boolean.FALSE);
    }

    @Test
    public void givenMemberIdIsValid_whenValidate_thenShouldReturnTrue() {
        when(memberRepository.findByMemberIdAndDeleted(anyLong(), anyBoolean())).thenReturn(of(Member.builder().build()));

        assertTrue(memberIdValidator.isValid(1L, constraintValidatorContext));

        verify(memberRepository).findByMemberIdAndDeleted(1L, Boolean.FALSE);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(memberRepository);
    }

}
