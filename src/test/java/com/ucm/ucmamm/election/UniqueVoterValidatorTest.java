package com.ucm.ucmamm.election;

import com.ucm.ucmamm.domain.Voter;
import com.ucm.ucmamm.domain.VoterRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UniqueVoterValidatorTest {

    @InjectMocks
    private UniqueVoterValidator uniqueVoteValidator;
    @Mock
    private VoterRepository voterRepository;
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Test
    public void givenMemberIdAndElectionIdAlreadyExists_whenValidate_thenShouldReturnFalse() {
        when(voterRepository.findByMemberIdAndElectionId(anyLong(), anyString())).thenReturn(of(Voter.builder().build()));

        assertFalse(uniqueVoteValidator.isValid(MemberIdAndElectionId.builder().memberId(1L).electionId("1").build(), constraintValidatorContext));

        verify(voterRepository).findByMemberIdAndElectionId(1L, "1");
    }

    @Test
    public void givenMemberIdAndElectionIdDoesNotExists_whenValidate_thenShouldReturnTrue() {
        when(voterRepository.findByMemberIdAndElectionId(anyLong(), anyString())).thenReturn(empty());

        assertTrue(uniqueVoteValidator.isValid(MemberIdAndElectionId.builder().memberId(1L).electionId("1").build(), constraintValidatorContext));

        verify(voterRepository).findByMemberIdAndElectionId(1L, "1");
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(voterRepository);
    }

}
