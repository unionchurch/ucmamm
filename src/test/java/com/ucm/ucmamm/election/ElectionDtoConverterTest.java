package com.ucm.ucmamm.election;

import com.ucm.ucmamm.domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;

@RunWith(BlockJUnit4ClassRunner.class)
public class ElectionDtoConverterTest {

    private static final String MOCK_NAME = "2018 UCM Annual Members Meeting";
    private static final LocalDate MOCK_DATE = LocalDate.parse("2018-01-01");
    private static final LocalTime MOCK_START_TIME = LocalTime.parse("00:00:00.000");
    private static final LocalTime MOCK_END_TIME = LocalTime.parse("00:00:00.000");

    private static final String MOCK_PROPOSAL_1 = "Mock Proposal 1";
    private static final String MOCK_PROPOSAL_2 = "Mock Proposal 2";

    private static final String MOCK_QUESTION_1 = "Mock Question 1";
    private static final String MOCK_QUESTION_2 = "Mock Question 2";
    private static final String MOCK_ANSWER_1 = "Mock Answer 1";
    private static final String MOCK_ANSWER_2 = "Mock Answer 2";
    private static final String MOCK_ANSWER_3 = "Mock Answer 3";
    private static final String MOCK_ANSWER_4 = "Mock Answer 4";

    private static final String MOCK_SEAT = "Council Member";
    private static final Long MOCK_MEMBER_ID_1 = 1234L;
    private static final String MOCK_FIRST_NAME_1 = "Feliz";
    private static final String MOCK_LAST_NAME_1 = "Navidad";
    private static final String MOCK_SUFFIX_1 = "IV";
    private static final Long MOCK_MEMBER_ID_2 = 1235L;
    private static final String MOCK_FIRST_NAME_2 = "Hannah";
    private static final String MOCK_MIDDLE_NAME_2 = "R.";
    private static final String MOCK_LAST_NAME_2 = "Navidad";

    private static final Instant MOCK_INSTANT = Instant.parse("2018-01-01T00:00:00.000Z");

    private static final Election MOCK_ELECTION = Election.builder()
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_1)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .proposal(Proposal.builder()
                    .id("2")
                    .content(MOCK_PROPOSAL_2)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("3")
                    .content(MOCK_QUESTION_1)
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_1)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("5")
                            .content(MOCK_ANSWER_2)
                            .selected(0L)
                            .build())
                    .build())
            .question(Question.builder()
                    .id("6")
                    .content(MOCK_QUESTION_2)
                    .answer(Answer.builder()
                            .id("7")
                            .content(MOCK_ANSWER_3)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("8")
                            .content(MOCK_ANSWER_4)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("9")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("10")
                            .memberId(MOCK_MEMBER_ID_1)
                            .firstName(MOCK_FIRST_NAME_1)
                            .lastName(MOCK_LAST_NAME_1)
                            .suffix(MOCK_SUFFIX_1)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .candidate(Candidate.builder()
                            .id("11")
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .createdBy("admin")
            .createdAt(MOCK_INSTANT)
            .build();

    private static final ElectionDto EXPECTED_RESULT = ElectionDto.builder()
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_1)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .proposal(Proposal.builder()
                    .id("2")
                    .content(MOCK_PROPOSAL_2)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("3")
                    .content(MOCK_QUESTION_1)
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_1)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("5")
                            .content(MOCK_ANSWER_2)
                            .selected(0L)
                            .build())
                    .build())
            .question(Question.builder()
                    .id("6")
                    .content(MOCK_QUESTION_2)
                    .answer(Answer.builder()
                            .id("7")
                            .content(MOCK_ANSWER_3)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("8")
                            .content(MOCK_ANSWER_4)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("9")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("10")
                            .memberId(MOCK_MEMBER_ID_1)
                            .firstName(MOCK_FIRST_NAME_1)
                            .lastName(MOCK_LAST_NAME_1)
                            .suffix(MOCK_SUFFIX_1)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .candidate(Candidate.builder()
                            .id("11")
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .build();

    private ElectionDtoConverter electionDtoConverter = new ElectionDtoConverter();

    @Test
    public void givenElection_whenConvert_thenShouldReturnElectionDto() {
        ElectionDto electionDto = electionDtoConverter.convert(MOCK_ELECTION);

        assertEquals(EXPECTED_RESULT, electionDto);
    }

}
