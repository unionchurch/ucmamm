package com.ucm.ucmamm.election;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NonActiveElectionValidatorTest {

    @InjectMocks
    private NonActiveElectionValidator nonActiveElectionValidator;
    @Mock
    private ElectionService electionService;
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Test
    public void givenNoActiveElections_whenValidate_thenShouldReturnTrue() {
        when(electionService.getActiveElection()).thenReturn(empty());

        assertTrue(nonActiveElectionValidator.isValid(ElectionIdAndActive.builder().id("1").active(TRUE).build(), constraintValidatorContext));

        verify(electionService).getActiveElection();
    }

    @Test
    public void givenActiveElectionMatchingIdsAndActivate_whenValidate_thenShouldReturnFalse() {
        when(electionService.getActiveElection()).thenReturn(of(ElectionDto.builder().id("1").build()));

        assertFalse(nonActiveElectionValidator.isValid(ElectionIdAndActive.builder().id("1").active(TRUE).build(), constraintValidatorContext));

        verify(electionService).getActiveElection();
    }

    @Test
    public void givenActiveElectionMatchingIdsAndInactivate_whenValidate_thenShouldReturnTrue() {
        when(electionService.getActiveElection()).thenReturn(of(ElectionDto.builder().id("1").build()));

        assertTrue(nonActiveElectionValidator.isValid(ElectionIdAndActive.builder().id("1").active(FALSE).build(), constraintValidatorContext));

        verify(electionService).getActiveElection();
    }

    @Test
    public void givenActiveElectionNonMatchingIds_whenValidate_thenShouldReturnTrue() {
        when(electionService.getActiveElection()).thenReturn(of(ElectionDto.builder().id("2").build()));

        assertTrue(nonActiveElectionValidator.isValid(ElectionIdAndActive.builder().id("1").active(TRUE).build(), constraintValidatorContext));

        verify(electionService).getActiveElection();
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(electionService);
    }

}
