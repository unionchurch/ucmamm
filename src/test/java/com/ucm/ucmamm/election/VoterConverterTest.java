package com.ucm.ucmamm.election;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.Member;
import com.ucm.ucmamm.domain.MemberRepository;
import com.ucm.ucmamm.domain.ProxyDetails;
import com.ucm.ucmamm.domain.Voter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class VoterConverterTest {

    private static final Instant MOCK_INSTANT = Instant.parse("2018-01-01T00:00:00.000Z");

    @InjectMocks
    private VoterConverter voterConverter;

    @Mock
    private MemberRepository memberRepository;
    @Mock
    private DateService dateService;

    @Before
    public void setUp() {
        when(dateService.getCurrentTime()).thenReturn(MOCK_INSTANT);
    }

    @Test
    public void givenExistingMember_whenConvert_thenShouldReturnVoter() {
        when(memberRepository.findByMemberIdAndDeleted(anyLong(), anyBoolean()))
                .thenReturn(of(Member.builder()
                        .memberId(1L)
                        .firstName("First")
                        .middleName("Middle")
                        .lastName("Last")
                        .suffix("Suffix")
                        .build()));

        Voter voter = voterConverter.convert(VoterInput.builder()
                .memberId(1L)
                .electionId("1234")
                .build());

        assertEquals(Voter.builder()
                .electionId("1234")
                .memberId(1L)
                .firstName("First")
                .middleName("Middle")
                .lastName("Last")
                .suffix("Suffix")
                .createdAt(MOCK_INSTANT)
                .build(), voter);

        verify(memberRepository).findByMemberIdAndDeleted(anyLong(), anyBoolean());
    }

    @Test
    public void givenNonExistingMember_whenConvert_thenShouldReturnVoter() {
        when(memberRepository.findByMemberIdAndDeleted(anyLong(), anyBoolean())).thenReturn(empty());

        Voter voter = voterConverter.convert(VoterInput.builder()
                .memberId(1L)
                .electionId("1234")
                .build());

        assertNull(voter);

        verify(memberRepository).findByMemberIdAndDeleted(anyLong(), anyBoolean());
    }

    @Test
    public void givenExitingMemberProxiedVoter_whenConvert_thenShouldReturnVoter() {
        when(memberRepository.findByMemberIdAndDeleted(eq(1L), anyBoolean()))
                .thenReturn(of(Member.builder()
                        .memberId(1L)
                        .firstName("First")
                        .middleName("Middle")
                        .lastName("Last")
                        .suffix("Suffix")
                        .build()));
        when(memberRepository.findByMemberIdAndDeleted(eq(2L), anyBoolean()))
                .thenReturn(of(Member.builder()
                        .memberId(2L)
                        .firstName("First 2")
                        .middleName("Middle")
                        .lastName("Last")
                        .build()));

        Voter voter = voterConverter.convert(VoterInput.builder()
                .memberId(1L)
                .electionId("1234")
                .proxyMemberId(2L)
                .build());

        assertEquals(Voter.builder()
                .electionId("1234")
                .memberId(1L)
                .firstName("First")
                .middleName("Middle")
                .lastName("Last")
                .suffix("Suffix")
                .proxyDetails(ProxyDetails.builder()
                        .memberId(2L)
                        .firstName("First 2")
                        .middleName("Middle")
                        .lastName("Last")
                        .build())
                .createdAt(MOCK_INSTANT)
                .build(), voter);

        verify(memberRepository, times(2)).findByMemberIdAndDeleted(anyLong(), anyBoolean());
    }

    public void teardown() {
        verifyNoMoreInteractions(memberRepository);
    }

}
