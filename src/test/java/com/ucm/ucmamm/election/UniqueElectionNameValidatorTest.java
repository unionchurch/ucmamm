package com.ucm.ucmamm.election;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UniqueElectionNameValidatorTest {

    @InjectMocks
    private UniqueElectionNameValidator uniqueElectionNameValidator;
    @Mock
    private ElectionService electionService;
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Test
    public void givenElectionNameAndDateAndElectionExists_whenValidate_thenShouldReturnFalse() {
        when(electionService.getElectionByNameAndDate(any(ElectionNameAndDate.class)))
                .thenReturn(of(ElectionDto.builder().build()));

        ElectionNameAndDate electionNameAndDate = ElectionNameAndDate.builder()
                .name("Mock Name")
                .date(LocalDate.parse("2018-01-01"))
                .build();
        assertFalse(uniqueElectionNameValidator.isValid(electionNameAndDate, constraintValidatorContext));

        verify(electionService).getElectionByNameAndDate(electionNameAndDate);
    }

    @Test
    public void givenElectionNameAndDateAndElectionDoesNotExist_whenValidate_thenShouldReturnTrue() {
        when(electionService.getElectionByNameAndDate(any(ElectionNameAndDate.class)))
                .thenReturn(empty());

        ElectionNameAndDate electionNameAndDate = ElectionNameAndDate.builder()
                .name("Mock Name")
                .date(LocalDate.parse("2018-01-01"))
                .build();
        assertTrue(uniqueElectionNameValidator.isValid(electionNameAndDate, constraintValidatorContext));

        verify(electionService).getElectionByNameAndDate(electionNameAndDate);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(electionService);
    }

}
