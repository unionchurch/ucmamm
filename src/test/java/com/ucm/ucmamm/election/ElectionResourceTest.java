package com.ucm.ucmamm.election;

import com.ucm.ucmamm.AuthServerConfiguration;
import com.ucm.ucmamm.ResourceServerConfiguration;
import com.ucm.ucmamm.UcmAmmOauth2Properties;
import com.ucm.ucmamm.common.ResourceTestSupport;
import com.ucm.ucmamm.domain.*;
import com.ucm.ucmamm.member.MemberResourceTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;

import static java.lang.Boolean.FALSE;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = ElectionResourceTest.Main.class)
public class ElectionResourceTest extends ResourceTestSupport {

    @SpringBootApplication(scanBasePackages = {"com.ucm.ucmamm.common", "com.ucm.ucmamm.election"}, exclude = MongoAutoConfiguration.class)
    @Import({UcmAmmOauth2Properties.class, ResourceServerConfiguration.class, AuthServerConfiguration.class, ResourceTestSupport.SecurityConfiguration.class})
    public static class Main {

        public static void main(String[] args) {
            SpringApplication.run(MemberResourceTest.Main.class, args);
        }

    }

    private static final String MOCK_EXISTING_ID = "existing";
    private static final String MOCK_NON_EXISTING_ID = "nonexisting";
    private static final String MOCK_EXISTING_ID_2 = "existing2";
    private static final String MOCK_NAME = "2018 UCM Annual Members Meeting";
    private static final LocalDate MOCK_DATE = LocalDate.parse("2018-01-01");
    private static final LocalTime MOCK_START_TIME = LocalTime.parse("00:00:00.000");
    private static final LocalTime MOCK_END_TIME = LocalTime.parse("04:00:00.000");

    private static final String MOCK_PROPOSAL_1 = "Mock Proposal 1";
    private static final String MOCK_PROPOSAL_2 = "Mock Proposal 2";

    private static final String MOCK_QUESTION_1 = "Mock Question 1";
    private static final String MOCK_QUESTION_2 = "Mock Question 2";
    private static final String MOCK_ANSWER_1 = "Mock Answer 1";
    private static final String MOCK_ANSWER_2 = "Mock Answer 2";
    private static final String MOCK_ANSWER_3 = "Mock Answer 3";
    private static final String MOCK_ANSWER_4 = "Mock Answer 4";

    private static final String MOCK_SEAT = "Council Member";
    private static final Long MOCK_MEMBER_ID_1 = 1234L;
    private static final String MOCK_FIRST_NAME_1 = "Feliz";
    private static final String MOCK_LAST_NAME_1 = "Navidad";
    private static final String MOCK_SUFFIX_1 = "IV";
    private static final Long MOCK_MEMBER_ID_2 = 1235L;
    private static final String MOCK_FIRST_NAME_2 = "Hannah";
    private static final String MOCK_MIDDLE_NAME_2 = "R.";
    private static final String MOCK_LAST_NAME_2 = "Navidad";

    private static final Instant MOCK_INSTANT = Instant.parse("2018-01-01T00:00:00.000Z");

    private static final ElectionInput NEW_ELECTION_INPUT = ElectionInput.builder()
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposals(asList(
                    ProposalInput.builder()
                            .content(MOCK_PROPOSAL_1)
                            .build(),
                    ProposalInput.builder()
                            .content(MOCK_PROPOSAL_2)
                            .build()))
            .questions(asList(
                    QuestionInput.builder()
                            .content(MOCK_QUESTION_1)
                            .answers(asList(
                                    AnswerInput.builder().content(MOCK_ANSWER_1).build(),
                                    AnswerInput.builder().content(MOCK_ANSWER_2).build()))
                            .build(),
                    QuestionInput.builder()
                            .content(MOCK_QUESTION_2)
                            .answers(asList(
                                    AnswerInput.builder().content(MOCK_ANSWER_3).build(),
                                    AnswerInput.builder().content(MOCK_ANSWER_4).build()))
                            .build()))
            .seats(singletonList(SeatInput.builder()
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidates(asList(
                            CandidateInput.builder()
                                    .memberId(MOCK_MEMBER_ID_1)
                                    .firstName(MOCK_FIRST_NAME_1)
                                    .lastName(MOCK_LAST_NAME_1)
                                    .suffix(MOCK_SUFFIX_1)
                                    .build(),
                            CandidateInput.builder()
                                    .memberId(MOCK_MEMBER_ID_2)
                                    .firstName(MOCK_FIRST_NAME_2)
                                    .middleName(MOCK_MIDDLE_NAME_2)
                                    .lastName(MOCK_LAST_NAME_2)
                                    .build()))
                    .build()))
            .build();
    private static final ElectionUpdate ELECTION_UPDATE = ElectionUpdate.builder()
            .id("existing")
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposals(singletonList(ProposalUpdate.builder()
                    .content(MOCK_PROPOSAL_2)
                    .build()))
            .questions(singletonList(QuestionUpdate.builder()
                    .content(MOCK_QUESTION_2)
                    .answers(asList(
                            AnswerUpdate.builder()
                                    .content(MOCK_ANSWER_3)
                                    .build(),
                            AnswerUpdate.builder()
                                    .content(MOCK_ANSWER_4)
                                    .build()))
                    .build()))
            .seats(singletonList(SeatUpdate.builder()
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidates(singletonList(CandidateUpdate.builder()
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .build()))
                    .build()))
            .operator("admin")
            .build();
    private static final Election EXISTING_ELECTION = Election.builder()
            .id(MOCK_EXISTING_ID)
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_1)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .proposal(Proposal.builder()
                    .id("2")
                    .content(MOCK_PROPOSAL_2)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("3")
                    .content(MOCK_QUESTION_1)
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_1)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("5")
                            .content(MOCK_ANSWER_2)
                            .selected(0L)
                            .build())
                    .build())
            .question(Question.builder()
                    .id("6")
                    .content(MOCK_QUESTION_2)
                    .answer(Answer.builder()
                            .id("7")
                            .content(MOCK_ANSWER_3)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("8")
                            .content(MOCK_ANSWER_4)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("9")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("10")
                            .memberId(MOCK_MEMBER_ID_1)
                            .firstName(MOCK_FIRST_NAME_1)
                            .lastName(MOCK_LAST_NAME_1)
                            .suffix(MOCK_SUFFIX_1)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .candidate(Candidate.builder()
                            .id("11")
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .createdBy("admin")
            .createdAt(MOCK_INSTANT)
            .build();
    private static final ElectionDto EXISTING_ELECTION_DTO = ElectionDto.builder()
            .id(MOCK_EXISTING_ID)
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_1)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .proposal(Proposal.builder()
                    .id("2")
                    .content(MOCK_PROPOSAL_2)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("3")
                    .content(MOCK_QUESTION_1)
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_1)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("5")
                            .content(MOCK_ANSWER_2)
                            .selected(0L)
                            .build())
                    .build())
            .question(Question.builder()
                    .id("6")
                    .content(MOCK_QUESTION_2)
                    .answer(Answer.builder()
                            .id("7")
                            .content(MOCK_ANSWER_3)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("8")
                            .content(MOCK_ANSWER_4)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("9")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("10")
                            .memberId(MOCK_MEMBER_ID_1)
                            .firstName(MOCK_FIRST_NAME_1)
                            .lastName(MOCK_LAST_NAME_1)
                            .suffix(MOCK_SUFFIX_1)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .candidate(Candidate.builder()
                            .id("11")
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .deleted(FALSE)
            .version(1)
            .build();
    private static final ElectionDto EXISTING_ELECTION_DTO_2 = ElectionDto.builder()
            .id(MOCK_EXISTING_ID_2)
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_1)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .proposal(Proposal.builder()
                    .id("2")
                    .content(MOCK_PROPOSAL_2)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("3")
                    .content(MOCK_QUESTION_1)
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_1)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("5")
                            .content(MOCK_ANSWER_2)
                            .selected(0L)
                            .build())
                    .build())
            .question(Question.builder()
                    .id("6")
                    .content(MOCK_QUESTION_2)
                    .answer(Answer.builder()
                            .id("7")
                            .content(MOCK_ANSWER_3)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("8")
                            .content(MOCK_ANSWER_4)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("9")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("10")
                            .memberId(MOCK_MEMBER_ID_1)
                            .firstName(MOCK_FIRST_NAME_1)
                            .lastName(MOCK_LAST_NAME_1)
                            .suffix(MOCK_SUFFIX_1)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .candidate(Candidate.builder()
                            .id("11")
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .active(Boolean.TRUE)
            .deleted(FALSE)
            .version(1)
            .build();
    private static final Voter VOTER_1 = Voter.builder()
            .id("1234")
            .electionId("1234")
            .firstName("First")
            .middleName("Middle")
            .lastName("Last")
            .suffix("Suffix")
            .memberId(10L)
            .build();
    private static final Voter VOTER_2 = Voter.builder()
            .id("1235")
            .electionId("1235")
            .firstName("First")
            .middleName("Middle")
            .lastName("Last")
            .suffix("2")
            .memberId(11L)
            .build();
    private static final VoterDto VOTER_DTO_1 = VoterDto.builder()
            .id("1234")
            .electionId("1234")
            .firstName("First")
            .middleName("Middle")
            .lastName("Last")
            .suffix("Suffix")
            .memberId(10L)
            .build();
    private static final VoterDto VOTER_DTO_2 = VoterDto.builder()
            .id("1235")
            .electionId("1235")
            .firstName("First")
            .middleName("Middle")
            .lastName("Last")
            .suffix("2")
            .memberId(11L)
            .build();
    private static final VoterInput VOTER_INPUT = VoterInput.builder()
            .electionId("1234")
            .memberId(10L)
            .build();

    @MockBean
    private ElectionService electionService;
    @MockBean
    private ElectionRepository electionRepository;
    @MockBean
    private MemberRepository memberRepository;
    @MockBean
    private VoterRepository voterRepository;

    @Override
    protected void initializeMocks() {
        when(electionService.getElection(MOCK_EXISTING_ID)).thenReturn(of(EXISTING_ELECTION_DTO));
        when(electionService.getElection(MOCK_NON_EXISTING_ID)).thenReturn(empty());
        when(electionService.getElections(0, 5, "date", Boolean.TRUE))
                .thenReturn(asList(EXISTING_ELECTION_DTO, EXISTING_ELECTION_DTO_2));
        when(electionService.saveElection(any(ElectionInput.class))).thenReturn(EXISTING_ELECTION);
        when(electionService.getActiveElection()).thenReturn(of(EXISTING_ELECTION_DTO_2));
        when(electionService.registerVoter(any(VoterInput.class))).thenReturn(VOTER_1);
        when(electionService.getVoters("1234",0, 10, "", "lastName", true))
                .thenReturn(asList(VOTER_DTO_1, VOTER_DTO_2));
        when(electionService.getVoter(10L, "1234")).thenReturn(of(VOTER_DTO_1));
    }

    @Test
    public void givenExistingElection_whenGet_thenShouldReturnElection() throws Exception {
        mockMvc.perform(get("/api/v1/elections/existing")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(MOCK_EXISTING_ID)))
                .andExpect(jsonPath("$.name", is(MOCK_NAME)))
                .andExpect(jsonPath("$.date", is("2018-01-01")))
                .andExpect(jsonPath("$.startTime", is("00:00:00")))
                .andExpect(jsonPath("$.endTime", is("04:00:00")))
                .andExpect(jsonPath("$.proposals[0].id", is("1")))
                .andExpect(jsonPath("$.proposals[0].content", is(MOCK_PROPOSAL_1)))
                .andExpect(jsonPath("$.proposals[0].approved", is(0)))
                .andExpect(jsonPath("$.proposals[0].disapproved", is(0)))
                .andExpect(jsonPath("$.proposals[1].id", is("2")))
                .andExpect(jsonPath("$.proposals[1].content", is(MOCK_PROPOSAL_2)))
                .andExpect(jsonPath("$.proposals[1].approved", is(0)))
                .andExpect(jsonPath("$.proposals[1].disapproved", is(0)))
                .andExpect(jsonPath("$.questions[0].id", is("3")))
                .andExpect(jsonPath("$.questions[0].content", is(MOCK_QUESTION_1)))
                .andExpect(jsonPath("$.questions[0].answers[0].id", is("4")))
                .andExpect(jsonPath("$.questions[0].answers[0].content", is(MOCK_ANSWER_1)))
                .andExpect(jsonPath("$.questions[0].answers[0].selected", is(0)))
                .andExpect(jsonPath("$.questions[0].answers[1].id", is("5")))
                .andExpect(jsonPath("$.questions[0].answers[1].content", is(MOCK_ANSWER_2)))
                .andExpect(jsonPath("$.questions[0].answers[1].selected", is(0)))
                .andExpect(jsonPath("$.questions[1].id", is("6")))
                .andExpect(jsonPath("$.questions[1].content", is(MOCK_QUESTION_2)))
                .andExpect(jsonPath("$.questions[1].answers[0].id", is("7")))
                .andExpect(jsonPath("$.questions[1].answers[0].content", is(MOCK_ANSWER_3)))
                .andExpect(jsonPath("$.questions[1].answers[0].selected", is(0)))
                .andExpect(jsonPath("$.questions[1].answers[1].id", is("8")))
                .andExpect(jsonPath("$.questions[1].answers[1].content", is(MOCK_ANSWER_4)))
                .andExpect(jsonPath("$.questions[1].answers[1].selected", is(0)))
                .andExpect(jsonPath("$.seats[0].id", is("9")))
                .andExpect(jsonPath("$.seats[0].name", is(MOCK_SEAT)))
                .andExpect(jsonPath("$.seats[0].count", is(14)))
                .andExpect(jsonPath("$.seats[0].candidates[0].id", is("10")))
                .andExpect(jsonPath("$.seats[0].candidates[0].memberId", is(MOCK_MEMBER_ID_1.intValue())))
                .andExpect(jsonPath("$.seats[0].candidates[0].firstName", is(MOCK_FIRST_NAME_1)))
                .andExpect(jsonPath("$.seats[0].candidates[0].middleName", nullValue()))
                .andExpect(jsonPath("$.seats[0].candidates[0].lastName", is(MOCK_LAST_NAME_1)))
                .andExpect(jsonPath("$.seats[0].candidates[0].suffix", is(MOCK_SUFFIX_1)))
                .andExpect(jsonPath("$.seats[0].candidates[0].approved", is(0)))
                .andExpect(jsonPath("$.seats[0].candidates[0].disapproved", is(0)))
                .andExpect(jsonPath("$.seats[0].candidates[1].id", is("11")))
                .andExpect(jsonPath("$.seats[0].candidates[1].memberId", is(MOCK_MEMBER_ID_2.intValue())))
                .andExpect(jsonPath("$.seats[0].candidates[1].firstName", is(MOCK_FIRST_NAME_2)))
                .andExpect(jsonPath("$.seats[0].candidates[1].middleName", is(MOCK_MIDDLE_NAME_2)))
                .andExpect(jsonPath("$.seats[0].candidates[1].lastName", is(MOCK_LAST_NAME_2)))
                .andExpect(jsonPath("$.seats[0].candidates[1].suffix", nullValue()))
                .andExpect(jsonPath("$.seats[0].candidates[1].approved", is(0)))
                .andExpect(jsonPath("$.seats[0].candidates[1].disapproved", is(0)))
                .andExpect(jsonPath("$.active", nullValue()))
                .andExpect(jsonPath("$.deleted", is(FALSE)))
                .andExpect(jsonPath("$.version", is(1)));
    }

    @Test
    public void givenExistingActiveElection_whenGet_thenShouldReturnElection() throws Exception {
        mockMvc.perform(get("/api/v1/elections/active")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(MOCK_EXISTING_ID_2)))
                .andExpect(jsonPath("$.name", is(MOCK_NAME)))
                .andExpect(jsonPath("$.date", is("2018-01-01")))
                .andExpect(jsonPath("$.startTime", is("00:00:00")))
                .andExpect(jsonPath("$.endTime", is("04:00:00")))
                .andExpect(jsonPath("$.proposals[0].id", is("1")))
                .andExpect(jsonPath("$.proposals[0].content", is(MOCK_PROPOSAL_1)))
                .andExpect(jsonPath("$.proposals[0].approved", is(0)))
                .andExpect(jsonPath("$.proposals[0].disapproved", is(0)))
                .andExpect(jsonPath("$.proposals[1].id", is("2")))
                .andExpect(jsonPath("$.proposals[1].content", is(MOCK_PROPOSAL_2)))
                .andExpect(jsonPath("$.proposals[1].approved", is(0)))
                .andExpect(jsonPath("$.proposals[1].disapproved", is(0)))
                .andExpect(jsonPath("$.questions[0].id", is("3")))
                .andExpect(jsonPath("$.questions[0].content", is(MOCK_QUESTION_1)))
                .andExpect(jsonPath("$.questions[0].answers[0].id", is("4")))
                .andExpect(jsonPath("$.questions[0].answers[0].content", is(MOCK_ANSWER_1)))
                .andExpect(jsonPath("$.questions[0].answers[0].selected", is(0)))
                .andExpect(jsonPath("$.questions[0].answers[1].id", is("5")))
                .andExpect(jsonPath("$.questions[0].answers[1].content", is(MOCK_ANSWER_2)))
                .andExpect(jsonPath("$.questions[0].answers[1].selected", is(0)))
                .andExpect(jsonPath("$.questions[1].id", is("6")))
                .andExpect(jsonPath("$.questions[1].content", is(MOCK_QUESTION_2)))
                .andExpect(jsonPath("$.questions[1].answers[0].id", is("7")))
                .andExpect(jsonPath("$.questions[1].answers[0].content", is(MOCK_ANSWER_3)))
                .andExpect(jsonPath("$.questions[1].answers[0].selected", is(0)))
                .andExpect(jsonPath("$.questions[1].answers[1].id", is("8")))
                .andExpect(jsonPath("$.questions[1].answers[1].content", is(MOCK_ANSWER_4)))
                .andExpect(jsonPath("$.questions[1].answers[1].selected", is(0)))
                .andExpect(jsonPath("$.seats[0].id", is("9")))
                .andExpect(jsonPath("$.seats[0].name", is(MOCK_SEAT)))
                .andExpect(jsonPath("$.seats[0].count", is(14)))
                .andExpect(jsonPath("$.seats[0].candidates[0].id", is("10")))
                .andExpect(jsonPath("$.seats[0].candidates[0].memberId", is(MOCK_MEMBER_ID_1.intValue())))
                .andExpect(jsonPath("$.seats[0].candidates[0].firstName", is(MOCK_FIRST_NAME_1)))
                .andExpect(jsonPath("$.seats[0].candidates[0].middleName", nullValue()))
                .andExpect(jsonPath("$.seats[0].candidates[0].lastName", is(MOCK_LAST_NAME_1)))
                .andExpect(jsonPath("$.seats[0].candidates[0].suffix", is(MOCK_SUFFIX_1)))
                .andExpect(jsonPath("$.seats[0].candidates[0].approved", is(0)))
                .andExpect(jsonPath("$.seats[0].candidates[0].disapproved", is(0)))
                .andExpect(jsonPath("$.seats[0].candidates[1].id", is("11")))
                .andExpect(jsonPath("$.seats[0].candidates[1].memberId", is(MOCK_MEMBER_ID_2.intValue())))
                .andExpect(jsonPath("$.seats[0].candidates[1].firstName", is(MOCK_FIRST_NAME_2)))
                .andExpect(jsonPath("$.seats[0].candidates[1].middleName", is(MOCK_MIDDLE_NAME_2)))
                .andExpect(jsonPath("$.seats[0].candidates[1].lastName", is(MOCK_LAST_NAME_2)))
                .andExpect(jsonPath("$.seats[0].candidates[1].suffix", nullValue()))
                .andExpect(jsonPath("$.seats[0].candidates[1].approved", is(0)))
                .andExpect(jsonPath("$.seats[0].candidates[1].disapproved", is(0)))
                .andExpect(jsonPath("$.active", is(Boolean.TRUE)))
                .andExpect(jsonPath("$.deleted", is(FALSE)))
                .andExpect(jsonPath("$.version", is(1)));
    }

    @Test
    public void givenNonExistingElection_whenGet_thenShouldReturnNotFound() throws Exception {
        mockMvc.perform(get("/api/v1/elections/nonexisting")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenExistingElections_whenGet_thenShouldReturnElectionList() throws Exception {
        mockMvc.perform(get("/api/v1/elections")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(MOCK_EXISTING_ID)))
                .andExpect(jsonPath("$[0].name", is(MOCK_NAME)))
                .andExpect(jsonPath("$[0].date", is("2018-01-01")))
                .andExpect(jsonPath("$[0].startTime", is("00:00:00")))
                .andExpect(jsonPath("$[0].endTime", is("04:00:00")))
                .andExpect(jsonPath("$[0].proposals[0].id", is("1")))
                .andExpect(jsonPath("$[0].proposals[0].content", is(MOCK_PROPOSAL_1)))
                .andExpect(jsonPath("$[0].proposals[0].approved", is(0)))
                .andExpect(jsonPath("$[0].proposals[0].disapproved", is(0)))
                .andExpect(jsonPath("$[0].proposals[1].id", is("2")))
                .andExpect(jsonPath("$[0].proposals[1].content", is(MOCK_PROPOSAL_2)))
                .andExpect(jsonPath("$[0].proposals[1].approved", is(0)))
                .andExpect(jsonPath("$[0].proposals[1].disapproved", is(0)))
                .andExpect(jsonPath("$[0].questions[0].id", is("3")))
                .andExpect(jsonPath("$[0].questions[0].content", is(MOCK_QUESTION_1)))
                .andExpect(jsonPath("$[0].questions[0].answers[0].id", is("4")))
                .andExpect(jsonPath("$[0].questions[0].answers[0].content", is(MOCK_ANSWER_1)))
                .andExpect(jsonPath("$[0].questions[0].answers[0].selected", is(0)))
                .andExpect(jsonPath("$[0].questions[0].answers[1].id", is("5")))
                .andExpect(jsonPath("$[0].questions[0].answers[1].content", is(MOCK_ANSWER_2)))
                .andExpect(jsonPath("$[0].questions[0].answers[1].selected", is(0)))
                .andExpect(jsonPath("$[0].questions[1].id", is("6")))
                .andExpect(jsonPath("$[0].questions[1].content", is(MOCK_QUESTION_2)))
                .andExpect(jsonPath("$[0].questions[1].answers[0].id", is("7")))
                .andExpect(jsonPath("$[0].questions[1].answers[0].content", is(MOCK_ANSWER_3)))
                .andExpect(jsonPath("$[0].questions[1].answers[0].selected", is(0)))
                .andExpect(jsonPath("$[0].questions[1].answers[1].id", is("8")))
                .andExpect(jsonPath("$[0].questions[1].answers[1].content", is(MOCK_ANSWER_4)))
                .andExpect(jsonPath("$[0].questions[1].answers[1].selected", is(0)))
                .andExpect(jsonPath("$[0].seats[0].id", is("9")))
                .andExpect(jsonPath("$[0].seats[0].name", is(MOCK_SEAT)))
                .andExpect(jsonPath("$[0].seats[0].count", is(14)))
                .andExpect(jsonPath("$[0].seats[0].candidates[0].id", is("10")))
                .andExpect(jsonPath("$[0].seats[0].candidates[0].memberId", is(MOCK_MEMBER_ID_1.intValue())))
                .andExpect(jsonPath("$[0].seats[0].candidates[0].firstName", is(MOCK_FIRST_NAME_1)))
                .andExpect(jsonPath("$[0].seats[0].candidates[0].middleName", nullValue()))
                .andExpect(jsonPath("$[0].seats[0].candidates[0].lastName", is(MOCK_LAST_NAME_1)))
                .andExpect(jsonPath("$[0].seats[0].candidates[0].suffix", is(MOCK_SUFFIX_1)))
                .andExpect(jsonPath("$[0].seats[0].candidates[0].approved", is(0)))
                .andExpect(jsonPath("$[0].seats[0].candidates[0].disapproved", is(0)))
                .andExpect(jsonPath("$[0].seats[0].candidates[1].id", is("11")))
                .andExpect(jsonPath("$[0].seats[0].candidates[1].memberId", is(MOCK_MEMBER_ID_2.intValue())))
                .andExpect(jsonPath("$[0].seats[0].candidates[1].firstName", is(MOCK_FIRST_NAME_2)))
                .andExpect(jsonPath("$[0].seats[0].candidates[1].middleName", is(MOCK_MIDDLE_NAME_2)))
                .andExpect(jsonPath("$[0].seats[0].candidates[1].lastName", is(MOCK_LAST_NAME_2)))
                .andExpect(jsonPath("$[0].seats[0].candidates[1].suffix", nullValue()))
                .andExpect(jsonPath("$[0].seats[0].candidates[1].approved", is(0)))
                .andExpect(jsonPath("$[0].seats[0].candidates[1].disapproved", is(0)))
                .andExpect(jsonPath("$[0].active", nullValue()))
                .andExpect(jsonPath("$[0].deleted", is(FALSE)))
                .andExpect(jsonPath("$[0].version", is(1)))
                .andExpect(jsonPath("$[1].id", is(MOCK_EXISTING_ID_2)))
                .andExpect(jsonPath("$[1].name", is(MOCK_NAME)))
                .andExpect(jsonPath("$[1].date", is("2018-01-01")))
                .andExpect(jsonPath("$[1].startTime", is("00:00:00")))
                .andExpect(jsonPath("$[1].endTime", is("04:00:00")))
                .andExpect(jsonPath("$[1].proposals[0].id", is("1")))
                .andExpect(jsonPath("$[1].proposals[0].content", is(MOCK_PROPOSAL_1)))
                .andExpect(jsonPath("$[1].proposals[0].approved", is(0)))
                .andExpect(jsonPath("$[1].proposals[0].disapproved", is(0)))
                .andExpect(jsonPath("$[1].proposals[1].id", is("2")))
                .andExpect(jsonPath("$[1].proposals[1].content", is(MOCK_PROPOSAL_2)))
                .andExpect(jsonPath("$[1].proposals[1].approved", is(0)))
                .andExpect(jsonPath("$[1].proposals[1].disapproved", is(0)))
                .andExpect(jsonPath("$[1].questions[0].id", is("3")))
                .andExpect(jsonPath("$[1].questions[0].content", is(MOCK_QUESTION_1)))
                .andExpect(jsonPath("$[1].questions[0].answers[0].id", is("4")))
                .andExpect(jsonPath("$[1].questions[0].answers[0].content", is(MOCK_ANSWER_1)))
                .andExpect(jsonPath("$[1].questions[0].answers[0].selected", is(0)))
                .andExpect(jsonPath("$[1].questions[0].answers[1].id", is("5")))
                .andExpect(jsonPath("$[1].questions[0].answers[1].content", is(MOCK_ANSWER_2)))
                .andExpect(jsonPath("$[1].questions[0].answers[1].selected", is(0)))
                .andExpect(jsonPath("$[1].questions[1].id", is("6")))
                .andExpect(jsonPath("$[1].questions[1].content", is(MOCK_QUESTION_2)))
                .andExpect(jsonPath("$[1].questions[1].answers[0].id", is("7")))
                .andExpect(jsonPath("$[1].questions[1].answers[0].content", is(MOCK_ANSWER_3)))
                .andExpect(jsonPath("$[1].questions[1].answers[0].selected", is(0)))
                .andExpect(jsonPath("$[1].questions[1].answers[1].id", is("8")))
                .andExpect(jsonPath("$[1].questions[1].answers[1].content", is(MOCK_ANSWER_4)))
                .andExpect(jsonPath("$[1].questions[1].answers[1].selected", is(0)))
                .andExpect(jsonPath("$[1].seats[0].id", is("9")))
                .andExpect(jsonPath("$[1].seats[0].name", is(MOCK_SEAT)))
                .andExpect(jsonPath("$[1].seats[0].count", is(14)))
                .andExpect(jsonPath("$[1].seats[0].candidates[0].id", is("10")))
                .andExpect(jsonPath("$[1].seats[0].candidates[0].memberId", is(MOCK_MEMBER_ID_1.intValue())))
                .andExpect(jsonPath("$[1].seats[0].candidates[0].firstName", is(MOCK_FIRST_NAME_1)))
                .andExpect(jsonPath("$[1].seats[0].candidates[0].middleName", nullValue()))
                .andExpect(jsonPath("$[1].seats[0].candidates[0].lastName", is(MOCK_LAST_NAME_1)))
                .andExpect(jsonPath("$[1].seats[0].candidates[0].suffix", is(MOCK_SUFFIX_1)))
                .andExpect(jsonPath("$[1].seats[0].candidates[0].approved", is(0)))
                .andExpect(jsonPath("$[1].seats[0].candidates[0].disapproved", is(0)))
                .andExpect(jsonPath("$[1].seats[0].candidates[1].id", is("11")))
                .andExpect(jsonPath("$[1].seats[0].candidates[1].memberId", is(MOCK_MEMBER_ID_2.intValue())))
                .andExpect(jsonPath("$[1].seats[0].candidates[1].firstName", is(MOCK_FIRST_NAME_2)))
                .andExpect(jsonPath("$[1].seats[0].candidates[1].middleName", is(MOCK_MIDDLE_NAME_2)))
                .andExpect(jsonPath("$[1].seats[0].candidates[1].lastName", is(MOCK_LAST_NAME_2)))
                .andExpect(jsonPath("$[1].seats[0].candidates[1].suffix", nullValue()))
                .andExpect(jsonPath("$[1].seats[0].candidates[1].approved", is(0)))
                .andExpect(jsonPath("$[1].seats[0].candidates[1].disapproved", is(0)))
                .andExpect(jsonPath("$[1].active", is(Boolean.TRUE)))
                .andExpect(jsonPath("$[1].deleted", is(FALSE)))
                .andExpect(jsonPath("$[1].version", is(1)));
    }

    @Test
    public void givenElectionInput_whenPost_thenShouldReturnURL() throws Exception {
        mockMvc.perform(post("/api/v1/elections")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken())
                .content(toJson(NEW_ELECTION_INPUT)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/api/v1/elections/existing"));
    }

    @Test
    public void givenElectionUpdate_whenPut_thenShouldReturnNoContent() throws Exception {
        mockMvc.perform(put("/api/v1/elections/existing")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken())
                .content(toJson(ELECTION_UPDATE)))
                .andExpect(status().isNoContent());
    }

    @Test
    public void givenElectionDelete_whenDelete_thenShouldReturnNoContent() throws Exception {
        mockMvc.perform(delete("/api/v1/elections/existing")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken()))
                .andExpect(status().isNoContent());
    }

    @Test
    public void givenVoterInput_whenRegister_thenShouldReturnURL() throws Exception {
        mockMvc.perform(post("/api/v1/elections/1234/voters")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer "+ getAccessToken())
                .content(toJson(VOTER_INPUT)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/api/v1/elections/1234/voters/1234"));
    }

    @Test
    public void givenMatchedVoters_whenGetList_thenShouldReturnVoters() throws Exception {
        mockMvc.perform(get("/api/v1/elections/1234/voters")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is("1234")))
                .andExpect(jsonPath("$[0].electionId", is("1234")))
                .andExpect(jsonPath("$[0].memberId", is(10)))
                .andExpect(jsonPath("$[0].firstName", is("First")))
                .andExpect(jsonPath("$[0].middleName", is("Middle")))
                .andExpect(jsonPath("$[0].lastName", is("Last")))
                .andExpect(jsonPath("$[0].suffix", is("Suffix")))
                .andExpect(jsonPath("$[1].id", is("1235")))
                .andExpect(jsonPath("$[1].electionId", is("1235")))
                .andExpect(jsonPath("$[1].memberId", is(11)))
                .andExpect(jsonPath("$[1].firstName", is("First")))
                .andExpect(jsonPath("$[1].middleName", is("Middle")))
                .andExpect(jsonPath("$[1].lastName", is("Last")))
                .andExpect(jsonPath("$[1].suffix", is("2")));
    }

    @Test
    public void givenMemberIdAndElectionId_whenDelete_thenShouldReturnNoContent() throws Exception {
        mockMvc.perform(delete("/api/v1/elections/1234/voters/10")
                .header(HttpHeaders.AUTHORIZATION, "Bearer "+ getAccessToken())).andExpect(status().isNoContent());
    }

    @Test
    public void givenRegisteredVoter_whenGet_thenShouldReturnVoter() throws Exception {
        mockMvc.perform(get("/api/v1/elections/1234/voters/10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is("1234")))
                .andExpect(jsonPath("$.electionId", is("1234")))
                .andExpect(jsonPath("$.memberId", is(10)))
                .andExpect(jsonPath("$.firstName", is("First")))
                .andExpect(jsonPath("$.middleName", is("Middle")))
                .andExpect(jsonPath("$.lastName", is("Last")))
                .andExpect(jsonPath("$.suffix", is("Suffix")))
                .andExpect(jsonPath("$.proxy", is(FALSE)));
    }

}
