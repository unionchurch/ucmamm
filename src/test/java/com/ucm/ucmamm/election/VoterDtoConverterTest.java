package com.ucm.ucmamm.election;

import com.ucm.ucmamm.domain.Voter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(BlockJUnit4ClassRunner.class)
public class VoterDtoConverterTest {

    private VoterDtoConverter voterDtoConverter = new VoterDtoConverter();

    @Test
    public void givenVoter_whenConvert_thenShouldReturnVoterDto() {
        VoterDto voterDto = voterDtoConverter.convert(Voter.builder()
                .id("1234")
                .electionId("1234")
                .firstName("First")
                .middleName("Middle")
                .lastName("Last")
                .suffix("Suffix")
                .memberId(1L)
                .build());

        assertEquals(VoterDto.builder()
                .id("1234")
                .electionId("1234")
                .firstName("First")
                .middleName("Middle")
                .lastName("Last")
                .suffix("Suffix")
                .memberId(1L)
                .build(), voterDto);
    }

}
