package com.ucm.ucmamm.election;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UniqueElectionNameOnUpdateValidatorTest {

    @InjectMocks
    private UniqueElectionNameOnUpdateValidator uniqueElectionNameOnUpdateValidator;
    @Mock
    private ElectionService electionService;
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Test
    public void givenElectionIdNameAndDateAndElectionExistsAndIdDoesNotMatch_whenValidate_thenShouldReturnFalse() {
        when(electionService.getElectionByNameAndDate(any(ElectionNameAndDate.class)))
                .thenReturn(of(ElectionDto.builder().id("electionID").build()));

        assertFalse(uniqueElectionNameOnUpdateValidator.isValid(ElectionIdNameAndDate.builder()
                .id("anotherElectionID")
                .name("Mock Election")
                .date(LocalDate.parse("2018-01-01"))
                .build(), constraintValidatorContext));

        verify(electionService).getElectionByNameAndDate(ElectionNameAndDate.builder()
                .name("Mock Election")
                .date(LocalDate.parse("2018-01-01"))
                .build());
    }

    @Test
    public void givenElectionIdNameAndDateAndElectionExistsAndIdMatched_whenValidate_thenShouldReturnTrue() {
        when(electionService.getElectionByNameAndDate(any(ElectionNameAndDate.class)))
                .thenReturn(of(ElectionDto.builder().id("electionID").build()));

        assertTrue(uniqueElectionNameOnUpdateValidator.isValid(ElectionIdNameAndDate.builder()
                .id("electionID")
                .name("Mock Election")
                .date(LocalDate.parse("2018-01-01"))
                .build(), constraintValidatorContext));

        verify(electionService).getElectionByNameAndDate(ElectionNameAndDate.builder()
                .name("Mock Election")
                .date(LocalDate.parse("2018-01-01"))
                .build());
    }

    @Test
    public void givenElectionIdNameAndDateAndElectionDoesNotExists_whenValidate_thenShouldReturnTrue() {
        when(electionService.getElectionByNameAndDate(any(ElectionNameAndDate.class)))
                .thenReturn(empty());

        assertTrue(uniqueElectionNameOnUpdateValidator.isValid(ElectionIdNameAndDate.builder()
                .id("electionID")
                .name("Mock Election")
                .date(LocalDate.parse("2018-01-01"))
                .build(), constraintValidatorContext));

        verify(electionService).getElectionByNameAndDate(ElectionNameAndDate.builder()
                .name("Mock Election")
                .date(LocalDate.parse("2018-01-01"))
                .build());
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(electionService);
    }

}
