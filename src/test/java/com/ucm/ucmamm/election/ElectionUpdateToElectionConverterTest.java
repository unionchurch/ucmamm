package com.ucm.ucmamm.election;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.common.RandomizingService;
import com.ucm.ucmamm.common.ResourceNotFoundException;
import com.ucm.ucmamm.domain.*;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ElectionUpdateToElectionConverterTest {

    private static final String MOCK_NAME = "2018 UCM Annual Members Meeting";
    private static final LocalDate MOCK_DATE = LocalDate.parse("2018-01-01");
    private static final LocalTime MOCK_START_TIME = LocalTime.parse("00:00:00.000");
    private static final LocalTime MOCK_END_TIME = LocalTime.parse("00:00:00.000");

    private static final String MOCK_PROPOSAL_1 = "Mock Proposal 1";
    private static final String MOCK_PROPOSAL_2 = "Mock Proposal 2";

    private static final String MOCK_QUESTION_1 = "Mock Question 1";
    private static final String MOCK_QUESTION_2 = "Mock Question 2";
    private static final String MOCK_ANSWER_1 = "Mock Answer 1";
    private static final String MOCK_ANSWER_2 = "Mock Answer 2";
    private static final String MOCK_ANSWER_3 = "Mock Answer 3";
    private static final String MOCK_ANSWER_4 = "Mock Answer 4";

    private static final String MOCK_SEAT = "Council Member";
    private static final Long MOCK_MEMBER_ID_1 = 1234L;
    private static final String MOCK_FIRST_NAME_1 = "Feliz";
    private static final String MOCK_LAST_NAME_1 = "Navidad";
    private static final String MOCK_SUFFIX_1 = "IV";
    private static final Long MOCK_MEMBER_ID_2 = 1235L;
    private static final String MOCK_FIRST_NAME_2 = "Hannah";
    private static final String MOCK_MIDDLE_NAME_2 = "R.";
    private static final String MOCK_LAST_NAME_2 = "Navidad";

    private static final Instant MOCK_INSTANT = Instant.parse("2018-01-01T00:00:00.000Z");

    private static final Election EXISTING_EMPTY_ELECTION = Election.builder()
            .id("existing")
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .active(Boolean.FALSE)
            .deleted(Boolean.FALSE)
            .version(1)
            .build();
    private static final ElectionUpdate ELECTION_UPDATE = ElectionUpdate.builder()
            .id("existing")
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposals(singletonList(ProposalUpdate.builder()
                    .content(MOCK_PROPOSAL_2)
                    .build()))
            .questions(singletonList(QuestionUpdate.builder()
                    .content(MOCK_QUESTION_2)
                    .answers(asList(
                            AnswerUpdate.builder()
                                    .content(MOCK_ANSWER_3)
                                    .build(),
                            AnswerUpdate.builder()
                                    .content(MOCK_ANSWER_4)
                                    .build()))
                    .build()))
            .seats(singletonList(SeatUpdate.builder()
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidates(singletonList(CandidateUpdate.builder()
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .build()))
                    .build()))
            .operator("admin")
            .build();
    private static final Election EXISTING_ELECTION_NON_EMPTY = Election.builder()
            .id("existing")
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_1)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("2")
                    .content(MOCK_QUESTION_1)
                    .answer(Answer.builder()
                            .id("3")
                            .content(MOCK_ANSWER_1)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_2)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("5")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("6")
                            .memberId(MOCK_MEMBER_ID_1)
                            .firstName(MOCK_FIRST_NAME_1)
                            .lastName(MOCK_LAST_NAME_1)
                            .suffix(MOCK_SUFFIX_1)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .active(Boolean.FALSE)
            .deleted(Boolean.FALSE)
            .version(1)
            .build();
    private static final Election EXPECTED_RESULT = Election.builder()
            .id("existing")
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_2)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("2")
                    .content(MOCK_QUESTION_2)
                    .answer(Answer.builder()
                            .id("3")
                            .content(MOCK_ANSWER_3)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_4)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("5")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("6")
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .active(Boolean.FALSE)
            .deleted(Boolean.FALSE)
            .version(2)
            .updatedBy("admin")
            .updatedAt(MOCK_INSTANT)
            .build();

    @InjectMocks
    private ElectionUpdateToElectionConverter electionUpdateToElectionConverter;
    @Mock
    private ElectionRepository electionRepository;
    @Mock
    private RandomizingService randomizingService;
    @Mock
    private DateService dateService;

    @Test(expected = ResourceNotFoundException.class)
    public void givenNonExistingElection_whenConvert_thenShouldThrowException() {
        when(electionRepository.findByIdAndDeleted(anyString(), anyBoolean())).thenReturn(empty());

        ElectionUpdate electionUpdate = ElectionUpdate.builder()
                .id("nonexisting")
                .name(MOCK_NAME)
                .date(MOCK_DATE)
                .startTime(MOCK_START_TIME)
                .endTime(MOCK_END_TIME)
                .build();

        try {
            electionUpdateToElectionConverter.convert(electionUpdate);
        } catch (ResourceNotFoundException e) {
            verify(electionRepository).findByIdAndDeleted("nonexisting", Boolean.FALSE);
            verify(randomizingService, never()).getRandomString();

            throw e;
        }
    }

    @Test
    public void givenElectionUpdateOnExistingEmptyElection_whenConvert_thenShouldReturnCorrectResult() {
        when(electionRepository.findByIdAndDeleted(anyString(), anyBoolean())).thenReturn(of(EXISTING_EMPTY_ELECTION));
        LinkedList<String> queue = new LinkedList<>(asList("1", "2", "3", "4", "5", "6"));
        when(randomizingService.getRandomString()).thenAnswer(invocationOnMock -> queue.pop());
        when(dateService.getCurrentTime()).thenReturn(MOCK_INSTANT);

        Election updatedElection = electionUpdateToElectionConverter.convert(ELECTION_UPDATE);

        assertEquals(EXPECTED_RESULT, updatedElection);

        verify(electionRepository).findByIdAndDeleted("existing", Boolean.FALSE);
        verify(randomizingService, times(6)).getRandomString();
        verify(dateService).getCurrentTime();
    }

    @Test
    public void givenElectionUpdateOnExistingNonEmptyElection_whenConvert_thenShouldReturnCorrectResult() {
        when(electionRepository.findByIdAndDeleted(anyString(), anyBoolean())).thenReturn(of(EXISTING_ELECTION_NON_EMPTY));
        LinkedList<String> queue = new LinkedList<>(asList("1", "2", "3", "4", "5", "6"));
        when(randomizingService.getRandomString()).thenAnswer(invocationOnMock -> queue.pop());
        when(dateService.getCurrentTime()).thenReturn(MOCK_INSTANT);

        Election updatedElection = electionUpdateToElectionConverter.convert(ELECTION_UPDATE);

        assertEquals(EXPECTED_RESULT, updatedElection);

        verify(electionRepository).findByIdAndDeleted("existing", Boolean.FALSE);
        verify(randomizingService, times(6)).getRandomString();
        verify(dateService).getCurrentTime();
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(electionRepository, randomizingService);
    }

}
