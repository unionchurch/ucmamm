package com.ucm.ucmamm.election;

import com.ucm.ucmamm.common.CacheService;
import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.common.ResourceNotFoundException;
import com.ucm.ucmamm.domain.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static com.ucm.ucmamm.common.Caches.*;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ElectionServiceTest {

    private static final String MOCK_EXISTING_ID = "existing";
    private static final String MOCK_EXISTING_ID_2 = "existing2";
    private static final String MOCK_NAME = "2018 UCM Annual Members Meeting";
    private static final LocalDate MOCK_DATE = LocalDate.parse("2018-01-01");
    private static final LocalTime MOCK_START_TIME = LocalTime.parse("00:00:00.000");
    private static final LocalTime MOCK_END_TIME = LocalTime.parse("00:00:00.000");

    private static final String MOCK_PROPOSAL_1 = "Mock Proposal 1";
    private static final String MOCK_PROPOSAL_2 = "Mock Proposal 2";

    private static final String MOCK_QUESTION_1 = "Mock Question 1";
    private static final String MOCK_QUESTION_2 = "Mock Question 2";
    private static final String MOCK_ANSWER_1 = "Mock Answer 1";
    private static final String MOCK_ANSWER_2 = "Mock Answer 2";
    private static final String MOCK_ANSWER_3 = "Mock Answer 3";
    private static final String MOCK_ANSWER_4 = "Mock Answer 4";

    private static final String MOCK_SEAT = "Council Member";
    private static final Long MOCK_MEMBER_ID_1 = 1234L;
    private static final String MOCK_FIRST_NAME_1 = "Feliz";
    private static final String MOCK_LAST_NAME_1 = "Navidad";
    private static final String MOCK_SUFFIX_1 = "IV";
    private static final Long MOCK_MEMBER_ID_2 = 1235L;
    private static final String MOCK_FIRST_NAME_2 = "Hannah";
    private static final String MOCK_MIDDLE_NAME_2 = "R.";
    private static final String MOCK_LAST_NAME_2 = "Navidad";

    private static final Instant MOCK_INSTANT = Instant.parse("2018-01-01T00:00:00.000Z");

    private static final ElectionInput NEW_ELECTION_INPUT = ElectionInput.builder()
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposals(asList(
                    ProposalInput.builder()
                            .content(MOCK_PROPOSAL_1)
                            .build(),
                    ProposalInput.builder()
                            .content(MOCK_PROPOSAL_2)
                            .build()))
            .questions(asList(
                    QuestionInput.builder()
                            .content(MOCK_QUESTION_1)
                            .answers(asList(
                                    AnswerInput.builder().content(MOCK_ANSWER_1).build(),
                                    AnswerInput.builder().content(MOCK_ANSWER_2).build()))
                            .build(),
                    QuestionInput.builder()
                            .content(MOCK_QUESTION_2)
                            .answers(asList(
                                    AnswerInput.builder().content(MOCK_ANSWER_3).build(),
                                    AnswerInput.builder().content(MOCK_ANSWER_4).build()))
                            .build()))
            .seats(singletonList(SeatInput.builder()
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidates(asList(
                            CandidateInput.builder()
                                    .memberId(MOCK_MEMBER_ID_1)
                                    .firstName(MOCK_FIRST_NAME_1)
                                    .lastName(MOCK_LAST_NAME_1)
                                    .suffix(MOCK_SUFFIX_1)
                                    .build(),
                            CandidateInput.builder()
                                    .memberId(MOCK_MEMBER_ID_2)
                                    .firstName(MOCK_FIRST_NAME_2)
                                    .middleName(MOCK_MIDDLE_NAME_2)
                                    .lastName(MOCK_LAST_NAME_2)
                                    .build()))
                    .build()))
            .build();
    private static final ElectionUpdate ELECTION_UPDATE = ElectionUpdate.builder()
            .id("existing")
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposals(singletonList(ProposalUpdate.builder()
                    .content(MOCK_PROPOSAL_2)
                    .build()))
            .questions(singletonList(QuestionUpdate.builder()
                    .content(MOCK_QUESTION_2)
                    .answers(asList(
                            AnswerUpdate.builder()
                                    .content(MOCK_ANSWER_3)
                                    .build(),
                            AnswerUpdate.builder()
                                    .content(MOCK_ANSWER_4)
                                    .build()))
                    .build()))
            .seats(singletonList(SeatUpdate.builder()
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidates(singletonList(CandidateUpdate.builder()
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .build()))
                    .build()))
            .operator("admin")
            .build();
    private static final Election NEW_ELECTION = Election.builder()
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_1)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .proposal(Proposal.builder()
                    .id("2")
                    .content(MOCK_PROPOSAL_2)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("3")
                    .content(MOCK_QUESTION_1)
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_1)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("5")
                            .content(MOCK_ANSWER_2)
                            .selected(0L)
                            .build())
                    .build())
            .question(Question.builder()
                    .id("6")
                    .content(MOCK_QUESTION_2)
                    .answer(Answer.builder()
                            .id("7")
                            .content(MOCK_ANSWER_3)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("8")
                            .content(MOCK_ANSWER_4)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("9")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("10")
                            .memberId(MOCK_MEMBER_ID_1)
                            .firstName(MOCK_FIRST_NAME_1)
                            .lastName(MOCK_LAST_NAME_1)
                            .suffix(MOCK_SUFFIX_1)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .candidate(Candidate.builder()
                            .id("11")
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .createdBy("admin")
            .createdAt(MOCK_INSTANT)
            .build();
    private static final Election EXISTING_ELECTION = Election.builder()
            .id(MOCK_EXISTING_ID)
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_1)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .proposal(Proposal.builder()
                    .id("2")
                    .content(MOCK_PROPOSAL_2)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("3")
                    .content(MOCK_QUESTION_1)
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_1)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("5")
                            .content(MOCK_ANSWER_2)
                            .selected(0L)
                            .build())
                    .build())
            .question(Question.builder()
                    .id("6")
                    .content(MOCK_QUESTION_2)
                    .answer(Answer.builder()
                            .id("7")
                            .content(MOCK_ANSWER_3)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("8")
                            .content(MOCK_ANSWER_4)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("9")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("10")
                            .memberId(MOCK_MEMBER_ID_1)
                            .firstName(MOCK_FIRST_NAME_1)
                            .lastName(MOCK_LAST_NAME_1)
                            .suffix(MOCK_SUFFIX_1)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .candidate(Candidate.builder()
                            .id("11")
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .createdBy("admin")
            .createdAt(MOCK_INSTANT)
            .build();
    private static final Election EXISTING_ELECTION_2 = Election.builder()
            .id(MOCK_EXISTING_ID_2)
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_1)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .proposal(Proposal.builder()
                    .id("2")
                    .content(MOCK_PROPOSAL_2)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("3")
                    .content(MOCK_QUESTION_1)
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_1)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("5")
                            .content(MOCK_ANSWER_2)
                            .selected(0L)
                            .build())
                    .build())
            .question(Question.builder()
                    .id("6")
                    .content(MOCK_QUESTION_2)
                    .answer(Answer.builder()
                            .id("7")
                            .content(MOCK_ANSWER_3)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("8")
                            .content(MOCK_ANSWER_4)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("9")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("10")
                            .memberId(MOCK_MEMBER_ID_1)
                            .firstName(MOCK_FIRST_NAME_1)
                            .lastName(MOCK_LAST_NAME_1)
                            .suffix(MOCK_SUFFIX_1)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .candidate(Candidate.builder()
                            .id("11")
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .createdBy("admin")
            .createdAt(MOCK_INSTANT)
            .active(TRUE)
            .build();
    private static final Election DELETED_ELECTION = Election.builder()
            .id(MOCK_EXISTING_ID)
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_1)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .proposal(Proposal.builder()
                    .id("2")
                    .content(MOCK_PROPOSAL_2)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("3")
                    .content(MOCK_QUESTION_1)
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_1)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("5")
                            .content(MOCK_ANSWER_2)
                            .selected(0L)
                            .build())
                    .build())
            .question(Question.builder()
                    .id("6")
                    .content(MOCK_QUESTION_2)
                    .answer(Answer.builder()
                            .id("7")
                            .content(MOCK_ANSWER_3)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("8")
                            .content(MOCK_ANSWER_4)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("9")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("10")
                            .memberId(MOCK_MEMBER_ID_1)
                            .firstName(MOCK_FIRST_NAME_1)
                            .lastName(MOCK_LAST_NAME_1)
                            .suffix(MOCK_SUFFIX_1)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .candidate(Candidate.builder()
                            .id("11")
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .deleted(TRUE)
            .createdBy("admin")
            .createdAt(MOCK_INSTANT)
            .updatedBy("admin")
            .updatedAt(MOCK_INSTANT)
            .build();
    private static final ElectionDto EXISTING_ELECTION_DTO = ElectionDto.builder()
            .id(MOCK_EXISTING_ID)
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_1)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .proposal(Proposal.builder()
                    .id("2")
                    .content(MOCK_PROPOSAL_2)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("3")
                    .content(MOCK_QUESTION_1)
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_1)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("5")
                            .content(MOCK_ANSWER_2)
                            .selected(0L)
                            .build())
                    .build())
            .question(Question.builder()
                    .id("6")
                    .content(MOCK_QUESTION_2)
                    .answer(Answer.builder()
                            .id("7")
                            .content(MOCK_ANSWER_3)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("8")
                            .content(MOCK_ANSWER_4)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("9")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("10")
                            .memberId(MOCK_MEMBER_ID_1)
                            .firstName(MOCK_FIRST_NAME_1)
                            .lastName(MOCK_LAST_NAME_1)
                            .suffix(MOCK_SUFFIX_1)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .candidate(Candidate.builder()
                            .id("11")
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .build();
    private static final ElectionDto EXISTING_ELECTION_DTO_2 = ElectionDto.builder()
            .id(MOCK_EXISTING_ID_2)
            .name(MOCK_NAME)
            .date(MOCK_DATE)
            .startTime(MOCK_START_TIME)
            .endTime(MOCK_END_TIME)
            .proposal(Proposal.builder()
                    .id("1")
                    .content(MOCK_PROPOSAL_1)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .proposal(Proposal.builder()
                    .id("2")
                    .content(MOCK_PROPOSAL_2)
                    .approved(0L)
                    .disapproved(0L)
                    .build())
            .question(Question.builder()
                    .id("3")
                    .content(MOCK_QUESTION_1)
                    .answer(Answer.builder()
                            .id("4")
                            .content(MOCK_ANSWER_1)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("5")
                            .content(MOCK_ANSWER_2)
                            .selected(0L)
                            .build())
                    .build())
            .question(Question.builder()
                    .id("6")
                    .content(MOCK_QUESTION_2)
                    .answer(Answer.builder()
                            .id("7")
                            .content(MOCK_ANSWER_3)
                            .selected(0L)
                            .build())
                    .answer(Answer.builder()
                            .id("8")
                            .content(MOCK_ANSWER_4)
                            .selected(0L)
                            .build())
                    .build())
            .seat(Seat.builder()
                    .id("9")
                    .name(MOCK_SEAT)
                    .count(14)
                    .candidate(Candidate.builder()
                            .id("10")
                            .memberId(MOCK_MEMBER_ID_1)
                            .firstName(MOCK_FIRST_NAME_1)
                            .lastName(MOCK_LAST_NAME_1)
                            .suffix(MOCK_SUFFIX_1)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .candidate(Candidate.builder()
                            .id("11")
                            .memberId(MOCK_MEMBER_ID_2)
                            .firstName(MOCK_FIRST_NAME_2)
                            .middleName(MOCK_MIDDLE_NAME_2)
                            .lastName(MOCK_LAST_NAME_2)
                            .approved(0L)
                            .disapproved(0L)
                            .build())
                    .build())
            .active(TRUE)
            .build();
    private static final VoterInput VOTER_INPUT = VoterInput.builder()
            .electionId("1234")
            .memberId(10L)
            .build();
    private static final Voter VOTER_1 = Voter.builder()
            .id("1234")
            .electionId("1234")
            .firstName("First")
            .middleName("Middle")
            .lastName("Last")
            .suffix("Suffix")
            .memberId(10L)
            .build();
    private static final Voter VOTER_2 = Voter.builder()
            .id("1234")
            .electionId("1234")
            .firstName("First")
            .middleName("Middle")
            .lastName("Last")
            .suffix("2")
            .memberId(11L)
            .build();
    private static final VoterDto VOTER_DTO_1 = VoterDto.builder()
            .id("1234")
            .electionId("1234")
            .firstName("First")
            .middleName("Middle")
            .lastName("Last")
            .suffix("Suffix")
            .memberId(10L)
            .build();
    private static final VoterDto VOTER_DTO_2 = VoterDto.builder()
            .id("1234")
            .electionId("1234")
            .firstName("First")
            .middleName("Middle")
            .lastName("Last")
            .suffix("2")
            .memberId(11L)
            .build();

    @InjectMocks
    private ElectionService electionService;
    @Mock
    private ElectionRepository electionRepository;
    @Mock
    private VoterRepository voterRepository;
    @Mock
    private ConversionService conversionService;
    @Mock
    private DateService dateService;
    @Mock
    private CacheService cacheService;
    @Mock
    private Page<Election> page;
    @Mock
    private Page<Voter> voterPage;

    @Before
    public void setUp() {
        when(dateService.getCurrentTime()).thenReturn(MOCK_INSTANT);
    }

    @Test
    public void givenElectionInput_whenSave_thenVerifyElectionSave() {
        when(conversionService.convert(any(ElectionInput.class), eq(Election.class)))
                .thenReturn(NEW_ELECTION);
        when(electionRepository.save(NEW_ELECTION)).thenReturn(EXISTING_ELECTION);

        Election savedElection = electionService.saveElection(NEW_ELECTION_INPUT);

        assertEquals(EXISTING_ELECTION, savedElection);

        verify(conversionService).convert(NEW_ELECTION_INPUT, Election.class);
        verify(electionRepository).save(NEW_ELECTION);
        verify(cacheService).evictCache(ELECTIONS);
    }

    @Test
    public void givenElectionInputActive_whenSave_thenVerifyElectionSave() {
        when(conversionService.convert(any(ElectionInput.class), eq(Election.class)))
                .thenReturn(NEW_ELECTION);
        when(electionRepository.save(NEW_ELECTION)).thenReturn(EXISTING_ELECTION.withActive(TRUE));
        when(electionRepository.findByActiveAndDeleted(anyBoolean(), anyBoolean(), any(Sort.class)))
                .thenReturn(singletonList(EXISTING_ELECTION));

        Election savedElection = electionService.saveElection(NEW_ELECTION_INPUT.withActive(TRUE));

        assertEquals(EXISTING_ELECTION.withActive(TRUE), savedElection);

        verify(conversionService).convert(NEW_ELECTION_INPUT.withActive(TRUE), Election.class);
        verify(electionRepository).save(NEW_ELECTION);
        verify(cacheService).evictCache(ELECTIONS);
        verify(cacheService).evictCache(ELECTION, "active_election");
        verify(electionRepository).findByActiveAndDeleted(anyBoolean(), any(), any(Sort.class));
        verify(electionRepository).saveAll(anyList());
    }

    @Test
    public void givenElectionUpdate_whenUpdate_thenVerifyElectionUpdate() {
        when(conversionService.convert(any(ElectionUpdate.class), eq(Election.class)))
                .thenReturn(EXISTING_ELECTION);

        electionService.updateElection(ELECTION_UPDATE);

        verify(conversionService).convert(ELECTION_UPDATE, Election.class);
        verify(electionRepository).save(EXISTING_ELECTION);
        verify(electionRepository).findByActiveAndDeleted(anyBoolean(), any(), any(Sort.class));
        verify(cacheService).evictCache(ELECTIONS);
        verify(cacheService).evictCache(ELECTION, "existing");
    }

    @Test
    public void givenElectionUpdateActive_whenUpdate_thenVerifyElectionUpdate() {
        when(conversionService.convert(any(ElectionUpdate.class), eq(Election.class)))
                .thenReturn(EXISTING_ELECTION.withActive(TRUE));
        when(electionRepository.findByActiveAndDeleted(anyBoolean(), anyBoolean(), any(Sort.class)))
                .thenReturn(singletonList(EXISTING_ELECTION));

        electionService.updateElection(ELECTION_UPDATE.withActive(TRUE));

        verify(conversionService).convert(ELECTION_UPDATE.withActive(TRUE), Election.class);
        verify(electionRepository).save(EXISTING_ELECTION.withActive(TRUE));
        verify(cacheService).evictCache(ELECTIONS);
        verify(cacheService).evictCache(ELECTION, "existing");
        verify(cacheService).evictCache(ELECTIONS);
        verify(cacheService).evictCache(ELECTION, "active_election");
        verify(electionRepository).findByActiveAndDeleted(anyBoolean(), any(), any(Sort.class));
        verify(electionRepository).saveAll(anyList());
    }

    @Test
    public void givenElectionUpdateInactive_whenUpdate_thenVerifyElectionUpdate() {
        when(conversionService.convert(any(ElectionUpdate.class), eq(Election.class)))
                .thenReturn(EXISTING_ELECTION.withActive(FALSE));
        when(conversionService.convert(any(Election.class), eq(ElectionDto.class)))
                .thenReturn(EXISTING_ELECTION_DTO.withActive(TRUE));
        when(electionRepository.findByActiveAndDeleted(anyBoolean(), anyBoolean(), any(Sort.class)))
                .thenReturn(singletonList(EXISTING_ELECTION.withActive(TRUE)));

        electionService.updateElection(ELECTION_UPDATE.withActive(FALSE));

        verify(conversionService).convert(ELECTION_UPDATE.withActive(FALSE), Election.class);
        verify(conversionService).convert(EXISTING_ELECTION.withActive(TRUE), ElectionDto.class);
        verify(electionRepository).save(EXISTING_ELECTION.withActive(FALSE));
        verify(cacheService).evictCache(ELECTIONS);
        verify(cacheService).evictCache(ELECTION, "existing");
        verify(cacheService).evictCache(ELECTIONS);
        verify(cacheService).evictCache(ELECTION, "active_election");
        verify(electionRepository).findByActiveAndDeleted(anyBoolean(), any(), any(Sort.class));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void givenElectionUpdateNonExistingRecord_whenUpdate_thenShouldThrowException() {
        when(conversionService.convert(any(ElectionUpdate.class), eq(Election.class)))
                .thenThrow(new ResourceNotFoundException());

        try {
            electionService.updateElection(ELECTION_UPDATE);
        } catch (ResourceNotFoundException e) {
            verify(conversionService).convert(ELECTION_UPDATE, Election.class);
            verify(electionRepository).findByActiveAndDeleted(anyBoolean(), any(), any(Sort.class));
            verify(electionRepository, never()).save(EXISTING_ELECTION);

            throw e;
        }
    }

    @Test
    public void givenElectionDelete_whenDelete_thenVerifyElectionDelete() {
        when(electionRepository.findById(anyString())).thenReturn(of(EXISTING_ELECTION));

        electionService.deleteElection(MOCK_EXISTING_ID, "admin");

        verify(electionRepository).findById(MOCK_EXISTING_ID);
        verify(electionRepository).save(DELETED_ELECTION);
        verify(dateService).getCurrentTime();
        verify(cacheService).evictCache(ELECTIONS);
        verify(cacheService).evictCache(ELECTION, "existing");
    }

    @Test
    public void givenExistingElection_whenGet_thenShouldReturnElectionDto() {
        when(electionRepository.findByIdAndDeleted(anyString(), eq(Boolean.FALSE)))
                .thenReturn(of(EXISTING_ELECTION));
        when(conversionService.convert(any(Election.class), eq(ElectionDto.class)))
                .thenReturn(EXISTING_ELECTION_DTO);

        Optional<ElectionDto> electionDto = electionService.getElection(MOCK_EXISTING_ID);

        assertTrue(electionDto.isPresent());
        assertEquals(EXISTING_ELECTION_DTO, electionDto.get());

        verify(electionRepository).findByIdAndDeleted(MOCK_EXISTING_ID, Boolean.FALSE);
        verify(conversionService).convert(EXISTING_ELECTION, ElectionDto.class);
    }

    @Test
    public void givenNonExistingElection_whenGetByNameAndDate_thenShouldReturnOptionalEmpty() {
        when(electionRepository.findByNameAndDateAndDeleted(anyString(), any(LocalDate.class), eq(Boolean.FALSE))).thenReturn(empty());

        Optional<ElectionDto> electionDto = electionService.getElectionByNameAndDate(ElectionNameAndDate.builder()
                .name(MOCK_NAME)
                .date(MOCK_DATE)
                .build());
        assertFalse(electionDto.isPresent());

        verify(electionRepository).findByNameAndDateAndDeleted(MOCK_NAME, MOCK_DATE, Boolean.FALSE);
        verify(conversionService, never()).convert(any(Election.class), eq(ElectionDto.class));
    }

    @Test
    public void givenExistingElection_whenGetByNameAndDate_thenShouldReturnElectionDto() {
        when(electionRepository.findByNameAndDateAndDeleted(anyString(), any(LocalDate.class), eq(Boolean.FALSE)))
                .thenReturn(of(EXISTING_ELECTION));
        when(conversionService.convert(any(Election.class), eq(ElectionDto.class)))
                .thenReturn(EXISTING_ELECTION_DTO);

        Optional<ElectionDto> electionDto = electionService.getElectionByNameAndDate(ElectionNameAndDate.builder()
                .name(MOCK_NAME)
                .date(MOCK_DATE)
                .build());

        assertTrue(electionDto.isPresent());
        assertEquals(EXISTING_ELECTION_DTO, electionDto.get());

        verify(electionRepository).findByNameAndDateAndDeleted(MOCK_NAME, MOCK_DATE, Boolean.FALSE);
        verify(conversionService).convert(any(Election.class), eq(ElectionDto.class));
    }

    @Test
    public void givenNonExistingElection_whenGet_thenShouldReturnOptionalEmpty() {
        when(electionRepository.findByIdAndDeleted(anyString(), eq(Boolean.FALSE))).thenReturn(empty());

        Optional<ElectionDto> electionDto = electionService.getElection(MOCK_EXISTING_ID);

        assertFalse(electionDto.isPresent());

        verify(electionRepository).findByIdAndDeleted(MOCK_EXISTING_ID, Boolean.FALSE);
        verify(conversionService, never()).convert(EXISTING_ELECTION, ElectionDto.class);
    }

    @Test
    public void givenExistingElections_whenGetListAscending_thenShouldReturnElectionDtoList() {
        PageRequest pageRequest = PageRequest.of(0, 10, Sort.Direction.ASC, "date");
        when(electionRepository.findByDeleted(anyBoolean(), eq(pageRequest))).thenReturn(page);
        when(page.hasContent()).thenReturn(TRUE);
        when(page.getContent()).thenReturn(asList(EXISTING_ELECTION, EXISTING_ELECTION_2));
        when(conversionService.convert(EXISTING_ELECTION, ElectionDto.class)).thenReturn(EXISTING_ELECTION_DTO);
        when(conversionService.convert(EXISTING_ELECTION_2, ElectionDto.class)).thenReturn(EXISTING_ELECTION_DTO_2);

        List<ElectionDto> electionDtos = electionService.getElections(0, 10, "date", TRUE);

        assertEquals(asList(EXISTING_ELECTION_DTO, EXISTING_ELECTION_DTO_2), electionDtos);

        verify(electionRepository).findByDeleted(Boolean.FALSE, pageRequest);
        verify(page).hasContent();
        verify(page).getContent();
        verify(conversionService, times(2)).convert(any(Election.class), eq(ElectionDto.class));
    }

    @Test
    public void givenExistingElections_whenGetListDescending_thenShouldReturnElectionDtoList() {
        PageRequest pageRequest = PageRequest.of(0, 10, Sort.Direction.DESC, "date");
        when(electionRepository.findByDeleted(anyBoolean(), eq(pageRequest))).thenReturn(page);
        when(page.hasContent()).thenReturn(TRUE);
        when(page.getContent()).thenReturn(asList(EXISTING_ELECTION_2, EXISTING_ELECTION));
        when(conversionService.convert(EXISTING_ELECTION, ElectionDto.class)).thenReturn(EXISTING_ELECTION_DTO);
        when(conversionService.convert(EXISTING_ELECTION_2, ElectionDto.class)).thenReturn(EXISTING_ELECTION_DTO_2);

        List<ElectionDto> electionDtos = electionService.getElections(0, 10, "date", Boolean.FALSE);

        assertEquals(asList(EXISTING_ELECTION_DTO_2, EXISTING_ELECTION_DTO), electionDtos);

        verify(electionRepository).findByDeleted(Boolean.FALSE, pageRequest);
        verify(page).hasContent();
        verify(page).getContent();
        verify(conversionService, times(2)).convert(any(Election.class), eq(ElectionDto.class));
    }

    @Test
    public void givenExistingElections_whenGetActiveList_thenShouldReturnElectionDtoList() {
        Sort sort = Sort.by(Sort.Direction.DESC, "date");

        when(electionRepository.findByActiveAndDeleted(anyBoolean(), anyBoolean(), any(Sort.class)))
                .thenReturn(singletonList(EXISTING_ELECTION_2));
        when(conversionService.convert(eq(EXISTING_ELECTION_2), eq(ElectionDto.class))).thenReturn(EXISTING_ELECTION_DTO_2);

        Optional<ElectionDto> activeElection = electionService.getActiveElection();

        assertTrue(activeElection.isPresent());
        assertEquals(EXISTING_ELECTION_DTO_2, activeElection.get());

        verify(electionRepository).findByActiveAndDeleted(TRUE, Boolean.FALSE, sort);
        verify(conversionService).convert(EXISTING_ELECTION_2, ElectionDto.class);
    }

    @Test
    public void givenVoterInput_whenRegister_thenShouldSaveVoter() {
        when(electionRepository.findById("1234")).thenReturn(of(EXISTING_ELECTION.withVoter(1L)));
        when(conversionService.convert(any(VoterInput.class), eq(Voter.class))).thenReturn(VOTER_1);

        electionService.registerVoter(VOTER_INPUT);

        verify(conversionService).convert(VOTER_INPUT, Voter.class);
        verify(voterRepository).save(VOTER_1);
        verify(cacheService).evictCache(VOTERS);
        verify(electionRepository).findById("1234");
        verify(electionRepository).save(EXISTING_ELECTION.withVoter(2L).withProxy(0L));
        verify(cacheService).evictCache(ELECTION, "1234");
        verify(cacheService).evictCache(ELECTION, ACTIVE_ELECTION.replaceAll("'", ""));
        verify(cacheService).evictCache(ELECTIONS);
    }

    @Test
    public void givenMatchedVoters_whenGetVoterList_thenShouldReturnVoterDtoList() {
        Pageable pageRequest = PageRequest.of(0, 10, Sort.Direction.ASC, "lastName");
        when(voterPage.hasContent()).thenReturn(TRUE);
        when(voterPage.getContent()).thenReturn(asList(VOTER_1, VOTER_2));
        when(conversionService.convert(eq(VOTER_1), eq(VoterDto.class))).thenReturn(VOTER_DTO_1);
        when(conversionService.convert(eq(VOTER_2), eq(VoterDto.class))).thenReturn(VOTER_DTO_2);
        when(voterRepository.findByElectionId(anyString(), eq(pageRequest))).thenReturn(voterPage);

        List<VoterDto> voters = electionService.getVoters("1234",0, 10, "", "lastName", true);

        assertEquals(asList(VOTER_DTO_1, VOTER_DTO_2), voters);

        verify(voterPage).hasContent();
        verify(voterPage).getContent();
        verify(conversionService, times(2)).convert(any(Voter.class), eq(VoterDto.class));
        verify(voterRepository).findByElectionId(anyString(), eq(pageRequest));
    }

    @Test
    public void givenMatchedVoters_whenGetVoterListByKeyword_thenShouldReturnVoterDtoList() {
        Pageable pageRequest = PageRequest.of(0, 10, Sort.Direction.ASC, "lastName");
        when(voterPage.hasContent()).thenReturn(TRUE);
        when(voterPage.getContent()).thenReturn(asList(VOTER_1, VOTER_2));
        when(conversionService.convert(eq(VOTER_1), eq(VoterDto.class))).thenReturn(VOTER_DTO_1);
        when(conversionService.convert(eq(VOTER_2), eq(VoterDto.class))).thenReturn(VOTER_DTO_2);
        when(voterRepository.findByFirstNameOrLastName(eq("First"), eq("Last"), anyString(), eq(pageRequest))).thenReturn(voterPage);

        List<VoterDto> voters = electionService.getVoters("1234",0, 10, "First Last", "lastName", true);

        assertEquals(asList(VOTER_DTO_1, VOTER_DTO_2), voters);

        verify(voterPage).hasContent();
        verify(voterPage).getContent();
        verify(conversionService, times(2)).convert(any(Voter.class), eq(VoterDto.class));
        verify(voterRepository).findByFirstNameOrLastName(anyString(), anyString(), anyString(), eq(pageRequest));
    }

    @Test
    public void givenNoMatchedVoters_whenGetVoterList_thenShouldReturnEmptyVoterDtoList() {
        Pageable pageRequest = PageRequest.of(0, 10, Sort.Direction.ASC, "lastName");

        List<VoterDto> voters = electionService.getVoters("1234",0, 10, "", "lastName", true);

        assertTrue(voters.isEmpty());

        verify(voterPage, never()).hasContent();
        verify(voterPage, never()).getContent();
        verify(conversionService, never()).convert(any(Voter.class), eq(VoterDto.class));
        verify(voterRepository).findByElectionId(anyString(), eq(pageRequest));
    }

    @Test
    public void givenMemberIdAndElectionId_whenDelete_thenShouldInvokeAppropriateMethods() {
        when(voterRepository.findByMemberIdAndElectionId(anyLong(), anyString())).thenReturn(of(VOTER_1.withProxy(TRUE)));
        when(electionRepository.findById("1234")).thenReturn(of(EXISTING_ELECTION.withVoter(1L).withProxy(1L)));

        electionService.unregisterVoter(1234L, "1234");

        verify(voterRepository).findByMemberIdAndElectionId(1234L, "1234");
        verify(voterRepository).delete(any(Voter.class));
        verify(cacheService).evictCache(VOTERS);
        verify(cacheService).evictCache(VOTER, "1234_" + 1234L);
        verify(electionRepository).findById("1234");
        verify(electionRepository).save(EXISTING_ELECTION.withVoter(0L).withProxy(0L));
        verify(cacheService).evictCache(ELECTION, "1234");
        verify(cacheService).evictCache(ELECTION, ACTIVE_ELECTION.replaceAll("'", ""));
        verify(cacheService).evictCache(ELECTIONS);
    }

    @Test
    public void givenActivateRequest_whenChangeStatus_thenShouldInvokeAppropriateMethods() {
        when(electionRepository.findById("1234")).thenReturn(of(EXISTING_ELECTION));

        electionService.changeElectionStatus("1234", TRUE);

        verify(electionRepository).findById("1234");
        verify(electionRepository).save(EXISTING_ELECTION.withActive(TRUE));
        verify(cacheService).evictCache(ELECTION, "1234");
        verify(cacheService).evictCache(ELECTION, ACTIVE_ELECTION.replaceAll("'", ""));
        verify(cacheService).evictCache(ELECTIONS);
    }

    @Test
    public void givenDeactivateRequest_whenChangeStatus_thenShouldInvokeAppropriateMethods() {
        when(electionRepository.findById("1234")).thenReturn(of(EXISTING_ELECTION));

        electionService.changeElectionStatus("1234", FALSE);

        verify(electionRepository).findById("1234");
        verify(electionRepository).save(EXISTING_ELECTION.withActive(FALSE));
        verify(cacheService).evictCache(ELECTION, "1234");
        verify(cacheService).evictCache(ELECTION, ACTIVE_ELECTION.replaceAll("'", ""));
        verify(cacheService).evictCache(ELECTIONS);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(electionRepository, voterRepository, conversionService, dateService, cacheService, page, voterPage);
    }

}
