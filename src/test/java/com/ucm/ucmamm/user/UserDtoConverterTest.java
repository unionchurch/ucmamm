package com.ucm.ucmamm.user;

import com.ucm.ucmamm.domain.Role;
import com.ucm.ucmamm.domain.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static java.util.Collections.singleton;

@RunWith(BlockJUnit4ClassRunner.class)
public class UserDtoConverterTest {

    private UserDtoConverter userDtoConverter = new UserDtoConverter();

    @Test
    public void givenUser_whenConvert_thenShouldReturnUserDto() {
        User user = User.builder()
                .id("1")
                .userName("existing")
                .password("P@ssw0rd")
                .firstName("FNTest")
                .middleName("MNTest")
                .lastName("LNTest")
                .active(Boolean.TRUE)
                .deleted(Boolean.FALSE)
                .version(1)
                .roles(singleton(Role.builder().roleName("ROLE_USER").build()))
                .build();

        UserDto userDto = userDtoConverter.convert(user);

        Assert.assertEquals(UserDto.builder()
                .id("1")
                .userName("existing")
                .firstName("FNTest")
                .middleName("MNTest")
                .lastName("LNTest")
                .active(Boolean.TRUE)
                .deleted(Boolean.FALSE)
                .version(1)
                .roles(singleton("ROLE_USER"))
                .build(), userDto);
    }

}
