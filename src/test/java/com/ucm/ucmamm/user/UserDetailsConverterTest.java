package com.ucm.ucmamm.user;

import com.ucm.ucmamm.domain.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import static java.util.Collections.singleton;
import static org.junit.Assert.assertEquals;

@RunWith(BlockJUnit4ClassRunner.class)
public class UserDetailsConverterTest {

    private static final boolean ACTIVE = Boolean.TRUE;
    private static final boolean ACCOUNT_NON_EXPIRED = Boolean.TRUE;
    private static final boolean CREDENTIALS_NON_EXPIRED = Boolean.TRUE;
    private static final boolean ACCOUNT_NON_LOCKED = Boolean.TRUE;

    private UserDetailsConverter userDetailsConverter = new UserDetailsConverter();

    @Test
    public void givenUser_whenConvert_thenShouldReturnCorrectUserDetails() {
        com.ucm.ucmamm.domain.User user = com.ucm.ucmamm.domain.User.builder()
                .id("1")
                .userName("existing")
                .password("P@ssw0rd")
                .firstName("FNTest")
                .middleName("MNTest")
                .lastName("LNTest")
                .active(Boolean.TRUE)
                .deleted(Boolean.FALSE)
                .version(1)
                .roles(singleton(Role.builder().roleName("ROLE_USER").build()))
                .build();

        UserDetails userDetails = userDetailsConverter.convert(user);

        assertEquals(new User("existing",
                "P@ssw0rd",
                ACTIVE,
                ACCOUNT_NON_EXPIRED,
                CREDENTIALS_NON_EXPIRED,
                ACCOUNT_NON_LOCKED,
                singleton(new SimpleGrantedAuthority("ROLE_USER"))), userDetails);
    }

}
