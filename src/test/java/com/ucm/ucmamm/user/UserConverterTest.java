package com.ucm.ucmamm.user;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.Role;
import com.ucm.ucmamm.domain.RoleRepository;
import com.ucm.ucmamm.domain.User;
import com.ucm.ucmamm.domain.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Instant;

import static java.util.Collections.singleton;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserConverterTest {

    private static final Instant MOCK_TIME = Instant.parse("2018-01-01T00:00:00.000Z");
    private static final String MOCK_PASSWORD = "mock_password";

    @InjectMocks
    private UserConverter userConverter;
    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private DateService dateService;
    @Mock
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() {
        when(dateService.getCurrentTime()).thenReturn(MOCK_TIME);
        when(passwordEncoder.encode(anyString())).thenReturn(MOCK_PASSWORD);
    }

    @Test
    public void givenNonExistingUserInput_whenConvert_thenReturnCorrectResult() {
        UserInput userInput = UserInput.builder()
                .userName("nonexisting")
                .password("P@ssw0rd")
                .firstName("FNTest")
                .middleName("MNTest")
                .lastName("LNTest")
                .roles(singleton("ROLE_USER"))
                .operator("admin")
                .build();

        when(roleRepository.findByRoleNameIn(singleton("ROLE_USER")))
                .thenReturn(singleton(Role.builder().roleName("ROLE_USER").build()));

        User user = userConverter.convert(userInput);

        assertEquals(User.builder()
                .userName("nonexisting")
                .password(MOCK_PASSWORD)
                .firstName("FNTest")
                .middleName("MNTest")
                .lastName("LNTest")
                .active(Boolean.TRUE)
                .deleted(Boolean.FALSE)
                .version(1)
                .createdBy("admin")
                .createdAt(MOCK_TIME)
                .roles(singleton(Role.builder().roleName("ROLE_USER").build()))
                .build(), user);

        verify(userRepository, never()).findByIdAndDeleted(anyString(), anyBoolean());
        verify(dateService).getCurrentTime();
        verify(passwordEncoder).encode("P@ssw0rd");
    }

    @After
    public void teardown() {
        verifyNoMoreInteractions(userRepository, dateService, passwordEncoder);
    }

}
