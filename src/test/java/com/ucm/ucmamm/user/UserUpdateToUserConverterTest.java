package com.ucm.ucmamm.user;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.common.ResourceNotFoundException;
import com.ucm.ucmamm.domain.Role;
import com.ucm.ucmamm.domain.RoleRepository;
import com.ucm.ucmamm.domain.User;
import com.ucm.ucmamm.domain.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;

import static java.util.Collections.singleton;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserUpdateToUserConverterTest {

    private static final Instant MOCK_TIME = Instant.parse("2018-01-01T00:00:00.000Z");

    @InjectMocks
    private UserUpdateToUserConverter userConverter;
    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private DateService dateService;

    @Before
    public void setUp() {
        when(dateService.getCurrentTime()).thenReturn(MOCK_TIME);
    }

    @Test
    public void givenExistingUserInput_whenConvert_thenReturnCorrectResult() {
        UserUpdate userInput = UserUpdate.builder()
                .id("1")
                .userName("existing")
                .password("P@ssw0rd")
                .firstName("FNTest")
                .middleName("MNTest")
                .lastName("LNTest")
                .roles(singleton("ROLE_USER"))
                .operator("admin")
                .build();

        when(userRepository.findByIdAndDeleted(eq("1"), eq(Boolean.FALSE)))
                .thenReturn(of(User.builder()
                        .id("1")
                        .userName("existing")
                        .password("P@ssw0rd")
                        .firstName("FNTest")
                        .middleName("MNTest")
                        .lastName("LNTest")
                        .active(Boolean.TRUE)
                        .deleted(Boolean.FALSE)
                        .version(1)
                        .createdBy("admin")
                        .createdAt(MOCK_TIME)
                        .roles(singleton(Role.builder().roleName("ROLE_USER").build()))
                        .build()));
        when(roleRepository.findByRoleNameIn(singleton("ROLE_USER")))
                .thenReturn(singleton(Role.builder().roleName("ROLE_USER").build()));

        User user = userConverter.convert(userInput);

        assertEquals(User.builder()
                .id("1")
                .userName("existing")
                .password("P@ssw0rd")
                .firstName("FNTest")
                .middleName("MNTest")
                .lastName("LNTest")
                .active(Boolean.TRUE)
                .deleted(Boolean.FALSE)
                .createdBy("admin")
                .createdAt(MOCK_TIME)
                .updatedBy("admin")
                .updatedAt(MOCK_TIME)
                .version(2)
                .roles(singleton(Role.builder().roleName("ROLE_USER").build()))
                .build(), user);

        verify(userRepository).findByIdAndDeleted(anyString(), anyBoolean());
        verify(roleRepository).findByRoleNameIn(anySet());
        verify(dateService).getCurrentTime();
    }

    @Test
    public void givenExistingUserInputNoPassword_whenConvert_thenReturnCorrectResult() {
        UserUpdate userInput = UserUpdate.builder()
                .id("1")
                .userName("existing")
                .firstName("FNTest")
                .middleName("MNTest")
                .lastName("LNTest")
                .roles(singleton("ROLE_USER"))
                .operator("admin")
                .build();

        when(userRepository.findByIdAndDeleted(eq("1"), eq(Boolean.FALSE)))
                .thenReturn(of(User.builder()
                        .id("1")
                        .userName("existing")
                        .password("P@ssw0rd")
                        .firstName("FNTest")
                        .middleName("MNTest")
                        .lastName("LNTest")
                        .active(Boolean.TRUE)
                        .deleted(Boolean.FALSE)
                        .version(1)
                        .createdBy("admin")
                        .createdAt(MOCK_TIME)
                        .roles(singleton(Role.builder().roleName("ROLE_USER").build()))
                        .build()));
        when(roleRepository.findByRoleNameIn(singleton("ROLE_USER")))
                .thenReturn(singleton(Role.builder().roleName("ROLE_USER").build()));

        User user = userConverter.convert(userInput);

        assertEquals(User.builder()
                .id("1")
                .userName("existing")
                .password("P@ssw0rd")
                .firstName("FNTest")
                .middleName("MNTest")
                .lastName("LNTest")
                .active(Boolean.TRUE)
                .deleted(Boolean.FALSE)
                .createdBy("admin")
                .createdAt(MOCK_TIME)
                .updatedBy("admin")
                .updatedAt(MOCK_TIME)
                .version(2)
                .roles(singleton(Role.builder().roleName("ROLE_USER").build()))
                .build(), user);

        verify(userRepository).findByIdAndDeleted(anyString(), anyBoolean());
        verify(roleRepository).findByRoleNameIn(anySet());
        verify(dateService).getCurrentTime();
    }

    @Test(expected = ResourceNotFoundException.class)
    public void givenNonExistingUserInput_whenConvert_thenReturnCorrectResult() {
        UserUpdate userInput = UserUpdate.builder()
                .id("1")
                .userName("existing")
                .password("P@ssw0rd")
                .firstName("FNTest")
                .middleName("MNTest")
                .lastName("LNTest")
                .roles(singleton("ROLE_USER"))
                .operator("admin")
                .build();

        when(userRepository.findByIdAndDeleted(eq("1"), eq(Boolean.FALSE))).thenReturn(empty());

        try {
            userConverter.convert(userInput);
        } catch (ResourceNotFoundException e) {
            verify(userRepository).findByIdAndDeleted(anyString(), anyBoolean());
            verify(roleRepository, never()).findByRoleNameIn(anySet());
            verify(dateService, never()).getCurrentTime();

            throw e;
        }
    }

    @After
    public void teardown() {
        verifyNoMoreInteractions(userRepository, roleRepository, dateService);
    }

}
