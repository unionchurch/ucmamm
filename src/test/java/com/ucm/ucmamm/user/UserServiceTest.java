package com.ucm.ucmamm.user;

import com.ucm.ucmamm.common.CacheService;
import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.Role;
import com.ucm.ucmamm.domain.User;
import com.ucm.ucmamm.domain.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.ucm.ucmamm.common.Caches.USER;
import static com.ucm.ucmamm.common.Caches.USERS;
import static java.util.Collections.singleton;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.data.domain.Sort.Direction.DESC;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private static final boolean ACTIVE = Boolean.TRUE;
    private static final boolean INACTIVE = Boolean.FALSE;
    private static final boolean ACCOUNT_NON_EXPIRED = Boolean.TRUE;
    private static final boolean CREDENTIALS_NON_EXPIRED = Boolean.TRUE;
    private static final boolean ACCOUNT_NON_LOCKED = Boolean.TRUE;

    private static final Instant MOCK_TIME = Instant.parse("2018-01-01T00:00:00.000Z");

    private static final UserInput NEW_USER_INPUT = UserInput.builder()
            .userName("nonexisting")
            .password("P@ssw0rd")
            .firstName("FNTest")
            .middleName("MNTest")
            .lastName("LNTest")
            .roles(singleton("ROLE_USER"))
            .build();
    private static final User NEW_USER = User.builder()
            .userName("nonexisting")
            .password("P@ssw0rd")
            .firstName("FNTest")
            .middleName("MNTest")
            .lastName("LNTest")
            .active(Boolean.TRUE)
            .deleted(Boolean.FALSE)
            .version(1)
            .roles(singleton(Role.builder().roleName("ROLE_USER").build()))
            .build();
    private static final UserUpdate USER_UPDATE = UserUpdate.builder()
            .id("1")
            .userName("existing")
            .password("P@ssw0rd")
            .firstName("FNTest")
            .middleName("MNTest")
            .lastName("LNTest")
            .roles(singleton("ROLE_USER"))
            .build();
    private static final User UPDATED_USER = User.builder()
            .id("1")
            .userName("existing")
            .password("P@ssw0rd")
            .firstName("FNTest")
            .middleName("MNTest")
            .lastName("LNTest")
            .active(Boolean.TRUE)
            .deleted(Boolean.FALSE)
            .version(2)
            .roles(singleton(Role.builder().roleName("ROLE_USER").build()))
            .build();
    private static final User EXISTING_USER = User.builder()
            .id("1")
            .userName("existing")
            .password("P@ssw0rd")
            .firstName("FNTest")
            .middleName("MNTest")
            .lastName("LNTest")
            .active(Boolean.TRUE)
            .deleted(Boolean.FALSE)
            .version(1)
            .createdBy("admin")
            .createdAt(MOCK_TIME)
            .roles(singleton(Role.builder().roleName("ROLE_USER").build()))
            .build();
    private static final UserDto EXISTING_USER_DTO = UserDto.builder()
            .id("1")
            .userName("existing")
            .firstName("FNTest")
            .middleName("MNTest")
            .lastName("LNTest")
            .active(Boolean.TRUE)
            .deleted(Boolean.FALSE)
            .version(1)
            .roles(singleton("ROLE_USER"))
            .build();
    private static final UserDetails EXISTING_USER_DETAILS = new org.springframework.security.core.userdetails.User(
            "existing",
            "P@ssw0rd",
            ACTIVE,
            ACCOUNT_NON_EXPIRED,
            CREDENTIALS_NON_EXPIRED,
            ACCOUNT_NON_LOCKED,
            singleton(new SimpleGrantedAuthority("ROLE_USER")));
    private static final User DELETED_USER = User.builder()
            .id("1")
            .userName("existing")
            .password("P@ssw0rd")
            .firstName("FNTest")
            .middleName("MNTest")
            .lastName("LNTest")
            .active(Boolean.TRUE)
            .deleted(Boolean.TRUE)
            .version(1)
            .createdBy("admin")
            .createdAt(MOCK_TIME)
            .updatedBy("admin")
            .updatedAt(MOCK_TIME)
            .roles(singleton(Role.builder().roleName("ROLE_USER").build()))
            .build();
    private static final User INACTIVE_USER = User.builder()
            .id("1")
            .userName("existing")
            .password("P@ssw0rd")
            .firstName("FNTest")
            .middleName("MNTest")
            .lastName("LNTest")
            .active(Boolean.FALSE)
            .deleted(Boolean.FALSE)
            .version(2)
            .roles(singleton(Role.builder().roleName("ROLE_USER").build()))
            .build();
    private static final UserDetails INACTIVE_USER_DETAILS = new org.springframework.security.core.userdetails.User(
            "existing",
            "P@ssw0rd",
            INACTIVE,
            ACCOUNT_NON_EXPIRED,
            CREDENTIALS_NON_EXPIRED,
            ACCOUNT_NON_LOCKED,
            singleton(new SimpleGrantedAuthority("ROLE_USER")));
    private static final User EXISTING_USER_2 = User.builder()
            .id("2")
            .userName("existing2")
            .password("P@ssw0rd")
            .firstName("FNTest2")
            .middleName("MNTest2")
            .lastName("LNTest2")
            .active(Boolean.TRUE)
            .deleted(Boolean.FALSE)
            .version(2)
            .roles(singleton(Role.builder().roleName("ROLE_ADMIN").build()))
            .build();
    private static final UserDto EXISTING_USER_DTO_2 = UserDto.builder()
            .id("2")
            .userName("existing2")
            .firstName("FNTest2")
            .middleName("MNTest2")
            .lastName("LNTest2")
            .active(Boolean.TRUE)
            .deleted(Boolean.FALSE)
            .version(2)
            .roles(singleton("ROLE_ADMIN"))
            .build();

    @InjectMocks
    private UserService userService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private ConversionService conversionService;
    @Mock
    private DateService dateService;
    @Mock
    private CacheService cacheService;
    @Mock
    private Page<User> userPage;
    @Mock
    private UserDetailsConverter userDetailsConverter;

    @Before
    public void setUp() {
        when(dateService.getCurrentTime()).thenReturn(MOCK_TIME);
    }

    @Test
    public void givenUserInput_whenSave_thenVerifyUserSave() {
        when(conversionService.convert(NEW_USER_INPUT, User.class)).thenReturn(NEW_USER);

        userService.saveUser(NEW_USER_INPUT);

        verify(conversionService).convert(NEW_USER_INPUT, User.class);
        verify(userRepository).save(NEW_USER);
        verify(cacheService).evictCache(USERS);
    }

    @Test
    public void givenUserUpdate_whenSave_thenVerifyUserUpdate() {
        when(conversionService.convert(USER_UPDATE, User.class)).thenReturn(UPDATED_USER);

        userService.updateUser(USER_UPDATE);

        verify(conversionService).convert(USER_UPDATE, User.class);
        verify(userRepository).save(UPDATED_USER);
        verify(cacheService).evictCache(USERS);
        verify(cacheService).evictCache(USER, "1");
        verify(cacheService).evictCache(USER, "existing");
    }

    @Test
    public void givenUserDelete_whenDelete_thenVerifyUserDelete() {
        when(userRepository.findById("1")).thenReturn(of(EXISTING_USER));

        userService.deleteUser("1", "admin");

        verify(userRepository).findById("1");
        verify(userRepository).save(DELETED_USER);
        verify(cacheService).evictCache(USERS);
        verify(cacheService).evictCache(USER, "1");
        verify(cacheService).evictCache(USER, "existing");
    }

    @Test
    public void givenExistingUserFromRepository_whenGet_thenShouldReturnUserDto() {
        when(userRepository.findByIdAndDeleted("1", Boolean.FALSE)).thenReturn(of(EXISTING_USER));
        when(conversionService.convert(EXISTING_USER, UserDto.class)).thenReturn(EXISTING_USER_DTO);

        Optional<UserDto> userDtoOpt = userService.getUser("1");

        assertTrue(userDtoOpt.isPresent());
        assertEquals(EXISTING_USER_DTO, userDtoOpt.get());

        verify(userRepository).findByIdAndDeleted("1", Boolean.FALSE);
        verify(conversionService).convert(EXISTING_USER, UserDto.class);
    }

    @Test
    public void givenExistingUserFromRepository_whenGetByUsername_thenShouldReturnUserDto() {
        when(userRepository.findByUserNameAndDeleted("existing", Boolean.FALSE)).thenReturn(of(EXISTING_USER));
        when(conversionService.convert(EXISTING_USER, UserDto.class)).thenReturn(EXISTING_USER_DTO);

        Optional<UserDto> userDtoOpt = userService.getUserByName("existing");

        assertTrue(userDtoOpt.isPresent());
        assertEquals(EXISTING_USER_DTO, userDtoOpt.get());

        verify(userRepository).findByUserNameAndDeleted("existing", Boolean.FALSE);
        verify(conversionService).convert(EXISTING_USER, UserDto.class);
    }

    @Test
    public void givenUserFromRepository_whenGetOptionalUserDetails_thenShouldReturnUserDetails() {
        when(userRepository.findByUserNameAndDeleted("existing", Boolean.FALSE)).thenReturn(of(EXISTING_USER));
        when(conversionService.convert(EXISTING_USER, UserDetails.class)).thenReturn(EXISTING_USER_DETAILS);

        Optional<UserDetails> userDetailsOpt = userService.getUserDetails("existing");

        assertTrue(userDetailsOpt.isPresent());
        assertEquals(EXISTING_USER_DETAILS, userDetailsOpt.get());

        verify(userRepository).findByUserNameAndDeleted("existing", Boolean.FALSE);
        verify(conversionService).convert(EXISTING_USER, UserDetails.class);
    }

    @Test
    public void givenUserFromRepository_whenGetUserDetails_thenShouldReturnUserDetails() {
        when(userRepository.findByUserNameAndDeleted("existing", Boolean.FALSE)).thenReturn(of(EXISTING_USER));
        when(userDetailsConverter.convert(EXISTING_USER)).thenReturn(EXISTING_USER_DETAILS);

        UserDetails userDetails2 = userService.loadUserByUsername("existing");

        assertEquals(EXISTING_USER_DETAILS, userDetails2);

        verify(userRepository).findByUserNameAndDeleted("existing", Boolean.FALSE);
        verify(userDetailsConverter).convert(EXISTING_USER);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void givenInactiveUserFromRepository_whenGetUserDetails_thenShouldThrowException() {
        when(userRepository.findByUserNameAndDeleted("existing", Boolean.FALSE)).thenReturn(of(INACTIVE_USER));
        when(userDetailsConverter.convert(INACTIVE_USER)).thenReturn(INACTIVE_USER_DETAILS);

        try {
            userService.loadUserByUsername("existing");
        } catch (UsernameNotFoundException e) {
            verify(userRepository).findByUserNameAndDeleted("existing", Boolean.FALSE);
            verify(userDetailsConverter).convert(INACTIVE_USER);
            throw e;
        }
    }

    @Test(expected = UsernameNotFoundException.class)
    public void givenNonExistingUserFromRepository_whenGetUserDetails_thenShouldThrowException() {
        when(userRepository.findByUserNameAndDeleted("nonexisting", Boolean.FALSE)).thenReturn(empty());

        try {
            userService.loadUserByUsername("nonexisting");
        } catch (UsernameNotFoundException e) {
            verify(userRepository).findByUserNameAndDeleted("nonexisting", Boolean.FALSE);
            throw e;
        }
    }

    @Test
    public void givenUsersFromRepository_whenGetUsersByPageAscending_thenShouldReturnUserPage() {
        PageRequest pageRequest = PageRequest.of(1, 5, ASC, "userName");
        when(userRepository.findByUserNameStartingWithAndDeleted(eq("existing"), eq(Boolean.FALSE), eq(pageRequest)))
                .thenReturn(userPage);
        when(userPage.hasContent()).thenReturn(Boolean.TRUE);
        when(userPage.getContent()).thenReturn(Arrays.asList(EXISTING_USER, EXISTING_USER_2));
        when(conversionService.convert(EXISTING_USER, UserDto.class)).thenReturn(EXISTING_USER_DTO);
        when(conversionService.convert(EXISTING_USER_2, UserDto.class)).thenReturn(EXISTING_USER_DTO_2);

        List<UserDto> userDtos = userService.getUsers(1, 5, "existing", null, Boolean.TRUE);

        assertEquals(Arrays.asList(EXISTING_USER_DTO, EXISTING_USER_DTO_2), userDtos);
        verify(userRepository).findByUserNameStartingWithAndDeleted(eq("existing"), eq(Boolean.FALSE), eq(pageRequest));
        verify(userPage).hasContent();
        verify(userPage).getContent();
        verify(conversionService, times(2)).convert(any(User.class), eq(UserDto.class));
    }

    @Test
    public void givenUsersFromRepository_whenGetUsersByPageDescending_thenShouldReturnUserPage() {
        PageRequest pageRequest = PageRequest.of(1, 5, DESC, "userName");
        when(userRepository.findByUserNameStartingWithAndDeleted(eq("existing"), eq(Boolean.FALSE), eq(pageRequest)))
                .thenReturn(userPage);
        when(userPage.hasContent()).thenReturn(Boolean.TRUE);
        when(userPage.getContent()).thenReturn(Arrays.asList(EXISTING_USER_2, EXISTING_USER));
        when(conversionService.convert(EXISTING_USER, UserDto.class)).thenReturn(EXISTING_USER_DTO);
        when(conversionService.convert(EXISTING_USER_2, UserDto.class)).thenReturn(EXISTING_USER_DTO_2);

        List<UserDto> userDtos = userService.getUsers(1, 5, "existing", null, Boolean.FALSE);

        assertEquals(Arrays.asList(EXISTING_USER_DTO_2, EXISTING_USER_DTO), userDtos);
        verify(userRepository).findByUserNameStartingWithAndDeleted(eq("existing"), eq(Boolean.FALSE), eq(pageRequest));
        verify(userPage).hasContent();
        verify(userPage).getContent();
        verify(conversionService, times(2)).convert(any(User.class), eq(UserDto.class));
    }

    @Test
    public void givenNoUsersMatchedFromRepository_whenGetUsersByPage_thenShouldReturnUserPage() {
        PageRequest pageRequest = PageRequest.of(1, 5, ASC, "userName");
        when(userRepository.findByUserNameStartingWithAndDeleted(eq("existing"), eq(Boolean.FALSE), eq(pageRequest)))
                .thenReturn(userPage);
        when(userPage.hasContent()).thenReturn(Boolean.FALSE);

        List<UserDto> userDtos = userService.getUsers(1, 5, "existing", null, Boolean.TRUE);

        assertEquals(Collections.emptyList(), userDtos);
        verify(userRepository).findByUserNameStartingWithAndDeleted(eq("existing"), eq(Boolean.FALSE), eq(pageRequest));
        verify(userPage).hasContent();
    }

    @Test
    public void givenUsersFromRepository_whenGetUsersByPageNoKeyword_thenShouldReturnUserPage() {
        PageRequest pageRequest = PageRequest.of(1, 5, ASC, "userName");
        when(userRepository.findByDeleted(eq(Boolean.FALSE), eq(pageRequest))).thenReturn(userPage);
        when(userPage.hasContent()).thenReturn(Boolean.TRUE);
        when(userPage.getContent()).thenReturn(Arrays.asList(EXISTING_USER, EXISTING_USER_2));
        when(conversionService.convert(EXISTING_USER, UserDto.class)).thenReturn(EXISTING_USER_DTO);
        when(conversionService.convert(EXISTING_USER_2, UserDto.class)).thenReturn(EXISTING_USER_DTO_2);

        List<UserDto> userDtos = userService.getUsers(1, 5, null, null, Boolean.TRUE);

        assertEquals(Arrays.asList(EXISTING_USER_DTO, EXISTING_USER_DTO_2), userDtos);
        verify(userRepository).findByDeleted(eq(Boolean.FALSE), eq(pageRequest));
        verify(userPage).hasContent();
        verify(userPage).getContent();
        verify(conversionService, times(2)).convert(any(User.class), eq(UserDto.class));
    }

    @After
    public void teardown() {
        verifyNoMoreInteractions(userRepository, conversionService, userPage);
    }

}
