package com.ucm.ucmamm.user;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.ConstraintValidatorContext;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UniqueUserNameValidatorTest {

    @InjectMocks
    private UniqueUserNameValidator uniqueUserNameValidator;

    @Mock
    private UserService userService;
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;
    @Mock
    private UserDetails userDetails;

    @Test
    public void givenNonExistingUserName_whenValidated_thenShouldReturnTrue() {
        when(userService.getUserDetails(anyString())).thenReturn(empty());

        assertTrue(uniqueUserNameValidator.isValid("nonexisting", constraintValidatorContext));

        verify(userService).getUserDetails(anyString());
    }

    @Test
    public void givenExistingUserName_whenValidated_thenShouldReturnTrue() {
        when(userService.getUserDetails(anyString())).thenReturn(of(userDetails));

        assertFalse(uniqueUserNameValidator.isValid("existing", constraintValidatorContext));

        verify(userService).getUserDetails(anyString());
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(userService);
    }

}
