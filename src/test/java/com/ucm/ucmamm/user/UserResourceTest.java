package com.ucm.ucmamm.user;

import com.ucm.ucmamm.AuthServerConfiguration;
import com.ucm.ucmamm.ResourceServerConfiguration;
import com.ucm.ucmamm.UcmAmmOauth2Properties;
import com.ucm.ucmamm.common.ResourceTestSupport;
import com.ucm.ucmamm.domain.Role;
import com.ucm.ucmamm.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Arrays;

import static java.util.Collections.singleton;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = UserResourceTest.Main.class)
public class UserResourceTest extends ResourceTestSupport {

    @SpringBootApplication(scanBasePackages = {"com.ucm.ucmamm.common", "com.ucm.ucmamm.user"}, exclude = MongoAutoConfiguration.class)
    @Import({UcmAmmOauth2Properties.class, ResourceServerConfiguration.class, AuthServerConfiguration.class, ResourceTestSupport.SecurityConfiguration.class})
    public static class Main {

        public static void main(String[] args) {
            SpringApplication.run(UserResourceTest.Main.class, args);
        }

    }

    private UserInput userInput;

    @Override
    protected void initializeMocks() {
        UserDto adminUser = UserDto.builder()
                .id("1")
                .userName("admin")
                .firstName("Admin")
                .middleName("")
                .lastName("Admin")
                .active(Boolean.TRUE)
                .deleted(Boolean.FALSE)
                .version(1)
                .roles(singleton("ROLE_ADMIN"))
                .build();
        UserDto testUser = UserDto.builder()
                .id("2")
                .userName("testuser")
                .firstName("Test")
                .middleName("")
                .lastName("User")
                .active(Boolean.TRUE)
                .deleted(Boolean.FALSE)
                .version(1)
                .roles(singleton("ROLE_USER"))
                .build();
        userInput = UserInput.builder()
                .userName("testuser2")
                .password("P@ssw0rd")
                .firstName("Test 2")
                .middleName("")
                .lastName("User")
                .roles(singleton("ROLE_USER"))
                .build();
        User testuser2 = User.builder()
                .id("3")
                .userName("testuser2")
                .password("P@ssw0rd")
                .firstName("Test 2")
                .middleName("")
                .lastName("User")
                .active(Boolean.TRUE)
                .deleted(Boolean.FALSE)
                .version(1)
                .roles(singleton(Role.builder().roleName("ROLE_NAME").build()))
                .build();

        when(userService.getUser("1")).thenReturn(of(adminUser));
        when(userService.getUserByName("admin")).thenReturn(of(adminUser));
        when(userService.getUsers(anyInt(), anyInt(), anyString(), anyString(), anyBoolean()))
                .thenReturn(Arrays.asList(adminUser, testUser));
        when(userService.saveUser(any(UserInput.class))).thenReturn(testuser2);
    }

    @Test
    public void givenExistingUser_whenGet_thenShouldReturnUser() throws Exception {
        mockMvc.perform(get("/api/v1/users/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken()))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is("1")))
                .andExpect(jsonPath("$.userName", is("admin")))
                .andExpect(jsonPath("$.firstName", is("Admin")))
                .andExpect(jsonPath("$.lastName", is("Admin")))
                .andExpect(jsonPath("$.middleName", is("")))
                .andExpect(jsonPath("$.active", is(Boolean.TRUE)))
                .andExpect(jsonPath("$.deleted", is(Boolean.FALSE)))
                .andExpect(jsonPath("$.version", is(1)))
                .andExpect(jsonPath("$.roles[0]", is("ROLE_ADMIN")));
    }

    @Test
    public void givenCurrentUser_whenGet_thenShouldReturnUser() throws Exception {
        mockMvc.perform(get("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken()))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is("1")))
                .andExpect(jsonPath("$.userName", is("admin")))
                .andExpect(jsonPath("$.firstName", is("Admin")))
                .andExpect(jsonPath("$.lastName", is("Admin")))
                .andExpect(jsonPath("$.middleName", is("")))
                .andExpect(jsonPath("$.active", is(Boolean.TRUE)))
                .andExpect(jsonPath("$.deleted", is(Boolean.FALSE)))
                .andExpect(jsonPath("$.version", is(1)))
                .andExpect(jsonPath("$.roles[0]", is("ROLE_ADMIN")));
    }

    @Test
    public void givenNonExistingUser_whenGet_thenShouldReturnNotFound() throws Exception {
        mockMvc.perform(get("/api/v1/users/nonexisting")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenExistingUsers_whenGetList_thenShouldReturnUserList() throws Exception {
        mockMvc.perform(get("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken())
                .param("keyword", "")
                .param("sort", ""))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is("1")))
                .andExpect(jsonPath("$[0].userName", is("admin")))
                .andExpect(jsonPath("$[0].firstName", is("Admin")))
                .andExpect(jsonPath("$[0].lastName", is("Admin")))
                .andExpect(jsonPath("$[0].middleName", is("")))
                .andExpect(jsonPath("$[0].active", is(Boolean.TRUE)))
                .andExpect(jsonPath("$[0].deleted", is(Boolean.FALSE)))
                .andExpect(jsonPath("$[0].version", is(1)))
                .andExpect(jsonPath("$[0].roles[0]", is("ROLE_ADMIN")))
                .andExpect(jsonPath("$[1].id", is("2")))
                .andExpect(jsonPath("$[1].userName", is("testuser")))
                .andExpect(jsonPath("$[1].firstName", is("Test")))
                .andExpect(jsonPath("$[1].lastName", is("User")))
                .andExpect(jsonPath("$[1].middleName", is("")))
                .andExpect(jsonPath("$[1].active", is(Boolean.TRUE)))
                .andExpect(jsonPath("$[1].deleted", is(Boolean.FALSE)))
                .andExpect(jsonPath("$[1].version", is(1)))
                .andExpect(jsonPath("$[1].roles[0]", is("ROLE_USER")));
    }

    @Test
    public void givenUserInput_whenPost_thenShouldReturnCreated() throws Exception {
        mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken())
                .content(toJson(userInput)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/api/v1/users/3"));
    }

    @Test
    public void givenUserInput_whenPut_thenShouldReturnNoContent() throws Exception {
        mockMvc.perform(put("/api/v1/users/3")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken())
                .content(toJson(userInput)))
                .andExpect(status().isNoContent());
    }

    @Test
    public void givenUserId_whenDelete_thenShouldReturnNoContent() throws Exception {
        mockMvc.perform(delete("/api/v1/users/3")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken()))
                .andExpect(status().isNoContent());
    }

}
