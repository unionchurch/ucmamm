package com.ucm.ucmamm.member;

import com.ucm.ucmamm.AuthServerConfiguration;
import com.ucm.ucmamm.ResourceServerConfiguration;
import com.ucm.ucmamm.UcmAmmOauth2Properties;
import com.ucm.ucmamm.common.ResourceTestSupport;
import com.ucm.ucmamm.domain.Member;
import com.ucm.ucmamm.domain.MemberCountRepository;
import com.ucm.ucmamm.domain.MemberIDRepository;
import com.ucm.ucmamm.domain.MemberRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.Instant;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = MemberResourceTest.Main.class)
public class MemberResourceTest extends ResourceTestSupport {

    @SpringBootApplication(scanBasePackages = {"com.ucm.ucmamm.common", "com.ucm.ucmamm.member"}, exclude = MongoAutoConfiguration.class)
    @Import({UcmAmmOauth2Properties.class, ResourceServerConfiguration.class, AuthServerConfiguration.class, ResourceTestSupport.SecurityConfiguration.class})
    public static class Main {

        public static void main(String[] args) {
            SpringApplication.run(MemberResourceTest.Main.class, args);
        }

    }

    private static final Instant MOCK_TIME = Instant.parse("2018-01-01T00:00:00.000Z");

    @MockBean
    private MemberService memberService;
    @MockBean
    private MemberRepository memberRepository;
    @MockBean
    private MemberIDRepository memberIDRepository;
    @MockBean
    private MemberCountRepository memberCountRepository;

    private MemberInput newMemberInput;
    private MemberUpdate memberUpdate;

    @Override
    protected void initializeMocks() {
        Member member = Member.builder()
                .id("1")
                .memberId(1L)
                .firstName("Romeo")
                .middleName("")
                .lastName("Maranan")
                .suffix("Jr.")
                .phoneNumbers(singletonList("02 999 9999"))
                .emailAddresses(singletonList("rmaranan@test.com"))
                .streetAddress("123 J. P. Rizal St.")
                .village("Barangay X")
                .city("Batangas City")
                .province("Batangas")
                .zipCode("1234")
                .deleted(Boolean.FALSE)
                .active(Boolean.TRUE)
                .version(1)
                .createdBy("admin")
                .createdAt(MOCK_TIME)
                .build();
        newMemberInput = MemberInput.builder()
                .firstName("Romeo")
                .middleName("")
                .lastName("Maranan")
                .suffix("Jr.")
                .phoneNumbers(singletonList("02 999 9999"))
                .emailAddresses(singletonList("rmaranan@test.com"))
                .streetAddress("123 J. P. Rizal St.")
                .village("Barangay X")
                .city("Batangas City")
                .province("Batangas")
                .zipCode("1234")
                .operator("admin")
                .build();
        memberUpdate = MemberUpdate.builder()
                .id("1")
                .memberId(1L)
                .firstName("Romeo")
                .middleName("")
                .lastName("Maranan")
                .suffix("Jr.")
                .phoneNumbers(singletonList("02 999 9999"))
                .emailAddresses(singletonList("rmaranan@test.com"))
                .streetAddress("123 J. P. Rizal St.")
                .village("Barangay X")
                .city("Batangas City")
                .province("Batangas")
                .zipCode("1234")
                .operator("admin")
                .build();
        MemberDto memberDto = MemberDto.builder()
                .id("1")
                .memberId(1L)
                .firstName("Romeo")
                .middleName("")
                .lastName("Maranan")
                .suffix("Jr.")
                .phoneNumbers(singletonList("02 999 9999"))
                .emailAddresses(singletonList("rmaranan@test.com"))
                .streetAddress("123 J. P. Rizal St.")
                .village("Barangay X")
                .city("Batangas City")
                .province("Batangas")
                .zipCode("1234")
                .deleted(Boolean.FALSE)
                .active(Boolean.TRUE)
                .version(1)
                .build();
        MemberDto memberDto2 = MemberDto.builder()
                .id("2")
                .memberId(2L)
                .firstName("Maven")
                .middleName("")
                .lastName("Maranan")
                .suffix("")
                .phoneNumbers(singletonList("02 999 9999"))
                .emailAddresses(singletonList("rmaranan@test.com"))
                .streetAddress("123 J. P. Rizal St.")
                .village("Barangay X")
                .city("Batangas City")
                .province("Batangas")
                .zipCode("1234")
                .deleted(Boolean.FALSE)
                .active(Boolean.TRUE)
                .version(1)
                .build();

        when(memberService.getMember("1")).thenReturn(of(memberDto));
        when(memberService.getMemberByMemberId(1L)).thenReturn(of(memberDto));
        when(memberService.getMember("nonexisting")).thenReturn(empty());
        when(memberService.getMembers(anyInt(), anyInt(), isNull(), isNull(), anyBoolean(), anyString()))
                .thenReturn(asList(memberDto, memberDto2));
        when(memberService.getMembers(anyString(), anyString())).thenReturn(singletonList(memberDto));
        when(memberService.saveMember(newMemberInput)).thenReturn(member);
    }

    @Test
    public void givenExistingMember_whenGet_thenShouldReturnMember() throws Exception {
        mockMvc.perform(get("/api/v1/members/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is("1")))
                .andExpect(jsonPath("$.memberId", is(1)))
                .andExpect(jsonPath("$.firstName", is("Romeo")))
                .andExpect(jsonPath("$.lastName", is("Maranan")))
                .andExpect(jsonPath("$.middleName", is("")))
                .andExpect(jsonPath("$.suffix", is("Jr.")))
                .andExpect(jsonPath("$.phoneNumbers[0]", is("02 999 9999")))
                .andExpect(jsonPath("$.emailAddresses[0]", is("rmaranan@test.com")))
                .andExpect(jsonPath("$.streetAddress", is("123 J. P. Rizal St.")))
                .andExpect(jsonPath("$.village", is("Barangay X")))
                .andExpect(jsonPath("$.city", is("Batangas City")))
                .andExpect(jsonPath("$.province", is("Batangas")))
                .andExpect(jsonPath("$.zipCode", is("1234")))
                .andExpect(jsonPath("$.active", is(Boolean.TRUE)))
                .andExpect(jsonPath("$.deleted", is(Boolean.FALSE)))
                .andExpect(jsonPath("$.version", is(1)));
    }

    @Test
    public void givenExistingMember_whenGetByMemberId_thenShouldReturnMember() throws Exception {
        mockMvc.perform(get("/api/v1/members/exact/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is("1")))
                .andExpect(jsonPath("$.memberId", is(1)))
                .andExpect(jsonPath("$.firstName", is("Romeo")))
                .andExpect(jsonPath("$.lastName", is("Maranan")))
                .andExpect(jsonPath("$.middleName", is("")))
                .andExpect(jsonPath("$.suffix", is("Jr.")))
                .andExpect(jsonPath("$.phoneNumbers[0]", is("02 999 9999")))
                .andExpect(jsonPath("$.emailAddresses[0]", is("rmaranan@test.com")))
                .andExpect(jsonPath("$.streetAddress", is("123 J. P. Rizal St.")))
                .andExpect(jsonPath("$.village", is("Barangay X")))
                .andExpect(jsonPath("$.city", is("Batangas City")))
                .andExpect(jsonPath("$.province", is("Batangas")))
                .andExpect(jsonPath("$.zipCode", is("1234")))
                .andExpect(jsonPath("$.active", is(Boolean.TRUE)))
                .andExpect(jsonPath("$.deleted", is(Boolean.FALSE)))
                .andExpect(jsonPath("$.version", is(1)));
    }

    @Test
    public void givenNonExistingMember_whenGet_thenShouldReturnNotFound() throws Exception {
        mockMvc.perform(get("/api/v1/members/nonexisting")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenExistingMembers_whenGetList_thenShouldReturnMemberList() throws Exception {
        mockMvc.perform(get("/api/v1/members")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is("1")))
                .andExpect(jsonPath("$[0].memberId", is(1)))
                .andExpect(jsonPath("$[0].firstName", is("Romeo")))
                .andExpect(jsonPath("$[0].lastName", is("Maranan")))
                .andExpect(jsonPath("$[0].middleName", is("")))
                .andExpect(jsonPath("$[0].suffix", is("Jr.")))
                .andExpect(jsonPath("$[0].phoneNumbers[0]", is("02 999 9999")))
                .andExpect(jsonPath("$[0].emailAddresses[0]", is("rmaranan@test.com")))
                .andExpect(jsonPath("$[0].streetAddress", is("123 J. P. Rizal St.")))
                .andExpect(jsonPath("$[0].village", is("Barangay X")))
                .andExpect(jsonPath("$[0].city", is("Batangas City")))
                .andExpect(jsonPath("$[0].province", is("Batangas")))
                .andExpect(jsonPath("$[0].zipCode", is("1234")))
                .andExpect(jsonPath("$[0].active", is(Boolean.TRUE)))
                .andExpect(jsonPath("$[0].deleted", is(Boolean.FALSE)))
                .andExpect(jsonPath("$[0].version", is(1)))
                .andExpect(jsonPath("$[1].id", is("2")))
                .andExpect(jsonPath("$[1].memberId", is(2)))
                .andExpect(jsonPath("$[1].firstName", is("Maven")))
                .andExpect(jsonPath("$[1].lastName", is("Maranan")))
                .andExpect(jsonPath("$[1].middleName", is("")))
                .andExpect(jsonPath("$[1].suffix", is("")))
                .andExpect(jsonPath("$[1].phoneNumbers[0]", is("02 999 9999")))
                .andExpect(jsonPath("$[1].emailAddresses[0]", is("rmaranan@test.com")))
                .andExpect(jsonPath("$[1].streetAddress", is("123 J. P. Rizal St.")))
                .andExpect(jsonPath("$[1].village", is("Barangay X")))
                .andExpect(jsonPath("$[1].city", is("Batangas City")))
                .andExpect(jsonPath("$[1].province", is("Batangas")))
                .andExpect(jsonPath("$[1].zipCode", is("1234")))
                .andExpect(jsonPath("$[1].active", is(Boolean.TRUE)))
                .andExpect(jsonPath("$[1].deleted", is(Boolean.FALSE)))
                .andExpect(jsonPath("$[1].version", is(1)));
    }

    @Test
    public void givenExistingMembers_whenGetExactList_thenShouldReturnMemberList() throws Exception {
        mockMvc.perform(get("/api/v1/members/exact")
                .contentType(MediaType.APPLICATION_JSON)
                .param("firstName", "Romeo")
                .param("lastName", "Maranan"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is("1")))
                .andExpect(jsonPath("$[0].memberId", is(1)))
                .andExpect(jsonPath("$[0].firstName", is("Romeo")))
                .andExpect(jsonPath("$[0].lastName", is("Maranan")))
                .andExpect(jsonPath("$[0].middleName", is("")))
                .andExpect(jsonPath("$[0].suffix", is("Jr.")))
                .andExpect(jsonPath("$[0].phoneNumbers[0]", is("02 999 9999")))
                .andExpect(jsonPath("$[0].emailAddresses[0]", is("rmaranan@test.com")))
                .andExpect(jsonPath("$[0].streetAddress", is("123 J. P. Rizal St.")))
                .andExpect(jsonPath("$[0].village", is("Barangay X")))
                .andExpect(jsonPath("$[0].city", is("Batangas City")))
                .andExpect(jsonPath("$[0].province", is("Batangas")))
                .andExpect(jsonPath("$[0].zipCode", is("1234")))
                .andExpect(jsonPath("$[0].active", is(Boolean.TRUE)))
                .andExpect(jsonPath("$[0].deleted", is(Boolean.FALSE)))
                .andExpect(jsonPath("$[0].version", is(1)));
    }

    @Test
    public void givenNonExistingMember_whenSave_thenShouldReturnCreated() throws Exception {
        mockMvc.perform(post("/api/v1/members")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken())
                .content(toJson(newMemberInput)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/api/v1/members/1"));
    }

    @Test
    public void givenExistingMember_whenUpdate_thenShouldReturnNoContent() throws Exception {
        mockMvc.perform(put("/api/v1/members/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken())
                .content(toJson(memberUpdate)))
                .andExpect(status().isNoContent());
    }

    @Test
    public void givenExistingMember_whenDelete_thenShouldReturnNoContent() throws Exception {
        mockMvc.perform(delete("/api/v1/members/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getAccessToken()))
                .andExpect(status().isNoContent());
    }

}
