package com.ucm.ucmamm.member;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.common.ResourceNotFoundException;
import com.ucm.ucmamm.domain.Member;
import com.ucm.ucmamm.domain.MemberRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MemberUpdateToMemberConverterTest {

    private static final Instant MOCK_TIME = Instant.parse("2018-01-01T00:00:00.000Z");

    private static final MemberUpdate MEMBER_UPDATE = MemberUpdate.builder()
            .id("1")
            .memberId(1234L)
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .active(TRUE)
            .operator("admin")
            .build();
    private static final Member EXISTING_MEMBER = Member.builder()
            .id("1")
            .memberId(1234L)
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .deleted(FALSE)
            .active(TRUE)
            .version(1)
            .createdBy("admin")
            .createdAt(MOCK_TIME)
            .build();
    private static final Member UPDATED_MEMBER = Member.builder()
            .id("1")
            .memberId(1234L)
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .deleted(FALSE)
            .active(TRUE)
            .version(2)
            .createdBy("admin")
            .createdAt(MOCK_TIME)
            .updatedBy("admin")
            .updatedAt(MOCK_TIME)
            .build();

    @InjectMocks
    private MemberUpdateToMemberConverter memberConverter;
    @Mock
    private MemberRepository memberRepository;
    @Mock
    private DateService dateService;

    @Before
    public void setUp() {
        when(dateService.getCurrentTime()).thenReturn(MOCK_TIME);
    }

    @Test
    public void givenExistingMember_whenConvert_thenShouldReturnCorrectResult() {
        when(memberRepository.findByIdAndDeleted(eq("1"), eq(FALSE))).thenReturn(of(EXISTING_MEMBER));

        Member actual = memberConverter.convert(MEMBER_UPDATE);

        assertEquals(UPDATED_MEMBER, actual);

        verify(memberRepository).findByIdAndDeleted(anyString(), anyBoolean());
        verify(dateService).getCurrentTime();
    }

    @Test(expected = ResourceNotFoundException.class)
    public void givenNonExistingMember_whenConvert_thenShouldReturnCorrectResult() {
        when(memberRepository.findByIdAndDeleted(eq("1"), eq(FALSE))).thenReturn(empty());

        try {
            memberConverter.convert(MEMBER_UPDATE);
        } catch (ResourceNotFoundException e) {
            verify(memberRepository).findByIdAndDeleted(anyString(), anyBoolean());
            verify(dateService, never()).getCurrentTime();

            throw e;
        }
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(memberRepository, dateService);
    }

}
