package com.ucm.ucmamm.member;

import com.ucm.ucmamm.common.CacheService;
import com.ucm.ucmamm.common.Caches;
import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.Member;
import com.ucm.ucmamm.domain.MemberCount;
import com.ucm.ucmamm.domain.MemberCountRepository;
import com.ucm.ucmamm.domain.MemberRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static com.ucm.ucmamm.common.Caches.MEMBERS;
import static com.ucm.ucmamm.domain.MemberCountRepository.MEMBER_COUNT_LOOKUP_NAME;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MemberServiceTest {

    private static final Instant MOCK_TIME = Instant.parse("2018-01-01T00:00:00.000Z");

    private static final MemberInput NEW_MEMBER_INPUT = MemberInput.builder()
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .operator("admin")
            .build();
    private static final MemberUpdate MEMBER_UPDATE = MemberUpdate.builder()
            .id("1")
            .memberId(1L)
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .operator("admin")
            .build();
    private static final Member NEW_MEMBER = Member.builder()
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .deleted(FALSE)
            .active(TRUE)
            .version(1)
            .createdBy("admin")
            .createdAt(MOCK_TIME)
            .build();
    private static final Member NEW_MEMBER_SAVED = Member.builder()
            .id("1")
            .memberId(1L)
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .deleted(FALSE)
            .active(TRUE)
            .version(1)
            .createdBy("admin")
            .createdAt(MOCK_TIME)
            .build();
    private static final Member MEMBER = Member.builder()
            .id("1")
            .memberId(1L)
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .deleted(FALSE)
            .active(TRUE)
            .version(1)
            .createdBy("admin")
            .createdAt(MOCK_TIME)
            .build();
    private static final Member MEMBER_2 = Member.builder()
            .id("2")
            .memberId(2L)
            .firstName("Maven")
            .middleName("")
            .lastName("Maranan")
            .suffix("")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .deleted(FALSE)
            .active(TRUE)
            .version(1)
            .createdBy("admin")
            .createdAt(MOCK_TIME)
            .build();
    private static final Member MEMBER_UPDATED = Member.builder()
            .id("1")
            .memberId(1L)
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .deleted(FALSE)
            .active(TRUE)
            .version(2)
            .createdBy("admin")
            .createdAt(MOCK_TIME)
            .build();
    private static final Member MEMBER_DELETED = Member.builder()
            .id("1")
            .memberId(1L)
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .deleted(TRUE)
            .active(TRUE)
            .version(1)
            .createdBy("admin")
            .createdAt(MOCK_TIME)
            .updatedBy("admin")
            .updatedAt(MOCK_TIME)
            .build();
    private static final MemberDto MEMBER_DTO = MemberDto.builder()
            .id("1")
            .memberId(1L)
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .deleted(FALSE)
            .active(TRUE)
            .version(1)
            .build();
    private static final MemberDto MEMBER_DTO_2 = MemberDto.builder()
            .id("2")
            .memberId(2L)
            .firstName("Maven")
            .middleName("")
            .lastName("Maranan")
            .suffix("")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .deleted(FALSE)
            .active(TRUE)
            .version(1)
            .build();
    private static final MemberCount EXISTING_MEMBER_COUNT = MemberCount.builder()
            .name(MEMBER_COUNT_LOOKUP_NAME)
            .active(1L)
            .total(1L)
            .build();
    private static final MemberCount EXISTING_MEMBER_COUNT_2 = MemberCount.builder()
            .name(MEMBER_COUNT_LOOKUP_NAME)
            .active(2L)
            .total(3L)
            .build();
    private static final MemberCount NEW_MEMBER_COUNT = MemberCount.builder()
            .name(MEMBER_COUNT_LOOKUP_NAME)
            .active(1L)
            .total(2L)
            .build();

    @InjectMocks
    private MemberService memberService;
    @Mock
    private MemberRepository memberRepository;
    @Mock
    private MemberCountRepository memberCountRepository;
    @Mock
    private ConversionService conversionService;
    @Mock
    private MemberDtoConverter memberDtoConverter;
    @Mock
    private DateService dateService;
    @Mock
    private CacheService cacheService;
    @Mock
    private Page<Member> memberPage;

    @Before
    public void setUp() {
        when(dateService.getCurrentTime()).thenReturn(MOCK_TIME);
    }

    @Test
    public void givenMemberInput_whenSave_thenVerifyMemberSave() {
        when(conversionService.convert(NEW_MEMBER_INPUT, Member.class)).thenReturn(NEW_MEMBER);
        when(memberRepository.save(NEW_MEMBER)).thenReturn(NEW_MEMBER_SAVED);

        Member member = memberService.saveMember(NEW_MEMBER_INPUT);

        assertEquals(NEW_MEMBER_SAVED, member);
        verify(conversionService).convert(any(MemberInput.class), eq(Member.class));
        verify(memberRepository).save(any(Member.class));
        verify(cacheService).evictCache(MEMBERS);
    }

    @Test
    public void givenMemberUpdate_whenSave_thenVerifyMemberSave() {
        when(conversionService.convert(MEMBER_UPDATE, Member.class)).thenReturn(MEMBER_UPDATED);
        when(memberRepository.save(MEMBER_UPDATED)).thenReturn(MEMBER_UPDATED);
        when(memberCountRepository.findMemberCount()).thenReturn(EXISTING_MEMBER_COUNT);
        when(memberRepository.findByMemberIdAndDeleted(anyLong(), anyBoolean())).thenReturn(of(MEMBER_UPDATED));

        memberService.updateMember(MEMBER_UPDATE);

        verify(conversionService).convert(any(MemberUpdate.class), eq(Member.class));
        verify(memberRepository).save(any(Member.class));
        verify(memberCountRepository).findMemberCount();
        verify(memberCountRepository).save(NEW_MEMBER_COUNT.withActive(0L).withTotal(1L));
        verify(memberRepository).findByMemberIdAndDeleted(anyLong(), anyBoolean());
        verify(cacheService).evictCache(MEMBERS);
        verify(cacheService).evictCache(Caches.MEMBER, "1");
    }

    @Test
    public void givenMemberDelete_whenDelete_thenVerifyMemberDelete() {
        when(memberRepository.findById("1")).thenReturn(of(MEMBER));
        when(memberCountRepository.findMemberCount()).thenReturn(EXISTING_MEMBER_COUNT_2);

        memberService.deleteMember("1", "admin");

        verify(memberRepository).findById(anyString());
        verify(memberRepository).save(MEMBER_DELETED);
        verify(memberCountRepository).findMemberCount();
        verify(memberCountRepository).save(NEW_MEMBER_COUNT);
        verify(dateService).getCurrentTime();
        verify(cacheService).evictCache(MEMBERS);
        verify(cacheService).evictCache(Caches.MEMBER, "1");
    }

    @Test
    public void givenExistingMember_whenGet_theShouldReturnMemberDto() {
        when(memberRepository.findByIdAndDeleted("1", FALSE)).thenReturn(of(MEMBER));
        when(conversionService.convert(MEMBER, MemberDto.class)).thenReturn(MEMBER_DTO);

        Optional<MemberDto> memberDto = memberService.getMember("1");

        assertEquals(MEMBER_DTO, memberDto.get());

        verify(memberRepository).findByIdAndDeleted(anyString(), anyBoolean());
        verify(conversionService).convert(any(Member.class), eq(MemberDto.class));
    }

    @Test
    public void givenExistingMember_whenGetByMemberId_theShouldReturnMemberDto() {
        when(memberRepository.findByMemberIdAndDeleted(1L, FALSE)).thenReturn(of(MEMBER));
        when(conversionService.convert(MEMBER, MemberDto.class)).thenReturn(MEMBER_DTO);

        Optional<MemberDto> memberDto = memberService.getMemberByMemberId(1L);

        assertEquals(MEMBER_DTO, memberDto.get());

        verify(memberRepository).findByMemberIdAndDeleted(anyLong(), anyBoolean());
        verify(conversionService).convert(any(Member.class), eq(MemberDto.class));
    }

    @Test
    public void givenExistingMembers_whenGetMemberByPageAscending_theShouldReturnMemberPage() {
        when(memberRepository.findByDeleted(FALSE, PageRequest.of(0, 10, Sort.Direction.ASC, "lastName")))
                .thenReturn(memberPage);
        when(memberPage.hasContent()).thenReturn(TRUE);
        when(memberPage.getContent()).thenReturn(asList(MEMBER, MEMBER_2));
        when(memberDtoConverter.convert(MEMBER)).thenReturn(MEMBER_DTO);
        when(memberDtoConverter.convert(MEMBER_2)).thenReturn(MEMBER_DTO_2);

        List<MemberDto> memberDtos = memberService.getMembers(0, 10, "", "", TRUE, "");

        assertEquals(asList(MEMBER_DTO, MEMBER_DTO_2), memberDtos);

        verify(memberRepository).findByDeleted(anyBoolean(), any(PageRequest.class));
        verify(memberPage).hasContent();
        verify(memberPage).getContent();
        verify(memberDtoConverter, times(2)).convert(any(Member.class));
    }

    @Test
    public void givenExistingMembers_whenGetMemberByPageDescending_theShouldReturnMemberPage() {
        when(memberRepository.findByDeleted(FALSE, PageRequest.of(0, 10, Sort.Direction.DESC, "lastName")))
                .thenReturn(memberPage);
        when(memberPage.hasContent()).thenReturn(TRUE);
        when(memberPage.getContent()).thenReturn(asList(MEMBER_2, MEMBER));
        when(memberDtoConverter.convert(MEMBER)).thenReturn(MEMBER_DTO);
        when(memberDtoConverter.convert(MEMBER_2)).thenReturn(MEMBER_DTO_2);

        List<MemberDto> memberDtos = memberService.getMembers(0, 10, "", "", FALSE, "");

        assertEquals(asList(MEMBER_DTO_2, MEMBER_DTO), memberDtos);

        verify(memberRepository).findByDeleted(anyBoolean(), any(PageRequest.class));
        verify(memberPage).hasContent();
        verify(memberPage).getContent();
        verify(memberDtoConverter, times(2)).convert(any(Member.class));
    }

    @Test
    public void givenExistingMembers_whenGetMemberByPageWithOneWordKeyword_theShouldReturnMemberPage() {
        when(memberRepository.findByFirstNameOrLastName("Maranan", "Maranan", PageRequest.of(0, 10, Sort.Direction.ASC, "lastName")))
                .thenReturn(memberPage);
        when(memberPage.hasContent()).thenReturn(TRUE);
        when(memberPage.getContent()).thenReturn(asList(MEMBER, MEMBER_2));
        when(memberDtoConverter.convert(MEMBER)).thenReturn(MEMBER_DTO);
        when(memberDtoConverter.convert(MEMBER_2)).thenReturn(MEMBER_DTO_2);

        List<MemberDto> memberDtos = memberService.getMembers(0, 10, "Maranan", "", TRUE, "");

        assertEquals(asList(MEMBER_DTO, MEMBER_DTO_2), memberDtos);

        verify(memberRepository).findByFirstNameOrLastName(anyString(), anyString(), any(PageRequest.class));
        verify(memberPage).hasContent();
        verify(memberPage).getContent();
        verify(memberDtoConverter, times(2)).convert(any(Member.class));
    }

    @Test
    public void givenExistingMembers_whenGetMemberByPageWithTwoWordKeyword_theShouldReturnMemberPage() {
        when(memberRepository.findByFirstNameOrLastName("Romeo", "Maranan", PageRequest.of(0, 10, Sort.Direction.ASC, "lastName")))
                .thenReturn(memberPage);
        when(memberPage.hasContent()).thenReturn(TRUE);
        when(memberPage.getContent()).thenReturn(singletonList(MEMBER));
        when(memberDtoConverter.convert(MEMBER)).thenReturn(MEMBER_DTO);

        List<MemberDto> memberDtos = memberService.getMembers(0, 10, "Romeo Maranan", "", TRUE, "");

        assertEquals(singletonList(MEMBER_DTO), memberDtos);

        verify(memberRepository).findByFirstNameOrLastName(anyString(), anyString(), any(PageRequest.class));
        verify(memberPage).hasContent();
        verify(memberPage).getContent();
        verify(memberDtoConverter).convert(any(Member.class));
    }

    @Test
    public void givenExistingMembers_whenGetMemberByPageWithTwoWordKeywordCommaSeparated_thenShouldReturnMemberPage() {
        when(memberRepository.findByFirstNameOrLastName("Maranan", "Romeo", PageRequest.of(0, 10, Sort.Direction.ASC, "lastName")))
                .thenReturn(memberPage);
        when(memberPage.hasContent()).thenReturn(TRUE);
        when(memberPage.getContent()).thenReturn(singletonList(MEMBER));
        when(memberDtoConverter.convert(MEMBER)).thenReturn(MEMBER_DTO);

        List<MemberDto> memberDtos = memberService.getMembers(0, 10, "Maranan, Romeo", "", TRUE, "");

        assertEquals(singletonList(MEMBER_DTO), memberDtos);

        verify(memberRepository).findByFirstNameOrLastName(anyString(), anyString(), any(PageRequest.class));
        verify(memberPage).hasContent();
        verify(memberPage).getContent();
        verify(memberDtoConverter).convert(any(Member.class));
    }

    @Test
    public void givenExistingMembers_whenGetMemberByPageWithoutKeyword_thenShouldReturnMemberPage() {
        when(memberRepository.findByDeletedAndActive(FALSE, TRUE, PageRequest.of(0, 10, Sort.Direction.ASC, "lastName")))
                .thenReturn(memberPage);
        when(memberPage.hasContent()).thenReturn(TRUE);
        when(memberPage.getContent()).thenReturn(singletonList(MEMBER));
        when(memberDtoConverter.convert(MEMBER)).thenReturn(MEMBER_DTO);

        List<MemberDto> memberDtos = memberService.getMembers(0, 10, null, "lastName", TRUE, "activeOnly");

        assertEquals(singletonList(MEMBER_DTO), memberDtos);

        verify(memberRepository).findByDeletedAndActive(anyBoolean(), anyBoolean(), any(PageRequest.class));
        verify(memberPage).hasContent();
        verify(memberPage).getContent();
        verify(memberDtoConverter).convert(any(Member.class));
    }

    @Test
    public void givenExistingMembers_whenGetMemberByPageWithKeyword_thenShouldReturnMemberPage() {
        when(memberRepository.findByFirstNameOrLastNameAndActive("Maranan", "Romeo", TRUE, PageRequest.of(0, 10, Sort.Direction.ASC, "lastName")))
                .thenReturn(memberPage);
        when(memberPage.hasContent()).thenReturn(TRUE);
        when(memberPage.getContent()).thenReturn(singletonList(MEMBER));
        when(memberDtoConverter.convert(MEMBER)).thenReturn(MEMBER_DTO);

        List<MemberDto> memberDtos = memberService.getMembers(0, 10, "Maranan, Romeo", "", TRUE, "activeOnly");

        assertEquals(singletonList(MEMBER_DTO), memberDtos);

        verify(memberRepository).findByFirstNameOrLastNameAndActive(anyString(), anyString(), anyBoolean(), any(PageRequest.class));
        verify(memberPage).hasContent();
        verify(memberPage).getContent();
        verify(memberDtoConverter).convert(any(Member.class));
    }

    @Test
    public void givenExistingMembers_whenGetMembersByFirstNameAndLastName_thenShouldReturnMemberList() {
        when(memberRepository.findByFirstNameAndLastName("Romeo", "Maranan", new Sort(Sort.Direction.ASC, "firstName")))
                .thenReturn(asList(MEMBER, MEMBER_2));
        when(conversionService.convert(MEMBER, MemberDto.class)).thenReturn(MEMBER_DTO);
        when(conversionService.convert(MEMBER_2, MemberDto.class)).thenReturn(MEMBER_DTO_2);

        List<MemberDto> memberDtos = memberService.getMembers("Romeo", "Maranan");

        assertEquals(asList(MEMBER_DTO, MEMBER_DTO_2), memberDtos);
        verify(memberRepository).findByFirstNameAndLastName(anyString(), anyString(), any(Sort.class));
        verify(conversionService, times(2)).convert(any(Member.class), eq(MemberDto.class));
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(conversionService, memberRepository, memberCountRepository, dateService, cacheService, memberPage);
    }

}
