package com.ucm.ucmamm.member;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;

import static com.ucm.ucmamm.domain.MemberCountRepository.MEMBER_COUNT_LOOKUP_NAME;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MemberConverterTest {

    private static final Instant MOCK_TIME = Instant.parse("2018-01-01T00:00:00.000Z");

    private static final MemberInput NON_EXISTING_MEMBER_INPUT = MemberInput.builder()
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .operator("admin")
            .build();
    private static final Member NON_EXISTING_MEMBER = Member.builder()
            .memberId(1L)
            .firstName("Romeo")
            .middleName("")
            .lastName("Maranan")
            .suffix("Jr.")
            .phoneNumbers(singletonList("02 999 9999"))
            .emailAddresses(singletonList("rmaranan@test.com"))
            .streetAddress("123 J. P. Rizal St.")
            .village("Barangay X")
            .city("Batangas City")
            .province("Batangas")
            .zipCode("1234")
            .deleted(Boolean.FALSE)
            .active(Boolean.TRUE)
            .version(1)
            .createdBy("admin")
            .createdAt(MOCK_TIME)
            .build();
    private static final MemberID EXISTING_MEMBER_ID = MemberID.builder().id("1").name(MemberIDRepository.LOOKUP_NAME).nextValue(1L).build();
    private static final MemberID NEXT_MEMBER_ID = MemberID.builder().id("1").name(MemberIDRepository.LOOKUP_NAME).nextValue(2L).build();
    private static final MemberCount EXISTING_MEMBER_COUNT = MemberCount.builder().id("1").name(MEMBER_COUNT_LOOKUP_NAME).active(1L).total(1L).build();
    private static final MemberCount NEXT_MEMBER_COUNT = MemberCount.builder().id("1").name(MEMBER_COUNT_LOOKUP_NAME).active(2L).total(2L).build();

    @InjectMocks
    private MemberConverter memberConverter;
    @Mock
    private MemberRepository memberRepository;
    @Mock
    private MemberIDRepository memberIDRepository;
    @Mock
    private MemberCountRepository memberCountRepository;
    @Mock
    private DateService dateService;

    @Before
    public void setUp() {
        when(dateService.getCurrentTime()).thenReturn(MOCK_TIME);
    }

    @Test
    public void givenNonExistingMember_whenConvert_thenShouldReturnCorrectResult() {
        when(memberIDRepository.findNextMemberID()).thenReturn(EXISTING_MEMBER_ID);
        when(memberCountRepository.findMemberCount()).thenReturn(EXISTING_MEMBER_COUNT);

        Member actual = memberConverter.convert(NON_EXISTING_MEMBER_INPUT);

        assertEquals(NON_EXISTING_MEMBER, actual);

        verify(memberRepository, never()).findByIdAndDeleted(anyString(), anyBoolean());
        verify(memberIDRepository).findNextMemberID();
        verify(memberCountRepository).findMemberCount();
        verify(memberIDRepository).save(NEXT_MEMBER_ID);
        verify(memberCountRepository).save(NEXT_MEMBER_COUNT);
        verify(dateService).getCurrentTime();
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(memberRepository, dateService, memberIDRepository);
    }

}
