package com.ucm.ucmamm.member;

import com.ucm.ucmamm.domain.Member;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

@RunWith(BlockJUnit4ClassRunner.class)
public class MemberDtoConverterTest {

    private MemberDtoConverter memberDtoConverter = new MemberDtoConverter();

    @Test
    public void givenMemberFromRepository_whenConvert_thenShouldReturnMemberDto() {
        Member member = Member.builder()
                .memberId(1L)
                .firstName("Romeo")
                .middleName("")
                .lastName("Maranan")
                .suffix("Jr.")
                .phoneNumbers(singletonList("02 999 9999"))
                .emailAddresses(singletonList("rmaranan@test.com"))
                .streetAddress("123 J. P. Rizal St.")
                .village("Barangay X")
                .city("Batangas City")
                .province("Batangas")
                .zipCode("1234")
                .deleted(Boolean.FALSE)
                .active(Boolean.TRUE)
                .version(1)
                .build();
        MemberDto memberDto = MemberDto.builder()
                .memberId(1L)
                .firstName("Romeo")
                .middleName("")
                .lastName("Maranan")
                .suffix("Jr.")
                .phoneNumbers(singletonList("02 999 9999"))
                .emailAddresses(singletonList("rmaranan@test.com"))
                .streetAddress("123 J. P. Rizal St.")
                .village("Barangay X")
                .city("Batangas City")
                .province("Batangas")
                .zipCode("1234")
                .deleted(Boolean.FALSE)
                .active(Boolean.TRUE)
                .version(1)
                .build();

        MemberDto actual = memberDtoConverter.convert(member);

        assertEquals(memberDto, actual);
    }

}
