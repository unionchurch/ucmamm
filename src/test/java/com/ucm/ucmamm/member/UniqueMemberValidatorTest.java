package com.ucm.ucmamm.member;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UniqueMemberValidatorTest {

    @InjectMocks
    private UniqueMemberIdValidator uniqueMemberIdValidator;
    @Mock
    private MemberService memberService;

    @Test
    public void givenExistingMemberId_whenValidate_thenShouldReturnFalse() {
        when(memberService.getMemberByMemberId(anyLong())).thenReturn(of(MemberDto.builder().build()));

        assertFalse(uniqueMemberIdValidator.isValid(1L, any(ConstraintValidatorContext.class)));

        verify(memberService).getMemberByMemberId(anyLong());
    }

    @Test
    public void givenNonExistingMemberId_whenValidate_thenShouldReturnTrue() {
        when(memberService.getMemberByMemberId(anyLong())).thenReturn(empty());

        assertTrue(uniqueMemberIdValidator.isValid(1L, any(ConstraintValidatorContext.class)));

        verify(memberService).getMemberByMemberId(anyLong());
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(memberService);
    }

}
