package com.ucm.ucmamm.common;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

import static com.ucm.ucmamm.common.CollectionUtils.defaultToEmptyIfNull;
import static com.ucm.ucmamm.common.CollectionUtils.defaultToEmptyMapIfNull;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(BlockJUnit4ClassRunner.class)
public class CollectionUtilsTest {

    @Test
    public void givenNullList_whenCheck_shouldReturnEmptyStream() {
        Stream<String> stream = defaultToEmptyIfNull(null);
        assertTrue(stream.noneMatch(Objects::nonNull));
    }

    @Test
    public void givenEmptyList_whenCheck_shouldReturnEmptyStream() {
        Stream<String> stream = defaultToEmptyIfNull(emptyList());
        assertTrue(stream.noneMatch(Objects::nonNull));
    }

    @Test
    public void givenNonEmptyList_whenCheck_shouldReturnNonEmptyStream() {
        Stream<String> stream = defaultToEmptyIfNull(Arrays.asList("1", "2", "3"));
        assertTrue(stream.allMatch(Objects::nonNull));
    }

    @Test
    public void givenNullMap_whenCheck_shouldReturnEmptyMap() {
        Map<String, String> map = defaultToEmptyMapIfNull(null);
        assertTrue(map.isEmpty());
    }

    @Test
    public void givenNonNullMap_whenCheck_shouldReturnEmptyMap() {
        Map<String, String> originalMap = new HashMap<>();
        originalMap.put("1", "2");
        Map<String, String> map = defaultToEmptyMapIfNull(originalMap);
        assertFalse(map.isEmpty());
    }

}
