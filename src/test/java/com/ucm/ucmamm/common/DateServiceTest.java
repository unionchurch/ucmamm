package com.ucm.ucmamm.common;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Slf4j
@RunWith(BlockJUnit4ClassRunner.class)
public class DateServiceTest {

    private DateService dateService = new DateService();

    @Test
    public void shouldReturnCurrentInstant() {
        Instant currentTime = dateService.getCurrentTime();
        log.info("Current Time is {}", currentTime);
        assertNotNull(currentTime);
    }

    @Test
    public void shouldReturnCurrentLocalDateTime() {
        LocalDateTime currentDateTime = dateService.getCurrentDateTime();
        log.info("Current Time is {}", currentDateTime);
        assertNotNull(currentDateTime);
    }

    @Test
    public void shouldReturnMergedDateAndTime() {
        LocalDate date = LocalDate.parse("2019-01-02");
        LocalTime time = LocalTime.parse("13:30:00");

        LocalDateTime dateTime = dateService.mergeDateAndTime(date, time);

        assertEquals(LocalDateTime.parse("2019-01-02T13:30:00"), dateTime);
    }

}
