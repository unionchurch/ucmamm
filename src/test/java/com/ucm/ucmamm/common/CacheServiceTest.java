package com.ucm.ucmamm.common;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

import java.util.List;
import java.util.Optional;

import static com.ucm.ucmamm.common.Caches.USER;
import static com.ucm.ucmamm.common.Caches.USERS;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CacheServiceTest {

    @InjectMocks
    private CacheService cacheService;
    @Mock
    private CacheManager cacheManager;
    @Mock
    private Cache cache;

    @Test
    public void givenExistingCacheWithCachedObject_whenGetObject_thenShouldReturnCachedObject() {
        when(cacheManager.getCache(anyString())).thenReturn(cache);
        when(cache.get(anyString(), eq(String.class))).thenReturn("Hello World!");

        Optional<String> value = cacheService.getCachedObject("greetings", "cached_object", String.class);

        assertTrue(value.isPresent());
        assertEquals("Hello World!", value.get());

        verify(cacheManager).getCache("greetings");
        verify(cache).get("cached_object", String.class);
    }

    @Test
    public void givenExistingCache_whenEvictAll_thenShouldInvokeRequiredMethods() {
        when(cacheManager.getCache(anyString())).thenReturn(cache);

        cacheService.evictCache(USERS);

        verify(cacheManager).getCache(USERS);
        verify(cache).clear();
    }

    @Test
    public void givenNonExistingCache_whenEvictAll_thenShouldInvokeRequiredMethods() {
        when(cacheManager.getCache(anyString())).thenReturn(null);

        cacheService.evictCache(USERS);

        verify(cacheManager).getCache(USERS);
        verify(cache, never()).clear();
    }

    @Test
    public void givenExistingCache_whenEvictCacheKey_thenShouldInvokeRequiredMethods() {
        when(cacheManager.getCache(anyString())).thenReturn(cache);

        cacheService.evictCache(USER, "1");

        verify(cacheManager).getCache(USER);
        verify(cache).evict("1");
    }

    @Test
    public void givenNonExistingCache_whenEvictCacheKey_thenShouldInvokeRequiredMethods() {
        when(cacheManager.getCache(anyString())).thenReturn(null);

        cacheService.evictCache(USER, "1");

        verify(cacheManager).getCache(USER);
        verify(cache, never()).evict("1");
    }

    @Test
    public void givenConfiguredCache_whenGetCacheNames_thenShouldReturnConfiguredCacheNames() {
        when(cacheManager.getCacheNames()).thenReturn(asList("users", "members", "elections"));

        List<String> cacheNames = cacheService.getCacheNames();

        assertEquals(asList("users", "members", "elections"), cacheNames);
        verify(cacheManager).getCacheNames();
    }

    @Test
    public void givenConfiguredCache_whenEvictAll_thenShouldInvokeRequiredMethods() {
        when(cacheManager.getCacheNames()).thenReturn(asList("users", "members", "elections"));
        when(cacheManager.getCache(anyString())).thenReturn(cache);

        cacheService.evictAllCache();

        verify(cacheManager).getCacheNames();
        verify(cacheManager, times(3)).getCache(anyString());
        verify(cache, times(3)).clear();
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(cacheManager, cache);
    }

}
