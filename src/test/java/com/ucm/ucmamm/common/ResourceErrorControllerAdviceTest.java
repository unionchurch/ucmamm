package com.ucm.ucmamm.common;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;

import java.util.Locale;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ResourceErrorControllerAdviceTest {

    @InjectMocks
    private ResourceErrorControllerAdvice resourceErrorControllerAdvice;

    @Mock
    private MessageSource messageSource;

    @Mock
    private MethodArgumentNotValidException methodArgumentNotValidException;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private HttpHeaders httpHeaders;

    @Mock
    private WebRequest webRequest;

    @Test
    public void givenMethodArgumentNotValidException_whenHandled_thenReturnCommonResourceErrorsFormat() {
        when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);
        when(bindingResult.getFieldErrors())
                .thenReturn(singletonList(new FieldError("user", "userName", "already exists")));
        when(messageSource.getMessage(any(MessageSourceResolvable.class), any(Locale.class)))
                .thenReturn("already exists");

        ResponseEntity<Object> responseEntity = resourceErrorControllerAdvice.handleMethodArgumentNotValid(methodArgumentNotValidException, httpHeaders, HttpStatus.BAD_REQUEST, webRequest);

        assertEquals(ResponseEntity.badRequest()
                .body(ResourceErrors.builder()
                        .error(ResourceError.builder()
                                .field("userName")
                                .name("User Name")
                                .message("already exists")
                                .build())
                        .build()), responseEntity);

        verify(methodArgumentNotValidException).getBindingResult();
        verify(bindingResult).getFieldErrors();
        verify(messageSource).getMessage(any(MessageSourceResolvable.class), any(Locale.class));
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(methodArgumentNotValidException, bindingResult, messageSource);
    }

}
