package com.ucm.ucmamm.common;

import com.ucm.ucmamm.AuthServerConfiguration;
import com.ucm.ucmamm.ResourceServerConfiguration;
import com.ucm.ucmamm.UcmAmmOauth2Properties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = CacheResourceTest.Main.class)
public class CacheResourceTest extends ResourceTestSupport {

    @SpringBootApplication(scanBasePackages = {"com.ucm.ucmamm.common"}, exclude = MongoAutoConfiguration.class)
    @Import({UcmAmmOauth2Properties.class, ResourceServerConfiguration.class, AuthServerConfiguration.class, ResourceTestSupport.SecurityConfiguration.class})
    public static class Main {

        public static void main(String[] args) {
            SpringApplication.run(CacheResourceTest.Main.class, args);
        }

    }

    @Override
    protected void initializeMocks() {
        when(cacheService.getCacheNames()).thenReturn(asList("users", "members", "elections"));
    }

    @Test
    public void givenConfiguredCache_whenGetCacheName_thenShouldReturnConfiguredCacheNames() throws Exception {
        mockMvc.perform(get("/api/v1/cache/names")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0]", is("users")))
                .andExpect(jsonPath("$[1]", is("members")))
                .andExpect(jsonPath("$[2]", is("elections")));
    }

    @Test
    public void givenConfiguredCache_whenDeleteCache_thenShouldReturnNoContent() throws Exception {
        mockMvc.perform(delete("/api/v1/cache")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "users"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void givenConfiguredCache_whenDeleteAllCache_thenShouldReturnNoContent() throws Exception {
        mockMvc.perform(delete("/api/v1/cache/all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

}
