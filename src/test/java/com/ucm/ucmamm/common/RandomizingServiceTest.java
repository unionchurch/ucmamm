package com.ucm.ucmamm.common;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(BlockJUnit4ClassRunner.class)
public class RandomizingServiceTest {

    private RandomizingService randomizingService = new RandomizingService();

    @Test
    public void shouldReturnStringWithLength8() {
        String randomString = randomizingService.getRandomString();
        assertEquals(8, randomString.length());
    }

}
