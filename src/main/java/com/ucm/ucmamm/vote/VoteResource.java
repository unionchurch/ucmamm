package com.ucm.ucmamm.vote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class VoteResource {

    @Autowired
    private VoteService voteService;

    @PostMapping("/api/v1/votes")
    public ResponseEntity<?> vote(@Valid @RequestBody VoteInput voteInput) {
        voteService.saveVote(voteInput);
        return ResponseEntity.noContent().build();
    }

}
