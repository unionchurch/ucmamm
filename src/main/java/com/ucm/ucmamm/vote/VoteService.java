package com.ucm.ucmamm.vote;

import com.ucm.ucmamm.common.CacheService;
import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static com.ucm.ucmamm.common.Caches.ACTIVE_ELECTION;
import static com.ucm.ucmamm.common.Caches.ELECTION;
import static com.ucm.ucmamm.common.CollectionUtils.defaultToEmptyIfNull;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.BooleanUtils.isFalse;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.springframework.data.domain.PageRequest.of;
import static org.springframework.data.domain.Sort.Direction.DESC;

@Slf4j
@Service
@AllArgsConstructor
public class VoteService {

    private static final int MAX_VOTES_TO_PROCESS = 10;
    private static final String SORT_VOTES_BY = "time";
    private static final String SORT_ELECTION_BY = "date";

    private final VoteRepository voteRepository;
    private final ElectionRepository electionRepository;
    private final ConversionService conversionService;
    private final DateService dateService;
    private final CacheService cacheService;

    Vote saveVote(VoteInput voteInput) {
        return voteRepository.save(conversionService.convert(voteInput, Vote.class));
    }

    @Scheduled(cron = "*/5 * * * * *")
    public void tallyVotes() {
        log.debug("Running thread for tallying of votes...");
        Optional<Election> election = getActiveElection();
        if (election.isPresent()) {
            Page<Vote> votes = voteRepository.findByTalliedAndDeleted(FALSE, FALSE, of(0, MAX_VOTES_TO_PROCESS, DESC, SORT_VOTES_BY));
            if (votes.hasContent()) {
                Election electionToUpdate = election.get();
                for (Vote vote : votes.getContent()) {
                    electionToUpdate = electionRepository.save(electionToUpdate
                            .withProposals(tallyProposalApprovals(vote.getProposals(), defaultToEmptyIfNull(electionToUpdate.getProposals())))
                            .withQuestions(tallyAnswerSelections(vote.getQuestions(), defaultToEmptyIfNull(electionToUpdate.getQuestions())))
                            .withSeats(tallySeatVotes(vote.getSeats(), defaultToEmptyIfNull(electionToUpdate.getSeats())))
                            .withProxy(electionToUpdate.getProxy())
                            .withProxyVoted(ofNullable(electionToUpdate.getProxyVoted()).orElse(0L) + (isTrue(vote.getProxy()) ? 1 : 0))
                            .withVoted(ofNullable(electionToUpdate.getVoted()).orElse(0L) + 1)
                            .withVersion(electionToUpdate.getVersion() + 1)
                            .withUpdatedAt(dateService.getCurrentTime())
                            .withUpdatedBy("SYSTEM"));
                    voteRepository.save(vote.withTallied(TRUE));
                }
                cacheService.evictCache(ELECTION, ACTIVE_ELECTION.replaceAll("'", ""));
                cacheService.evictCache(ELECTION, electionToUpdate.getId());
                log.info("Votes tallied: {}", votes.getTotalElements());
            } else {
                log.debug("No votes need to tally.");
            }
        } else {
            log.debug("No active election was found.");
        }
    }

    private List<Proposal> tallyProposalApprovals(Map<String, Boolean> proposalApprovals, Stream<Proposal> proposals) {
        return proposals.map(proposal -> {
            Boolean approval = proposalApprovals.get(proposal.getId());
            if (isTrue(approval)) {
                return Proposal.builder()
                        .id(proposal.getId())
                        .content(proposal.getContent())
                        .approved(proposal.getApproved() + 1)
                        .disapproved(proposal.getDisapproved())
                        .build();
            } else if (isFalse(approval)) {
                return Proposal.builder()
                        .id(proposal.getId())
                        .content(proposal.getContent())
                        .approved(proposal.getApproved())
                        .disapproved(proposal.getDisapproved() + 1)
                        .build();
            }
            return proposal;
        }).collect(toList());
    }

    private List<Question> tallyAnswerSelections(Map<String, String> answerSelections, Stream<Question> questions) {
        return questions.map(question -> {
            String answerId = answerSelections.get(question.getId());
            if (answerId != null) {
                return question.withAnswers(question.getAnswers().stream().map(answer -> {
                    if (answerId.equalsIgnoreCase(answer.getId())) {
                        return Answer.builder()
                                .id(answer.getId())
                                .content(answer.getContent())
                                .selected(answer.getSelected() + 1)
                                .build();
                    }
                    return answer;
                }).collect(toList()));
            }
            return question;
        }).collect(toList());
    }

    private List<Seat> tallySeatVotes(Map<String, List<SeatVote>> seatVotes, Stream<Seat> seats) {
        return seats.map(seat -> {
            List<SeatVote> seatVoteList = seatVotes.get(seat.getId());
            if (seatVoteList != null) {
                Map<String, Boolean> candidateVotes = seatVoteList.stream()
                        .filter(Objects::nonNull)
                        .filter(seatVote -> Objects.nonNull(seatVote.getCandidateId()))
                        .collect(toMap(SeatVote::getCandidateId, seatVote -> BooleanUtils.isTrue(seatVote.getApproved())));
                return seat.withCandidates(seat.getCandidates().stream().map(candidate -> {
                    Boolean approval = candidateVotes.get(candidate.getId());
                    if (isTrue(approval)) {
                        return Candidate.builder()
                                .id(candidate.getId())
                                .memberId(candidate.getMemberId())
                                .firstName(candidate.getFirstName())
                                .middleName(candidate.getMiddleName())
                                .lastName(candidate.getLastName())
                                .suffix(candidate.getSuffix())
                                .approved(candidate.getApproved() + 1)
                                .disapproved(candidate.getDisapproved())
                                .build();
                    } else if (isFalse(approval)) {
                        return Candidate.builder()
                                .id(candidate.getId())
                                .memberId(candidate.getMemberId())
                                .firstName(candidate.getFirstName())
                                .middleName(candidate.getMiddleName())
                                .lastName(candidate.getLastName())
                                .suffix(candidate.getSuffix())
                                .approved(candidate.getApproved())
                                .disapproved(candidate.getDisapproved() + 1)
                                .build();
                    }
                    return candidate;
                }).collect(toList()));
            }
            return seat;
        }).collect(toList());
    }

    private Optional<Election> getActiveElection() {
        return electionRepository.findByActiveAndDeleted(TRUE, FALSE, Sort.by(DESC, SORT_ELECTION_BY)).stream().findFirst();
    }

}
