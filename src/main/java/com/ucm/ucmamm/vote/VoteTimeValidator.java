package com.ucm.ucmamm.vote;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.ElectionRepository;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import java.time.LocalDateTime;

import static java.lang.Boolean.FALSE;

@Component
public class VoteTimeValidator implements ConstraintValidator<ValidVoteTime, String> {

    @Autowired
    private ElectionRepository electionRepository;
    @Autowired
    private DateService dateService;
    @Value("${vote.timeValidator.enabled:false}")
    private boolean enabled;

    @Override
    public boolean isValid(String id, ConstraintValidatorContext constraintValidatorContext) {
        if (!enabled) {
            return true;
        }
        LocalDateTime currentDateTime = dateService.getCurrentDateTime();
        return electionRepository.findByIdAndDeleted(id, FALSE)
                .map(election -> Pair.of(dateService.mergeDateAndTime(election.getDate(), election.getStartTime()), dateService.mergeDateAndTime(election.getDate(), election.getEndTime())))
                .filter(pair -> currentDateTime.isAfter(pair.getLeft()))
                .filter(pair -> currentDateTime.isBefore(pair.getRight()))
                .isPresent();
    }

}
