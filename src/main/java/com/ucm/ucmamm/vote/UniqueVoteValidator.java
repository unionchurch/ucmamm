package com.ucm.ucmamm.vote;

import com.ucm.ucmamm.domain.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueVoteValidator implements ConstraintValidator<UniqueVote, MemberIdAndElectionId> {

    @Autowired
    private VoteRepository voteRepository;

    @Override
    public boolean isValid(MemberIdAndElectionId memberIdAndElectionId, ConstraintValidatorContext constraintValidatorContext) {
        return !voteRepository.findByMemberIdAndElectionId(memberIdAndElectionId.getMemberId(), memberIdAndElectionId.getElectionId()).isPresent();
    }

}
