package com.ucm.ucmamm.vote;

import com.ucm.ucmamm.domain.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class MemberIdValidator implements ConstraintValidator<ValidMemberId, Long> {

    @Autowired
    private MemberRepository memberRepository;

    @Override
    public boolean isValid(Long memberId, ConstraintValidatorContext constraintValidatorContext) {
        return memberRepository.findByMemberIdAndDeleted(memberId, Boolean.FALSE).isPresent();
    }

}
