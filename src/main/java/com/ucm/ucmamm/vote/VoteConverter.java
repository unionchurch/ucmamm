package com.ucm.ucmamm.vote;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.Vote;
import com.ucm.ucmamm.domain.VoterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static java.lang.Boolean.FALSE;

@Component
public class VoteConverter implements Converter<VoteInput, Vote> {

    @Autowired
    private DateService dateService;
    @Autowired
    private VoterRepository voterRepository;

    @Override
    public Vote convert(VoteInput voteInput) {
        return voterRepository.findByMemberIdAndElectionId(voteInput.getMemberId(), voteInput.getElectionId())
                .map(voter -> Vote.builder()
                        .time(dateService.getCurrentTime())
                        .memberId(voteInput.getMemberId())
                        .electionId(voteInput.getElectionId())
                        .proposals(voteInput.getProposals())
                        .questions(voteInput.getQuestions())
                        .seats(voteInput.getSeats())
                        .tallied(FALSE)
                        .proxy(voter.isProxy())
                        .deleted(FALSE)
                        .build())
                .orElse(null);
    }

}
