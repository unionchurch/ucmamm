package com.ucm.ucmamm.vote;

import com.ucm.ucmamm.domain.SeatVote;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Value
@Builder
public class VoteInput {

    @NotNull
    @ValidMemberId
    Long memberId;

    @NotBlank
    @ValidVoteTime
    String electionId;

    Map<String, Boolean> proposals;

    Map<String, String> questions;

    Map<String, List<SeatVote>> seats;

    @UniqueVote
    MemberIdAndElectionId getVote() {
        return MemberIdAndElectionId.builder().memberId(memberId).electionId(electionId).build();
    }

    @ValidVoter
    MemberIdAndElectionId getVoter() {
        return MemberIdAndElectionId.builder().memberId(memberId).electionId(electionId).build();
    }

}
