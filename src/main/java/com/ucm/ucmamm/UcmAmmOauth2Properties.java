package com.ucm.ucmamm;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties("ucmamm-oauth2")
public class UcmAmmOauth2Properties {

    String clientId;

    String secret;

    List<String> authorizedGrantTypes;

    List<String> scopes;

    boolean autoApprove;

    String signingKey;

    int accessTokenValidity;

    int refreshTokenValidity;

}
