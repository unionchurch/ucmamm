package com.ucm.ucmamm.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Locale;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.of;
import static org.apache.commons.lang3.StringUtils.splitByCharacterTypeCamelCase;

@RestControllerAdvice
public class ResourceErrorControllerAdvice extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        BindingResult bindingResult = ex.getBindingResult();
        return ResponseEntity.badRequest()
                .body(ResourceErrors.builder()
                        .errors(bindingResult.getFieldErrors().stream()
                                .map(fieldError -> ResourceError.builder()
                                        .field(fieldError.getField())
                                        .name(titleCase(fieldError.getField()))
                                        .message(messageSource.getMessage(fieldError, Locale.getDefault()))
                                        .build())
                                .collect(toList()))
                        .build());
    }

    private String titleCase(String fieldName) {
        return of(splitByCharacterTypeCamelCase(fieldName))
                .map(string -> string.substring(0, 1).toUpperCase() + string.substring(1))
                .collect(joining(" "));
    }

}
