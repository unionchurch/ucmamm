package com.ucm.ucmamm.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CacheResource {

    @Autowired
    private CacheService cacheService;

    @GetMapping("/api/v1/cache/names")
    public List<String> getCacheNames() {
        return cacheService.getCacheNames();
    }

    @DeleteMapping("/api/v1/cache")
    public ResponseEntity<?> clearCache(@RequestParam("name") String cacheName) {
        cacheService.evictCache(cacheName);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/api/v1/cache/all")
    public ResponseEntity<?> clearAllCache() {
        cacheService.evictAllCache();
        return ResponseEntity.noContent().build();
    }

}
