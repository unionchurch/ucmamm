package com.ucm.ucmamm.common;

import java.util.*;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;

public final class CollectionUtils {

    protected CollectionUtils() {
        throw new UnsupportedOperationException();
    }

    public static <T> boolean isNotNullOrEmpty(List<T> list) {
        return list != null && !list.isEmpty();
    }

    public static <T> Stream<T> defaultToEmptyIfNull(List<T> list) {
        return ofNullable(list).orElse(emptyList()).stream();
    }

    public static <T> Map<String, T> defaultToEmptyMapIfNull(Map<String, T> map) {
        return ofNullable(map).orElse(Collections.emptyMap());
    }

}
