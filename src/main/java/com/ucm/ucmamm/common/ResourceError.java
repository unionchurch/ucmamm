package com.ucm.ucmamm.common;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ResourceError {

    String field;

    String name;

    String message;

}
