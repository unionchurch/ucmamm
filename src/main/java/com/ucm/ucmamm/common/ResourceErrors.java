package com.ucm.ucmamm.common;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class ResourceErrors {

    @Singular
    List<ResourceError> errors;

}
