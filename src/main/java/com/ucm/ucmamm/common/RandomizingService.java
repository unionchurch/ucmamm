package com.ucm.ucmamm.common;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service
public class RandomizingService {

    private static final int WORD_LENGTH = 8;

    public String getRandomString() {
        return getRandomString(WORD_LENGTH);
    }

    public String getRandomString(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }

}
