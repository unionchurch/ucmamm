package com.ucm.ucmamm.common;

public class Caches {

    public static final String USER = "user";

    public static final String USERS = "users";

    public static final String MEMBER = "member";

    public static final String MEMBERS = "members";

    public static final String ELECTION = "election";

    public static final String ELECTIONS = "elections";

    public static final String ACTIVE_ELECTION = "'active_election'";

    public static final String VOTER = "voter";

    public static final String VOTERS = "voters";

}
