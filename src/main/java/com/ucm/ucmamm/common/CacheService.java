package com.ucm.ucmamm.common;

import lombok.RequiredArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Service
@RequiredArgsConstructor
public class CacheService {

    private final CacheManager cacheManager;

    public <T> Optional<T> getCachedObject(String cacheName, String key, Class<T> type) {
        return ofNullable(cacheManager.getCache(cacheName)).map(cache -> cache.get(key, type));
    }

    public void evictCache(String cacheName) {
        ofNullable(cacheManager.getCache(cacheName)).ifPresent(Cache::clear);
    }

    public void evictCache(String cacheName, String key) {
        ofNullable(cacheManager.getCache(cacheName)).ifPresent(cache -> cache.evict(key));
    }

    public void evictAllCache() {
        getCacheNames().forEach(cacheName -> ofNullable(cacheManager.getCache(cacheName)).ifPresent(Cache::clear));
    }

    public List<String> getCacheNames() {
        return new ArrayList<>(cacheManager.getCacheNames());
    }

}
