package com.ucm.ucmamm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.http.HttpMethod.DELETE;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Autowired
    private DefaultTokenServices tokenService;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/**").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/api/v1/cache/**").permitAll()
                .antMatchers(GET, "/api/v1/elections/**").permitAll()
                .antMatchers(GET, "/api/v1/elections/**/voters/**").permitAll()
                .antMatchers(GET, "/api/v1/reports/**").permitAll()
                .antMatchers(GET, "/api/v1/members/**").permitAll()
                .antMatchers(GET, "/api/v1/users/**").permitAll()
                .antMatchers(POST, "/api/v1/elections/**").hasAuthority("ROLE_ELECTION_ADMIN")
                .antMatchers(POST, "/api/v1/elections/**/voters/**").hasAuthority("ROLE_VOTER_ADMIN")
                .antMatchers(POST, "/api/v1/votes").hasAuthority("ROLE_BALLOT_ENCODER")
                .antMatchers(POST, "/api/v1/reports/**").hasAuthority("ROLE_ELECTION_ADMIN")
                .antMatchers(POST, "/api/v1/members/**").hasAuthority("ROLE_MEMBER_ADMIN")
                .antMatchers(POST, "/api/v1/users/**").hasAuthority("ROLE_ADMIN")
                .antMatchers(POST, "/api/v1/changepassword").permitAll()
                .antMatchers(PUT, "/api/v1/elections/**").hasAuthority("ROLE_ELECTION_ADMIN")
                .antMatchers(PUT, "/api/v1/elections/**/voters/**").hasAuthority("ROLE_VOTER_ADMIN")
                .antMatchers(PUT, "/api/v1/votes").hasAuthority("ROLE_BALLOT_ENCODER")
                .antMatchers(PUT, "/api/v1/reports/**").hasAuthority("ROLE_ELECTION_ADMIN")
                .antMatchers(PUT, "/api/v1/members/**").hasAuthority("ROLE_MEMBER_ADMIN")
                .antMatchers(PUT, "/api/v1/users/**").hasAuthority("ROLE_ADMIN")
                .antMatchers(DELETE, "/api/v1/elections/**").hasAuthority("ROLE_ELECTION_ADMIN")
                .antMatchers(DELETE, "/api/v1/elections/**/voters/**").hasAuthority("ROLE_VOTER_ADMIN")
                .antMatchers(DELETE, "/api/v1/votes").hasAuthority("ROLE_BALLOT_ENCODER")
                .antMatchers(DELETE, "/api/v1/reports/**").hasAuthority("ROLE_ELECTION_ADMIN")
                .antMatchers(DELETE, "/api/v1/members/**").hasAuthority("ROLE_MEMBER_ADMIN")
                .antMatchers(DELETE, "/api/v1/users/**").hasAuthority("ROLE_ADMIN")
                .anyRequest().authenticated();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.tokenServices(tokenService);
    }

}
