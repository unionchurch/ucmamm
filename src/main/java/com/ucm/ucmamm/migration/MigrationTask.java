package com.ucm.ucmamm.migration;

public interface MigrationTask {

    void execute();

}
