package com.ucm.ucmamm.migration;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MigrationCommandLineRunner implements CommandLineRunner {

    private final MigrationService migrationService;

    public MigrationCommandLineRunner(MigrationService migrationService) {
        this.migrationService = migrationService;
    }

    @Override
    public void run(String... args) throws Exception {
        migrationService.performMigration();
    }

}
