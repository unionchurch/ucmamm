package com.ucm.ucmamm.migration;

import com.ucm.ucmamm.domain.Election;
import com.ucm.ucmamm.domain.ElectionRepository;
import com.ucm.ucmamm.domain.VoteRepository;
import com.ucm.ucmamm.domain.VoterRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

@Component
public class ElectionProxyCorrectionTask implements MigrationTask {

    private ElectionRepository electionRepository;
    private VoterRepository voterRepository;
    private VoteRepository voteRepository;

    public ElectionProxyCorrectionTask(ElectionRepository electionRepository, VoterRepository voterRepository, VoteRepository voteRepository) {
        this.electionRepository = electionRepository;
        this.voterRepository = voterRepository;
        this.voteRepository = voteRepository;
    }

    @Override
    public void execute() {
        List<Election> updatedElections = new ArrayList<>();
        List<Election> elections = electionRepository.findAll();

        for (Election election : elections) {
            updatedElections.add(election.withVoted(voteRepository.countByElectionIdAndDeleted(election.getId(), FALSE))
                    .withProxy(voterRepository.countByElectionIdAndProxy(election.getId(), TRUE))
                    .withProxyVoted(voteRepository.countByElectionIdAndProxyAndDeleted(election.getId(), TRUE, FALSE)));
        }

        electionRepository.saveAll(updatedElections);
    }

}
