package com.ucm.ucmamm.migration;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.MigrationTaskInfo;
import com.ucm.ucmamm.domain.MigrationTaskInfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.Map;

@Slf4j
@Service
public class MigrationService {

    private MigrationTaskInfoRepository migrationTaskInfoRepository;
    private DateService dateService;

    private Map<String, MigrationTask> migrationTaskMap;

    public MigrationService(ApplicationContext applicationContext, MigrationTaskInfoRepository migrationTaskInfoRepository, DateService dateService) {
        migrationTaskMap = applicationContext.getBeansOfType(MigrationTask.class);
        this.migrationTaskInfoRepository = migrationTaskInfoRepository;
        this.dateService = dateService;
    }

    public void performMigration() {
        log.info("Starting Migration...");
        migrationTaskMap.entrySet().forEach(this::performMigration);
        log.info("Migration Done.");
    }

    private void performMigration(Map.Entry<String, MigrationTask> migrationTaskEntry) {
        if (!isMigrationTaskAlreadyExecuted(migrationTaskEntry.getValue())) {
            log.info("Performing " + migrationTaskEntry.getKey() + "...");
            migrationTaskEntry.getValue().execute();
            saveMigrationTaskStatus(migrationTaskEntry.getValue());
            log.info("Migration task " + migrationTaskEntry.getKey() + " done.");
        }
    }

    private boolean isMigrationTaskAlreadyExecuted(MigrationTask migrationTask) {
        return migrationTaskInfoRepository.findByName(migrationTask.getClass().getName()).isPresent();
    }

    private void saveMigrationTaskStatus(MigrationTask value) {
        MigrationTaskInfo migrationTaskInfo = MigrationTaskInfo.builder()
                .name(value.getClass().getName())
                .executedAt(dateService.getCurrentTime())
                .build();
        migrationTaskInfoRepository.save(migrationTaskInfo);
    }

}
