package com.ucm.ucmamm.report;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class VoteDto {

    String id;

    Long memberId;

    String electionId;

}
