package com.ucm.ucmamm.report;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import net.sf.dynamicreports.report.constant.PageOrientation;

import java.util.List;

@Value
@Builder
public class ReportDetails {

    String title;

    String subtitle;

    @Singular
    List<ReportColumn> columns;

    PageOrientation pageOrientation;

}
