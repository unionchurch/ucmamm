package com.ucm.ucmamm.report;

import net.sf.dynamicreports.report.datasource.DRDataSource;
import org.springframework.stereotype.Component;

import static net.sf.dynamicreports.report.builder.DynamicReports.type;
import static net.sf.dynamicreports.report.constant.PageOrientation.LANDSCAPE;

@Component
public class TestReport extends BaseReport {

    TestReport() {
        super(ReportDetails.builder()
                .title("Sample Report")
                .subtitle("This is just a sample report")
                .column(ReportColumn.builder().title("First Name").column("firstName").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("Last Name").column("lastName").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("City").column("city").dataType(type.stringType()).build())
                .pageOrientation(LANDSCAPE)
                .build());
    }

    @Override
    void populateDataSource(ReportRequest reportRequest, DRDataSource dataSource) {
        dataSource.add("Romeo", "Maranan", "Lemery");
        dataSource.add("Grace Pamela", "Alvarez", "Batangas City");
        dataSource.add("Jomer", "Pangilinan", "Quezon City");
        dataSource.add("Jocel", "Legaspi", "Lemery");
        dataSource.add("Erville", "Valdeavilla", "Lucena");
        dataSource.add("Allan", "Dela Cruz", "Manila");
        dataSource.add("Antonino", "Estole", "Muntinlupa");
    }

    @Override
    String getFileName() {
        return "test-report";
    }

}
