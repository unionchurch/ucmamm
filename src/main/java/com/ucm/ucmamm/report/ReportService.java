package com.ucm.ucmamm.report;

import com.ucm.ucmamm.domain.*;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.springframework.data.domain.Sort.Direction.ASC;

@Service
public class ReportService {

    private static final Sort MEMBER_SORT = Sort.by(ASC, "lastName");

    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private ElectionRepository electionRepository;
    @Autowired
    private VoterRepository voterRepository;
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private MemberCountRepository memberCountRepository;

    List<MemberReportDto> getAllRegisteredMembers() {
        return memberRepository.findAllRegisteredMembers(MEMBER_SORT)
                .stream()
                .map(member -> MemberReportDto.builder()
                        .memberId(member.getMemberId())
                        .firstName(member.getFirstName())
                        .middleName(member.getMiddleName())
                        .lastName(member.getLastName())
                        .suffix(member.getSuffix())
                        .active(BooleanUtils.isTrue(member.getActive()))
                        .build())
                .collect(toList());
    }

    List<VoterReportDto> getAllVoters(String electionId) {
        return voterRepository.findAllVoters(electionId, Sort.by(ASC, "lastName"))
                .stream()
                .map(voter -> VoterReportDto.builder()
                        .id(voter.getId())
                        .electionId(electionId)
                        .memberId(voter.getMemberId())
                        .firstName(voter.getFirstName())
                        .middleName(voter.getMiddleName())
                        .lastName(voter.getLastName())
                        .suffix(voter.getSuffix())
                        .proxy(voter.isProxy())
                        .proxyDetails(voter.getProxyDetails())
                        .registeredBy(voter.getCreatedBy())
                        .registeredDate(voter.getCreatedAt())
                        .build())
                .collect(toList());
    }

    Optional<ElectionReportDto> getElection(String electionId) {
        return electionRepository.findById(electionId)
                .map(election -> ElectionReportDto.builder()
                        .id(electionId)
                        .active(election.getActive())
                        .date(election.getDate())
                        .deleted(election.getDeleted())
                        .endTime(election.getEndTime())
                        .name(election.getName())
                        .proposals(election.getProposals())
                        .proxy(election.getProxy())
                        .questions(election.getQuestions())
                        .seats(election.getSeats())
                        .startTime(election.getStartTime())
                        .version(election.getVersion())
                        .voted(election.getVoted())
                        .voter(election.getVoter())
                        .build());
    }

    Map<String, VoteDto> getVoteMap(String electionId) {
        return voteRepository.findByElectionId(electionId, Sort.by(ASC, "memberId")).stream()
                .map(vote -> VoteDto.builder().id(vote.getId()).electionId(electionId).memberId(vote.getMemberId()).build())
                .collect(toMap(vote -> vote.getElectionId() + "_" + vote.getMemberId(), identity()));
    }

    long getActiveMembersCount() {
        return ofNullable(memberCountRepository.findMemberCount())
                .map(MemberCount::getActive)
                .orElse(0L);
    }

}
