package com.ucm.ucmamm.report;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

@Value
@Builder
public class ReportRequest {

    String electionId;

    @Wither
    ElectionReportDto election;

    DownloadType downloadType;

    @Wither
    String titleOverride;

    @Wither
    String subTitleOverride;

}
