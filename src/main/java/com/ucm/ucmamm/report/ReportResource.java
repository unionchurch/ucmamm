package com.ucm.ucmamm.report;

import net.sf.dynamicreports.report.exception.DRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class ReportResource {

    @Autowired
    private TestReport testReport;
    @Autowired
    private AllRegisteredMembersReport allRegisteredMembersReport;
    @Autowired
    private AllVoterMembersReport allVoterMembersReport;
    @Autowired
    private AllVoterVsMembersReport allVoterVsMembersReport;
    @Autowired
    private ElectionReport electionReport;

    @GetMapping("/api/v1/reports/test")
    public void downloadTestReport(@RequestParam String type, HttpServletResponse httpServletResponse) {
        try {
            DownloadType downloadType = DownloadType.valueOf(type);
            testReport.build(httpServletResponse, ReportRequest.builder().downloadType(downloadType).build());
        } catch (IOException | DRException e) {
            throw new ReportException("Unable to generate report.", e);
        }
    }

    @GetMapping("/api/v1/reports/all-members")
    public void downloadAllRegisteredMembers(@RequestParam String type, HttpServletResponse httpServletResponse) {
        try {
            DownloadType downloadType = DownloadType.valueOf(type);
            allRegisteredMembersReport.build(httpServletResponse, ReportRequest.builder().downloadType(downloadType).build());
        } catch (IOException | DRException e) {
            throw new ReportException("Unable to generate report.", e);
        }
    }

    @GetMapping("/api/v1/reports/all-voters")
    public void downloadAllRegisteredVoters(@RequestParam String type, @RequestParam String electionId, HttpServletResponse httpServletResponse) {
        try {
            DownloadType downloadType = DownloadType.valueOf(type);
            allVoterMembersReport.build(httpServletResponse, ReportRequest.builder().downloadType(downloadType).electionId(electionId).build());
        } catch (IOException | DRException e) {
            throw new ReportException("Unable to generate report.", e);
        }
    }

    @GetMapping("/api/v1/reports/all-voters-vs-members")
    public void downloadAllRegisteredVotersVsMembers(@RequestParam String type, @RequestParam String electionId, HttpServletResponse httpServletResponse) {
        try {
            DownloadType downloadType = DownloadType.valueOf(type);
            allVoterVsMembersReport.build(httpServletResponse, ReportRequest.builder().downloadType(downloadType).electionId(electionId).build());
        } catch (IOException | DRException e) {
            throw new ReportException("Unable to generate report.", e);
        }
    }

    @GetMapping("/api/v1/reports/election")
    public void downloadElectionResult(@RequestParam String type, @RequestParam String electionId, HttpServletResponse httpServletResponse) {
        try {
            DownloadType downloadType = DownloadType.valueOf(type);
            electionReport.build(httpServletResponse, ReportRequest.builder().downloadType(downloadType).electionId(electionId).build());
        } catch (IOException | DRException e) {
            throw new ReportException("Unable to generate report.", e);
        }
    }

}
