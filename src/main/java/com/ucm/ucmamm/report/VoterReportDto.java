package com.ucm.ucmamm.report;

import com.ucm.ucmamm.domain.ProxyDetails;
import lombok.Builder;
import lombok.Value;

import java.time.Instant;

@Value
@Builder
public class VoterReportDto {

    String id;

    String electionId;

    Long memberId;

    String firstName;

    String middleName;

    String lastName;

    String suffix;

    boolean proxy;

    ProxyDetails proxyDetails;

    Instant registeredDate;

    String registeredBy;

}
