package com.ucm.ucmamm.report;

public enum DownloadType {

    XLSX, PDF, WORD;

    public String getExtension() {
        return "." + name().toLowerCase();
    }

}
