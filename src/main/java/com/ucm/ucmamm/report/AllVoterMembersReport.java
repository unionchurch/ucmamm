package com.ucm.ucmamm.report;

import com.ucm.ucmamm.common.ResourceNotFoundException;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.List;

import static net.sf.dynamicreports.report.builder.DynamicReports.type;
import static net.sf.dynamicreports.report.constant.PageOrientation.PORTRAIT;

@Component
public class AllVoterMembersReport extends BaseReport {

    @Autowired
    private ReportService reportService;

    private ElectionReportDto election;

    AllVoterMembersReport() {
        super(ReportDetails.builder()
                .title("All UCM AMM Voters")
                .subtitle("List of Registered Voters of UCM AMM")
                .column(ReportColumn.builder().title("Member ID").column("memberId").dataType(type.longType()).build())
                .column(ReportColumn.builder().title("Last Name").column("lastName").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("First Name").column("firstName").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("Suffix").column("suffix").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("Middle Name").column("middleName").dataType(type.stringType()).build())
                .pageOrientation(PORTRAIT)
                .build());
    }

    @Override
    void populateDataSource(ReportRequest reportRequest, DRDataSource dataSource) {
        List<VoterReportDto> members = reportService.getAllVoters(reportRequest.getElectionId());
        if (members != null) {
            members.forEach(member -> dataSource.add(
                    member.getMemberId(),
                    member.getLastName(),
                    member.getFirstName(),
                    member.getSuffix(),
                    member.getMiddleName()));
        }
    }

    @Override
    ReportRequest overrideReportRequest(ReportRequest reportRequest) {
        election = reportService.getElection(reportRequest.getElectionId()).orElseThrow(ResourceNotFoundException::new);
        return reportRequest.withSubTitleOverride("List of Registered Voters of UCM AMM " + election.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }

    @Override
    String getFileName() {
        return "All UCM AMM Voters of " + election.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

}
