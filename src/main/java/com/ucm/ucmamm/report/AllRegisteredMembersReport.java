package com.ucm.ucmamm.report;

import net.sf.dynamicreports.report.datasource.DRDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static net.sf.dynamicreports.report.builder.DynamicReports.type;
import static net.sf.dynamicreports.report.constant.HorizontalTextAlignment.CENTER;
import static net.sf.dynamicreports.report.constant.PageOrientation.PORTRAIT;

@Component
public class AllRegisteredMembersReport extends BaseReport {

    @Autowired
    private ReportService reportService;

    AllRegisteredMembersReport() {
        super(ReportDetails.builder()
                .title("All UCM Members")
                .subtitle("Current List of Registered Members of the Union Church of Manila\nas of " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm a")))
                .column(ReportColumn.builder().title("Member ID").column("memberId").dataType(type.longType()).build())
                .column(ReportColumn.builder().title("Last Name").column("lastName").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("First Name").column("firstName").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("Suffix").column("suffix").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("Middle Name").column("middleName").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("Is Active?").column("active").dataType(type.stringType()).horizontalTextAlignment(CENTER).build())
                .pageOrientation(PORTRAIT)
                .build());
    }

    @Override
    void populateDataSource(ReportRequest reportRequest, DRDataSource dataSource) {
        List<MemberReportDto> members = reportService.getAllRegisteredMembers();
        if (members != null) {
            members.forEach(member -> dataSource.add(
                    member.getMemberId(),
                    member.getLastName(),
                    member.getFirstName(),
                    member.getSuffix(),
                    member.getMiddleName(),
                    member.isActive() ? "Yes" : "No"));
        }
    }

    @Override
    String getFileName() {
        return "All Registered Members as of " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HHmm"));
    }

}
