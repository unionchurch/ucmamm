package com.ucm.ucmamm.report;

import com.ucm.ucmamm.common.ResourceNotFoundException;
import com.ucm.ucmamm.domain.ProxyDetails;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;

import static java.lang.Boolean.TRUE;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toMap;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;
import static net.sf.dynamicreports.report.constant.HorizontalTextAlignment.CENTER;
import static net.sf.dynamicreports.report.constant.PageOrientation.PORTRAIT;

@Component
public class AllVoterVsMembersReport extends BaseReport {

    @Autowired
    private ReportService reportService;

    private ElectionReportDto election;

    AllVoterVsMembersReport() {
        super(ReportDetails.builder()
                .title("Voters Turnout Report")
                .subtitle("List of Registered Voters vs Members")
                .column(ReportColumn.builder().title("Member ID").column("memberId").dataType(type.longType()).build())
                .column(ReportColumn.builder().title("Last Name").column("lastName").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("First Name").column("firstName").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("Suffix").column("suffix").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("Middle Name").column("middleName").dataType(type.stringType()).build())
                .column(ReportColumn.builder().title("Active?").column("active").dataType(type.stringType()).horizontalTextAlignment(CENTER).build())
                .column(ReportColumn.builder().title("Registered Voter?").column("registeredVoter").dataType(type.stringType()).horizontalTextAlignment(CENTER).build())
                .column(ReportColumn.builder().title("Proxy Voter").column("proxyVoter").dataType(type.stringType()).horizontalTextAlignment(CENTER).build())
                .column(ReportColumn.builder().title("Voted?").column("voted").dataType(type.stringType()).horizontalTextAlignment(CENTER).build())
                .pageOrientation(PORTRAIT)
                .build());
    }

    @Override
    void populateDataSource(ReportRequest reportRequest, DRDataSource dataSource) {
        Map<Long, MemberReportDto> members = new HashMap<>(reportService.getAllRegisteredMembers().stream()
                .collect(toMap(MemberReportDto::getMemberId, Function.identity())));
        List<VoterReportDto> voters = reportService.getAllVoters(reportRequest.getElectionId());

        if (voters != null) {
            voters.stream().map(voter -> MemberReportDto.builder()
                    .memberId(voter.getMemberId())
                    .firstName(voter.getFirstName())
                    .middleName(voter.getMiddleName())
                    .lastName(voter.getLastName())
                    .suffix(voter.getSuffix())
                    .active(TRUE)
                    .registeredVoter(TRUE)
                    .proxyDetails(voter.getProxyDetails())
                    .build())
                    .forEach(member -> members.put(member.getMemberId(), member));
        }

        List<MemberReportDto> membersList = new ArrayList<>(members.values());
        membersList.sort(new MemberReportDtoComparator());

        Map<String, VoteDto> votesMap = reportService.getVoteMap(reportRequest.getElectionId());

        membersList.forEach(member -> dataSource.add(
                member.getMemberId(),
                member.getLastName(),
                member.getFirstName(),
                member.getSuffix(),
                member.getMiddleName(),
                member.isActive() ? "Yes" : "No",
                member.isRegisteredVoter() ? "Yes" : "No",
                buildProxyVoter(member.getProxyDetails()),
                votesMap.containsKey(reportRequest.getElectionId() + "_" + member.getMemberId()) ? "Yes" : "No"));
    }

    private String buildProxyVoter(ProxyDetails proxyDetails) {
        return ofNullable(proxyDetails)
                .map(pd -> proxyDetails.getFirstName() + " " + proxyDetails.getLastName() +
                        ofNullable(proxyDetails.getSuffix()).map(suffix -> " " + suffix).orElse(""))
                .orElse("N/A");
    }

    @Override
    ReportRequest overrideReportRequest(ReportRequest reportRequest) {
        election = reportService.getElection(reportRequest.getElectionId()).orElseThrow(ResourceNotFoundException::new);
        return reportRequest.withSubTitleOverride("List of Registered Voters vs Members " + election.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }

    @Override
    String getFileName() {
        return "All Registered Members as of " + election.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    private static class MemberReportDtoComparator implements Comparator<MemberReportDto> {

        @Override
        public int compare(MemberReportDto member1, MemberReportDto member2) {
            int lastNameCompare = member1.getLastName().compareTo(member2.getLastName());
            if (lastNameCompare == 0) {
                return member1.getFirstName().compareTo(member2.getFirstName());
            }
            return lastNameCompare;
        }

    }

}
