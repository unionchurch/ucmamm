package com.ucm.ucmamm.report;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.style.FontBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.PageType;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import static java.awt.Color.LIGHT_GRAY;
import static java.lang.Boolean.TRUE;
import static net.sf.dynamicreports.report.builder.DynamicReports.*;
import static net.sf.dynamicreports.report.constant.HorizontalTextAlignment.CENTER;
import static net.sf.dynamicreports.report.constant.HorizontalTextAlignment.LEFT;

abstract class BaseReport {

    private static final FontBuilder titleFont = stl.font().setBold(TRUE).setFontSize(24);
    private static final FontBuilder subtitleFont = stl.font().setFontSize(14);
    private static final StyleBuilder titleStyle = stl.style().setFont(titleFont);
    private static final StyleBuilder subtitleStyle = stl.style().setFont(subtitleFont).setBottomPadding(5);

    private static final FontBuilder columnTitleFont = stl.font().setBold(TRUE);
    private static final StyleBuilder columnTitleStyle = stl.style(columnTitleFont)
            .setBorder(stl.pen1Point())
            .setBackgroundColor(LIGHT_GRAY)
            .setHorizontalTextAlignment(CENTER)
            .setPadding(5);

    private static final StyleBuilder columnStyle = stl.style()
            .setBorder(stl.pen1Point())
            .setHorizontalTextAlignment(LEFT)
            .setPadding(5);

    private ReportDetails reportDetails;

    BaseReport(ReportDetails reportDetails) {
        this.reportDetails = reportDetails;
    }

    void build(HttpServletResponse httpServletResponse, ReportRequest reportRequest) throws DRException, IOException {
        ReportRequest reportRequestOverride = overrideReportRequest(reportRequest);
        OutputStream outputStream = httpServletResponse.getOutputStream();
        JasperReportBuilder reportBuilder = buildReportComponents(reportRequestOverride, reportDetails);
        DownloadType downloadType = reportRequestOverride.getDownloadType();
        switch (downloadType) {
            case XLSX:
                httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                httpServletResponse.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + getFileName() + reportRequestOverride.getDownloadType().getExtension());
                reportBuilder.ignorePagination();
                reportBuilder.ignorePageWidth();
                reportBuilder.toXlsx(outputStream);
                break;
            case PDF:
                httpServletResponse.setContentType(MediaType.APPLICATION_PDF_VALUE);
                httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE);
                httpServletResponse.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + getFileName() + reportRequestOverride.getDownloadType().getExtension());
                reportBuilder.toPdf(outputStream);
                break;
            case WORD:
                httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                httpServletResponse.setHeader(HttpHeaders.CONTENT_TYPE, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                httpServletResponse.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + getFileName() + reportRequestOverride.getDownloadType().getExtension());
                reportBuilder.ignorePagination();
                reportBuilder.ignorePageWidth();
                reportBuilder.toDocx(outputStream);
                break;
        }
    }

    ReportRequest overrideReportRequest(ReportRequest reportRequest) {
        return reportRequest;
    }

    JasperReportBuilder buildReportComponents(ReportRequest reportRequest, ReportDetails reportDetails) {
        JasperReportBuilder reportBuilder = DynamicReports.report()
                .title(buildTitle(reportRequest, reportDetails))
                .setDataSource(buildDataSource(reportRequest, reportDetails.getColumns()));
        reportDetails.getColumns().forEach(reportColumn -> addColumn(reportBuilder, reportRequest, reportColumn));
        reportBuilder.setColumnTitleStyle(columnTitleStyle);
        reportBuilder.setColumnStyle(columnStyle);
        reportBuilder.setPageFormat(PageType.LETTER, reportDetails.getPageOrientation());
        return reportBuilder;
    }

    ComponentBuilder<?, ?> buildTitle(ReportRequest reportRequest, ReportDetails reportDetails) {
        return cmp.verticalList(
                cmp.text(StringUtils.defaultIfBlank(reportRequest.getTitleOverride(), reportDetails.getTitle())).setStyle(titleStyle).setHorizontalTextAlignment(CENTER),
                cmp.text(StringUtils.defaultIfBlank(reportRequest.getSubTitleOverride(), reportDetails.getSubtitle())).setStyle(subtitleStyle).setHorizontalTextAlignment(CENTER),
                cmp.line());
    }

    @SuppressWarnings("unchecked")
    JasperReportBuilder addColumn(JasperReportBuilder reportBuilder, ReportRequest reportRequest, ReportColumn reportColumn) {
        return reportBuilder.addColumn(col.column(reportColumn.getTitle(), reportColumn.getColumn(), reportColumn.getDataType()).setHorizontalTextAlignment(reportColumn.getHorizontalTextAlignment()));
    }

    private JRDataSource buildDataSource(ReportRequest reportRequest, List<ReportColumn> columns) {
        DRDataSource dataSource = new DRDataSource(columns.stream().map(ReportColumn::getColumn).toArray(String[]::new));
        populateDataSource(reportRequest, dataSource);
        return dataSource;
    }

    abstract void populateDataSource(ReportRequest reportRequest, DRDataSource dataSource);

    abstract String getFileName();

}
