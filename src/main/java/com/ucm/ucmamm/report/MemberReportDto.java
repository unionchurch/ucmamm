package com.ucm.ucmamm.report;

import com.ucm.ucmamm.domain.ProxyDetails;
import lombok.Builder;
import lombok.Value;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Value
@Builder
public class MemberReportDto {

    long memberId;

    String firstName;

    String middleName;

    String lastName;

    String suffix;

    boolean active;

    boolean registeredVoter;

    ProxyDetails proxyDetails;

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(memberId).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof MemberReportDto) {
            MemberReportDto comparison = (MemberReportDto) obj;
            return memberId == comparison.getMemberId();
        }
        return false;
    }

}
