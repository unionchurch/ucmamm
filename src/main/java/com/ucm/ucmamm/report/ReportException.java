package com.ucm.ucmamm.report;

public class ReportException extends RuntimeException {

    private static final long serialVersionUID = -1706139905027766593L;

    public ReportException(String message, Throwable cause) {
        super(message, cause);
    }

}
