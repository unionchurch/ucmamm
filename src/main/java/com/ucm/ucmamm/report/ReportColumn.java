package com.ucm.ucmamm.report;

import lombok.Builder;
import lombok.Value;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.definition.datatype.DRIDataType;

@Value
@Builder
public class ReportColumn {

    String column;

    String title;

    DRIDataType dataType;

    @Builder.Default
    HorizontalTextAlignment horizontalTextAlignment = HorizontalTextAlignment.LEFT;

}
