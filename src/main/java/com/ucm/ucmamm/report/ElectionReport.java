package com.ucm.ucmamm.report;

import com.ucm.ucmamm.common.ResourceNotFoundException;
import com.ucm.ucmamm.domain.Question;
import com.ucm.ucmamm.domain.Seat;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.constant.PageType;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.util.Optional.ofNullable;
import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
import static net.sf.dynamicreports.report.builder.component.Components.*;
import static net.sf.dynamicreports.report.constant.HorizontalTextAlignment.CENTER;
import static net.sf.dynamicreports.report.constant.HorizontalTextAlignment.RIGHT;
import static net.sf.dynamicreports.report.constant.PageOrientation.LANDSCAPE;

@Component
public class ElectionReport extends BaseReport {

    private static BigDecimal ONE_HUNDRED = toBigDecimal("100");

    @Autowired
    private ReportService reportService;

    private ElectionReportDto election;

    private BigDecimal activeMembersParticipatedPercentage;
    private BigDecimal proxyPercentage;
    private BigDecimal votedPercentageOutOfVoters;
    private BigDecimal votedPercentageOutOfActiveMembers;

    ElectionReport() {
        super(ReportDetails.builder()
                .title("Election Result Report")
                .subtitle("Election Result Report")
                .pageOrientation(LANDSCAPE)
                .build());
    }

    @Override
    void populateDataSource(ReportRequest reportRequest, DRDataSource dataSource) {

    }

    @Override
    JasperReportBuilder buildReportComponents(ReportRequest reportRequest, ReportDetails reportDetails) {
        JasperReportBuilder reportBuilder = DynamicReports.report()
                .title(verticalList(
                        buildTitle(reportRequest, reportDetails),
                        verticalGap(10),
                        buildReportBody()
                ));
        reportBuilder.setPageFormat(PageType.LETTER, reportDetails.getPageOrientation());
        return reportBuilder;
    }

    private ComponentBuilder<?, ?> buildReportBody() {
        return horizontalList(
                buildResultsSummary(),
                buildResultsDetails()
        );
    }

    private ComponentBuilder<?, ?> buildResultsSummary() {
        return verticalList(
                cmp.text("Results Summary").setStyle(stl.style().bold().setFontSize(12)),
                verticalGap(10),
                buildSummaryItem("Active Members Participated", election.getVoter(), null),
                buildSummaryItem("Active Members Participated (%)", activeMembersParticipatedPercentage, "0.000"),
                buildSummaryItem("Proxy", election.getProxy(), null),
                buildSummaryItem("Proxy out of Registered Voters (%)", proxyPercentage, null),
                buildSummaryItem("Submitted Votes", election.getVoted(), null),
                buildSummaryItem("Voted out of Registered Voters (%)", votedPercentageOutOfVoters, null),
                buildSummaryItem("Voted out of Active Members (%)", votedPercentageOutOfActiveMembers, null)
        ).setFixedWidth(220).setFixedHeight(250).setStyle(stl.style().setRightPadding(10));
    }

    private ComponentBuilder<?, ?> buildResultsDetails() {
        return verticalList(
                buildProposalsResults(),
                verticalGap(20),
                buildQuestionsResults(),
                verticalGap(20),
                buildCandidatesVoteResults()
        ).setFixedWidth(542).setStyle(stl.style().setLeftPadding(10).setLeftBorder(stl.pen2Point()));
    }

    private ComponentBuilder<?, ?> buildCandidatesVoteResults() {
        VerticalListBuilder verticalListBuilder = verticalList();
        if (election.getSeats() != null) {
            election.getSeats().forEach(seat -> verticalListBuilder.add(buildSeatResult(seat)));
        }
        return verticalListBuilder;
    }

    private ComponentBuilder<?, ?> buildSeatResult(Seat seat) {
        VerticalListBuilder verticalListBuilder = verticalList(
                cmp.text(seat.getName()).setStyle(stl.style().bold().setFontSize(12)),
                verticalGap(10)
        );
        if (seat.getCandidates() != null) {
            verticalListBuilder.add(horizontalList(
                    horizontalGap(412), buildHeaderText("Approved"), buildHeaderText("Percentage (%)")
            ));
            seat.getCandidates().forEach(candidate -> verticalListBuilder.add(horizontalList(
                    cmp.text(candidate.getFirstName() + " " + candidate.getLastName() + " " + candidate.getSuffix()).setStyle(stl.style().bold().setFontSize(8)).setFixedWidth(412),
                    cmp.text(candidate.getApproved()).setStyle(stl.style().bold().setFontSize(8)).setHorizontalTextAlignment(CENTER).setFixedWidth(60),
                    cmp.text(toBigDecimal(candidate.getApproved()).multiply(ONE_HUNDRED).divide(toBigDecimal(election.getVoted()), 3, BigDecimal.ROUND_HALF_EVEN)).setPattern("0.000").setStyle(stl.style().bold().setFontSize(8)).setHorizontalTextAlignment(CENTER).setFixedWidth(60)
            ).setStyle(stl.style().setTopPadding(8).setBottomPadding(8).setTopBorder(stl.pen2Point()))));
        }
        return verticalListBuilder;
    }

    private ComponentBuilder<?, ?> buildQuestionsResults() {
        VerticalListBuilder verticalListBuilder = verticalList(
                cmp.text("Questions").setStyle(stl.style().bold().setFontSize(12)),
                verticalGap(10)
        );
        if (election.getQuestions() != null) {
            election.getQuestions().forEach(question -> {
                verticalListBuilder.add(horizontalList(
                        cmp.text(question.getContent()).setStyle(stl.style().bold().setFontSize(8)).setFixedWidth(532)
                ).setStyle(stl.style().setTopPadding(8).setBottomPadding(8).setTopBorder(stl.pen2Point())));
                verticalListBuilder.add(horizontalList(
                        horizontalGap(412), buildHeaderText("Count"), buildHeaderText("Percentage (%)")
                ));
                verticalListBuilder.add(buildAnswers(question));
            });
        }
        return verticalListBuilder;
    }

    private ComponentBuilder<?, ?> buildAnswers(Question question) {
        VerticalListBuilder verticalListBuilder = verticalList();
        if (question.getAnswers() != null) {
            question.getAnswers().forEach(answer -> verticalListBuilder.add(horizontalList(
                    horizontalGap(20),
                    cmp.text(answer.getContent()).setStyle(stl.style().bold().setFontSize(8)).setFixedWidth(392),
                    cmp.text(answer.getSelected()).setStyle(stl.style().bold().setFontSize(8)).setHorizontalTextAlignment(CENTER).setFixedWidth(60),
                    cmp.text(toBigDecimal(answer.getSelected()).multiply(ONE_HUNDRED).divide(toBigDecimal(election.getVoted()), 3, BigDecimal.ROUND_HALF_EVEN)).setPattern("0.000").setStyle(stl.style().bold().setFontSize(8)).setHorizontalTextAlignment(CENTER).setFixedWidth(60)
            )));
        }
        return verticalListBuilder;
    }

    private ComponentBuilder<?, ?> buildProposalsResults() {
        VerticalListBuilder verticalListBuilder = verticalList(
                cmp.text("Proposals").setStyle(stl.style().bold().setFontSize(12)),
                horizontalList(horizontalGap(412), buildHeaderText("Approved"), buildHeaderText("Percentage (%)"))
        );
        if (election.getProposals() != null) {
            election.getProposals().forEach(proposal -> verticalListBuilder.add(horizontalList(
                    cmp.text(proposal.getContent()).setStyle(stl.style().bold().setFontSize(8)).setFixedWidth(412),
                    cmp.text(proposal.getApproved()).setStyle(stl.style().bold().setFontSize(8)).setHorizontalTextAlignment(CENTER).setFixedWidth(60),
                    cmp.text(toBigDecimal(proposal.getApproved()).multiply(ONE_HUNDRED).divide(toBigDecimal(election.getVoted()), 3, BigDecimal.ROUND_HALF_EVEN)).setPattern("0.000").setStyle(stl.style().bold().setFontSize(8)).setHorizontalTextAlignment(CENTER).setFixedWidth(60)
            ).setStyle(stl.style().setTopPadding(8).setBottomPadding(8).setTopBorder(stl.pen2Point()))));
        }
        return verticalListBuilder;
    }

    private ComponentBuilder<?, ?> buildHeaderText(String approved) {
        return cmp.text(approved).setStyle(stl.style().bold().setFontSize(8).setHorizontalTextAlignment(CENTER)).setFixedWidth(60);
    }

    private <T extends Number> ComponentBuilder<?, ?> buildSummaryItem(String title, T value, String pattern) {
        return horizontalList(
                ofNullable(pattern)
                        .map(p -> cmp.text(title).setPattern(p).setStyle(stl.style().bold().setFontSize(8)).setFixedWidth(160))
                        .orElse(cmp.text(title).setStyle(stl.style().bold().setFontSize(8)).setFixedWidth(160)),
                cmp.text(value).setStyle(stl.style().bold().setFontSize(8)).setFixedWidth(50).setHorizontalTextAlignment(RIGHT)
        );
    }

    @Override
    ReportRequest overrideReportRequest(ReportRequest reportRequest) {
        election = reportService.getElection(reportRequest.getElectionId()).orElseThrow(ResourceNotFoundException::new);
        long activeMembersCount = reportService.getActiveMembersCount();

        activeMembersParticipatedPercentage = toBigDecimal(election.getVoter()).multiply(ONE_HUNDRED).divide(toBigDecimal(activeMembersCount), 3, BigDecimal.ROUND_HALF_EVEN);
        proxyPercentage = toBigDecimal(election.getProxy()).multiply(ONE_HUNDRED).divide(toBigDecimal(election.getVoter()), 3, BigDecimal.ROUND_HALF_EVEN);

        votedPercentageOutOfVoters = toBigDecimal(election.getVoted()).multiply(ONE_HUNDRED).divide(toBigDecimal(election.getVoter()), 3, BigDecimal.ROUND_HALF_EVEN);
        votedPercentageOutOfActiveMembers = toBigDecimal(election.getVoted()).multiply(ONE_HUNDRED).divide(toBigDecimal(activeMembersCount), 3, BigDecimal.ROUND_HALF_EVEN);

        return reportRequest.withSubTitleOverride("UCM AMM " + election.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " Election Result Report (Downloaded " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("MMMM dd, yyyy hh:mm a")) + ")");
    }

    private <T extends Number> BigDecimal toBigDecimal(T number) {
        return new BigDecimal(String.valueOf(number));
    }

    private static BigDecimal toBigDecimal(String number) {
        return new BigDecimal(number);
    }

    @Override
    String getFileName() {
        return "UCM AMM " + election.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " Election Result Report";
    }

}
