package com.ucm.ucmamm.user.changepassword;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.constraints.NotBlank;

@Value
@Builder
public class ChangePasswordInput {

    @NotBlank
    String oldPassword;

    @NotBlank
    String newPassword;

    @NotBlank
    String confirmPassword;

    @Wither
    String userName;

    @PasswordsMatched
    public Passwords getPasswords() {
        return Passwords.builder()
                .confirmPassword(confirmPassword)
                .newPassword(newPassword)
                .build();
    }

    @CorrectOldPassword
    public UserNamePassword getUserNamePassword() {
        return UserNamePassword.builder()
                .userName(userName)
                .oldPassword(oldPassword)
                .build();
    }

}
