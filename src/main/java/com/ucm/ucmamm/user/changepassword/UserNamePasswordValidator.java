package com.ucm.ucmamm.user.changepassword;

import com.ucm.ucmamm.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Base64;

import static java.lang.Boolean.FALSE;

@Component
public class UserNamePasswordValidator implements ConstraintValidator<CorrectOldPassword, UserNamePassword> {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public boolean isValid(UserNamePassword userNamePassword, ConstraintValidatorContext constraintValidatorContext) {
        return userRepository.findByUserNameAndDeleted(userNamePassword.getUserName(), FALSE)
                .filter(user -> passwordEncoder.matches(decode(userNamePassword.getOldPassword()), user.getPassword()))
                .isPresent();
    }

    private CharSequence decode(String oldPassword) {
        return new String(Base64.getDecoder().decode(oldPassword));
    }

}
