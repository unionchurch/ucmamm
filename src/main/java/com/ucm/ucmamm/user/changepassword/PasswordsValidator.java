package com.ucm.ucmamm.user.changepassword;

import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class PasswordsValidator implements ConstraintValidator<PasswordsMatched, Passwords> {

    @Override
    public boolean isValid(Passwords passwords, ConstraintValidatorContext constraintValidatorContext) {
        return passwords.getConfirmPassword().equals(passwords.getNewPassword());
    }

}
