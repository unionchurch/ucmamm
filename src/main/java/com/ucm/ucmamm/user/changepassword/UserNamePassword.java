package com.ucm.ucmamm.user.changepassword;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class UserNamePassword {

    String oldPassword;

    String userName;

}
