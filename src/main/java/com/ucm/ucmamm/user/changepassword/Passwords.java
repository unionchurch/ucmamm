package com.ucm.ucmamm.user.changepassword;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Passwords {

    String newPassword;

    String confirmPassword;

}
