package com.ucm.ucmamm.user.changepassword;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = PasswordsValidator.class)
@Target({METHOD, FIELD})
@Retention(RUNTIME)
public @interface PasswordsMatched {

    String message() default "passwords didn't match";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
