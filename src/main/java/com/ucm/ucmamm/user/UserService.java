package com.ucm.ucmamm.user;

import com.ucm.ucmamm.common.CacheService;
import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.User;
import com.ucm.ucmamm.domain.UserRepository;
import com.ucm.ucmamm.user.changepassword.ChangePasswordInput;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.ucm.ucmamm.common.Caches.USER;
import static com.ucm.ucmamm.common.Caches.USERS;
import static java.lang.Boolean.FALSE;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.data.domain.Sort.Direction.DESC;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final ConversionService conversionService;
    private final DateService dateService;
    private final CacheService cacheService;
    private final UserDetailsConverter userDetailsConverter;
    private final PasswordEncoder passwordEncoder;

    User saveUser(UserInput userInput) {
        User user = userRepository.save(conversionService.convert(userInput, User.class));
        cacheService.evictCache(USERS);
        return user;
    }

    void updateUser(UserUpdate userUpdate) {
        userRepository.save(conversionService.convert(userUpdate, User.class));
        cacheService.evictCache(USERS);
        cacheService.evictCache(USER, userUpdate.getId());
        cacheService.evictCache(USER, userUpdate.getUserName());
    }

    void deleteUser(String id, String operator) {
        userRepository.findById(id)
                .map(user -> user.withDeleted(Boolean.TRUE).withUpdatedBy(operator).withUpdatedAt(dateService.getCurrentTime()))
                .ifPresent(this::onDeleteUser);
    }

    Optional<UserDto> getUser(String id) {
        return userRepository.findByIdAndDeleted(id, FALSE)
                .map(user -> conversionService.convert(user, UserDto.class));
    }

    Optional<UserDto> getUserByName(String username) {
        return userRepository.findByUserNameAndDeleted(username, FALSE)
                .map(user -> conversionService.convert(user, UserDto.class));
    }

    Optional<UserDetails> getUserDetails(String name) {
        return userRepository.findByUserNameAndDeleted(name, FALSE)
                .map(user -> conversionService.convert(user, UserDetails.class));
    }

    List<UserDto> getUsers(int page, int pageSize, String keyword, String sortField, boolean ascending) {
        return ofNullable(getUserPage(page, pageSize, keyword, sortField, ascending))
                .filter(Slice::hasContent)
                .map(Slice::getContent)
                .map(users -> users.stream().map(user -> conversionService.convert(user, UserDto.class)).collect(toList()))
                .orElse(Collections.emptyList());
    }

    void changepassword(ChangePasswordInput changePasswordInput) {
        userRepository.findByUserNameAndDeleted(changePasswordInput.getUserName(), FALSE)
                .map(user -> user.withPassword(passwordEncoder.encode(new String(Base64.getDecoder().decode(changePasswordInput.getNewPassword())))))
                .ifPresent(userRepository::save);
    }

    private Page<User> getUserPage(int page, int pageSize, String keyword, String sortField, boolean ascending) {
        PageRequest pageRequest = PageRequest.of(page, pageSize, ascending ? ASC : DESC,
                ofNullable(sortField).filter(StringUtils::isNotBlank).orElse("userName"));
        return StringUtils.isBlank(keyword)
                ? userRepository.findByDeleted(FALSE, pageRequest)
                : userRepository.findByUserNameStartingWithAndDeleted(keyword, FALSE, pageRequest);
    }

    private void onDeleteUser(User user) {
        cacheService.evictCache(USERS);
        cacheService.evictCache(USER, user.getId());
        cacheService.evictCache(USER, user.getUserName());
        userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUserNameAndDeleted(username, FALSE)
                .map(userDetailsConverter::convert)
                .filter(UserDetails::isEnabled)
                .orElseThrow(() -> new UsernameNotFoundException(username + " is not found"));
    }

}
