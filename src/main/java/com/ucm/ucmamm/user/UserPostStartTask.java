package com.ucm.ucmamm.user;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.ApplicationInfo;
import com.ucm.ucmamm.domain.ApplicationInfoRepository;
import com.ucm.ucmamm.domain.RoleRepository;
import com.ucm.ucmamm.domain.UserRepository;
import com.ucm.ucmamm.user.config.AppDefaultConfig;
import com.ucm.ucmamm.user.config.DefaultRoleListLoader;
import com.ucm.ucmamm.user.config.DefaultUserListLoader;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserPostStartTask implements CommandLineRunner {

    private static final String APP_INFO_ID = "1";
    private static final ClassPathResource ROLE_LIST_CONFIG_FILE = new ClassPathResource("defaultConfig/default-auth-config.json");

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final ApplicationInfoRepository applicationInfoRepository;
    private final PasswordEncoder passwordEncoder;
    private final DateService dateService;
    private final DefaultRoleListLoader defaultRoleListLoader;
    private final DefaultUserListLoader defaultUserListLoader;

    @Override
    public void run(String... args) throws Exception {
        AppDefaultConfig appDefaultConfig = getAppDefaultConfig();
        String savedAppVersion = getSavedAppVersion();
        createDefaultRolesWhenNonExistent(appDefaultConfig, savedAppVersion);
        createAdminUserWhenNonExistent(appDefaultConfig);
        updateAppVersion(appDefaultConfig);
    }

    private void updateAppVersion(AppDefaultConfig appDefaultConfig) {
        ApplicationInfo applicationInfo = ApplicationInfo.builder()
                .id(APP_INFO_ID)
                .version(appDefaultConfig.getAppVersion())
                .build();
        applicationInfoRepository.save(applicationInfo);
        log.info("Updated version to " + applicationInfo.getVersion());
    }

    private void createDefaultRolesWhenNonExistent(AppDefaultConfig appDefaultConfig, String savedAppVersion) {
        defaultRoleListLoader.loadDefaultRoles(appDefaultConfig, savedAppVersion);
    }

    private void createAdminUserWhenNonExistent(AppDefaultConfig appDefaultConfig) {
        defaultUserListLoader.loadDefaultUsers(appDefaultConfig);
    }

    private AppDefaultConfig getAppDefaultConfig() {
        if (ROLE_LIST_CONFIG_FILE.exists()) {
            try (InputStream inputStream = ROLE_LIST_CONFIG_FILE.getInputStream()) {
                ObjectMapper mapper = new ObjectMapper();
                return mapper.reader(AppDefaultConfig.class).readValue(inputStream);
            } catch (IOException e) {
                log.warn("Updates in Default Role List was not applied due to the following errors", e);
            }
        }
        return defaultRoleListConfig();
    }

    private AppDefaultConfig defaultRoleListConfig() {
        return new AppDefaultConfig();
    }

    private String getSavedAppVersion() {
        return applicationInfoRepository.findById(APP_INFO_ID).map(ApplicationInfo::getVersion).orElse("0.0.0");
    }

}
