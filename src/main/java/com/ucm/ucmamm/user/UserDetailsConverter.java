package com.ucm.ucmamm.user;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import static java.util.stream.Collectors.toSet;

@Component
public class UserDetailsConverter implements Converter<com.ucm.ucmamm.domain.User, UserDetails> {

    private static final boolean ACCOUNT_NON_EXPIRED = Boolean.TRUE;
    private static final boolean CREDENTIALS_NON_EXPIRED = Boolean.TRUE;
    private static final boolean ACCOUNT_NON_LOCKED = Boolean.TRUE;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails convert(com.ucm.ucmamm.domain.User user) {
        return new User(user.getUserName(),
                user.getPassword(),
                BooleanUtils.isTrue(user.getActive()),
                ACCOUNT_NON_EXPIRED,
                CREDENTIALS_NON_EXPIRED,
                ACCOUNT_NON_LOCKED,
                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getRoleName())).collect(toSet()));
    }

}
