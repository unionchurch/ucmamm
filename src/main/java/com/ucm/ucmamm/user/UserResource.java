package com.ucm.ucmamm.user;

import com.ucm.ucmamm.common.ResourceNotFoundException;
import com.ucm.ucmamm.domain.User;
import com.ucm.ucmamm.user.changepassword.ChangePasswordInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;
import java.util.List;

import static com.ucm.ucmamm.common.Caches.USER;
import static com.ucm.ucmamm.common.Caches.USERS;

@RestController
public class UserResource {

    @Autowired
    private UserService userService;

    @Cacheable(cacheNames = USER)
    @GetMapping("/api/v1/users/{id}")
    public UserDto getUser(@PathVariable String id) {
        return userService.getUser(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Cacheable(cacheNames = USERS)
    @GetMapping("/api/v1/users")
    public List<UserDto> getUsers(@RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size,
                                  @RequestParam(required = false) String keyword,
                                  @RequestParam(required = false) String sort,
                                  @RequestParam(defaultValue = "true") boolean ascending) {
        return userService.getUsers(page, size, keyword, sort, ascending);
    }

    @PostMapping("/api/v1/users")
    public ResponseEntity<?> saveUser(@Valid @RequestBody UserInput userInput, HttpServletRequest request) {
        User user = userService.saveUser(userInput.withOperator(request.getUserPrincipal().getName()));
        return ResponseEntity.created(URI.create(request.getRequestURL().append("/").append(user.getId()).toString())).build();
    }

    @PutMapping("/api/v1/users/{id}")
    public ResponseEntity<?> updateUser(@PathVariable String id, @Valid @RequestBody UserUpdate userInput, Principal principal) {
        userService.updateUser(userInput.withId(id).withOperator(principal.getName()));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/api/v1/users/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable String id, Principal principal) {
        userService.deleteUser(id, principal.getName());
        return ResponseEntity.noContent().build();
    }

    @Cacheable(cacheNames = USER, key = "#principal.getName()")
    @GetMapping("/user")
    public UserDto getUser(Principal principal) {
        return userService.getUserByName(principal.getName()).orElseThrow(ResourceNotFoundException::new);
    }

    @PostMapping("/api/v1/changepassword")
    public ResponseEntity<?> changepassword(@Valid @RequestBody ChangePasswordInput changePasswordInput, Principal principal) {
        userService.changepassword(changePasswordInput);
        return ResponseEntity.noContent().build();
    }

}
