package com.ucm.ucmamm.user.roles;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class RoleDto {

    String roleName;

    String title;

    String description;

}
