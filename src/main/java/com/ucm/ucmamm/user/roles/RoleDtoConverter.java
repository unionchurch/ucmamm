package com.ucm.ucmamm.user.roles;

import com.ucm.ucmamm.domain.Role;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RoleDtoConverter implements Converter<Role, RoleDto> {

    @Override
    public RoleDto convert(Role role) {
        return RoleDto.builder()
                .roleName(role.getRoleName())
                .title(role.getTitle())
                .description(role.getDescription())
                .build();
    }

}
