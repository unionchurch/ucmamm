package com.ucm.ucmamm.user.roles;

import com.ucm.ucmamm.domain.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;
    private final ConversionService conversionService;

    List<RoleDto> getAllRoles() {
        return roleRepository.findAll(Sort.by(Sort.Direction.ASC, "order"))
                .stream()
                .map(role -> conversionService.convert(role, RoleDto.class))
                .collect(toList());
    }

}
