package com.ucm.ucmamm.user.roles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RoleResource {

    @Autowired
    private RoleService roleService;

    @GetMapping("/api/v1/roles")
    public List<RoleDto> getAllRoles() {
        return roleService.getAllRoles();
    }

}
