package com.ucm.ucmamm.user;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.RoleRepository;
import com.ucm.ucmamm.domain.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserConverter implements Converter<UserInput, User> {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private DateService dateService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User convert(UserInput userInput) {
        String userName = StringUtils.lowerCase(userInput.getUserName());
        return User.builder()
                .userName(userName)
                .password(passwordEncoder.encode(userInput.getPassword()))
                .firstName(userInput.getFirstName())
                .middleName(userInput.getMiddleName())
                .lastName(userInput.getLastName())
                .active(Boolean.TRUE)
                .deleted(Boolean.FALSE)
                .version(1)
                .createdBy(userInput.getOperator())
                .createdAt(dateService.getCurrentTime())
                .roles(roleRepository.findByRoleNameIn(userInput.getRoles()))
                .build();
    }

}
