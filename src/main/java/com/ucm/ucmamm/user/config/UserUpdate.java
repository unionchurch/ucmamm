package com.ucm.ucmamm.user.config;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserUpdate {

    String userName;

    String password;

    String firstName;

    String middleName;

    String lastName;

    List<String> roles;

}
