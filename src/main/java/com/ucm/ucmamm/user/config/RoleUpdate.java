package com.ucm.ucmamm.user.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleUpdate {

    String roleName;

    String title;

    String description;

    Boolean deleted;

    Boolean active;

    Integer version;

    Integer order;

}
