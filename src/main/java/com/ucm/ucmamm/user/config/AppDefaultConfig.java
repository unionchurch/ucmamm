package com.ucm.ucmamm.user.config;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AppDefaultConfig {

    String appVersion;

    @Builder.Default
    List<RoleUpdate> roles = new ArrayList<>();

    @Builder.Default
    List<UserUpdate> users = new ArrayList<>();

}
