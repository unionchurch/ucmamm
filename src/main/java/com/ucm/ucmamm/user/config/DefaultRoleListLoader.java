package com.ucm.ucmamm.user.config;

import com.ucm.ucmamm.common.CollectionUtils;
import com.ucm.ucmamm.domain.ApplicationInfoRepository;
import com.ucm.ucmamm.domain.Role;
import com.ucm.ucmamm.domain.RoleRepository;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.time.Instant.now;
import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultRoleListLoader {

    private final ApplicationInfoRepository applicationInfoRepository;
    private final RoleRepository roleRepository;

    public void loadDefaultRoles(AppDefaultConfig appDefaultConfig, String savedAppVersion) {
        ofNullable(appDefaultConfig)
                .filter(config -> CollectionUtils.isNotNullOrEmpty(config.getRoles()))
                .filter(config -> StringUtils.isNotBlank(config.getAppVersion()))
                .filter(config -> isTargetAppVersionHigherThanSaved(config.getAppVersion(), savedAppVersion))
                .ifPresent(this::updateRoles);
    }

    private boolean isTargetAppVersionHigherThanSaved(String updateConfigVersion, String savedAppVersion) {
        Version targetAppVersion = convertToVersionObject(updateConfigVersion);
        Version currentAppVersion = convertToVersionObject(savedAppVersion);
        if (targetAppVersion.getMajor() > currentAppVersion.getMajor()) {
            return true;
        } else if (targetAppVersion.getMajor() == currentAppVersion.getMajor() &&
                targetAppVersion.getMinor() > currentAppVersion.getMinor()) {
            return true;
        } else if (targetAppVersion.getMajor() == currentAppVersion.getMajor() &&
                targetAppVersion.getMinor() == currentAppVersion.getMinor() &&
                targetAppVersion.getHotfix() > currentAppVersion.getHotfix()) {
            return true;
        }
        return false;
    }

    private Version convertToVersionObject(String appVersion) {
        return ofNullable(appVersion)
                .map(string -> string.split("\\.", 3))
                .map(parts  -> Version.builder()
                        .major(Integer.parseInt(parts[0]))
                        .minor(Integer.parseInt(parts[1]))
                        .hotfix(Integer.parseInt(parts[2]))
                        .build())
                .orElse(Version.builder().major(0).minor(0).hotfix(0).build());
    }

    private void updateRoles(AppDefaultConfig appDefaultConfig) {
        List<RoleUpdate> newRoleList = appDefaultConfig.getRoles();
        log.info("Updating User Roles...");
        Map<String, Role> roleMap = buildRoleMap(newRoleList);
        newRoleList.forEach(roleUpdate ->
                roleMap.put(roleUpdate.getRoleName(), convertToRole(roleUpdate, roleMap.get(roleUpdate.getRoleName()))));
        roleRepository.saveAll(roleMap.values());
        log.info("User Roles updated!");
    }

    private Map<String, Role> buildRoleMap(List<RoleUpdate> newRoleList) {
        return roleRepository.findByRoleNameIn
                (newRoleList.stream()
                        .map(RoleUpdate::getRoleName)
                        .collect(Collectors.toList()))
                .stream()
                .collect(toMap(Role::getRoleName, identity()));
    }

    private Role convertToRole(RoleUpdate roleUpdate, Role role) {
        return ofNullable(role)
                .map(r -> convertToRole(roleUpdate)
                        .withId(r.getId())
                        .withCreatedAt(r.getCreatedAt())
                        .withCreatedBy(r.getCreatedBy())
                        .withUpdatedAt(now())
                        .withUpdatedBy("SYSTEM"))
                .orElseGet(() -> convertToRole(roleUpdate)
                        .withCreatedAt(now())
                        .withCreatedBy("SYSTEM"));
    }

    private Role convertToRole(RoleUpdate roleUpdate) {
        return Role.builder()
                .roleName(roleUpdate.getRoleName())
                .title(roleUpdate.getTitle())
                .description(roleUpdate.getDescription())
                .active(roleUpdate.getActive())
                .deleted(roleUpdate.getDeleted())
                .version(roleUpdate.getVersion())
                .order(roleUpdate.getOrder())
                .build();
    }

    @Value
    @Builder
    private static class Version {
        int major;
        int minor;
        int hotfix;
    }

}
