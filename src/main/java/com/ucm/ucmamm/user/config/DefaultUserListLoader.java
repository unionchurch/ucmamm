package com.ucm.ucmamm.user.config;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.RoleRepository;
import com.ucm.ucmamm.domain.User;
import com.ucm.ucmamm.domain.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultUserListLoader {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final DateService dateService;
    private final PasswordEncoder passwordEncoder;

    public void loadDefaultUsers(AppDefaultConfig appDefaultConfig) {
        if (appDefaultConfig.getUsers() != null) {
            appDefaultConfig.getUsers().forEach(userUpdate -> userRepository.save(convertToUser(userUpdate)));
        }
    }

    private User convertToUser(UserUpdate userUpdate) {
        User user = findUser(userUpdate);
        if (user.getId() == null) {
            log.info("Creating user: " + user.getUserName());
        } else {
            user = User.builder()
                    .id(user.getId())
                    .userName(user.getUserName())
                    .firstName(userUpdate.getFirstName())
                    .middleName(userUpdate.getMiddleName())
                    .lastName(userUpdate.getLastName())
                    .roles(roleRepository.findByRoleNameIn(userUpdate.getRoles()))
                    .password(user.getPassword())
                    .deleted(FALSE)
                    .active(TRUE)
                    .version((user.getVersion() != null ? user.getVersion() : 0) + 1)
                    .createdAt(user.getCreatedAt())
                    .createdBy(user.getCreatedBy())
                    .updatedAt(dateService.getCurrentTime())
                    .updatedBy("SYSTEM")
                    .build();
            log.info("Updating user: " + user.getUserName());
        }
        return user;
    }

    private User findUser(UserUpdate userUpdate) {
        return userRepository.findByUserNameAndDeleted(userUpdate.getUserName(), FALSE)
                .orElseGet(() -> User.builder()
                        .userName(userUpdate.getUserName())
                        .firstName(userUpdate.getFirstName())
                        .middleName(userUpdate.getMiddleName())
                        .lastName(userUpdate.getLastName())
                        .password(passwordEncoder.encode(userUpdate.getPassword()))
                        .roles(roleRepository.findByRoleNameIn(userUpdate.getRoles()))
                        .deleted(FALSE)
                        .active(TRUE)
                        .version(1)
                        .createdAt(dateService.getCurrentTime())
                        .createdBy("SYSTEM")
                        .build());
    }

}
