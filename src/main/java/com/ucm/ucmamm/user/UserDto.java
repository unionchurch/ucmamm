package com.ucm.ucmamm.user;

import lombok.Builder;
import lombok.Value;

import java.util.Set;

@Value
@Builder
public class UserDto {

    String id;

    String userName;

    String firstName;

    String middleName;

    String lastName;

    Boolean deleted;

    Boolean active;

    Integer version;

    Set<String> roles;

}
