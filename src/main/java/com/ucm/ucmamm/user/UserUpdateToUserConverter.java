package com.ucm.ucmamm.user;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.common.ResourceNotFoundException;
import com.ucm.ucmamm.domain.RoleRepository;
import com.ucm.ucmamm.domain.User;
import com.ucm.ucmamm.domain.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static java.util.Optional.ofNullable;

@Component
public class UserUpdateToUserConverter implements Converter<UserUpdate, User> {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private DateService dateService;

    @Override
    public User convert(UserUpdate userUpdate) {
        return getUserDomain(userUpdate)
                .map(user -> User.builder()
                        .id(user.getId())
                        .userName(user.getUserName())
                        .password(getNewPassword(userUpdate.getPassword(), user.getPassword()))
                        .firstName(userUpdate.getFirstName())
                        .middleName(userUpdate.getMiddleName())
                        .lastName(userUpdate.getLastName())
                        .active(user.getActive())
                        .deleted(user.getDeleted())
                        .version(user.getVersion() + 1)
                        .createdBy(user.getCreatedBy())
                        .createdAt(user.getCreatedAt())
                        .updatedBy(userUpdate.getOperator())
                        .updatedAt(dateService.getCurrentTime())
                        .roles(roleRepository.findByRoleNameIn(userUpdate.getRoles()))
                        .build())
                .orElseThrow(ResourceNotFoundException::new);
    }

    private String getNewPassword(String updatePassword, String currentPassword) {
        if (StringUtils.isBlank(updatePassword)) {
            return currentPassword;
        }
        return updatePassword;
    }

    private Optional<User> getUserDomain(UserUpdate userInput) {
        return ofNullable(userInput.getId()).flatMap(id -> userRepository.findByIdAndDeleted(id, Boolean.FALSE));
    }

}
