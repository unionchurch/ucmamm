package com.ucm.ucmamm.user;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.constraints.NotBlank;
import java.util.Collections;
import java.util.Set;

@Value
@Builder
public class UserInput {

    @NotBlank
    @UniqueUserName
    String userName;

    @NotBlank
    String password;

    @NotBlank
    String firstName;

    String middleName;

    @NotBlank
    String lastName;

    @Builder.Default
    Set<String> roles = Collections.emptySet();

    @Wither
    String operator;

}
