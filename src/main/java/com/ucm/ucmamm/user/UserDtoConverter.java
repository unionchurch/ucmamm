package com.ucm.ucmamm.user;

import com.ucm.ucmamm.domain.Role;
import com.ucm.ucmamm.domain.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static java.util.stream.Collectors.toSet;

@Component
public class UserDtoConverter implements Converter<User, UserDto> {

    @Override
    public UserDto convert(User user) {
        return UserDto.builder()
                .id(user.getId())
                .userName(user.getUserName())
                .firstName(user.getFirstName())
                .middleName(user.getMiddleName())
                .lastName(user.getLastName())
                .active(user.getActive())
                .deleted(user.getDeleted())
                .version(user.getVersion())
                .roles(user.getRoles().stream().map(Role::getRoleName).collect(toSet()))
                .build();
    }

}
