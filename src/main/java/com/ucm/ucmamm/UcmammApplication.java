package com.ucm.ucmamm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@EnableCaching
@EnableScheduling
@EnableWebSecurity
@SpringBootApplication
public class UcmammApplication {

	public static void main(String[] args) {
		SpringApplication.run(UcmammApplication.class, args);
	}

}
