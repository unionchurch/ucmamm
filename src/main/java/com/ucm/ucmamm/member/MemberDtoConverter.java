package com.ucm.ucmamm.member;

import com.ucm.ucmamm.domain.Member;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class MemberDtoConverter implements Converter<Member, MemberDto> {

    @Override
    public MemberDto convert(Member member) {
        return MemberDto.builder()
                .id(member.getId())
                .memberId(member.getMemberId())
                .firstName(member.getFirstName())
                .middleName(member.getMiddleName())
                .lastName(member.getLastName())
                .suffix(member.getSuffix())
                .phoneNumbers(member.getPhoneNumbers())
                .emailAddresses(member.getEmailAddresses())
                .streetAddress(member.getStreetAddress())
                .village(member.getVillage())
                .city(member.getCity())
                .province(member.getProvince())
                .zipCode(member.getZipCode())
                .country(member.getCountry())
                .deleted(member.getDeleted())
                .active(member.getActive())
                .version(member.getVersion())
                .build();
    }

}
