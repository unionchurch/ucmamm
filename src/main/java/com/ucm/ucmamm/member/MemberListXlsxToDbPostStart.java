package com.ucm.ucmamm.member;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.*;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang3.StringUtils.defaultString;
import static org.apache.poi.ss.usermodel.CellType.NUMERIC;
import static org.apache.poi.ss.usermodel.CellType.STRING;

@Slf4j
@Component
public class MemberListXlsxToDbPostStart implements CommandLineRunner {

    @Autowired
    private MemberService memberService;

    @Override
    public void run(String... args) throws Exception {
        List<MemberDto> members = memberService.getMembers(0, 10, null, null, TRUE, "");
        if (CollectionUtils.isEmpty(members)) {
            log.info("No members saved yet. Initial member list will loaded to the database.");
            Resource resource = new ClassPathResource("2018_ucm_members.xls");
            if (resource.exists()) {
                try (InputStream inputStream = resource.getInputStream()) {
                    ofNullable(WorkbookFactory.create(inputStream))
                            .map(workbook -> workbook.getSheet("2017 Members' List"))
                            .map(Sheet::rowIterator)
                            .ifPresent(this::iterateRows);
                    log.info("Initial member list loaded!");
                }
            } else {
                log.warn("File 2018_ucm_members.xls is not found.");
            }
        } else {
            log.info("Initial member data already exists!");
        }
    }

    private void iterateRows(Iterator<Row> rowIterator) {
        List<MemberInput> members = new ArrayList<>();
        Long memberIdOverride = null;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();

            if (hasReachedInactiveRows(row)) {
                members.sort(Comparator.comparing(MemberInput::getMemberId));
                MemberInput lastMemberParsed = members.get(members.size() - 1);
                memberIdOverride = lastMemberParsed.getMemberId() + 1;
            }

            MemberInput member1 = parseMemberInfo(row, 0, memberIdOverride);
            if (member1 != null) {
                members.add(member1);
                if (memberIdOverride != null) {
                    memberIdOverride++;
                }
            }

            MemberInput member2 = parseMemberInfo(row, 4, memberIdOverride);
            if (member2 != null) {
                members.add(member2);
                if (memberIdOverride != null) {
                    memberIdOverride++;
                }
            }
        }
        members.forEach(this::process);
    }

    private void process(MemberInput member) {
        MemberInput enhancedMember = enhanceMemberData(member);
        memberService.saveMember(enhancedMember);
    }

    private MemberInput enhanceMemberData(MemberInput member) {
        List<String> firstNameWords = Arrays.asList(member.getFirstName().split("[ ,]"));
        if (firstNameWords.size() > 1) {
            String lastWord = firstNameWords.get(firstNameWords.size() - 1);
            if (lastWord.matches("(Jr|Sr)\\.") || lastWord.matches("[IVXLC]+")) {
                return member.withSuffix(lastWord)
                        .withFirstName(firstNameWords.subList(0, firstNameWords.size() - 1).stream().map(String::trim).collect(joining(" ")));
            } else if (lastWord.matches("[A-Z]\\.")) {
                return member.withMiddleName(lastWord)
                        .withFirstName(firstNameWords.subList(0, firstNameWords.size() - 1).stream().map(String::trim).collect(joining(" ")));
            }
        }
        return member;
    }

    private boolean hasReachedInactiveRows(Row row) {
        Cell cell = row.getCell(0);
        return cell != null &&
                STRING.equals(cell.getCellType()) &&
                cell.getStringCellValue() != null &&
                cell.getStringCellValue().startsWith("INACTIVE");
    }

    private MemberInput parseMemberInfo(Row row, int cellOffset, Long memberIdOverride) {
        Cell cell = row.getCell(cellOffset);
        if (cell != null && NUMERIC.equals(cell.getCellType())) {
            return MemberInput.builder()
                    .memberId(memberIdOverride != null ? memberIdOverride : (long) cell.getNumericCellValue())
                    .firstName(defaultString(row.getCell(cellOffset + 2).getStringCellValue()).trim())
                    .lastName(defaultString(row.getCell(cellOffset + 1).getStringCellValue()).trim())
                    .build();
        }
        return null;
    }

}
