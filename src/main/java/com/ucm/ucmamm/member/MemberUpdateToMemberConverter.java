package com.ucm.ucmamm.member;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.common.ResourceNotFoundException;
import com.ucm.ucmamm.domain.Member;
import com.ucm.ucmamm.domain.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MemberUpdateToMemberConverter implements Converter<MemberUpdate, Member> {

    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private DateService dateService;

    @Override
    public Member convert(MemberUpdate memberUpdate) {
        return getMemberDomain(memberUpdate)
                .map(member -> Member.builder()
                        .id(memberUpdate.getId())
                        .memberId(member.getMemberId())
                        .firstName(memberUpdate.getFirstName())
                        .middleName(memberUpdate.getMiddleName())
                        .lastName(memberUpdate.getLastName())
                        .suffix(memberUpdate.getSuffix())
                        .phoneNumbers(memberUpdate.getPhoneNumbers())
                        .emailAddresses(memberUpdate.getEmailAddresses())
                        .streetAddress(memberUpdate.getStreetAddress())
                        .village(memberUpdate.getVillage())
                        .city(memberUpdate.getCity())
                        .province(memberUpdate.getProvince())
                        .zipCode(memberUpdate.getZipCode())
                        .country(memberUpdate.getCountry())
                        .deleted(Boolean.FALSE)
                        .active(memberUpdate.isActive())
                        .version(member.getVersion() + 1)
                        .createdBy(member.getCreatedBy())
                        .createdAt(member.getCreatedAt())
                        .updatedBy(memberUpdate.getOperator())
                        .updatedAt(dateService.getCurrentTime())
                        .build())
                .orElseThrow(ResourceNotFoundException::new);
    }

    private Optional<Member> getMemberDomain(MemberUpdate memberUpdate) {
        return memberRepository.findByIdAndDeleted(memberUpdate.getId(), Boolean.FALSE);
    }

}
