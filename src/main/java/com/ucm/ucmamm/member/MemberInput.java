package com.ucm.ucmamm.member;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Value
@Builder
public class MemberInput {

    @UniqueMemberId
    Long memberId;

    @Wither
    @NotBlank
    String firstName;

    @Wither
    String middleName;

    @NotBlank
    String lastName;

    @Wither
    String suffix;

    List<String> phoneNumbers;

    List<String> emailAddresses;

    String streetAddress;

    String village;

    String city;

    String province;

    String zipCode;

    String country;

    @Wither
    String operator;

}
