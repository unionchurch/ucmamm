package com.ucm.ucmamm.member;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.ucm.ucmamm.domain.MemberCountRepository.MEMBER_COUNT_LOOKUP_NAME;
import static com.ucm.ucmamm.domain.MemberIDRepository.LOOKUP_NAME;
import static java.util.Optional.ofNullable;

@Component
public class MemberConverter implements Converter<MemberInput, Member> {

    @Autowired
    private MemberIDRepository memberIDRepository;
    @Autowired
    private MemberCountRepository memberCountRepository;
    @Autowired
    private DateService dateService;

    @Override
    public Member convert(MemberInput memberInput) {
        return Member.builder()
                .memberId(getNextMemberIdAndIncrement())
                .firstName(memberInput.getFirstName())
                .middleName(memberInput.getMiddleName())
                .lastName(memberInput.getLastName())
                .suffix(memberInput.getSuffix())
                .phoneNumbers(memberInput.getPhoneNumbers())
                .emailAddresses(memberInput.getEmailAddresses())
                .streetAddress(memberInput.getStreetAddress())
                .village(memberInput.getVillage())
                .city(memberInput.getCity())
                .province(memberInput.getProvince())
                .zipCode(memberInput.getZipCode())
                .country(memberInput.getCountry())
                .deleted(Boolean.FALSE)
                .active(Boolean.TRUE)
                .version(1)
                .createdBy(memberInput.getOperator())
                .createdAt(dateService.getCurrentTime())
                .build();
    }

    private Long getNextMemberIdAndIncrement() {
        MemberID memberID = ofNullable(memberIDRepository.findNextMemberID())
                .orElseGet(() -> MemberID.builder().name(LOOKUP_NAME).nextValue(1L).build());
        long nextValue = memberID.getNextValue();
        memberIDRepository.save(memberID.withNextValue(nextValue + 1));

        MemberCount memberCount = ofNullable(memberCountRepository.findMemberCount())
                .orElseGet(() -> MemberCount.builder().name(MEMBER_COUNT_LOOKUP_NAME).active(0L).total(0L).build());
        memberCountRepository.save(memberCount.withActive(memberCount.getActive() + 1).withTotal(memberCount.getTotal() + 1));

        return nextValue;
    }

}
