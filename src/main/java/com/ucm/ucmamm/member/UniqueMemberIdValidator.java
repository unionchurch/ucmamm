package com.ucm.ucmamm.member;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueMemberIdValidator implements ConstraintValidator<UniqueMemberId, Long> {

    @Autowired
    private MemberService memberService;

    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return !memberService.getMemberByMemberId(value).isPresent();
    }

}
