package com.ucm.ucmamm.member;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class MemberDto {

    String id;

    Long memberId;

    String firstName;

    String middleName;

    String lastName;

    String suffix;

    List<String> phoneNumbers;

    List<String> emailAddresses;

    String streetAddress;

    String village;

    String city;

    String province;

    String zipCode;

    String country;

    Boolean deleted;

    Boolean active;

    Integer version;

}
