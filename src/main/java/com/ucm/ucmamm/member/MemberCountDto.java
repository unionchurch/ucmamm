package com.ucm.ucmamm.member;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class MemberCountDto {

    Long active;

    Long total;

}
