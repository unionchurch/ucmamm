package com.ucm.ucmamm.member;

import com.ucm.ucmamm.common.ResourceNotFoundException;
import com.ucm.ucmamm.domain.Member;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.security.Principal;
import java.util.List;

import static com.ucm.ucmamm.common.Caches.MEMBER;
import static com.ucm.ucmamm.common.Caches.MEMBERS;

@Slf4j
@RestController
public class MemberResource {

    @Autowired
    private MemberService memberService;

    @Cacheable(cacheNames = MEMBER)
    @GetMapping("/api/v1/members/{id}")
    public MemberDto getMember(@PathVariable String id) {
        return memberService.getMember(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Cacheable(cacheNames = MEMBER)
    @GetMapping("/api/v1/members/exact/{memberId}")
    public MemberDto getMemberByMemberId(@PathVariable Long memberId) {
        return memberService.getMemberByMemberId(memberId).orElseThrow(ResourceNotFoundException::new);
    }

    @Cacheable(cacheNames = MEMBERS)
    @GetMapping("/api/v1/members")
    public List<MemberDto> getMembers(@RequestParam(defaultValue = "0") int page,
                                      @RequestParam(defaultValue = "10") int size,
                                      @RequestParam(required = false) String keyword,
                                      @RequestParam(required = false) String sort,
                                      @RequestParam(defaultValue = "true") boolean ascending,
                                      @RequestParam(defaultValue = "") String activeFilter) {
        return memberService.getMembers(page, size, keyword, sort, ascending, activeFilter);
    }

    @Cacheable(cacheNames = MEMBERS)
    @GetMapping("/api/v1/members/exact")
    public List<MemberDto> getMembers(@RequestParam String firstName, @RequestParam String lastName) {
        return memberService.getMembers(firstName, lastName);
    }

    @PostMapping("/api/v1/members")
    public ResponseEntity<?> saveMember(@Valid @RequestBody MemberInput memberInput, HttpServletRequest request) {
        Member member = memberService.saveMember(memberInput.withOperator(request.getUserPrincipal().getName()));
        return ResponseEntity.created(URI.create(request.getRequestURL().append("/").append(member.getId()).toString())).build();
    }

    @PutMapping("/api/v1/members/{id}")
    public ResponseEntity<?> updateMember(@PathVariable String id, @Valid @RequestBody MemberUpdate memberInput, Principal principal) {
        memberService.updateMember(memberInput.withId(id).withOperator(principal.getName()));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/api/v1/members/{id}")
    public ResponseEntity<?> deleteMember(@PathVariable String id, Principal principal) {
        memberService.deleteMember(id, principal.getName());
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/api/v1/members/count")
    public MemberCountDto getMembersCount() {
        return memberService.getMemberCount();
    }

    @PostMapping("/api/v1/members/upload")
    public ResponseEntity<?> uploadMembersExcelFile(@RequestParam("file") MultipartFile file, @RequestParam(defaultValue = "false") boolean saveAsActive) throws IOException {
        memberService.uploadMembers(file.getInputStream(), saveAsActive);
        return ResponseEntity.noContent().build();
    }

}
