package com.ucm.ucmamm.member;

import com.ucm.ucmamm.common.CacheService;
import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.Member;
import com.ucm.ucmamm.domain.MemberCount;
import com.ucm.ucmamm.domain.MemberCountRepository;
import com.ucm.ucmamm.domain.MemberRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Stream;

import static com.ucm.ucmamm.common.Caches.MEMBER;
import static com.ucm.ucmamm.common.Caches.MEMBERS;
import static com.ucm.ucmamm.domain.MemberCountRepository.MEMBER_COUNT_LOOKUP_NAME;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.data.domain.Sort.Direction.DESC;

@Slf4j
@Service
@RequiredArgsConstructor
public class MemberService {

    private static final String ACTIVE_ONLY = "activeOnly";
    private static final String INACTIVE_ONLY = "inactiveOnly";

    private final MongoTemplate mongoTemplate;
    private final MemberRepository memberRepository;
    private final MemberCountRepository memberCountRepository;
    private final ConversionService conversionService;
    private final DateService dateService;
    private final CacheService cacheService;
    private final MemberDtoConverter memberDtoConverter;

    Member saveMember(MemberInput memberInput) {
        Member member = memberRepository.save(conversionService.convert(memberInput, Member.class));
        cacheService.evictCache(MEMBERS);
        return member;
    }

    void updateMember(MemberUpdate memberUpdate) {
        memberRepository.findByMemberIdAndDeleted(memberUpdate.getMemberId(), FALSE).ifPresent(member -> {
            memberRepository.save(conversionService.convert(memberUpdate, Member.class));
            if (BooleanUtils.toBoolean(member.getActive()) != memberUpdate.isActive()) {
                MemberCount memberCount = ofNullable(memberCountRepository.findMemberCount())
                        .orElse(MemberCount.builder().name(MEMBER_COUNT_LOOKUP_NAME).active(0L).total(0L).build());
                memberCountRepository.save(memberCount.withActive(memberCount.getActive() + (memberUpdate.isActive() ? 1 : -1)));
            }
            cacheService.evictCache(MEMBERS);
            cacheService.evictCache(MEMBER, memberUpdate.getId());
        });
    }

    void deleteMember(String id, String operator) {
        memberRepository.findById(id)
                .map(member -> member.withDeleted(TRUE).withUpdatedBy(operator).withUpdatedAt(dateService.getCurrentTime()))
                .ifPresent(member -> {
                    memberRepository.save(member);
                    MemberCount memberCount = ofNullable(memberCountRepository.findMemberCount())
                            .orElse(MemberCount.builder().name(MEMBER_COUNT_LOOKUP_NAME).active(isTrue(member.getActive()) ? 1L : 0L).total(1L).build());
                    memberCountRepository.save(memberCount.withActive(memberCount.getActive() - (isTrue(member.getActive()) ? 1 : 0)).withTotal(memberCount.getTotal() - 1));
                });
        cacheService.evictCache(MEMBERS);
        cacheService.evictCache(MEMBER, id);
    }

    Optional<MemberDto> getMember(String id) {
        return memberRepository.findByIdAndDeleted(id, FALSE)
                .map(member -> conversionService.convert(member, MemberDto.class));
    }

    Optional<MemberDto> getMemberByMemberId(Long memberId) {
        return memberRepository.findByMemberIdAndDeleted(memberId, FALSE)
                .map(member -> conversionService.convert(member, MemberDto.class));
    }

    List<MemberDto> getMembers(int page, int pageSize, String keyword, String sortField, boolean ascending, String activeFilter) {
        return ofNullable(getMemberPage(page, pageSize, keyword, sortField, ascending, activeFilter))
                .filter(Slice::hasContent)
                .map(Slice::getContent)
                .map(members -> members.stream().map(memberDtoConverter::convert).collect(toList()))
                .orElse(Collections.emptyList());
    }

    List<MemberDto> getMembers(String firstName, String lastName) {
        return memberRepository.findByFirstNameAndLastName(firstName, lastName, new Sort(Sort.Direction.ASC, "firstName")).stream()
                .map(member -> conversionService.convert(member, MemberDto.class))
                .collect(toList());
    }

    MemberCountDto getMemberCount() {
        MemberCount memberCount = ofNullable(memberCountRepository.findMemberCount())
                .orElse(MemberCount.builder().active(0L).active(0L).build());
        return MemberCountDto.builder().active(memberCount.getActive()).total(memberCount.getTotal()).build();
    }

    void uploadMembers(InputStream inputStream, boolean saveAsActive) throws IOException {
        if (saveAsActive) {
            Query query = new Query();
            Update update = new Update();
            update.set("active", FALSE);
            mongoTemplate.updateMulti(query, update, Member.class);

            MemberCount memberCount = ofNullable(memberCountRepository.findMemberCount())
                    .orElse(MemberCount.builder().name(MEMBER_COUNT_LOOKUP_NAME).active(0L).total(0L).build());
            memberCountRepository.save(memberCount.withActive(0L));
        }
        ofNullable(WorkbookFactory.create(inputStream))
                .map(workbook -> workbook.getSheetAt(0))
                .map(Sheet::rowIterator)
                .ifPresent(rowIterator -> iterateExcelRows(rowIterator, saveAsActive));
        log.info("Member List updated!");
    }

    private void iterateExcelRows(Iterator<Row> rowIterator, boolean saveAsActive) {
        int consecutiveBlankFirstCell = 0;
        while (rowIterator.hasNext() && consecutiveBlankFirstCell < 5) {
            Row row = rowIterator.next();
            Cell firstCell = row.getCell(0);
            if (firstCell != null && firstCell.getCellType().equals(CellType.NUMERIC)) {
                MemberInput memberInput = enhanceMemberData(
                        MemberInput.builder()
                                .lastName(getCellStringValue(row.getCell(1)))
                                .firstName(getCellStringValue(row.getCell(2)))
                                .middleName(getCellStringValue(row.getCell(3)))
                                .build());
                createOrUpdateMember(memberInput, saveAsActive);
            } else {
                consecutiveBlankFirstCell++;
            }
        }
    }

    private String getCellStringValue(Cell cell) {
        return ofNullable(cell)
                .map(Cell::getStringCellValue)
                .map(String::trim)
                .orElse("");
    }

    private MemberInput enhanceMemberData(MemberInput member) {
        List<String> firstNameWords = Arrays.asList(member.getFirstName().split("[ ,]"));
        if (firstNameWords.size() > 1) {
            String lastWord = firstNameWords.get(firstNameWords.size() - 1);
            if (lastWord.matches("(Jr|Sr)\\.") || lastWord.matches("[IVXLC]+")) {
                return member.withSuffix(lastWord)
                        .withFirstName(firstNameWords.subList(0, firstNameWords.size() - 1).stream().map(String::trim).collect(joining(" ")).trim());
            } else if (lastWord.matches("[A-Z]\\.")) {
                return member.withMiddleName(lastWord)
                        .withFirstName(firstNameWords.subList(0, firstNameWords.size() - 1).stream().map(String::trim).collect(joining(" ")).trim());
            }
        }
        return member;
    }

    private void createOrUpdateMember(MemberInput memberInput, boolean saveAsActive) {
        Optional<Member> foundMember = memberRepository.findByFirstNameAndLastName(
                memberInput.getFirstName().replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)"),
                memberInput.getLastName().replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)"),
                new Sort(Sort.Direction.ASC, "firstName"))
                .stream()
                .findFirst();
        if (foundMember.isPresent()) {
            Member member = foundMember.get();
            updateMember(MemberUpdate.builder()
                    .id(member.getId())
                    .memberId(member.getMemberId())
                    .firstName(member.getFirstName())
                    .middleName(member.getMiddleName())
                    .lastName(member.getLastName())
                    .suffix(member.getSuffix())
                    .streetAddress(member.getStreetAddress())
                    .village(member.getVillage())
                    .city(member.getCity())
                    .province(member.getProvince())
                    .zipCode(member.getZipCode())
                    .country(member.getCountry())
                    .phoneNumbers(member.getPhoneNumbers())
                    .emailAddresses(member.getEmailAddresses())
                    .active(saveAsActive)
                    .build());
        } else {
            saveMember(memberInput);
        }
    }

    private Page<Member> getMemberPage(int page, int pageSize, String keyword, String sortField, boolean ascending, String activeFilter) {
        PageRequest pageRequest = PageRequest.of(page, pageSize, ascending ? ASC : DESC,
                ofNullable(sortField).filter(StringUtils::isNotBlank).orElse("lastName"));
        List<String> keywords = ofNullable(keyword).map(this::splitKeyword).orElse(Collections.emptyList());
        if (StringUtils.equalsAnyIgnoreCase(activeFilter, ACTIVE_ONLY, INACTIVE_ONLY)) {
            return keywords.isEmpty()
                    ? memberRepository.findByDeletedAndActive(FALSE, StringUtils.equalsIgnoreCase(activeFilter, ACTIVE_ONLY), pageRequest)
                    : memberRepository.findByFirstNameOrLastNameAndActive(keywords.get(0), keywords.get(1), activeFilter.equalsIgnoreCase(ACTIVE_ONLY), pageRequest);
        }
        return keywords.isEmpty()
                ? memberRepository.findByDeleted(FALSE, pageRequest)
                : memberRepository.findByFirstNameOrLastName(keywords.get(0), keywords.get(1), pageRequest);
    }

    private List<String> splitKeyword(String keyword) {
        List<String> keywords = Stream.of(keyword.split("[ ,]"))
                .filter(StringUtils::isNotBlank)
                .map(String::trim).collect(toList());
        if (keywords.size() == 1) {
            keywords.add(keywords.get(0));
        }
        return keywords;
    }

}
