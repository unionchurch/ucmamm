package com.ucm.ucmamm.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface MemberRepository extends MongoRepository<Member, String> {

    Optional<Member> findByIdAndDeleted(String id, Boolean deleted);

    Optional<Member> findByMemberIdAndDeleted(Long memberId, Boolean deleted);

    @Query(value = "{ $or: [{ $and: [{ firstName: { $regex: ?0, $options: 'i'} }, { lastName: { $regex: ?1, $options: 'i'} }] }, " +
            "{ $or: [{ lastName: { $regex: ?0, $options: 'i'} }, { firstName: { $regex: ?1, $options: 'i'} }] }], deleted: false }")
    Page<Member> findByFirstNameOrLastName(String firstNamePrefix, String lastNamePrefix, Pageable pageable);

    Page<Member> findByDeleted(Boolean deleted, Pageable pageable);

    @Query(value = "{ deleted: false }")
    List<Member> findAllRegisteredMembers(Sort sort);

    @Query(value = "{ $or: [{ $and: [{ firstName: { $regex: ?0, $options: 'i'} }, { lastName: { $regex: ?1, $options: 'i'} }] }, " +
            "{ $or: [{ lastName: { $regex: ?0, $options: 'i'} }, { firstName: { $regex: ?1, $options: 'i'} }] }], deleted: false, active: ?2}")
    Page<Member> findByFirstNameOrLastNameAndActive(String firstNamePrefix, String lastNamePrefix, Boolean active, Pageable pageable);

    Page<Member> findByDeletedAndActive(Boolean deleted, Boolean active, Pageable pageable);

    @Query(value = "{ firstName: { $regex: ?0, $options: 'i' }, lastName: { $regex: ?1, $options: 'i' }, deleted: false }")
    List<Member> findByFirstNameAndLastName(String firstName, String lastName, Sort sort);

}
