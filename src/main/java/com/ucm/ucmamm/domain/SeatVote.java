package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class SeatVote {

    String candidateId;

    Boolean approved;

}
