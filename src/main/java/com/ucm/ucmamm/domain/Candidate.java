package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Candidate {

    String id;

    Long memberId;

    String firstName;

    String middleName;

    String lastName;

    String suffix;

    Long approved;

    Long disapproved;

    public Candidate updateWith(Long memberId, String firstName, String middleName, String lastName, String suffix) {
        return Candidate.builder()
                .id(id)
                .memberId(memberId)
                .firstName(firstName)
                .middleName(middleName)
                .lastName(lastName)
                .suffix(suffix)
                .approved(approved)
                .disapproved(disapproved)
                .build();
    }

}
