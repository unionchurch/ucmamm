package com.ucm.ucmamm.domain;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface PositionRepository extends MongoRepository<Position, String> {

    Optional<Position> findByIdAndDeleted(String id, Boolean deleted);

    Optional<Position> findByNameAndDeleted(String name, Boolean deleted);

    List<Position> findByDeleted(Boolean deleted, Sort sort);

}
