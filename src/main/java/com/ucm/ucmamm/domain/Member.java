package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.List;

@Value
@Builder
@Document(collection = "members")
@CompoundIndexes({
        @CompoundIndex(name = "id_deleted", def = "{_id: 1, deleted: 1}"),
        @CompoundIndex(name = "memberId_deleted", def = "{memberId: 1, deleted: 1}"),
        @CompoundIndex(name = "firstName_lastName_deleted", def = "{firstName: 1, lastName: 1, deleted: 1}")
})
public class Member {

    @Id
    String id;

    @Wither
    @Indexed
    Long memberId;

    @Indexed
    String firstName;

    String middleName;

    @Indexed
    String lastName;

    String suffix;

    List<String> phoneNumbers;

    List<String> emailAddresses;

    String streetAddress;

    String village;

    String city;

    String province;

    String zipCode;

    String country;

    @Wither
    @Indexed
    Boolean deleted;

    Boolean active;

    Integer version;

    @Wither
    Instant createdAt;

    @Wither
    String createdBy;

    @Wither
    Instant updatedAt;

    @Wither
    String updatedBy;

}
