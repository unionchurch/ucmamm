package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.List;
import java.util.Map;

@Value
@Builder
@Document(collection = "votes")
@CompoundIndexes({
        @CompoundIndex(name = "memberId_electionId", def = "{memberId: 1, electionId: 1}"),
        @CompoundIndex(name = "tallied_deleted", def = "{tallied: 1, deleted: 1}")
})
public class Vote {

    @Id
    @Wither
    String id;

    Instant time;

    Long memberId;

    String electionId;

    Map<String, Boolean> proposals;

    Map<String, String> questions;

    Map<String, List<SeatVote>> seats;

    Boolean deleted;

    @Wither
    Boolean tallied;

    Boolean proxy;

}
