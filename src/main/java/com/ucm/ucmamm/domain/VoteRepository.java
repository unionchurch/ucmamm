package com.ucm.ucmamm.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface VoteRepository extends MongoRepository<Vote, String> {

    Optional<Vote> findByMemberIdAndElectionId(Long memberId, String electionId);

    Page<Vote> findByTalliedAndDeleted(Boolean tallied, Boolean deleted, Pageable pageRequest);

    List<Vote> findByElectionId(String electionId, Sort sort);

    long countByElectionIdAndDeleted(String electionId, Boolean deleted);

    long countByElectionIdAndProxyAndDeleted(String electionId, Boolean proxy, Boolean deleted);

}
