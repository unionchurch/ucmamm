package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ProxyDetails {

    Long memberId;

    String firstName;

    String middleName;

    String lastName;

    String suffix;

}
