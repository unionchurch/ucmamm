package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Value
@Builder
@Document(collection = "positions")
@CompoundIndexes({
        @CompoundIndex(name = "name_deleted", def = "{name: 1, deleted: 1}"),
        @CompoundIndex(name = "id_deleted", def = "{_id: 1, deleted: 1}")
})
public class Position {

    @Id
    String id;

    String name;

    Boolean deleted;

    Boolean active;

    Integer version;

    @Wither
    Instant createdAt;

    @Wither
    String createdBy;

    @Wither
    Instant updatedAt;

    @Wither
    String updatedBy;

}
