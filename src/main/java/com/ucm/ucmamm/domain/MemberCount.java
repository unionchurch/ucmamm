package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Value
@Builder
@Document(collection = "member_count")
public class MemberCount {

    @Id
    String id;

    @Indexed
    String name;

    @Wither
    Long active;

    @Wither
    Long total;

}
