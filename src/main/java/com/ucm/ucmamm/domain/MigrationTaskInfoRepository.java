package com.ucm.ucmamm.domain;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface MigrationTaskInfoRepository extends MongoRepository<MigrationTaskInfo, String> {

    Optional<MigrationTaskInfo> findByName(String name);

}
