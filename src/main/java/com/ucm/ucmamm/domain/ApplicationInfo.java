package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Value
@Builder
@Document(collection = "app_info")
public class ApplicationInfo {

    @Id
    String id;

    String version;

}
