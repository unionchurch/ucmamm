package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

@Value
@Builder
public class Answer {

    String id;

    @Wither
    String content;

    Long selected;

}
