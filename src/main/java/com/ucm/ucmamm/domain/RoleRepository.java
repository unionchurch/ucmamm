package com.ucm.ucmamm.domain;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;
import java.util.Set;

public interface RoleRepository extends MongoRepository<Role, String> {

    Set<Role> findByRoleNameIn(Iterable<String> roleNames);

    Optional<Role> findByRoleName(String roleName);

}
