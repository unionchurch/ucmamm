package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Value
@Builder
@Document(collection = "voters")
@CompoundIndexes({
        @CompoundIndex(name = "memberId_electionId", def = "{memberId: 1, electionId: 1}"),
        @CompoundIndex(name = "electionId_proxy", def = "{electionId: 1, proxy: 1}")
})
public class Voter {

    @Id
    String id;

    @Indexed
    String electionId;

    Long memberId;

    @Indexed
    String firstName;

    String middleName;

    @Indexed
    String lastName;

    String suffix;

    @Wither
    @Indexed
    boolean proxy;

    @Wither
    ProxyDetails proxyDetails;

    public Voter withProxyDetails(Long memberId, String firstName, String middleName, String lastName, String suffix) {
        return withProxyDetails(ProxyDetails.builder()
                .memberId(memberId)
                .firstName(firstName)
                .middleName(middleName)
                .lastName(lastName)
                .suffix(suffix)
                .build());
    }

    @Wither
    Instant createdAt;

    @Wither
    String createdBy;

}
