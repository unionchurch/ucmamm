package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.Collections;
import java.util.Set;

@Value
@Builder
@Document(collection = "users")
@CompoundIndexes({
        @CompoundIndex(name = "userName_deleted", def = "{userName: 1, deleted: 1}"),
        @CompoundIndex(name = "id_deleted", def = "{_id: 1, deleted: 1}")
})
public class User {

    @Id
    String id;

    String userName;

    @Wither
    String password;

    String firstName;

    String middleName;

    String lastName;

    @Wither
    @Indexed
    Boolean deleted;

    Boolean active;

    Integer version;

    @Builder.Default
    Set<Role> roles = Collections.emptySet();

    @Wither
    Instant createdAt;

    @Wither
    String createdBy;

    @Wither
    Instant updatedAt;

    @Wither
    String updatedBy;

}
