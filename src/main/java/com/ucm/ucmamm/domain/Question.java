package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import lombok.experimental.Wither;

import java.util.List;

@Value
@Builder
public class Question {

    String id;

    String content;

    @Wither
    @Singular
    List<Answer> answers;

}
