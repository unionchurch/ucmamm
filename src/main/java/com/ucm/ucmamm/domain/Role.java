package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Value
@Builder
@Document(collection = "roles")
public class Role {

    @Id
    @Wither
    String id;

    @Indexed
    String roleName;

    String title;

    String description;

    Boolean deleted;

    Boolean active;

    Integer version;

    @Indexed
    Integer order;

    @Wither
    Instant createdAt;

    @Wither
    String createdBy;

    @Wither
    Instant updatedAt;

    @Wither
    String updatedBy;

}
