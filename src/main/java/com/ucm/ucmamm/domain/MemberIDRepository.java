package com.ucm.ucmamm.domain;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface MemberIDRepository extends MongoRepository<MemberID, String> {

    String LOOKUP_NAME = "next_member_id";

    @Query(value = "{ name: '" + LOOKUP_NAME + "' }")
    MemberID findNextMemberID();

}
