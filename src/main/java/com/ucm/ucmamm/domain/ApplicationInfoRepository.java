package com.ucm.ucmamm.domain;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ApplicationInfoRepository extends MongoRepository<ApplicationInfo, String> {

}
