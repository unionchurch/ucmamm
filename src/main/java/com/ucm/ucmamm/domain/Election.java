package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import lombok.experimental.Wither;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Value
@Builder
@Document(collection = "elections")
@CompoundIndexes({
        @CompoundIndex(name = "name_date_deleted", def = "{name: 1, date: 1, deleted: 1}"),
        @CompoundIndex(name = "id_deleted", def = "{_id: 1, deleted: 1}"),
        @CompoundIndex(name = "active_deleted", def = "{active: 1, deleted: 1}")
})
public class Election {

    @Id
    String id;

    String name;

    LocalDate date;

    LocalTime startTime;

    LocalTime endTime;

    @Wither
    @Singular
    List<Proposal> proposals;

    @Wither
    @Singular
    List<Question> questions;

    @Wither
    @Singular
    List<Seat> seats;

    @Wither
    Long proxy;

    @Wither
    Long voter;

    @Wither
    Long proxyVoted;

    @Wither
    Long voted;

    @Wither
    @Indexed
    Boolean deleted;

    @Wither
    Boolean active;

    @Wither
    Integer version;

    @Wither
    Instant createdAt;

    @Wither
    String createdBy;

    @Wither
    Instant updatedAt;

    @Wither
    String updatedBy;

}
