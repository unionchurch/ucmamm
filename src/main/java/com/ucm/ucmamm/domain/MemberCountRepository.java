package com.ucm.ucmamm.domain;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface MemberCountRepository extends MongoRepository<MemberCount, String> {

    String MEMBER_COUNT_LOOKUP_NAME = "member_count_idx";

    @Query(value = "{ name: '" + MEMBER_COUNT_LOOKUP_NAME + "' }")
    MemberCount findMemberCount();

}
