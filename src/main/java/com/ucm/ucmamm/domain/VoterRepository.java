package com.ucm.ucmamm.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface VoterRepository extends MongoRepository<Voter, String> {

    Optional<Voter> findByMemberIdAndElectionId(Long memberId, String electionId);

    Page<Voter> findByElectionId(String electionId, Pageable pageable);

    @Query(value = "{ electionId: ?0 }")
    List<Voter> findAllVoters(String electionId, Sort sort);

    @Query(value = "{ $or: [{ $and: [{ firstName: { $regex: ?0, $options: 'i'} }, { lastName: { $regex: ?1, $options: 'i'} }] }, " +
            "{ $or: [{ lastName: { $regex: ?0, $options: 'i'} }, { firstName: { $regex: ?1, $options: 'i'} }] }], electionId: ?2 }")
    Page<Voter> findByFirstNameOrLastName(String firstNamePrefix, String lastNamePrefix, String electionId, Pageable pageable);

    void deleteByMemberIdAndElectionId(Long memberId, String electionId);

    long countByElectionIdAndProxy(String electionId, Boolean proxy);

}
