package com.ucm.ucmamm.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findByUserNameAndDeleted(String userName, Boolean deleted);

    Optional<User> findByIdAndDeleted(String id, Boolean deleted);

    Page<User> findByDeleted(Boolean deleted, Pageable pageable);

    Page<User> findByUserNameStartingWithAndDeleted(String keyword, Boolean deleted, Pageable pageable);

}
