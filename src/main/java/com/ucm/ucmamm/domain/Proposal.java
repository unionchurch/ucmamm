package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

@Value
@Builder
public class Proposal {

    String id;

    @Wither
    String content;

    @Builder.Default
    Long approved = 0L;

    @Builder.Default
    Long disapproved = 0L;

}
