package com.ucm.ucmamm.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ElectionRepository extends MongoRepository<Election, String> {

    Optional<Election> findByIdAndDeleted(String id, Boolean deleted);

    Optional<Election> findByNameAndDateAndDeleted(String name, LocalDate date, Boolean deleted);

    List<Election> findByActiveAndDeleted(Boolean active, Boolean deleted, Sort sort);

    Page<Election> findByDeleted(Boolean deleted, Pageable pageable);

}
