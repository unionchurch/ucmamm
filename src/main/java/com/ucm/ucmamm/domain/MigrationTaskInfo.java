package com.ucm.ucmamm.domain;

import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Value
@Builder
@Document(collection = "migration_task_info")
public class MigrationTaskInfo {

    @Id
    String id;

    String name;

    Instant executedAt;

}

