package com.ucm.ucmamm.election;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class CandidateUpdate {

    String id;

    Long memberId;

    String firstName;

    String middleName;

    String lastName;

    String suffix;

}
