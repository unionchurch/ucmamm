package com.ucm.ucmamm.election;

import com.ucm.ucmamm.common.ResourceNotFoundException;
import com.ucm.ucmamm.domain.Election;
import com.ucm.ucmamm.domain.Voter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;
import java.util.List;

import static com.ucm.ucmamm.common.Caches.*;

@RestController
public class ElectionResource {

    @Autowired
    private ElectionService electionService;

    @Cacheable(cacheNames = ELECTION)
    @GetMapping("/api/v1/elections/{id}")
    public ElectionDto getElection(@PathVariable String id) {
        return electionService.getElection(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Cacheable(cacheNames = ELECTIONS)
    @GetMapping("/api/v1/elections")
    public List<ElectionDto> getElections(@RequestParam(defaultValue = "0") int page,
                                          @RequestParam(defaultValue = "5") int size,
                                          @RequestParam(defaultValue = "date") String sort,
                                          @RequestParam(defaultValue = "true") boolean ascending) {
        return electionService.getElections(page, size, sort, ascending);
    }

    @Cacheable(cacheNames = ELECTION, key = ACTIVE_ELECTION)
    @GetMapping("/api/v1/elections/active")
    public ElectionDto getActiveElections() {
        return electionService.getActiveElection().orElseThrow(ResourceNotFoundException::new);
    }

    @PostMapping("/api/v1/elections")
    public ResponseEntity<?> saveElection(@Valid @RequestBody ElectionInput electionInput, HttpServletRequest request) {
        Election election = electionService.saveElection(electionInput.withOperator(request.getUserPrincipal().getName()));
        return ResponseEntity.created(URI.create(request.getRequestURL().append("/").append(election.getId()).toString())).build();
    }

    @PutMapping("/api/v1/elections/{id}")
    public ResponseEntity<?> updateElection(@PathVariable String id, @Valid @RequestBody ElectionUpdate electionUpdate, Principal principal) {
        electionService.updateElection(electionUpdate.withId(id).withOperator(principal.getName()));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/api/v1/elections/{id}")
    public ResponseEntity<?> deleteElection(@PathVariable String id, Principal principal) {
        electionService.deleteElection(id, principal.getName());
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/api/v1/elections/{id}/voters")
    public ResponseEntity<?> registerVoter(@PathVariable String id, @Valid @RequestBody VoterInput voterInput, HttpServletRequest request) {
        Voter voter = electionService.registerVoter(voterInput.withOperator(request.getUserPrincipal().getName()));
        return ResponseEntity.created(URI.create(request.getRequestURL().append("/").append(voter.getId()).toString())).build();
    }

    @Cacheable(cacheNames = VOTERS)
    @GetMapping("/api/v1/elections/{id}/voters")
    public List<VoterDto> getVoters(@PathVariable String id,
                                    @RequestParam(defaultValue = "0") int page,
                                    @RequestParam(defaultValue = "10") int size,
                                    @RequestParam(defaultValue = "") String keyword,
                                    @RequestParam(defaultValue = "lastName") String sort,
                                    @RequestParam(defaultValue = "true") boolean ascending) {
        return electionService.getVoters(id, page, size, keyword, sort, ascending);
    }

    @DeleteMapping("/api/v1/elections/{id}/voters/{memberId}")
    public ResponseEntity<?> unregisterVoter(@PathVariable String id, @PathVariable Long memberId) {
        electionService.unregisterVoter(memberId, id);
        return ResponseEntity.noContent().build();
    }

    @Cacheable(cacheNames = VOTER, key = "#id + '_' + #memberId")
    @GetMapping("/api/v1/elections/{id}/voters/{memberId}")
    public VoterDto getVoter(@PathVariable String id, @PathVariable Long memberId) {
        return electionService.getVoter(memberId, id).orElseThrow(ResourceNotFoundException::new);
    }

    @PutMapping("/api/v1/elections/{id}/activate")
    public ResponseEntity<?> activateElection(@PathVariable String id) {
        electionService.changeElectionStatus(id, Boolean.TRUE);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/api/v1/elections/{id}/deactivate")
    public ResponseEntity<?> deactivateElection(@PathVariable String id) {
        electionService.changeElectionStatus(id, Boolean.FALSE);
        return ResponseEntity.noContent().build();
    }

}
