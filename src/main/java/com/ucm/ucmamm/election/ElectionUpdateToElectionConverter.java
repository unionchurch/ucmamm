package com.ucm.ucmamm.election;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.common.RandomizingService;
import com.ucm.ucmamm.common.ResourceNotFoundException;
import com.ucm.ucmamm.domain.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static com.ucm.ucmamm.common.CollectionUtils.defaultToEmptyIfNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

@Component
public class ElectionUpdateToElectionConverter implements Converter<ElectionUpdate, Election> {

    private static final String NON_EXISTING_ID = "non-existing-id";

    @Autowired
    private ElectionRepository electionRepository;
    @Autowired
    private RandomizingService randomizingService;
    @Autowired
    private DateService dateService;

    @Override
    public Election convert(ElectionUpdate electionUpdate) {
        return getElectionDomain(electionUpdate)
                .map(election -> Election.builder()
                        .id(election.getId())
                        .name(electionUpdate.getName())
                        .date(electionUpdate.getDate())
                        .startTime(electionUpdate.getStartTime())
                        .endTime(electionUpdate.getEndTime())
                        .proposals(buildProposals(electionUpdate, election))
                        .questions(buildQuestions(electionUpdate, election))
                        .seats(buildSeats(electionUpdate, election))
                        .active(isTrue(electionUpdate.getActive()))
                        .deleted(election.getDeleted())
                        .version(election.getVersion() + 1)
                        .updatedBy(electionUpdate.getOperator())
                        .updatedAt(dateService.getCurrentTime())
                        .build())
                .orElseThrow(ResourceNotFoundException::new);
    }

    private List<Proposal> buildProposals(ElectionUpdate electionUpdate, Election election) {
        Map<String, Proposal> proposalMap = defaultToEmptyIfNull(election.getProposals())
                .collect(toMap(Proposal::getId, Function.identity()));
        return defaultToEmptyIfNull(electionUpdate.getProposals())
                .map(proposalUpdate -> proposalMap.getOrDefault(checkModelId(proposalUpdate.getId()), Proposal.builder()
                        .id(randomizingService.getRandomString())
                        .content(proposalUpdate.getContent())
                        .approved(0L)
                        .disapproved(0L)
                        .build())
                        .withContent(proposalUpdate.getContent()))
                .collect(toList());
    }

    private List<Question> buildQuestions(ElectionUpdate electionUpdate, Election election) {
        Map<String, Question> questionMap = defaultToEmptyIfNull(election.getQuestions())
                .collect(toMap(Question::getId, Function.identity()));
        return defaultToEmptyIfNull(electionUpdate.getQuestions())
                .map(questionUpdate -> populateOrUpdateAnswers(questionUpdate,
                        questionMap.getOrDefault(checkModelId(questionUpdate.getId()), Question.builder()
                                .id(randomizingService.getRandomString())
                                .content(questionUpdate.getContent())
                                .build())))
                .collect(toList());
    }

    private Question populateOrUpdateAnswers(QuestionUpdate questionUpdate, Question question) {
        Map<String, Answer> answerMap = defaultToEmptyIfNull(question.getAnswers()).collect(toMap(Answer::getId, Function.identity()));
        return question.withAnswers(defaultToEmptyIfNull(questionUpdate.getAnswers())
                .map(answerUpdate -> answerMap.getOrDefault(checkModelId(answerUpdate.getId()), Answer.builder()
                        .id(randomizingService.getRandomString())
                        .selected(0L)
                        .build())
                        .withContent(answerUpdate.getContent()))
                .collect(toList()));
    }

    private List<Seat> buildSeats(ElectionUpdate electionUpdate, Election election) {
        Map<String, Seat> seatMap = defaultToEmptyIfNull(election.getSeats()).collect(toMap(Seat::getId, Function.identity()));
        return defaultToEmptyIfNull(electionUpdate.getSeats())
                .map(seatUpdate -> populateOrUpdateCandidates(seatUpdate,
                        seatMap.getOrDefault(checkModelId(seatUpdate.getId()), Seat.builder()
                                .id(randomizingService.getRandomString())
                                .name(seatUpdate.getName())
                                .count(seatUpdate.getCount())
                                .build())))
                .collect(toList());
    }

    private Seat populateOrUpdateCandidates(SeatUpdate seatUpdate, Seat seat) {
        Map<String, Candidate> candidateMap = defaultToEmptyIfNull(seat.getCandidates()).collect(toMap(Candidate::getId, Function.identity()));
        return seat.withCandidates(defaultToEmptyIfNull(seatUpdate.getCandidates())
                .map(candidateUpdate -> candidateMap.getOrDefault(checkModelId(candidateUpdate.getId()), Candidate.builder()
                        .id(randomizingService.getRandomString())
                        .approved(0L)
                        .disapproved(0L)
                        .build())
                        .updateWith(candidateUpdate.getMemberId(),
                            candidateUpdate.getFirstName(),
                            candidateUpdate.getMiddleName(),
                            candidateUpdate.getLastName(),
                            candidateUpdate.getSuffix()))
                .sorted(comparator())
                .collect(toList()));
    }

    private Optional<Election> getElectionDomain(ElectionUpdate electionUpdate) {
        return electionRepository.findByIdAndDeleted(electionUpdate.getId(), Boolean.FALSE);
    }

    private String checkModelId(String id) {
        return StringUtils.defaultIfBlank(id, NON_EXISTING_ID);
    }

    private Comparator<Candidate> comparator() {
        return (Candidate c1, Candidate c2) -> {
            int lastNameCompare = c1.getLastName().compareTo(c2.getLastName());
            if (lastNameCompare < 0) {
                return -1;
            }
            if (lastNameCompare > 0) {
                return 0;
            }
            return c1.getFirstName().compareTo(c2.getFirstName());
        };
    }

}
