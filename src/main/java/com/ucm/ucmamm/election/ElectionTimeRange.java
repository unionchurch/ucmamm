package com.ucm.ucmamm.election;

import lombok.Builder;
import lombok.Value;

import java.time.LocalTime;

@Value
@Builder
public class ElectionTimeRange {

    LocalTime startTime;

    LocalTime endTime;

}
