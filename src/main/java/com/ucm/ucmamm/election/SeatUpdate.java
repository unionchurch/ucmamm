package com.ucm.ucmamm.election;

import lombok.Builder;
import lombok.Value;

import java.util.List;

import static java.util.Collections.emptyList;

@Value
@Builder
public class SeatUpdate {

    String id;

    String name;

    Integer count;

    @Builder.Default
    List<CandidateUpdate> candidates = emptyList();

}
