package com.ucm.ucmamm.election;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.common.RandomizingService;
import com.ucm.ucmamm.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static com.ucm.ucmamm.common.CollectionUtils.defaultToEmptyIfNull;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

@Component
public class ElectionConverter implements Converter<ElectionInput, Election> {

    @Autowired
    private RandomizingService randomizingService;
    @Autowired
    private DateService dateService;

    @Override
    public Election convert(ElectionInput electionInput) {
        return Election.builder()
                .name(electionInput.getName())
                .date(electionInput.getDate())
                .startTime(electionInput.getStartTime())
                .endTime(electionInput.getEndTime())
                .proposals(buildProposals(electionInput))
                .questions(buildQuestions(electionInput))
                .seats(buildSeats(electionInput))
                .active(isTrue(electionInput.getActive()))
                .deleted(Boolean.FALSE)
                .version(1)
                .createdBy(electionInput.getOperator())
                .createdAt(dateService.getCurrentTime())
                .build();
    }

    private List<Proposal> buildProposals(ElectionInput electionInput) {
        return defaultToEmptyIfNull(electionInput.getProposals())
                .map(proposalInput -> Proposal.builder()
                        .id(randomizingService.getRandomString())
                        .content(proposalInput.getContent())
                        .approved(0L)
                        .disapproved(0L)
                        .build())
                .collect(toList());
    }

    private List<Question> buildQuestions(ElectionInput electionInput) {
        return defaultToEmptyIfNull(electionInput.getQuestions())
                .map(questionInput -> Question.builder()
                        .id(randomizingService.getRandomString())
                        .content(questionInput.getContent())
                        .answers(buildAnswers(questionInput))
                        .build())
                .collect(toList());
    }

    private List<Answer> buildAnswers(QuestionInput questionInput) {
        return defaultToEmptyIfNull(questionInput.getAnswers())
                .map(answerInput -> Answer.builder()
                        .id(randomizingService.getRandomString())
                        .content(answerInput.getContent())
                        .selected(0L)
                        .build())
                .collect(toList());
    }

    private List<Seat> buildSeats(ElectionInput electionInput) {
        return defaultToEmptyIfNull(electionInput.getSeats())
                .map(seatInput -> Seat.builder()
                        .id(randomizingService.getRandomString())
                        .name(seatInput.getName())
                        .count(seatInput.getCount())
                        .candidates(buildCandidates(seatInput))
                        .build())
                .collect(toList());
    }

    private List<Candidate> buildCandidates(SeatInput seatInput) {
        return defaultToEmptyIfNull(seatInput.getCandidates())
                .map(candidateInput -> Candidate.builder()
                        .id(randomizingService.getRandomString())
                        .memberId(candidateInput.getMemberId())
                        .firstName(candidateInput.getFirstName())
                        .middleName(candidateInput.getMiddleName())
                        .lastName(candidateInput.getLastName())
                        .suffix(candidateInput.getSuffix())
                        .approved(0L)
                        .disapproved(0L)
                        .build())
                .sorted(comparator())
                .collect(Collectors.toList());
    }

    private Comparator<Candidate> comparator() {
        return (Candidate c1, Candidate c2) -> {
            int lastNameCompare = c1.getLastName().compareTo(c2.getLastName());
            if (lastNameCompare < 0) {
                return -1;
            }
            if (lastNameCompare > 0) {
                return 0;
            }
            return c1.getFirstName().compareTo(c2.getFirstName());
        };
    }

}
