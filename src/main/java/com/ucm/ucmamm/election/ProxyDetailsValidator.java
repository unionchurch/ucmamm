package com.ucm.ucmamm.election;

import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class ProxyDetailsValidator implements ConstraintValidator<ValidProxy, ProxyProxyDetails> {

    @Override
    public boolean isValid(ProxyProxyDetails value, ConstraintValidatorContext context) {
        if (value.isProxy()) {
            return value.getProxyMemberId() != null && value.getProxyMemberId() != 0L;
        }
        return true;
    }

}
