package com.ucm.ucmamm.election;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
public class ElectionIdNameAndDate {

    String id;

    String name;

    LocalDate date;

}
