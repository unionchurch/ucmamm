package com.ucm.ucmamm.election;

import com.ucm.ucmamm.domain.Proposal;
import com.ucm.ucmamm.domain.Question;
import com.ucm.ucmamm.domain.Seat;
import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import lombok.experimental.Wither;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Value
@Builder
public class ElectionDto {

    String id;

    String name;

    LocalDate date;

    LocalTime startTime;

    LocalTime endTime;

    @Singular
    List<Proposal> proposals;

    @Singular
    List<Question> questions;

    @Singular
    List<Seat> seats;

    Boolean deleted;

    @Wither
    Boolean active;

    Integer version;

    Long voter;

    Long voted;

    Long proxy;

    Long proxyVoted;

}
