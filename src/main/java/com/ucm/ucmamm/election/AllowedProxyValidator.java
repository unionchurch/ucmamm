package com.ucm.ucmamm.election;

import com.ucm.ucmamm.domain.Voter;
import com.ucm.ucmamm.domain.VoterRepository;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class AllowedProxyValidator implements ConstraintValidator<AllowedProxy, MemberIdAndElectionId> {

    private final VoterRepository voterRepository;

    public AllowedProxyValidator(VoterRepository voterRepository) {
        this.voterRepository = voterRepository;
    }

    @Override
    public boolean isValid(MemberIdAndElectionId value, ConstraintValidatorContext context) {
        if (value.getMemberId() != null) {
            return !voterRepository.findByMemberIdAndElectionId(value.getMemberId(), value.getElectionId())
                    .filter(Voter::isProxy)
                    .isPresent();
        }
        return true;
    }

}
