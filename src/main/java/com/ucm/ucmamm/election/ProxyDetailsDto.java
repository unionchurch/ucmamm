package com.ucm.ucmamm.election;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ProxyDetailsDto {

    Long memberId;

    String firstName;

    String middleName;

    String lastName;

    String suffix;

}
