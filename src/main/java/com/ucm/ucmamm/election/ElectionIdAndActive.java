package com.ucm.ucmamm.election;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ElectionIdAndActive {

    String id;

    Boolean active;

}
