package com.ucm.ucmamm.election;

import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class TimeRangeValidator implements ConstraintValidator<ValidTimeRange, ElectionTimeRange> {

    @Override
    public boolean isValid(ElectionTimeRange electionTimeRange, ConstraintValidatorContext constraintValidatorContext) {
        return electionTimeRange.getStartTime().isBefore(electionTimeRange.getEndTime());
    }

}
