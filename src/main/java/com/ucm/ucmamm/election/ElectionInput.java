package com.ucm.ucmamm.election;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static java.util.Collections.emptyList;

@Value
@Builder
public class ElectionInput {

    @NotBlank
    String name;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    LocalDate date;

    @NotNull
    @JsonFormat(pattern = "HH:mm:ss")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    LocalTime startTime;

    @NotNull
    @JsonFormat(pattern = "HH:mm:ss")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    LocalTime endTime;

    @Builder.Default
    List<ProposalInput> proposals = emptyList();

    @Builder.Default
    List<QuestionInput> questions = emptyList();

    @Builder.Default
    List<SeatInput> seats = emptyList();

    @Wither
    String operator;

    @Wither
    Boolean active;

    @UniqueElectionName
    public ElectionNameAndDate getElectionName() {
        return ElectionNameAndDate.builder().name(name).date(date).build();
    }

    @ValidTimeRange
    public ElectionTimeRange getElectionTimeRange() {
        return ElectionTimeRange.builder().startTime(startTime).endTime(endTime).build();
    }

}
