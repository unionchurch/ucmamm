package com.ucm.ucmamm.election;

import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.Member;
import com.ucm.ucmamm.domain.MemberRepository;
import com.ucm.ucmamm.domain.ProxyDetails;
import com.ucm.ucmamm.domain.Voter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static java.lang.Boolean.FALSE;
import static java.util.Optional.ofNullable;

@Component
public class VoterConverter implements Converter<VoterInput, Voter> {

    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private DateService dateService;

    @Override
    public Voter convert(VoterInput voterInput) {
        Optional<Member> member = memberRepository.findByMemberIdAndDeleted(voterInput.getMemberId(), FALSE);
        return member.map(m -> Voter.builder()
                .electionId(voterInput.getElectionId())
                .memberId(voterInput.getMemberId())
                .firstName(m.getFirstName())
                .middleName(m.getMiddleName())
                .lastName(m.getLastName())
                .suffix(m.getSuffix())
                .proxy(voterInput.isProxy())
                .proxyDetails(buildProxyDetails(voterInput))
                .createdBy(voterInput.getOperator())
                .createdAt(dateService.getCurrentTime())
                .build())
                .orElse(null);
    }

    private ProxyDetails buildProxyDetails(VoterInput voterInput) {
        return ofNullable(voterInput.getProxyMemberId())
                .flatMap(proxyMemberId -> memberRepository.findByMemberIdAndDeleted(proxyMemberId, FALSE))
                .map(member -> ProxyDetails.builder()
                        .memberId(member.getMemberId())
                        .firstName(member.getFirstName())
                        .middleName(member.getMiddleName())
                        .lastName(member.getLastName())
                        .suffix(member.getSuffix())
                        .build())
                .orElse(null);
    }

}
