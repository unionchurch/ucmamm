package com.ucm.ucmamm.election;

import com.ucm.ucmamm.domain.VoterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueVoterValidator implements ConstraintValidator<UniqueVoter, MemberIdAndElectionId> {

    @Autowired
    private VoterRepository voterRepository;

    @Override
    public boolean isValid(MemberIdAndElectionId memberIdAndElectionId, ConstraintValidatorContext constraintValidatorContext) {
        return !voterRepository.findByMemberIdAndElectionId(memberIdAndElectionId.getMemberId(), memberIdAndElectionId.getElectionId()).isPresent();
    }

}
