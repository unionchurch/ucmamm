package com.ucm.ucmamm.election;

import lombok.Builder;
import lombok.Value;

import java.util.List;

import static java.util.Collections.emptyList;

@Value
@Builder
public class QuestionUpdate {

    String id;

    String content;

    @Builder.Default
    List<AnswerUpdate> answers = emptyList();

}
