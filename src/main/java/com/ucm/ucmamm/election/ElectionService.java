package com.ucm.ucmamm.election;

import com.ucm.ucmamm.common.CacheService;
import com.ucm.ucmamm.common.DateService;
import com.ucm.ucmamm.domain.Election;
import com.ucm.ucmamm.domain.ElectionRepository;
import com.ucm.ucmamm.domain.Voter;
import com.ucm.ucmamm.domain.VoterRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static com.ucm.ucmamm.common.Caches.*;
import static java.lang.Boolean.FALSE;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.data.domain.Sort.Direction.DESC;

@Service
@RequiredArgsConstructor
public class ElectionService {

    private final ElectionRepository electionRepository;
    private final VoterRepository voterRepository;
    private final ConversionService conversionService;
    private final DateService dateService;
    private final CacheService cacheService;

    Election saveElection(ElectionInput electionInput) {
        if (isTrue(electionInput.getActive())) {
            ofNullable(getActiveElections())
                    .filter(CollectionUtils::isNotEmpty)
                    .map(elections -> elections.stream().map(election -> election.withActive(FALSE)).collect(toList()))
                    .ifPresent(elections -> {
                        electionRepository.saveAll(elections);
                        cacheService.evictCache(ELECTION, ACTIVE_ELECTION.replaceAll("'", ""));
                    });
        }
        Election election = electionRepository.save(conversionService.convert(electionInput, Election.class));
        cacheService.evictCache(ELECTIONS);
        return election;
    }

    void updateElection(ElectionUpdate electionUpdate) {
        if (isTrue(electionUpdate.getActive())) {
            ofNullable(getActiveElections())
                    .filter(CollectionUtils::isNotEmpty)
                    .map(elections -> elections.stream().map(election -> election.withActive(FALSE)).collect(toList()))
                    .ifPresent(elections -> {
                        electionRepository.saveAll(elections);
                        cacheService.evictCache(ELECTION, ACTIVE_ELECTION.replaceAll("'", ""));
                    });
        } else {
            getActiveElection()
                    .map(ElectionDto::getId)
                    .filter(id -> id.equalsIgnoreCase(electionUpdate.getId()))
                    .ifPresent(id -> cacheService.evictCache(ELECTION, ACTIVE_ELECTION.replaceAll("'", "")));
        }
        electionRepository.save(conversionService.convert(electionUpdate, Election.class));
        cacheService.evictCache(ELECTIONS);
        cacheService.evictCache(ELECTION, electionUpdate.getId());
    }

    void deleteElection(String id, String operator) {
        electionRepository.findById(id)
                .map(election -> {
                    if (isTrue(election.getActive())) {
                        cacheService.evictCache(ELECTION, ACTIVE_ELECTION.replaceAll("'", ""));
                    }
                    return election.withDeleted(Boolean.TRUE)
                            .withUpdatedBy(operator)
                            .withUpdatedAt(dateService.getCurrentTime());
                })
                .ifPresent(electionRepository::save);
        cacheService.evictCache(ELECTIONS);
        cacheService.evictCache(ELECTION, id);
    }

    Optional<ElectionDto> getElection(String id) {
        return electionRepository.findByIdAndDeleted(id, FALSE)
                .map(election -> conversionService.convert(election, ElectionDto.class));
    }

    Optional<ElectionDto> getElectionByNameAndDate(ElectionNameAndDate electionNameAndDate) {
        return electionRepository.findByNameAndDateAndDeleted(electionNameAndDate.getName(), electionNameAndDate.getDate(), FALSE)
                .map(election -> conversionService.convert(election, ElectionDto.class));
    }

    List<ElectionDto> getElections(int page, int pageSize, String sortField, boolean ascending) {
        return ofNullable(electionRepository.findByDeleted(FALSE, PageRequest.of(page, pageSize, ascending ? Sort.Direction.ASC : Sort.Direction.DESC, sortField)))
                .filter(Slice::hasContent)
                .map(Slice::getContent)
                .map(elections -> elections.stream().map(election -> conversionService.convert(election, ElectionDto.class)).collect(toList()))
                .orElse(emptyList());
    }

    Optional<ElectionDto> getActiveElection() {
        return getActiveElections().stream()
                .map(election -> conversionService.convert(election, ElectionDto.class))
                .findFirst();
    }

    Voter registerVoter(VoterInput voterInput) {
        Voter voter = voterRepository.save(conversionService.convert(voterInput, Voter.class));
        electionRepository.findById(voterInput.getElectionId())
                .ifPresent(election -> {
                    electionRepository.save(election
                            .withProxy(ofNullable(election.getProxy()).orElse(0L) + (voterInput.isProxy() ? 1 : 0))
                            .withVoter(ofNullable(election.getVoter()).orElse(0L) + 1));
                });
        cacheService.evictCache(VOTERS);
        cacheService.evictCache(ELECTION, voterInput.getElectionId());
        cacheService.evictCache(ELECTION, ACTIVE_ELECTION.replaceAll("'", ""));
        cacheService.evictCache(ELECTIONS);
        return voter;
    }

    Optional<VoterDto> getVoter(Long memberId, String electionId) {
        return voterRepository.findByMemberIdAndElectionId(memberId, electionId).map(voter -> conversionService.convert(voter, VoterDto.class));
    }

    void unregisterVoter(Long memberId, String electionId) {
        voterRepository.findByMemberIdAndElectionId(memberId, electionId).ifPresent(voter -> {
            electionRepository.findById(electionId)
                    .ifPresent(election -> {
                        electionRepository.save(election
                                .withProxy(ofNullable(election.getProxy()).orElse(0L) - (voter.isProxy() ? 1 : 0))
                                .withVoter(ofNullable(election.getVoter()).orElse(0L) - 1));
                        voterRepository.delete(voter);
                    });
            cacheService.evictCache(VOTERS);
            cacheService.evictCache(VOTER, electionId + "_" + memberId);
            cacheService.evictCache(ELECTION, electionId);
            cacheService.evictCache(ELECTION, ACTIVE_ELECTION.replaceAll("'", ""));
            cacheService.evictCache(ELECTIONS);
        });
    }

    List<VoterDto> getVoters(String electionId, int page, int pageSize, String keyword, String sortField, boolean ascending) {
        return ofNullable(getVoterPage(electionId, page, pageSize, keyword, sortField, ascending))
                .filter(Slice::hasContent)
                .map(Slice::getContent)
                .map(voters -> voters.stream().map(voter -> conversionService.convert(voter, VoterDto.class)).collect(toList()))
                .orElse(Collections.emptyList());
    }

    void changeElectionStatus(String id, boolean active) {
        electionRepository.findById(id).ifPresent(election -> {
            electionRepository.save(election.withActive(active));
            cacheService.evictCache(ELECTION, id);
            cacheService.evictCache(ELECTION, ACTIVE_ELECTION.replaceAll("'", ""));
            cacheService.evictCache(ELECTIONS);
        });
    }

    private List<Election> getActiveElections() {
        return electionRepository.findByActiveAndDeleted(Boolean.TRUE, FALSE, Sort.by(Sort.Direction.DESC, "date"));
    }

    private Page<Voter> getVoterPage(String electionId, int page, int pageSize, String keyword, String sortField, boolean ascending) {
        PageRequest pageRequest = PageRequest.of(page, pageSize, ascending ? ASC : DESC,
                ofNullable(sortField).filter(StringUtils::isNotBlank).orElse("lastName"));
        List<String> keywords = ofNullable(keyword).map(this::splitKeyword).orElse(Collections.emptyList());
        return keywords.isEmpty()
                ? voterRepository.findByElectionId(electionId, pageRequest)
                : voterRepository.findByFirstNameOrLastName(keywords.get(0), keywords.get(1), electionId, pageRequest);
    }

    private List<String> splitKeyword(String keyword) {
        List<String> keywords = Stream.of(keyword.split("[ ,]"))
                .filter(StringUtils::isNotBlank)
                .map(String::trim).collect(toList());
        if (keywords.size() == 1) {
            keywords.add(keywords.get(0));
        }
        return keywords;
    }

}
