package com.ucm.ucmamm.election;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class MemberIdAndElectionId {

    Long memberId;

    String electionId;

}
