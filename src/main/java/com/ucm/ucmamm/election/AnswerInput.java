package com.ucm.ucmamm.election;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AnswerInput {

    String content;

    @JsonCreator
    public AnswerInput(@JsonProperty("content") String content) {
        this.content = content;
    }

}
