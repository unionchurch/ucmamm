package com.ucm.ucmamm.election;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class NonActiveElectionValidator implements ConstraintValidator<NonActiveElection, ElectionIdAndActive> {

    @Autowired
    private ElectionService electionService;

    @Override
    public boolean isValid(ElectionIdAndActive electionIdAndActive, ConstraintValidatorContext context) {
        return !electionService.getActiveElection()
                .map(ElectionDto::getId)
                .filter(id -> id.equalsIgnoreCase(electionIdAndActive.getId()))
                .filter(id -> BooleanUtils.isTrue(electionIdAndActive.getActive()))
                .isPresent();
    }

}
