package com.ucm.ucmamm.election;

import com.ucm.ucmamm.domain.VoterRepository;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class RegisteredProxyValidator implements ConstraintValidator<RegisteredProxy, MemberIdAndElectionId> {

    private VoterRepository voterRepository;

    public RegisteredProxyValidator(VoterRepository voterRepository) {
        this.voterRepository = voterRepository;
    }

    @Override
    public boolean isValid(MemberIdAndElectionId value, ConstraintValidatorContext context) {
        if (value.getMemberId() != null) {
            return voterRepository.findByMemberIdAndElectionId(value.getMemberId(), value.getElectionId()).isPresent();
        }
        return true;
    }

}
