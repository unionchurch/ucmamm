package com.ucm.ucmamm.election;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Value
@Builder
public class VoterInput {

    @NotBlank
    String electionId;

    @NotNull
    Long memberId;

    boolean proxy;

    Long proxyMemberId;

    @Wither
    String operator;

    @UniqueVoter
    MemberIdAndElectionId getVoter() {
        return MemberIdAndElectionId.builder().memberId(memberId).electionId(electionId).build();
    }

    @AllowedProxy
    @RegisteredProxy
    MemberIdAndElectionId getProxyingMember() {
        return MemberIdAndElectionId.builder().memberId(proxyMemberId).electionId(electionId).build();
    }

    @ValidProxy
    ProxyProxyDetails getValidProxy() {
        return ProxyProxyDetails.builder().proxy(proxy).proxyMemberId(proxyMemberId).build();
    }

}
