package com.ucm.ucmamm.election;

import lombok.Builder;
import lombok.Value;

import java.time.Instant;

@Value
@Builder
public class VoterDto {

    String id;

    String electionId;

    Long memberId;

    String firstName;

    String middleName;

    String lastName;

    String suffix;

    boolean proxy;

    Instant registeredDate;

    String registeredBy;

    ProxyDetailsDto proxyDetails;

}
