package com.ucm.ucmamm.election;

import com.ucm.ucmamm.domain.Election;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ElectionDtoConverter implements Converter<Election, ElectionDto> {

    @Override
    public ElectionDto convert(Election election) {
        return ElectionDto.builder()
                .id(election.getId())
                .name(election.getName())
                .date(election.getDate())
                .startTime(election.getStartTime())
                .endTime(election.getEndTime())
                .proposals(election.getProposals())
                .questions(election.getQuestions())
                .seats(election.getSeats())
                .deleted(election.getDeleted())
                .active(election.getActive())
                .version(election.getVersion())
                .voter(election.getVoter())
                .voted(election.getVoted())
                .proxy(election.getProxy())
                .proxyVoted(election.getProxyVoted())
                .build();
    }

}
