package com.ucm.ucmamm.election;

import com.ucm.ucmamm.domain.ProxyDetails;
import com.ucm.ucmamm.domain.Voter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class VoterDtoConverter implements Converter<Voter, VoterDto> {

    @Override
    public VoterDto convert(Voter voter) {
        return VoterDto.builder()
                .id(voter.getId())
                .electionId(voter.getElectionId())
                .firstName(voter.getFirstName())
                .middleName(voter.getMiddleName())
                .lastName(voter.getLastName())
                .suffix(voter.getSuffix())
                .memberId(voter.getMemberId())
                .proxy(voter.isProxy())
                .proxyDetails(buildProxyDetails(voter))
                .registeredBy(voter.getCreatedBy())
                .registeredDate(voter.getCreatedAt())
                .build();
    }

    private ProxyDetailsDto buildProxyDetails(Voter voter) {
        if (voter.getProxyDetails() !=  null) {
            ProxyDetails proxyDetails = voter.getProxyDetails();
            return ProxyDetailsDto.builder()
                    .memberId(proxyDetails.getMemberId())
                    .firstName(proxyDetails.getFirstName())
                    .middleName(proxyDetails.getMiddleName())
                    .lastName(proxyDetails.getLastName())
                    .suffix(proxyDetails.getSuffix())
                    .build();
        }
        return null;
    }

}
