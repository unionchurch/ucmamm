package com.ucm.ucmamm.election;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueElectionNameOnUpdateValidator implements ConstraintValidator<UniqueElectionNameOnUpdate, ElectionIdNameAndDate> {

    @Autowired
    private ElectionService electionService;

    @Override
    public boolean isValid(ElectionIdNameAndDate electionIdNameAndDate, ConstraintValidatorContext context) {
        return !electionService.getElectionByNameAndDate(ElectionNameAndDate.builder()
                .name(electionIdNameAndDate.getName())
                .date(electionIdNameAndDate.getDate())
                .build())
                .filter(electionDto -> !electionDto.getId().equalsIgnoreCase(electionIdNameAndDate.getId()))
                .isPresent();
    }

}
