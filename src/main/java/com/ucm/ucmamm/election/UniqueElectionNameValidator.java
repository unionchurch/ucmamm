package com.ucm.ucmamm.election;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueElectionNameValidator implements ConstraintValidator<UniqueElectionName, ElectionNameAndDate> {

    @Autowired
    private ElectionService electionService;

    @Override
    public boolean isValid(ElectionNameAndDate electionNameAndDate, ConstraintValidatorContext context) {
        return !electionService.getElectionByNameAndDate(electionNameAndDate).isPresent();
    }

}
