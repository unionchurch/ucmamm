import { Injectable } from '@angular/core';

import { Observable, OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';

import { RestapiService } from '../common/restapi.service';
import { Member } from './member';
import { BaseService, errorMapper } from '../common/base.service';
import { HttpErrorResponse, HttpRequest, HttpEvent, HttpParams } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class MemberService implements BaseService<Member> {

    private baseUri = 'api/v1/members';
    private memberMapper: OperatorFunction<{}, Member> = map(value => value as Member);

    constructor(private restapi: RestapiService) { }

    get(id: string): Observable<Member> {
        return this.restapi.get<Member>(`${this.baseUri}/${id}`, 'Get Member').pipe(this.memberMapper);
    }

    list(page: number, size: number, keyword: string, sort: string, ascending: boolean, activeFilter: string) {
        return this.restapi.get<Member[]>(this.baseUri, 'Get Members', { page, size, keyword, sort, ascending, activeFilter });
    }

    create(member: Member): Observable<HttpErrorResponse> {
       return this.restapi.post<Member>(this.baseUri, member, 'Create New Member').pipe(errorMapper);
    }

    update(member: Member): Observable<HttpErrorResponse> {
        return this.restapi.put<Member>(`${this.baseUri}/${member.id}`, member, 'Update Member').pipe(errorMapper);
    }

    delete(id: string): Observable<HttpErrorResponse> {
        return this.restapi.delete(`${this.baseUri}/${id}`, 'Delete Member').pipe(errorMapper);
    }

    getExactMembers(firstName: string, lastName: string) {
        return this.restapi.get<Member[]>(`${this.baseUri}/exact`, 'Get Members with the same name', { firstName, lastName });
    }

    count(): Observable<any> {
        return this.restapi.getNoAuth(`${this.baseUri}/count`, {});
    }

    uploadMembersList(form: any, params: HttpParams): Observable<HttpEvent<{}>> {
        return this.restapi.request('POST', `${this.baseUri}/upload`, form, params);
    }

}
