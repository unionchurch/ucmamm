export class Member {
    id: string;
    memberId: number;
    firstName: string;
    middleName: string;
    lastName: string;
    suffix: string;
    phoneNumbers: string[];
    emailAddresses: string[];
    streetAddress: string;
    village: string; 
    city: string;
    province: string;
    zipCode: string;
    country: string;
    active: boolean;
}
