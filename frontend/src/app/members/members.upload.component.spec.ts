import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MembersUploadComponent } from './members.upload.component';

describe('MembersUploadComponent', () => {
  let component: MembersUploadComponent;
  let fixture: ComponentFixture<MembersUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MembersUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MembersUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
