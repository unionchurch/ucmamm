import { Component, OnInit } from '@angular/core';

import { HttpEvent, HttpEventType, HttpResponse, HttpParams } from '@angular/common/http';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MemberService } from './member.service';
import { MessageService } from '../common/message.service';

@Component({
    selector: 'app-members.upload',
    templateUrl: './members.upload.component.html',
    styleUrls: ['./members.upload.component.css']
})
export class MembersUploadComponent implements OnInit {

    membersUploadForm: FormData;
    progressPercentage = 0;
    saveAsActive = false;
    filename = 'Choose a file';
    successful = false;
    title = 'Upload Members'

    constructor(public activeModal: NgbActiveModal, private memberService: MemberService, private messageService: MessageService) { }

    ngOnInit() {
        this.membersUploadForm = new FormData();
    }

    onFileChange(event: Event) {
        const target = event.target as HTMLInputElement;
        if (target.files && target.files.length) {
            const file = target.files[0];
            this.membersUploadForm.append('file', file, file.name);
            this.filename = file.name;
        }
    }

    upload() {
        this.memberService.uploadMembersList(this.membersUploadForm, new HttpParams().append('saveAsActive', `${this.saveAsActive}`)).subscribe((event: HttpEvent<{}>) => {
            if (event.type === HttpEventType.UploadProgress) {
                this.progressPercentage = Math.round(100 * event.loaded / event.total);
            } else if (event instanceof HttpResponse) {
                this.successful = true;
            }
        });
    }

    cancel() {
        this.activeModal.close();
    }

    close() {
        this.activeModal.close(this.successful);
    }

}
