import { Component } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { faAddressCard, faPhone, faPlus, faTrashAlt, faEnvelope } from '@fortawesome/free-solid-svg-icons';

import { Member } from './member';
import { MemberService } from './member.service';
import { MessageService } from '../common/message.service';
import { LoginService } from '../common/login.service';
import { BaseFormComponent } from '../common/base.form.component';

@Component({
    selector: 'app-member.form',
    templateUrl: './member.form.component.html',
    styleUrls: ['./member.form.component.css']
})
export class MemberFormComponent extends BaseFormComponent<Member, MemberService> {

    faAddressCard = faAddressCard;
    faPhone = faPhone;
    faPlus = faPlus;
    faTrashAlt = faTrashAlt;
    faEnvelope = faEnvelope;
    phoneNumbers: FormArray;
    emailAddresses: FormArray;

    private memberId: number;

    constructor(public formBuilder: FormBuilder, public router: Router, public route: ActivatedRoute, public messageService: MessageService, public memberService: MemberService, private loginService: LoginService) {
        super(formBuilder, router, route, messageService);
        this.memberService = memberService;
    }

    buildForm(): FormGroup {
        return this.formBuilder.group({
            firstName: ['', Validators.required],
            middleName: [''],
            lastName: ['', Validators.required],
            suffix: [''],
            phoneNumbers: this.formBuilder.array([this.createPhoneNumberControl(false)]),
            emailAddresses: this.formBuilder.array([this.createEmailAddressControl(false)]),
            streetAddress: [''],
            village: [''],
            city: [''],
            province: [''],
            zipCode: [''],
            country: [''],
            active: [true]
        });
    }
    
    initializeOtherFormVariables(form: FormGroup) {
        this.phoneNumbers = form.get('phoneNumbers') as FormArray;
        this.emailAddresses = form.get('emailAddresses') as FormArray;
    }

    prepareDomainToUpdate(member: Member) {
        member.id = this.getId();
        member.memberId = this.memberId;
    }

    updateFormValues(form: FormGroup, member: Member) {
        this.memberId = member.memberId;
        this.addPhoneNumber((member.phoneNumbers || []).length - 1, this.viewOnly);
        this.addEmailAddress((member.emailAddresses || []).length - 1, this.viewOnly);

        form.setValue({
            firstName: member.firstName,
            middleName: member.middleName,
            lastName: member.lastName,
            suffix: member.suffix,
            phoneNumbers: member.phoneNumbers || [''],
            emailAddresses: member.emailAddresses || [''],
            streetAddress: member.streetAddress,
            village: member.village,
            city: member.city,
            province: member.province,
            zipCode: member.zipCode,
            country: member.country,
            active: member.active
        });
    }

    addPhoneNumber(count: number = 1, disabled: boolean) {
        Array.apply(null, { length: count }).forEach(() => this.phoneNumbers.push(this.createPhoneNumberControl(disabled)));
    }

    removePhoneNumber(i: number) {
        this.phoneNumbers.removeAt(i);
    }

    addEmailAddress(count: number = 1, disabled: boolean) {
        Array.apply(null, { length: count }).forEach(() => this.emailAddresses.push(this.createEmailAddressControl(disabled)));
    }

    removeEmailAddress(i: number) {
        this.emailAddresses.removeAt(i);
    }

    getDomain(): string {
        return 'Member';
    }

    getMainService(): MemberService {
        return this.memberService;
    }

    hasRole(role: string) {
        return this.loginService.hasRole(role);
    }
 
    private createPhoneNumberControl(disabled: boolean): FormControl {
        return this.createControl(disabled);
    }

    private createEmailAddressControl(disabled: boolean): FormControl {
        return this.createControl(disabled);
    }

    private createControl(disabled: boolean) {
        const control = this.formBuilder.control('');
        if (disabled) {
            control.disable();
        }
        return control;
    }

    get memberForm() {
        return this.getForm();
    }

}
