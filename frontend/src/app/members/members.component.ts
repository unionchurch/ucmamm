import { Component, OnInit } from '@angular/core';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { faAddressCard, faFilePdf, faFileExcel, faFileUpload } from '@fortawesome/free-solid-svg-icons';

import { TableRow, TableHeader, TableReloadRequest, defaultTableReloadRequest } from '../common/table';
import { TableService } from '../common/table.service';
import { TableEvent } from '../common/table.event';
import { Member } from './member';
import { MemberService } from './member.service';
import { MessageService } from '../common/message.service';
import { MembersUploadComponent } from './members.upload.component';
import { LoginService } from '../common/login.service';

@Component({
    selector: 'app-members',
    templateUrl: './members.component.html',
    styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {

    rows: TableRow[] = [];
    isLastRow = false;
    currentActiveFilter: string;

    memberFormLink = '/members/form';
    activeFilterShown = true

    activeCount: number;
    totalCount: number;

    faAddressCard = faAddressCard;
    faFilePdf = faFilePdf;
    faFileExcel = faFileExcel;
    faFileUpload = faFileUpload;

    constructor(private modalService: NgbModal, private memberService: MemberService, private tableService: TableService, private messageService: MessageService, private loginService: LoginService) { }

    ngOnInit() {
        this.loadMembers();

        this.memberService.count().subscribe((count: any) => {
            this.activeCount = count.active;
            this.totalCount = count.total;
        });
    }

    onTableReloadRequest(request: TableReloadRequest) {
        this.loadMembers(request);
    }

    onDeleteRow(id: string) {
        this.memberService.delete(id).subscribe(() => {
            this.loadMembers();
            this.tableService.publishTableEvent({ name: 'clearSearch', domain: 'Member', data: {} } as TableEvent);
        });
    }

    notifyDownload(type: string): void {
        this.messageService.displayExpiringModalMessage('Members List Download', `You our downloading the ${type} file version of the Members List. Please wait.`, undefined, 5000);
    }

    onFileUploadLinkClicked() {
        const modalRef = this.modalService.open(MembersUploadComponent, { centered: true });
        modalRef.componentInstance.title = 'Members List File Upload';
        modalRef.result.then((result) => {
            if (result) {
                this.tableService.publishTableEvent({ name: 'reset', domain: 'Member', data: {} } as TableEvent);
            }
        }, () => {});
    }

    isAllowedToUploadMembersList(): boolean {
        return this.loginService.hasRole('ROLE_MEMBER_ADMIN');
    }
 
    get headers(): TableHeader[] {
        return [
            { name: 'Member ID', field: 'memberId', sortable: true },
            { name: 'Last Name', field: 'lastName', sortable: true },
            { name: 'First Name', field: 'firstName', sortable: true },
            { name: 'Suffix', field: undefined, sortable: false },
            { name: 'Middle Name', field: undefined, sortable: false }
        ];
    }

    private loadMembers(request: TableReloadRequest = defaultTableReloadRequest('lastName')) {
        const dataCount = this.rows.length;
        this.memberService.list(request.page, request.size, request.keyword, request.sort, request.ascending, request.activeFilter).subscribe((members: Member[]) => {
            this.isLastRow = members.length === dataCount;
            if ((this.currentActiveFilter || '') !== (request.activeFilter || '')) {
                this.isLastRow = false;
            }
            this.currentActiveFilter = request.activeFilter;
            this.rows = this.toRows(members);
        });
    }

    private toRows(members: Member[]): TableRow[] {
        return members.map(member => {
            return {
                id: member.id,
                columns: [
                    { value: member.memberId },
                    { value: member.lastName },
                    { value: member.firstName },
                    { value: member.suffix },
                    { value: member.middleName }
                ],
                description: `${member.firstName} ${member.middleName ? member.middleName.substring(0, 1) : ''} ${member.lastName} ${member.suffix ? member.suffix : ''}`,
                disabled: false,
                active: member.active
            }
        });
    }

}
