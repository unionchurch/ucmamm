import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { formatNumber } from '@angular/common';

import { faChartBar } from '@fortawesome/free-solid-svg-icons';

import { Election } from '../elections/election';
import { ElectionService } from '../elections/election.service';
import { MemberService } from '../members/member.service';
import { MessageService } from '../common/message.service';

@Component({
    selector: 'app-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit, OnDestroy {

    id: string;

    faChartBar = faChartBar;

    activeElection: Election;
    activeElectionLoadingInterval: any;
    memberCount: any = {};

    constructor(private route: ActivatedRoute, private electionService: ElectionService, private memberService: MemberService, private messageService: MessageService) {
        this.id = this.route.snapshot.paramMap.get('id');
    }

    ngOnInit() {
        this.loadElectionInfo();
        this.memberService.count().subscribe((result: any) => this.memberCount = result);

        this.activeElectionLoadingInterval = setInterval(() => {
            this.loadElectionInfo();
            this.memberService.count().subscribe((result: any) => this.memberCount = result);
        }, 5000);
    }

    ngOnDestroy() {
        clearInterval(this.activeElectionLoadingInterval);
    }

    computePercent(value: number) {
        return value * 100 / this.activeElection.voted;
    }

    determineType(value: number) {
        const percent = this.computePercent(value);
        if (percent > 66.6) {
            return 'success';
        }
        if (percent > 50) {
            return 'warning';
        }
        return 'danger';
    }

    formatProgressbarLabel(value: number) {
        return `${formatNumber(this.computePercent(value), 'en', '1.2')}%`;
    }

    notifyDownloading() {
        this.messageService.displayExpiringModalMessage('Downloading Report', 'Your download is in-progress. Please wait.', undefined, 5000);
    }

    private loadElectionInfo(): void {
        if (this.id) {
            this.electionService.get(this.id).subscribe((election: Election) => this.activeElection = election);
        } else {
            this.electionService.getActive().subscribe((election: Election) => this.activeElection = election);
        }
    }

    get membersParticipated() {
        if (this.activeElection && this.memberCount) {
            return this.activeElection.voter * 100 / this.memberCount.active;
        }
        return 0;
    }

    get voted() {
        if (this.activeElection && this.activeElection.voted) {
            return this.activeElection.voted * 100 / this.activeElection.voter;
        }
        return 0;
    }

}
