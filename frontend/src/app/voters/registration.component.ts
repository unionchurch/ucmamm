import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';

import { MemberService } from '../members/member.service';
import { Member } from '../members/member';
import { ElectionService } from '../elections/election.service';
import { Election } from '../elections/election';
import { Voter, ProxyDetails } from './voter';
import { MessageService } from '../common/message.service';

@Component({
    selector: 'voter-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

    proxy: boolean = false;
    member: Member = new Member();
    proxyDetails: ProxyDetails;
    voterInput: FormGroup;
    activeElection: Election;

    search = (text$: Observable<string>) =>
        text$.pipe(
            debounceTime(300),
            distinctUntilChanged(),
            switchMap(term =>
                this.memberService.list(0, 10, term, 'lastName', true, '').pipe(
                catchError(() => of([]))
            )
        )
    );

    searchProxy = (text$: Observable<string>) =>
        text$.pipe(
            debounceTime(300),
            distinctUntilChanged(),
            switchMap(term =>
                this.memberService.list(0, 10, term, 'lastName', true, '').pipe(
                catchError(() => of([]))
            )
        )
    );

    formatter = (result: any) => `${result.firstName} ${result.lastName}`;

    constructor(private formBuilder: FormBuilder, private memberService: MemberService, private electionService: ElectionService, private messageService: MessageService) { }

    ngOnInit() {
        this.voterInput = this.formBuilder.group({
            memberId: [undefined],
            electionId: [''],
            proxy: [false],
            proxyMemberId: [undefined]
        });

        this.electionService.getActive().subscribe((election: Election) => {
            if (election) {
                this.activeElection = election;
                this.voterInput.get('electionId').setValue(election.id);
            } else {
                this.voterInput.disable();
            }
        }, (error: any) => this.voterInput.disable());

        this.onRegisterSuccess = this.onRegisterSuccess.bind(this);
    }

    onSubmit() {
        const voter = this.voterInput.value as Voter;
        voter.proxy = this.proxy;
        voter.proxyMemberId = this.proxy ? voter.proxyMemberId : undefined;

        this.electionService.registerVoter(voter).subscribe((error: HttpErrorResponse) => {
            if (error) {
                this.messageService.displayFormError(error, 'Register Voter Error');
            } else {
                this.messageService.displayModalMessage('Register Voter', `You have successfully registered voter ${this.member.firstName} ${this.member.lastName}`, this.onRegisterSuccess);
            }
        }, (error) => this.messageService.displayFormError(error, 'Register Voter Error'));
    }

    registerVoter(event: NgbTypeaheadSelectItemEvent) {
        this.member = event.item as Member;
        this.voterInput.get('memberId').setValue(this.member.memberId);
    }

    addProxy(event: NgbTypeaheadSelectItemEvent) {
        const member = event.item as Member;
        this.proxyDetails = {
            memberId: member.memberId,
            firstName: member.firstName,
            middleName: member.middleName,
            lastName: member.lastName,
            suffix: member.suffix
        };
        this.voterInput.get('proxyMemberId').setValue(member.memberId);
    }

    private onRegisterSuccess(): void {
        this.member = new Member();
        this.voterInput.reset();
        this.voterInput.get('electionId').setValue(this.activeElection.id);
        this.proxy = false;
        this.proxyDetails = {
            memberId: null,
            firstName: '',
            middleName: '',
            lastName: '',
            suffix: ''
        };
        document.getElementById('clearSearch').click();
    }

}
