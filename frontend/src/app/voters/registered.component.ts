import { Component, OnInit, OnDestroy } from '@angular/core';

import { faPollH } from '@fortawesome/free-solid-svg-icons';

import { TableRow, TableHeader, TableReloadRequest, defaultTableReloadRequest } from '../common/table';
import { TableService } from '../common/table.service';
import { TableEvent } from '../common/table.event';
import { Voter, RegistrationUpdateEvent } from './voter';
import { ElectionService } from '../elections/election.service';
import { Election } from '../elections/election';

@Component({
    selector: 'voters-registered',
    templateUrl: './registered.component.html',
    styleUrls: ['./registered.component.css']
})
export class RegisteredComponent implements OnInit, OnDestroy {

    faPollH = faPollH;

    electionId: string;
    rows: TableRow[] = [];
    isLastRow = false;

    electionFormLink = '/members/form';
    activeFilterShown = true

    votersLoadingInterval: any;

    constructor(private electionService: ElectionService, private tableService: TableService) { }

    ngOnInit() {
        this.loadVoters = this.loadVoters.bind(this);
        this.votersLoadingInterval = setInterval(() => this.loadVoters(defaultTableReloadRequest("lastName"), true), 60000);

        this.loadVoters();

        this.electionService.registrationEvent.subscribe((event: RegistrationUpdateEvent) => {
            if (event) {
                this.loadVoters(defaultTableReloadRequest("lastName"), true);
            }
        });
    }

    ngOnDestroy() {
        clearInterval(this.votersLoadingInterval);
    }

    onTableReloadRequest(request: TableReloadRequest) {
        this.loadVoters(request);
    }

    onDeleteRow(memberId: string) {
        this.electionService.unregisterVoter(memberId, this.electionId).subscribe(() => {
            this.loadVoters();
            this.tableService.publishTableEvent({ name: 'clearSearch', domain: 'Election', data: {} } as TableEvent);
        });
    }

    get headers(): TableHeader[] {
        return [
            { name: 'Member ID', field: 'memberId', sortable: true },
            { name: 'Last Name', field: 'lastName', sortable: true },
            { name: 'First Name', field: 'firstName', sortable: true },
            { name: 'Suffix', field: 'suffix', sortable: false },
            { name: 'Proxy', field: 'proxy', sortable: true }
        ];
    }

    private loadVoters(request: TableReloadRequest = defaultTableReloadRequest('lastName'), intervalTriggered: boolean = false) {
        const dataCount = this.rows.length;
        this.electionService.getActive().subscribe((election: Election) => {
            this.electionId = election.id;
            this.electionService.listVoters(election.id, request.page, request.size, request.keyword, request.sort, request.ascending).subscribe((voters: Voter[]) => {
                if (intervalTriggered) {
                    this.isLastRow = voters.length < 20;
                } else {
                    this.isLastRow = voters.length === dataCount;
                }
                this.rows = this.toRows(voters);
            });
        });
    }

    private toRows(voters: Voter[]): TableRow[] {
        return voters.map((voter: Voter) => {
            return {
                id: voter.memberId.toString(),
                columns: [
                    { value: voter.memberId },
                    { value: voter.lastName },
                    { value: voter.firstName },
                    { value: voter.suffix },
                    { value: this.buildVoterName(voter.proxyDetails) }
                ],
                description: this.buildVoterName(voter),
                disabled: false,
                active: true
             }
        });
    }

    private buildVoterName(voter: any): string {
        return voter ? `${voter.firstName}  ${voter.lastName}${voter.suffix ? ' ' + voter.suffix : ''}` : '';
    }

}
