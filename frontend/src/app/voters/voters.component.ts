import { Component, OnInit } from '@angular/core';

import { faFingerprint } from '@fortawesome/free-solid-svg-icons';

import { LoginService } from '../common/login.service';

@Component({
    selector: 'app-voters',
    templateUrl: './voters.component.html',
    styleUrls: ['./voters.component.css']
})
export class VotersComponent implements OnInit {

    constructor(private loginService: LoginService) { }

    faFingerprint = faFingerprint;

    ngOnInit() {

    }

    hasRole(role: string) {
        return this.loginService.hasRole(role);
    }

}
