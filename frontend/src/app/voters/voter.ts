export class Voter {
    id: string;
    electionId: string;
    memberId: number;
    firstName: string;
    middleName: string;
    lastName: string;
    suffix: string;
    proxy: boolean;
    proxyMemberId: number;
    proxyDetails: ProxyDetails;
    registeredDate: Date;
    registeredBy: string;
}

export class ProxyDetails {
    memberId: number;
    firstName: string;
    middleName: string;
    lastName: string;
    suffix: string;
}

export class RegistrationUpdateEvent {
    voter: Voter;
}