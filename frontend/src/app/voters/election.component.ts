import { Component, OnInit, OnDestroy } from '@angular/core';

import { ElectionService } from '../elections/election.service';
import { Election } from '../elections/election';
import { MemberService } from '../members/member.service';
import { RegistrationUpdateEvent } from './voter';

@Component({
    selector: 'election-info',
    templateUrl: './election.component.html',
    styleUrls: ['./election.component.css']
})
export class ElectionComponent implements OnInit, OnDestroy {

    activeElection: Election;
    membersCount: number = 0;
    date: Date;
    startTime12Hour: Date
    endTime12Hour: Date
    electionDetailsUpdateInterval: any;

    constructor(private electionService: ElectionService, private memberService: MemberService) { }

    ngOnInit() {
        this.updateElectionDetails = this.updateElectionDetails.bind(this);
        this.electionDetailsUpdateInterval = setInterval(this.updateElectionDetails, 60000);

        this.updateElectionDetails();

        this.electionService.registrationEvent.subscribe((event: RegistrationUpdateEvent) => {
            if (event) {
                this.updateElectionDetails();
            }
        });

        this.memberService.count().subscribe((result: any) => this.membersCount = result.active);
    }

    ngOnDestroy() {
        clearInterval(this.electionDetailsUpdateInterval);
    }

    private updateElectionDetails(): void {
        this.electionService.getActive().subscribe((election: Election) => {
            this.activeElection = election;
            this.date = new Date(`${election.date}T00:00:00`);
            this.startTime12Hour = new Date(`${election.date}T${election.startTime}`);
            this.endTime12Hour = new Date(`${election.date}T${election.endTime}`);
        });
    }

}
