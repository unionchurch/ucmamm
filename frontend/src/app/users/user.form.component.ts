import { Component } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators, ValidatorFn, ValidationErrors, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { faUser } from '@fortawesome/free-solid-svg-icons';

import { User, Role } from '../common/user';
import { MessageService } from '../common/message.service';
import { UserService } from './user.service';
import { BaseFormComponent } from '../common/base.form.component';

@Component({
    selector: 'app-user-form',
    templateUrl: './user.form.component.html',
    styleUrls: ['./user.form.component.css']
})
export class UserFormComponent extends BaseFormComponent<User, UserService> {

    faUser = faUser;

    private DUMMY_PASSWORD: string = '                ';

    update: boolean;
    roleList: Role[] = [];
    userRoleList: Role[] = [];
  
    private confirmPasswordValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
        const password = control.get('password').value;
        const confirmPassword = control.get('confirmPassword').value;

        if (password && confirmPassword) {
            return password === confirmPassword ? null : { passwordsDidntMatch: true };
        }

        return null;
    };
  
    constructor(public formBuilder: FormBuilder, public router: Router, public route: ActivatedRoute, public messageService: MessageService, public userService: UserService) {
        super(formBuilder, router, route, messageService);
        this.userService = userService;
    }

    buildForm(): FormGroup {        
        return this.formBuilder.group({
            userName: ['', Validators.required],
            password: this.update ? [''] : ['', Validators.required],
            confirmPassword: this.update ? [''] : ['', Validators.required],
            firstName: ['', Validators.required],
            middleName: [''],
            lastName: ['', Validators.required],
            roles: this.formBuilder.array([])
        }, { validator: this.update ? undefined : this.confirmPasswordValidator });
    }

    initializeOtherFormVariables(form: FormGroup) {
        if (!this.isUpdate()) {
            this.userService.listRoles().subscribe(roles => {
                this.roleList = roles as Role[];
                const roleCheckboxControls = this.roleList.map(role => this.formBuilder.control(role.roleName));            
                form.setControl('roles', this.formBuilder.array(roleCheckboxControls));
            });
        }
    }

    updateFormValues(form: FormGroup, user: User) {
        this.userService.listRoles().subscribe(roles => {
            this.roleList = roles as Role[];
            this.userRoleList = this.getUserRoles(user, roles);
            const roleCheckboxControls = this.roleList.map(role => this.formBuilder.control(role.roleName));            
            form.setControl('roles', this.formBuilder.array(roleCheckboxControls));
            
            this.getForm().setValue({
                userName: user.userName,
                password: this.DUMMY_PASSWORD,
                confirmPassword: this.DUMMY_PASSWORD,
                firstName: user.firstName,
                middleName: user.middleName,
                lastName: user.lastName,
                roles: this.updateRoleCheckboxControlValues(user.roles),
            });

            this.viewOnly && this.getForm().disable();
        });
    }

    updateRoleCheckboxControlValues(roles: string[]): boolean[] {
        return this.roleList.map(role => roles.indexOf(role.roleName) > -1);
    }

    getUserRoles(user: User, roles: Role[]): Role[] {
        const userRoles = user.roles || [];
        return roles.filter(role => userRoles.indexOf(role.roleName) > -1);
    }

    onSubmit() {
        const user = this.userForm.value;
        delete user.confirmPassword;

        user.roles = [];

        const roles = this.getForm().get('roles') as FormArray;
        let roleIndex = 0;
        while (roleIndex < roles.length) {
            const role = roles.at(roleIndex);
            role.value && user.roles.push(this.roleList[roleIndex].roleName);
            roleIndex++;
        }

        if (this.update) {
            user.id = this.getId();

            this.userService.update(user as User).subscribe((error: HttpErrorResponse) => {
                if (error) {
                    this.onUpdateError(error);
                } else {
                    this.onUpdateSuccess(user);
                }
            });
        } else {
            this.userService.create(user as User).subscribe((error: HttpErrorResponse) => {
                if (error) {
                    this.onCreateError(error);
                } else {
                    this.onCreateSuccess(user);
                }
            });
        }
    }

    isUpdate() {
        return this.getId() != undefined;
    }

    getDomain() {
        return 'Administrator';
    }

    getMainService() {
        return this.userService;
    }

    prepareDomainToUpdate(user: User) {
        user.id = this.getId();
    }

    checkAllRoles() {
        const roles = this.getForm().get('roles') as FormArray;
        let roleIndex = 0;
        while (roleIndex < roles.length) {
            roles.at(roleIndex).setValue(true);
            roleIndex++;
        }
    }

    uncheckAllRoles() {
        const roles = this.getForm().get('roles') as FormArray;
        let roleIndex = 0;
        while (roleIndex < roles.length) {
            roles.at(roleIndex).setValue(false);
            roleIndex++;
        }
    }

    get passwordsMatched() {
        const password1 = this.userForm.get('password').value;
        const password2 = this.userForm.get('confirmPassword').value;
        return !password1 || !password2 || password1 === password2;
    }

    get userForm(): FormGroup {
        return this.getForm();
    }
}
