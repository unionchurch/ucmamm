import { Component } from '@angular/core';

import { faKey } from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, Validators } from '@angular/forms';
import { BaseFormComponent } from '../common/base.form.component';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from '../common/message.service';
import { UserService } from './user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { LoginService } from '../common/login.service';

@Component({
    selector: 'app-changepassword',
    templateUrl: './changepassword.component.html',
    styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent extends BaseFormComponent<any, UserService> {

    faKey = faKey;

    constructor(public formBuilder: FormBuilder, public router: Router, public route: ActivatedRoute, public messageService: MessageService, private userService: UserService, private loginService: LoginService) {
        super(formBuilder, router, route, messageService);
    }

    onSubmit(): void {
        const form = this.getForm();

        const changepassword = form.value;

        this.getMainService().changepassword({
            oldPassword: btoa(changepassword.oldPassword),
            newPassword: btoa(changepassword.newPassword),
            confirmPassword: btoa(changepassword.confirmPassword),
            userName: this.loginService.getUser().userName,
        }).subscribe((error: HttpErrorResponse) => {
            if (error) {
                this.onUpdateError(error);
            } else {
                this.onUpdateSuccess(form);
                this.getForm().reset();
            }
        });
    }

    onUpdateSuccess(domain: any): void {
        this.messageService.displayModalMessage(`Update Password Successful`, 'You have successfully updated your password', this.routeToList);
    }

    onUpdateError(error: HttpErrorResponse): void {
        this.messageService.displayFormError(error, `Update Password Error`, ['electionIdAndActive', 'userNamePassword']);
    }

    buildForm() {
        return this.formBuilder.group({
            oldPassword: ['', Validators.required],
            newPassword: ['', Validators.required],
            confirmPassword: ['', Validators.required],
        });
    }

    getDomain() {
        return 'User';
    }

    getMainService() {
        return this.userService;
    }

    prepareDomainToUpdate() {

    }

    updateFormValues() {

    }

    get changepasswordForm() {
        return this.getForm();
    }
}
