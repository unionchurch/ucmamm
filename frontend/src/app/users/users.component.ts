import { Component, OnInit } from '@angular/core';

import { faUser, faUsers } from '@fortawesome/free-solid-svg-icons';

import { TableRow, TableHeader, TableReloadRequest, defaultTableReloadRequest } from '../common/table';
import { User } from '../common/user';
import { UserService } from '../users/user.service';
import { LoginService } from '../common/login.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  rows: TableRow[] = [];
  isLastRow = false;

  faUser = faUser;
  faUsers = faUsers;
  administratorFormLink = '/administrators/form';

  constructor(private userService: UserService, private loginService: LoginService) { }

  ngOnInit() {
    this.loadUsers();
  }

  onTableReloadRequest(request: TableReloadRequest) {
    this.loadUsers(request);
  }

  onDeleteRow(id: string) {
    this.userService.delete(id).subscribe(() => this.loadUsers());
  }

  get headers(): TableHeader[] {
    return [
      { name: 'User Name', field: 'userName', sortable: true },
      { name: 'Last Name', field: 'lastName', sortable: true },
      { name: 'First Name', field: 'firstName', sortable: true },
      { name: 'Middle Name', field: undefined, sortable: false }
    ];
  }

  private loadUsers(request: TableReloadRequest = defaultTableReloadRequest('userName')) {
    const dataCount = this.rows.length;
    this.userService.list(request.page, request.size, request.keyword, request.sort, request.ascending).subscribe((users: User[]) => {
      this.isLastRow = users.length === dataCount;
      this.rows = this.toRows(users);
    });
  }

  private toRows(users: User[]): TableRow[] {
    const currentUser = this.loginService.getUser();
    return users.map(user => {
      return {
        id: user.id,
        columns: [
          { value: user.userName },
          { value: user.lastName },
          { value: user.firstName },
          { value: user.middleName }
        ],
        description: `${user.userName} (${user.firstName} ${user.lastName})`,
        disabled: currentUser.id === user.id,
        active: user.active
      }
    });
  }

}
