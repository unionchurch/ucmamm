import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { OperatorFunction, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User, Role } from '../common/user';
import { RestapiService } from '../common/restapi.service';
import { BaseService, errorMapper } from '../common/base.service';

@Injectable({
  providedIn: 'root'
})
export class UserService implements BaseService<User> {

    private baseUri = 'api/v1/users';
    private rolesBaseUri = 'api/v1/roles';
    private changepasswordBaseUri = 'api/v1/changepassword';
    private userMapper:OperatorFunction<{}, User> = map(value => value as User);
    private userArrayMapper:OperatorFunction<{}, User[]> = map(value => value as User[]);
    private roleArrayMapper:OperatorFunction<{}, Role[]> = map(value => value as Role[]);

    constructor(private restapi: RestapiService) { }

    getCurrentUser() {
        return this.restapi.get<User>('user', 'Get Current User');
    }

    get(id: string): Observable<User> {
        return this.restapi.get<User>(`${this.baseUri}/${id}`, 'Get User').pipe(this.userMapper);
    }

    list(page: number, size: number, keyword: string, sort: string, ascending: boolean): Observable<User[]> {
        return this.restapi.get<User[]>(this.baseUri, 'Get Users', { page, size, keyword, sort, ascending }).pipe(this.userArrayMapper);
    }

    create(user: User): Observable<HttpErrorResponse> {
        return this.restapi.post<User>(this.baseUri, user, 'Create New User').pipe(errorMapper);
    }

    update(user: User): Observable<HttpErrorResponse> {
        return this.restapi.put<User>(`${this.baseUri}/${user.id}`, user, 'Update User').pipe(errorMapper);
    }

    delete(id: string): Observable<HttpErrorResponse> {
        return this.restapi.delete(`${this.baseUri}/${id}`, 'Delete User').pipe(errorMapper);
    }

    listRoles(): Observable<Role[]> {
        return this.restapi.get(this.rolesBaseUri, 'Get All Roles').pipe(this.roleArrayMapper);
    }

    changepassword(input: any): Observable<HttpErrorResponse> {
        return this.restapi.post<any>(this.changepasswordBaseUri, input, 'Change Password').pipe(errorMapper);
    }

}
