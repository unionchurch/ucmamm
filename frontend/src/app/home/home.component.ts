import { Component, OnInit } from '@angular/core';

import { faHome } from '@fortawesome/free-solid-svg-icons';

import { ElectionService } from '../elections/election.service';
import { Election } from '../elections/election';
import { MemberService } from '../members/member.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    faHome = faHome;

    activeElection: Election;
    activeMembers: number;
    currentDate: Date = new Date();

    constructor(private electionService: ElectionService, private memberService: MemberService) { }

    ngOnInit() {
        this.electionService.getActive().subscribe((election: Election) => this.activeElection = election);

        this.memberService.count().subscribe((result: any) => this.activeMembers = result.active);
    }

    get participationPercentage() {
        if (this.activeElection && this.activeMembers) {
            return (this.activeElection.voter / this.activeMembers) * 100;
        }
        return 0;
    }

}
