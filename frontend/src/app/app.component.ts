import { Component } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons'

import { User } from './common/user';
import { LoginService } from './common/login.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'frontend';
    isLoginPage = false;
    isCollapsed = true;

    faSignOutAlt = faSignOutAlt;

    navStart: Observable<NavigationStart>;

    constructor(private router: Router, public loginService: LoginService) {
        this.navStart = router.events.pipe(
            filter(event => event instanceof NavigationStart)
        ) as Observable<NavigationStart>;
    }

    ngOnInit() {
        this.navStart.subscribe(event => this.isLoginPage = event.url === '/login');

        this.loginService.validateToken();
        this.loginService.getCurrentUser().subscribe();
        this.loginService.loginEvents.subscribe(event => {
            if (!event.loggedIn && (this.router.url !== '/' && this.router.url !== '/ballot' && !this.router.url.startsWith('/reports'))) {
                this.router.navigate(['/']);
            }
        });
    }

    logout() {
        this.loginService.setLogin(false);
        this.loginService.setUser(undefined);
        location.reload();
    }

    hasRole(roleName: string): boolean {
        return this.loginService.hasRole(roleName);
    }

    get fullName(): string {
        const user = this.loginService.getUser();
        return user !== undefined ? `${user.firstName} ${user.lastName}` : '';
    }

    get userName(): string {
        return (this.loginService.getUser() || new User()).userName;
    }

}
