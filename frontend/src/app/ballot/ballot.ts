export class SeatVote {
    candidateId: string;
    approved: boolean;
}

export class Ballot {
    id: string;
    time: string;
    memberId: number;
    electionId: string;
    proposals: any;
    questions: any;
    seats: any;
    deleted: boolean;
    tallied: boolean;
}