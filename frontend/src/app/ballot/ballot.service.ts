import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { Observable, OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';

import { RestapiService } from '../common/restapi.service';
import { Ballot } from './ballot';

@Injectable({
  providedIn: 'root'
})
export class BallotService {

    private baseUri = 'api/v1/votes';
    private errorMapper: OperatorFunction<{}, HttpErrorResponse> = map(value => value as HttpErrorResponse);

    constructor(private restapi: RestapiService) { }

    vote(ballot: Ballot): Observable<{}> {
        return this.restapi.post(`${this.baseUri}`, ballot, 'Submit Vote').pipe(this.errorMapper);
    }

}
