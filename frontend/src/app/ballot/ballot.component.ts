import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

import { faVoteYea, faCheck, faTimes, faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import { BallotService } from './ballot.service';
import { MessageService } from '../common/message.service';
import { ElectionService } from '../elections/election.service';
import { Election } from '../elections/election';
import { Proposal, Question, Seat, Candidate } from '../elections/election.data';
import { Ballot } from './ballot';

@Component({
    selector: 'app-ballot',
    templateUrl: './ballot.component.html',
    styleUrls: ['./ballot.component.css']
})
export class BallotComponent implements OnInit {

    faVoteYea = faVoteYea;
    faCheck = faCheck;
    faTimes = faTimes;
    faArrowLeft = faArrowLeft;

    activeElection: Election;
    date: Date;
    startTime12Hour: Date;
    endTime12Hour: Date;
    proposalList: Proposal[];
    questionList: Question[];
    seatList: Seat[];
    ballot: FormGroup;
    proposals: FormGroup;
    questions: FormGroup;
    seats: FormGroup;

    constructor(public formBuilder: FormBuilder, public messageService: MessageService, public electionService: ElectionService, public ballotService: BallotService) { }

    ngOnInit() {
        this.ballot = this.formBuilder.group({
            memberId: [undefined, Validators.required],
            electionId: ['']
        });

        this.electionService.getActive().subscribe((election: Election) => {
            this.activeElection = election;
            this.ballot.get('electionId').setValue(election.id);
            this.date = new Date(`${election.date}T00:00:00`);
            this.startTime12Hour = new Date(`${election.date}T${election.startTime}`);
            this.endTime12Hour = new Date(`${election.date}T${election.endTime}`);
            this.proposalList = election.proposals;
            this.questionList = election.questions;
            this.seatList = election.seats;
            this.ballot.addControl('proposals', this.buildProposalsGroup());
            this.proposals = this.ballot.get('proposals') as FormGroup;
            this.ballot.addControl('questions', this.buildQuestionsGroup());
            this.questions = this.ballot.get('questions') as FormGroup;
            this.ballot.addControl('seats', this.buildSeatsGroup());
            this.seats = this.ballot.get('seats') as FormGroup;
        });
    }

    onSubmit() {
        const ballot = this.ballot.value as Ballot;
        this.ballotService.vote(ballot).subscribe((error: HttpErrorResponse) => {
            if (error) {
                this.messageService.displayFormError(error, 'Submit Vote Error', ['electionId']);
            } else {
                this.messageService.displayModalMessage('Submit Vote Successful', `You have successfully submitted a vote for member ${ballot.memberId}`, () => {
                    this.ballot.get('memberId').reset();
                    if (this.proposals) {
                        this.proposals.reset();
                    }
                    if (this.questions) {
                        this.questions.reset();
                    }
                    if (this.seats) {
                        Object.keys(this.seats.controls).forEach((key) => {
                            const seat = this.seats.get(key) as FormArray;
                            for (let seatVote of seat.controls) {
                                seatVote.get('approved').reset();
                            }
                        });
                    }
                });
            }
        }, (error: any) => {
            this.messageService.displayFormError(error, 'Submit Vote Error', ['electionId']);
        });
    }

    approveAllProposals() {
        this.proposalList.forEach((proposal: Proposal) => this.proposals.get(proposal.id).setValue(true));
    }

    disapproveAllProposals() {
        this.proposalList.forEach((proposal: Proposal) => this.proposals.get(proposal.id).setValue(false));
    }

    approveAllCandidates(seat: Seat) {
        const array = this.seats.get(seat.id) as FormArray;
        (seat.candidates || []).forEach((candidate: Candidate, index: number) => {
            array.at(index).get('approved').setValue(true);
        });
    }

    disapproveAllCandidates(seat: Seat) {
        const array = this.seats.get(seat.id) as FormArray;
        (seat.candidates || []).forEach((candidate: Candidate, index: number) => {
            array.at(index).get('approved').setValue(false);
        });
    }

    private buildProposalsGroup(): FormGroup {
        const group = this.formBuilder.group({});
        if (this.proposalList && this.proposalList.length) {
            this.proposalList.forEach((proposal: Proposal) => {
                group.addControl(proposal.id, this.formBuilder.control(false));
            });
        }
        return group;
    }

    private buildQuestionsGroup(): FormGroup {
        const group = this.formBuilder.group({});
        if (this.questionList && this.questionList.length) {
            this.questionList.forEach((question: Question) => {
                group.addControl(question.id, this.formBuilder.control(''));
            });
        }
        return group;
    }

    private buildSeatsGroup(): FormGroup {
        const group = this.formBuilder.group({});
        if (this.seatList && this.seatList.length) {
            this.seatList.forEach((seat: Seat) => {
                group.addControl(seat.id, this.buildCandidatesArray(seat));
            });
        }
        return group;
    }

    private buildCandidatesArray(seat: Seat): FormArray {
        return this.formBuilder.array((seat.candidates || []).map((candidate: Candidate) => this.formBuilder.group({
            candidateId: [candidate.id],
            approved: [false]
        })));
    }
}
