import { Component, OnInit } from '@angular/core';

import { faPollH } from '@fortawesome/free-solid-svg-icons';

import { TableRow, TableHeader, TableReloadRequest } from '../common/table';
import { TableService } from '../common/table.service';
import { TableEvent } from '../common/table.event';
import { Election } from './election';
import { ElectionService } from './election.service';

const defaultTableReloadRequest = (sort: string) => {
    return { page: 0, size: 20, keyword: '', sort, ascending: false, activeFilter: '' } as TableReloadRequest;
}

@Component({
  selector: 'app-elections',
  templateUrl: './elections.component.html',
  styleUrls: ['./elections.component.css']
})
export class ElectionsComponent implements OnInit {

    rows: TableRow[] = [];
    isLastRow = false;

    electionFormLink = '/elections/form';
    activeFilterShown = true

    faPollH = faPollH;

    constructor(private electionService: ElectionService, private tableService: TableService) { }

    ngOnInit() {
        this.loadElections();
    }

    onTableReloadRequest(request: TableReloadRequest) {
        this.loadElections(request);
    }

    onDeleteRow(id: string) {
        this.electionService.delete(id).subscribe(() => {
            this.loadElections();
            this.tableService.publishTableEvent({ name: 'clearSearch', domain: 'Election', data: {} } as TableEvent);
        });
    }

    get headers(): TableHeader[] {
        return [
            { name: 'Date', field: 'date', sortable: true },
            { name: 'Description', field: 'name', sortable: true },
            { name: 'Start Time', field: 'startTime', sortable: false },
            { name: 'End Time', field: 'endTime', sortable: false },
            { name: '', field: 'active', sortable: false }
        ];
    }

    private loadElections(request: TableReloadRequest = defaultTableReloadRequest('date')) {
        const dataCount = this.rows.length;
        this.electionService.list(request.page, request.size, request.keyword, request.sort, request.ascending).subscribe((elections: Election[]) => {
            this.isLastRow = elections.length === dataCount;
            this.rows = this.toRows(elections);
        });
    }

    private toRows(elections: Election[]): TableRow[] {
        return elections.map(election => {
            return {
                id: election.id,
                columns: [
                    { value: election.date },
                    { value: election.name },
                    { value: election.startTime },
                    { value: election.endTime },
                    { value: election.active ? 'ACTIVE' : 'INACTIVE' }
                ],
                description: `${election.date} - ${election.name}`,
                disabled: false,
                active: election.active
             }
        });
    }

}
