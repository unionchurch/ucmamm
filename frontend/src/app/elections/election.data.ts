export class Proposal {
    id: string;
    content: string;
    approved: number;
    disapproved: number;
}

export class Answer {
    id: string;
    content: string;
    selected: number;
}

export class Question {
    id: string;
    content: string;
    answers: Answer[];
}

export class Candidate {
    id: string;
    memberId: number;
    firstName: string;
    middleName: string;
    lastName: string;
    suffix: string;
    approved: number;
    disapproved: number;
}

export class Seat {
    id: string;
    name: string;
    count: number;
    candidates: Candidate[];
}
