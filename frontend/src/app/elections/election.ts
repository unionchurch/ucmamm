export class Election {
    id: string;
    name: string;
    date: string;
    startTime: string;
    endTime: string;
    proposals: any;
    questions: any;
    seats: any;
    deleted: boolean;
    active: boolean;
    version: number;
    voter: number;
    proxy: number;
    voted: number;
    proxyVoted: number;
}
