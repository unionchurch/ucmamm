import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { Observable, OperatorFunction, Observer } from 'rxjs';
import { map, flatMap, catchError, tap } from 'rxjs/operators';

import { BaseService, errorMapper } from '../common/base.service';
import { RestapiService } from '../common/restapi.service';
import { Election } from './election';
import { Voter, RegistrationUpdateEvent } from '../voters/voter';

@Injectable({
    providedIn: 'root'
})
export class ElectionService implements BaseService<Election> {

    private baseUri = 'api/v1/elections';
    private electionMapper: OperatorFunction<{}, Election> = map(value => value as Election);
    private electionArrayMapper: OperatorFunction<{}, Election[]> = map(value => value as Election[]);

    private lastRegisteredVoter: Voter;
    private registrationUpdateHappened: boolean;
    
    registrationEvent = new Observable<RegistrationUpdateEvent>((observer: Observer<RegistrationUpdateEvent>) => {
        
        const nextInterval = setInterval(() => {
            if (this.registrationUpdateHappened) {
                observer.next({ voter: this.lastRegisteredVoter });
                setTimeout(() => this.registrationUpdateHappened = false, 1000);
            }
        }, 1000);

        return { unsubscribe() { clearInterval(nextInterval); } };
    });

    constructor(private restapi: RestapiService) { }

    get(id: string): Observable<Election> {
        return this.restapi.getNoAuth<Election>(`${this.baseUri}/${id}`, 'Get Election').pipe(this.electionMapper);
    }

    getActive(): Observable<Election> {
        return this.restapi.getNoAuth<Election>(`${this.baseUri}/active`).pipe(this.electionMapper);
    }

    list(page: number, size: number, keyword: string, sort: string, ascending: boolean): Observable<Election[]> {
        return this.restapi.get<Election[]>(this.baseUri, 'Get Elections', { page, size, keyword, sort, ascending }).pipe(this.electionArrayMapper);
    }

    listVoters(id: string, page: number, size: number, keyword: string, sort: string, ascending: boolean): Observable<Voter[]> {
        return this.restapi.get<Voter[]>(`${this.baseUri}/${id}/voters`, 'Get Elections', { page, size, keyword, sort, ascending }).pipe(map(value => value as Voter[]));
    }

    create(election: Election): Observable<HttpErrorResponse> {
        return this.restapi.post<Election>(this.baseUri, election, 'Create New Election').pipe(errorMapper);
    }

    createAndActivate(election: Election): Observable<HttpErrorResponse> {
        election.active = true;
        return this.getActive().pipe(
            flatMap((activeElection: Election) => {
                return this.deactivate(activeElection).pipe(
                    flatMap(() => this.create(election))
                );
            }),
            catchError(() => this.create(election))
        );
    }

    update(election: Election): Observable<HttpErrorResponse> {
        return this.restapi.put<Election>(`${this.baseUri}/${election.id}`, election, 'Update Election').pipe(errorMapper);
    }

    updateAndActivate(election: Election) {
        election.active = true;
        return this.getActive().pipe(
            flatMap((activeElection: Election) => {
                return this.deactivate(activeElection).pipe(
                    flatMap(() => this.update(election))
                );
            }),
            catchError(() => this.update(election))
        );
    }

    delete(id: string): Observable<HttpErrorResponse> {
        return this.restapi.delete(`${this.baseUri}/${id}`, 'Delete Election').pipe(errorMapper);
    }

    activate(election: Election): Observable<HttpErrorResponse> {
        return this.restapi.put(`${this.baseUri}/${election.id}/activate`, undefined, 'Activate Election').pipe(errorMapper);
    }

    deactivate(election: Election): Observable<HttpErrorResponse> {
        return this.restapi.put(`${this.baseUri}/${election.id}/deactivate`, undefined, 'Activate Election').pipe(errorMapper);
    }

    registerVoter(voterInput: Voter): Observable<HttpErrorResponse> {
        return this.restapi.post(`${this.baseUri}/${voterInput.electionId}/voters`, voterInput, 'Register Voter').pipe(
            tap(() => {
                this.lastRegisteredVoter = voterInput;
                this.registrationUpdateHappened = true;
            }),
            errorMapper
        );
    }

    unregisterVoter(memberId: string, electionId: string): Observable<HttpErrorResponse> {
        return this.restapi.delete(`${this.baseUri}/${electionId}/voters/${memberId}`, 'Unregister Voter', {}).pipe(
            tap(() => {
                this.lastRegisteredVoter = new Voter();
                this.registrationUpdateHappened = true;
            }),
            errorMapper
        );
    }
}
