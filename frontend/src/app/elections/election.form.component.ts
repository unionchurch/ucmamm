import { Component } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { NgbDate, NgbDateAdapter, NgbTimeAdapter, NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import { faPollH, faPhone, faPlus, faTrashAlt, faEnvelope, faFileAlt, faQuestionCircle, faChair } from '@fortawesome/free-solid-svg-icons';

import { Election } from './election';
import { Proposal, Answer, Question, Candidate, Seat } from './election.data';
import { ElectionService } from './election.service';
import { MemberService } from '../members/member.service';
import { MessageService } from '../common/message.service';
import { LoginService } from '../common/login.service';
import { NgbSimpleDateAdapter } from '../common/datepicker.adapter';
import { NgbTimeStringAdapter } from '../common/timepicker.adapter';
import { BaseFormComponent } from '../common/base.form.component';

@Component({
    selector: 'app-election.form',
    templateUrl: './election.form.component.html',
    styleUrls: ['./election.form.component.css'],
    providers: [
        { provide: NgbDateAdapter, useClass: NgbSimpleDateAdapter },
        {  provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter }
    ]
})
export class ElectionFormComponent extends BaseFormComponent<Election, ElectionService> {

    faPollH = faPollH;
    faPhone = faPhone;
    faPlus = faPlus;
    faTrashAlt = faTrashAlt;
    faEnvelope = faEnvelope;
    faFileAlt = faFileAlt;
    faQuestionCircle = faQuestionCircle;
    faChair = faChair;
    update: boolean = false;
    viewOnly: boolean = false;
    active: boolean = false;
    showMeridian: boolean = true;
    proposals: FormArray;
    questions: FormArray;
    seats: FormArray;

    search = (text$: Observable<string>) =>
        text$.pipe(
            debounceTime(300),
            distinctUntilChanged(),
            switchMap(term =>
                this.memberService.list(0, 10, term, 'lastName', true, '').pipe(
                catchError(() => of([]))
            )
        )
    );
    
    formatter = (result: any) => `${result.firstName} ${result.lastName}`;

    private selectedMemberIds = {};
    private closedForModification: boolean;

    constructor(public formBuilder: FormBuilder, public router: Router, public route: ActivatedRoute, public electionService: ElectionService, public memberService: MemberService, public messageService: MessageService, private loginService: LoginService) {
        super(formBuilder, router, route, messageService);
        this.memberService = memberService;
        this.electionService = electionService;
    }

    buildForm(): FormGroup {
        return this.formBuilder.group({
            name: [''],
            date: [''],
            startTime: ['08:00:00'],
            endTime: ['13:00:00'],
            proposals: this.formBuilder.array([]),
            questions: this.formBuilder.array([]),
            seats: this.formBuilder.array([]),
            active: [false]
        });
    }

    initializeOtherFormVariables(form: FormGroup) {
        this.proposals = form.get('proposals') as FormArray;
        this.questions = form.get('questions') as FormArray;
        this.seats = form.get('seats') as FormArray;  
    }

    prepareDomainToUpdate(election: Election) {
        election.id = this.getId();
    }

    updateFormValues(form: FormGroup, election: Election): void {
        if (election.proposals) {
            election.proposals.forEach((proposal: Proposal) => this.proposals.push(this.createProposal(proposal, this.viewOnly)));
        }

        if (election.questions) {
            election.questions.forEach((question: Question) => this.questions.push(this.createQuestion(question, this.viewOnly)));
        }

        if (election.seats) {
            election.seats.forEach((seat: Seat) => this.seats.push(this.createSeat(seat, this.viewOnly)));
        }

        this.active = election.active;
        this.closedForModification = (election.voter || 0) > 0;
        
        if (!this.viewOnly && this.closedForModification) {
            this.messageService.displayModalMessage('Action Not Allowed', 'This election is already closed for modification.', this.routeToList);
        }

        form.setValue({
            name: election.name,
            date: election.date,
            startTime: election.startTime,
            endTime: election.endTime,
            proposals: election.proposals,
            questions: election.questions,
            seats: election.seats,
            active: election.active
        });
    }

    onDateSelect(date: NgbDate) {
        this.electionForm.get('name').setValue(`UCM Annual Members Meeting ${date.year}`);
    }

    addProposal() {
        this.proposals.push(this.createProposal(null, false));
    }

    removeProposal(i: number) {
        this.proposals.removeAt(i);
    }

    addAnswer(questionIndex: number) {
        const question = this.questions.at(questionIndex) as FormGroup;
        const answers = question.get('answers') as FormArray;
        answers.push(this.createAnswer(null, false));
    }

    removeAnswer(questionIndex: number, i: number) {
        const question = this.questions.at(questionIndex) as FormGroup;
        const answers = question.get('answers') as FormArray;
        answers.removeAt(i);
    }

    addQuestion(count: number = 1) {
        this.questions.push(this.createQuestion(null, false));
    }

    removeQuestion(i: number) {
        this.questions.removeAt(i);
    }

    addSeat(count: number = 1) {
        this.seats.push(this.createSeat(null, false));
    }

    addCandidate(event: NgbTypeaheadSelectItemEvent, seatIndex: number) {
        const seat = this.seats.at(seatIndex) as FormGroup;
        const candidates = seat.get('candidates') as FormArray;
        
        const candidate = event.item;
        if (!this.selectedMemberIds[candidate.memberId]) {
            candidates.push(this.createCandidate(candidate, false));
            this.selectedMemberIds[candidate.memberId] = true;
            document.getElementById('clearSearch').click();
        }
    }

    removeCandidate(seatIndex: number, candidateIndex: number) {
        const seat = this.seats.at(seatIndex) as FormGroup;
        const candidates = seat.get('candidates') as FormArray;

        const candidate = candidates.at(candidateIndex);

        candidates.removeAt(candidateIndex);
        delete this.selectedMemberIds[candidate.get('memberId').value];
    }

    removeSeat(i: number) {
        this.seats.removeAt(i);
    }

    createProposal(proposal: Proposal, disabled: boolean): FormGroup {
        const _proposal = proposal || new Proposal();
        const group = this.formBuilder.group({
            id: [_proposal.id || ''],
            content: [_proposal.content || ''],
            approved: [_proposal.approved || 0],
            disapproved: [_proposal.disapproved || 0]
        });
        disabled && group.disable();
        return group;
    }

    createAnswer(answer: Answer, disabled: boolean): FormGroup {
        const _answer = answer || new Answer();
        const group = this.formBuilder.group({
            id: [_answer.id || ''],
            content: [_answer.content || ''],
            selected: [_answer.selected || 0]
        });
        disabled && group.disable();
        return group;
    }

    createQuestion(question: Question, disabled: boolean): FormGroup {
        const _question = question || new Question();
        const group = this.formBuilder.group({
            id: [_question.id || ''],
            content: [_question.content || ''],
            answers: this.formBuilder.array((_question.answers || []).map((answer: Answer) => this.createAnswer(answer, this.viewOnly))),
        });
        disabled && group.disable();
        return group;
    }

    createCandidate(candidate: any, disabled: boolean): FormGroup {
        const _candidate = candidate || new Candidate();
        const group = this.formBuilder.group({
            id: [_candidate.id || ''],
            memberId: [_candidate.memberId || ''],
            firstName: [_candidate.firstName || ''],
            middleName: [_candidate.middleName || ''],
            lastName: [_candidate.lastName || ''],
            suffix: [_candidate.suffix || ''],
            approved: [_candidate.approved || 0],
            disapproved: [_candidate.disapproved || 0]
        });
        disabled && group.disable();
        return group;
    }

    createSeat(seat: Seat, disabled: boolean) {
        const _seat = seat || new Seat();
        const group = this.formBuilder.group({
            id: [_seat.id || ''],
            name: [_seat.name || ''],
            count: [_seat.count || 0],
            candidates: this.formBuilder.array((_seat.candidates || []).map((candidate: Candidate) => this.createCandidate(candidate, this.viewOnly)))
        });
        disabled && group.disable();
        return group;
    }

    submitAndActivateElection() {
        this.messageService.confirm('submit and activate this election', (result: any) => {
            if (result.approved) {
                const election = this.getForm().value;
                this.prepareDomainToUpdate(election);
                if (this.update) {
                    this.electionService.updateAndActivate(election).subscribe((error: HttpErrorResponse) => {
                        if (error) {
                            this.messageService.displayFormError(error, 'Update and Activate Election Error');
                        } else {
                            this.messageService.displayModalMessage('Update and Activate Election Success', 'You have successfully updated an active election.', () => { this.router.navigate(['/elections']); });
                        }
                    });
                } else {
                    this.electionService.createAndActivate(election).subscribe((error: HttpErrorResponse) => {
                        if (error) {
                            this.messageService.displayFormError(error, 'Create and Activate Election Error');
                        } else {
                            this.messageService.displayModalMessage('Create and Activate Election Success', 'You have successfully created an active election.', () => { this.router.navigate(['/elections']); });
                        }
                    });
                }
            }
        });
    }

    activateElection() {
        this.messageService.confirm('activate this election', (result: any) => {
            if (result.approved) {
                this.electionService.get(this.getId()).subscribe((election: Election) => {
                    this.electionService.activate(election).subscribe((error: HttpErrorResponse) => {
                        if (error) {
                            this.messageService.displayFormError(error, 'Activate Election Error');
                        } else {
                            this.active = true;
                            this.getForm().get('active').setValue(true);
                            this.messageService.displayModalMessage('Activate Election Success', 'You have successfully activated an election.', undefined);
                        }
                    });
                });
            }
        });
    }

    deactivateElection() {
        this.messageService.confirm('activate this election', (result: any) => {
            if (result.approved) {
                this.electionService.get(this.getId()).subscribe((election: Election) => {
                    this.electionService.deactivate(election).subscribe((error: HttpErrorResponse) => {
                        if (error) {
                            this.messageService.displayFormError(error, 'Activate Election Error');
                        } else {
                            this.active = false;
                            this.getForm().get('active').setValue(false);
                            this.messageService.displayModalMessage('Deactivate Election Success', 'You have successfully deactivated an election.', undefined);
                        }
                    });
                });
            }
        });
    }

    getDomain(): string {
        return 'Election';
    }

    getMainService(): ElectionService {
        return this.electionService;
    }

    loadEditForm() {
        if (this.closedForModification) {
            this.messageService.displayModalMessage('Action Not Allowed', 'This election is already closed for modification.', undefined);
        } else {
            super.loadEditForm();
        }
    }

    viewResult() {
        this.router.navigate([`/reports/${this.getId()}`]);
    }

    hasRole(role: string) {
        return this.loginService.hasRole(role);
    }

    get electionForm() {
        return this.getForm();
    }
}
