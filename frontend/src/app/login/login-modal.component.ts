import { Component, OnInit, Inject, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { LoginService } from '../common/login.service';
import { MessageService } from '../common/message.service';

import { AccessToken } from '../common/access.token';
import Keys from '../common/common.keys';

@Component({
    selector: 'login-modal',
    templateUrl: './login-modal.component.html',
    styleUrls: ['./login-modal.component.css']
})
export class LoginModalComponent implements OnInit {

    @ViewChild('content') loginModalTemplate: TemplateRef<any>;

    loginForm: FormGroup = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
    });

    constructor(private modalService: NgbModal,
                private loginService: LoginService,
                @Inject(SESSION_STORAGE) private storageService: StorageService,
                public messageService: MessageService,
                private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.loginService.protectedLinkClickedEvents.subscribe(event => this.open(this.loginModalTemplate));
    }

    open(content: any) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true});
    }

    login() {
        this.messageService.clear();
        this.loginService.getToken(this.loginForm.get('username').value, this.loginForm.get('password').value)
            .subscribe((response: AccessToken) => {
                if (response) {
                    this.saveTokenAndExpiry(response);
                    console.log('Log-in Successful!');
                    this.loginService.setLogin(true);
                    location.href = '/';
                } else {
                    this.loginService.setLogin(false);
                    console.log('Invalid Log-in!');
                }
            });
    }

    private saveTokenAndExpiry(response: AccessToken) {
        this.storageService.set(Keys.TOKEN_KEY, response.access_token);
        this.storageService.set(Keys.TOKEN_EXPIRY_KEY, response.expires_in);
    }

}
