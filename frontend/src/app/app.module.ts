import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { StorageServiceModule } from 'angular-webstorage-service';

import { NgbModalModule, NgbCollapseModule, NgbDatepickerModule, NgbTimepickerModule, NgbTypeaheadModule, NgbActiveModal, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';

import { environment } from '../environments/environment';

import { AuthGuard } from './auth/auth.guard';

import { AppComponent } from './app.component';
import { BallotComponent } from './ballot/ballot.component';
import { ElectionsComponent } from './elections/elections.component';
import { LoginModalComponent } from "./login/login-modal.component";
import { UsersComponent } from './users/users.component';
import { MembersComponent } from './members/members.component';
import { ReportsComponent } from './reports/reports.component';
import { UserFormComponent } from './users/user.form.component';
import { ErrorModalComponent } from './common/error.modal.component';
import { TableComponent } from './common/table.component';
import { TableSearchComponent } from './common/table.search.component';
import { DeleteModalComponent } from './common/delete.modal.component';
import { ForbiddenComponent } from './common/forbidden.component';
import { HomeComponent } from './home/home.component';
import { MemberFormComponent } from './members/member.form.component';
import { SuccessModalComponent } from './common/success.modal.component';
import { ConfirmModalComponent } from './common/confirm.modal.component';
import { ElectionFormComponent } from './elections/election.form.component';
import { VotersComponent } from './voters/voters.component';
import { ElectionComponent } from './voters/election.component';
import { RegistrationComponent } from './voters/registration.component';
import { RegisteredComponent } from './voters/registered.component';
import { MembersUploadComponent } from './members/members.upload.component';
import { ChangepasswordComponent } from './users/changepassword.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'ballot', component: BallotComponent, canActivate: [AuthGuard] },
  { path: 'forbidden/:path', component: ForbiddenComponent, canActivate: [AuthGuard] },
  { path: 'elections', component: ElectionsComponent, canActivate: [AuthGuard] },
  { path: 'voters', component: VotersComponent, canActivate: [AuthGuard] },
  { path: 'elections/form', component: ElectionFormComponent, canActivate: [AuthGuard] },
  { path: 'elections/form/:id', component: ElectionFormComponent, canActivate: [AuthGuard] },
  { path: 'elections/form/:id/:action', component: ElectionFormComponent, canActivate: [AuthGuard] },
  { path: 'members', component: MembersComponent, canActivate: [AuthGuard] },
  { path: 'members/form', component: MemberFormComponent, canActivate: [AuthGuard] },
  { path: 'members/form/:id', component: MemberFormComponent, canActivate: [AuthGuard] },
  { path: 'members/form/:id/:action', component: MemberFormComponent, canActivate: [AuthGuard] },
  { path: 'reports', component: ReportsComponent, canActivate: [AuthGuard] },
  { path: 'reports/:id', component: ReportsComponent, canActivate: [AuthGuard] },
  { path: 'administrators', component: UsersComponent, canActivate: [AuthGuard] },
  { path: 'administrators/form', component: UserFormComponent, canActivate: [AuthGuard] },
  { path: 'administrators/form/:id', component: UserFormComponent, canActivate: [AuthGuard] },
  { path: 'administrators/form/:id/:action', component: UserFormComponent, canActivate: [AuthGuard] },
  { path: 'changepassword', component: ChangepasswordComponent, canActivate: [AuthGuard] },
];

@NgModule({
  declarations: [
    AppComponent,
    BallotComponent,
    ElectionsComponent,
    LoginModalComponent,
    UsersComponent,
    MembersComponent,
    ReportsComponent,
    UserFormComponent,
    ErrorModalComponent,
    TableComponent,
    TableSearchComponent,
    DeleteModalComponent,
    ForbiddenComponent,
    HomeComponent,
    MemberFormComponent,
    SuccessModalComponent,
    ConfirmModalComponent,
    ElectionFormComponent,
    VotersComponent,
    ElectionComponent,
    RegistrationComponent,
    RegisteredComponent,
    MembersUploadComponent,
    ChangepasswordComponent,
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    NgbModalModule,
    NgbCollapseModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    NgbTypeaheadModule,
    NgbProgressbarModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: environment.enableTracing }
    ),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    StorageServiceModule
  ],
  entryComponents: [
    ErrorModalComponent,
    DeleteModalComponent,
    ConfirmModalComponent,
    SuccessModalComponent,
    MembersUploadComponent,
  ],
  providers: [
    NgbActiveModal,
    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
