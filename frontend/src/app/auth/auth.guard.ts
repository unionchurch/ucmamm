import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';

import { LoginService } from '../common/login.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(private loginService: LoginService, private router: Router) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.checkLogin(state.url);
    }

    checkLogin(url: string): boolean {
        const isLoggedIn = this.loginService.isLoggedIn();
        const isHomePageOrReportsPage = url === '/' || url.startsWith('/reports');

        if (isHomePageOrReportsPage) {
            return true;
        }

        if (isLoggedIn) {
            const authorized = this.isAuthorized(url);

            if (authorized) {
                return true;
            }

            this.router.navigate([`/forbidden/${url.split('/')[1]}`]);

            return true;
        }

        this.loginService.notifyNoLogin();

        this.router.navigate(['/']);

        return true;
    }

    private isAuthorized(url: string): boolean {
        if (url.startsWith('/elections')) {
            return this.hasRoles(['ROLE_USER', 'ROLE_USER_2', 'ROLE_ELECTION_ADMIN']);
        }
        if (url.startsWith('/voters')) {
            return this.hasRoles(['ROLE_USER', 'ROLE_USER_2', 'ROLE_VOTER_ADMIN']);
        }
        if (url.startsWith('/ballot')) {
            return this.hasRoles(['ROLE_BALLOT_ENCODER']);
        }
        if (url.startsWith('/members')) {
            return this.hasRoles(['ROLE_USER', 'ROLE_USER_2', 'ROLE_MEMBER_ADMIN']);
        }
        if (url.startsWith('/administrators')) {
            return this.hasRoles(['ROLE_ADMIN']);
        }
        return true;
    }

    private hasRoles(roleNames: string[]): boolean {
        let passed = false;
        roleNames.forEach(roleName => {
            passed = passed || this.loginService.hasRole(roleName);
        });
        return passed;
    }
}
