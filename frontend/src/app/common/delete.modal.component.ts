import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'delete-modal',
  templateUrl: './delete.modal.component.html',
  styleUrls: ['./delete.modal.component.css']
})
export class DeleteModalComponent implements OnInit {

  @Input() domainId;
  @Input() title;
  @Input() domainName;
  @Input() description;

  @Output() onApproveDelete: EventEmitter<any> = new EventEmitter();

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  emitApproveDelete() {
    this.onApproveDelete.emit([this.domainId]);
    this.activeModal.dismiss();
  }

}
