import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'confirm-modal',
  templateUrl: './confirm.modal.component.html',
  styleUrls: ['./confirm.modal.component.css']
})
export class ConfirmModalComponent implements OnInit {

  @Input() action;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  emitApprove() {
    this.activeModal.close({ approved: true });
  }

  emitDisapprove() {
    this.activeModal.close({ approved: false });
  }

}
