import { HttpErrorResponse } from '@angular/common/http';

import { Observable, OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';

const errorMapper: OperatorFunction<{}, HttpErrorResponse> = map(value => value as HttpErrorResponse);

export { errorMapper };

export interface BaseService<T> {

    get(id: string): Observable<T>;

    create(domain: T): Observable<HttpErrorResponse>;

    update(domain: T): Observable<HttpErrorResponse>;

    delete(id: string): Observable<HttpErrorResponse>;

}