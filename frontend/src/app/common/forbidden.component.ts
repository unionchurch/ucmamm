import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-forbidden',
  templateUrl: './forbidden.component.html',
  styleUrls: ['./forbidden.component.css']
})
export class ForbiddenComponent implements OnInit {

  accessedPage: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.accessedPage = this.route.snapshot.paramMap.get('path');
  }

}
