import { Component, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'table-search',
  templateUrl: './table.search.component.html',
  styleUrls: ['./table.search.component.css']
})
export class TableSearchComponent implements OnChanges {

  @Input() searchKeyword;

  @Output() onSearchKeywordChanged: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    const keyword = changes.searchKeyword;
    if ((keyword.previousValue && !keyword.currentValue) || keyword.currentValue.length >= 3) {
      this.onSearchKeywordChanged.emit();
    }
  }

}
