import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ErrorModalComponent } from './error.modal.component';
import { ConfirmModalComponent } from './confirm.modal.component';
import { SuccessModalComponent } from './success.modal.component';

@Injectable({
    providedIn: 'root'
})
export class MessageService {
    messages: string[] = [];

    constructor(private modalService: NgbModal) {
    }

    add(message: string) {
        this.messages.push(message);
    }

    clear() {
        this.messages = [];
    }

    extractBadRequestErrors(error: HttpErrorResponse | any) {
        return error && error.error && error.error.errors ? error.error.errors : null;
    }

    displayFormError(error: HttpErrorResponse, title: string, fieldNameFilters: string[] = []) {
        const errors = this.extractBadRequestErrors(error);
        const modalRef = this.modalService.open(ErrorModalComponent, { centered: true });
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.messages = errors.map((error:any) => {
            if (fieldNameFilters && fieldNameFilters.indexOf(error.field) > -1) {
                return `${error.message}`;
            }
            return `${error.name} ${error.message}`;
        });
    }

    confirm(action: string, callback) {
        const modalRef = this.modalService.open(ConfirmModalComponent, { centered: true });
        modalRef.componentInstance.action = action;
        modalRef.result.then((result) => callback && callback(result)).catch(() => {});
    }

    displayModalMessage(title: string, message: string, callback) {
        const modalRef = this.modalService.open(SuccessModalComponent, { centered: true });
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.message = message;
        modalRef.result.then(() => callback && callback()).catch(() => {});
    }

    displayExpiringModalMessage(title: string, message: string, callback, timeout: number) {
        const modalRef = this.modalService.open(SuccessModalComponent, { centered: true });
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.message = message;
        modalRef.result.then(() => callback && callback()).catch(() => {});
        setTimeout(() => modalRef && modalRef.close(), timeout);
    }

}
