export class TableEvent {
    name: string;
    domain: string;
    data: any;
}
