import { Component, OnInit, Input } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'success-modal',
  templateUrl: './success.modal.component.html',
  styleUrls: ['./success.modal.component.css']
})
export class SuccessModalComponent implements OnInit {

  @Input() title;
  @Input() message;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
