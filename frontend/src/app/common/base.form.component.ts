import { OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { MessageService } from './message.service';
import { BaseService } from './base.service';

export abstract class BaseFormComponent<T, SERVICE extends BaseService<T>> implements OnInit, OnDestroy {

    private VIEW_ONLY = 'view';

    update: boolean;
    viewOnly: boolean;

    private id: string;
    private form: FormGroup;
    private action: string;
    
    constructor(public formBuilder: FormBuilder, public router: Router, public route: ActivatedRoute, public messageService: MessageService) {

    }

    ngOnInit() {
        this.initializeFormVariables()
        this.form = this.buildForm();
        this.initializeOtherFormVariables(this.form);
        this.update && this.updateForm(this.form);
        this.viewOnly && this.form.disable();
        this.routeToList = this.routeToList.bind(this);
    }

    initializeFormVariables(): void {
        this.id = this.route.snapshot.paramMap.get('id');
        this.action = this.route.snapshot.paramMap.get('action');
        this.update = this.id != undefined;
        this.viewOnly = this.action === this.VIEW_ONLY;
    }

    initializeOtherFormVariables(form: FormGroup) {

    }

    updateForm(form: FormGroup): void {
        this.getMainService().get(this.id).subscribe((domain: T) => this.updateFormValues(form, domain));
    }

    onSubmit(): void {
        const domain = this.form.value as T;
        if (this.update) {
            this.prepareDomainToUpdate(domain);
            this.updateDomain(domain);
        } else {
            this.createDomain(domain);
        }
    }

    onCancel() {
        this.messageService.confirm(this.id != undefined ? 'cancel election update' :'cancel election creation', (result) => {
          if (result.approved) {
            if (this.action === this.VIEW_ONLY) {
              this.form.disable();
            } else {
              this.routeToList()
            }
          }
        });
    }
  
    getId(): string {
        return this.id;
    }

    getForm(): FormGroup {
        return this.form;
    }

    loadEditForm() {
        return this.form.enable();
    }

    createDomain(domain: T): void {
        this.getMainService().create(domain).subscribe((error: HttpErrorResponse) => {
            if (error) {
                this.onCreateError(error);
            } else {
                this.onCreateSuccess(domain);
            }
        });
    }

    updateDomain(domain: T): void {
        this.getMainService().update(domain).subscribe((error: HttpErrorResponse) => {
            if (error) {
                this.onUpdateError(error);
            } else {
                this.onUpdateSuccess(domain);
            }
        });
    }

    onCreateSuccess(domain: T): void {
        this.messageService.displayModalMessage(`Create ${this.getDomain()} Successful`, `You have successfully created ${this.getDomainPhrase()}`, this.routeToList);
    }

    onCreateError(error: HttpErrorResponse): void {
        this.messageService.displayFormError(error, `Create ${this.getDomain()} Error`);
    }

    onUpdateSuccess(domain: T): void {
        this.messageService.displayModalMessage(`Update ${this.getDomain()} Successful`, `You have successfully updated ${this.getDomainPhrase()}`, this.routeToList);
    }

    onUpdateError(error: HttpErrorResponse): void {
        this.messageService.displayFormError(error, `Update ${this.getDomain()} Error`, ['electionIdAndActive']);
    }

    routeToList(): void {
        this.router.navigate([`/${this.getDomain().toLowerCase()}s`]);
    }

    private getDomainPhrase(): string {
        const domain = this.getDomain().toLowerCase();
        if (domain.match(/[aeiou][a-z]+/)) {
            return `an ${domain}`;
        }
        return `a ${domain}`;
    }

    abstract buildForm(): FormGroup;

    abstract updateFormValues(form: FormGroup, domain: T): void;

    abstract prepareDomainToUpdate(domain: T): void;

    abstract getMainService(): SERVICE;

    abstract getDomain(): string;

    getMessageService(): MessageService {
        return this.messageService;
    }

    ngOnDestroy() {

    }
}