import { Injectable } from '@angular/core';

import { NgbDateStruct, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class NgbSimpleDateAdapter extends NgbDateAdapter<string> {

    fromModel(value: string): NgbDateStruct {
        if (!value) {
            return null;
        }
        const split = value.split('-');
        return {
            year: parseInt(split[0], 10),
            month: parseInt(split[1], 10),
            day: parseInt(split[2], 10)
        }
    }

    toModel(date: NgbDateStruct): string {
        if (!date) {
            return null;
        }
        return `${date.year}-${this.pad(date.month)}-${this.pad(date.day)}`;
    }

    private pad(i: number): string {
        return i < 10 ? `0${i}` : `${i}`;
    }

}