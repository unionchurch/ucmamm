import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpParams, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';

import { LoginService } from './login.service';
import { Utils } from './utils';

@Injectable({
    providedIn: 'root'
})
export class RestapiService {

    constructor(private http: HttpClient, @Inject(SESSION_STORAGE) private storageService: StorageService, private loginService: LoginService, private utils: Utils) { }

    get<T>(uri: string, operation: string, params = {}): Observable<{} | T> {
        return this.loginService.performRestOperation(() => this.http.get(uri, this.utils.buildHttpOptions(params))).pipe(
            catchError(this.utils.handleError<T>(operation || 'Unnamed Operation'))
        );
    }

    getNoAuth<T>(uri: string, params = {}): Observable<{} | T> {
        return this.http.get(uri, params);
    }

    post<T>(uri: string, data: T, operation: string, params = {}): Observable<{} | T> {
        return this.loginService.performRestOperation(() => this.http.post(uri, data, this.utils.buildHttpOptions(params)));
    }

    postNoAuth<T>(uri: string, data: T, operation: string, params = {}): Observable<{} | T> {
        return this.http.post(uri, data, this.utils.buildHttpOptionsNoAuth(params));
    }

    put<T>(uri: string, data: T, operation: string, params = {}): Observable<{} | T> {
        return this.loginService.performRestOperation(() => this.http.put(uri, data, this.utils.buildHttpOptions(params)));
    }

    delete<T>(uri: string, operation: string, params = {}): Observable<{} | T> {
        return this.loginService.performRestOperation(() => this.http.delete(uri, this.utils.buildHttpOptions(params))).pipe(
            catchError(this.utils.handleError<any>(operation || 'Unnamed Operation'))
        );
    }

    request<T>(method: string, uri: string, form: any, params: HttpParams): Observable<HttpEvent<{}>> {
        return this.http.request(new HttpRequest(method, uri, form, { reportProgress: true, params, headers: this.utils.buildHttpHeadersWithAuthOnly() }));
    }

}
