import { User } from './user';

export class LoginEvent {
    loggedIn: boolean;
    user: User;
}

export class ProtectedLinkClickedEvent {
    loggedIn: boolean;
}