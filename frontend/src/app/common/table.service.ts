import { Injectable } from '@angular/core';

import { Observable, interval } from 'rxjs';
import { map, filter, tap } from 'rxjs/operators';

import { TableEvent } from './table.event';

@Injectable({
    providedIn: 'root'
})
export class TableService {

    private eventQueue: TableEvent[] = [];
    private tableEventObservable: Observable<TableEvent>;

    constructor() {
        this.tableEventObservable = interval(1000).pipe(
            map(__ => this.eventQueue.pop()),
            filter(event => event != undefined)
        );
    }

    publishTableEvent(event: TableEvent) {
        this.eventQueue.push(event);
    }

    listenToTableEvents(callback) {
        this.tableEventObservable.subscribe((event: TableEvent) => callback && callback(event));
    }
  
}
