import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { faAngleDown, faAngleUp, faPlus, faArrowDown, faEdit, faTrashAlt, faEye } from '@fortawesome/free-solid-svg-icons';

import { TableHeader, TableRow, TableReloadRequest } from './table';
import { TableService } from './table.service';
import { TableEvent } from './table.event';
import { LoginService } from './login.service';

import { DeleteModalComponent } from './delete.modal.component';


@Component({
    selector: 'custom-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

    @Input() headers: TableHeader[];
    @Input() rows: TableRow[];
    @Input() domainIcon: any;
    @Input() domainName: string;
    @Input() domainFormLink: string;
    @Input() isLastRow: boolean;
    @Input() allowView: boolean;
    @Input() allowDelete: boolean;
    @Input() allowEdit: boolean;
    @Input() searchKeyword: string;
    @Input() activeFilterShown: boolean;
    @Input() hideSearchPanel: boolean;
    @Input() allowDeleteActiveRow: boolean = true;
    @Input() addButtonShown: boolean = true;
    @Input() adminRole: string;
  
    @Output() onTableReloadRequest: EventEmitter<any> = new EventEmitter();
    @Output() onDeleteRow: EventEmitter<any> = new EventEmitter();

    page = 0;
    size = 20;
    keyword = '';
    sort = 'userName';
    ascending = true;

    sortedAscending: string;
    sortedDescending: string;

    faAngleUp = faAngleUp;
    faAngleDown = faAngleDown;
    faPlus = faPlus;
    faArrowDown = faArrowDown;
    faEdit = faEdit;
    faEye = faEye;
    faTrashAlt = faTrashAlt;

    private activeFilterSelected: string = '';

    constructor(private router: Router, private modalService: NgbModal, private tableService: TableService, private loginService: LoginService) { }

    ngOnInit() {
        this.tableService.listenToTableEvents((event: TableEvent = {} as TableEvent) => {
            switch (event.name) {
                case 'clearSearch':
                    this.keyword = '';
                case 'reset':
                    this.page = 0;
                    this.size = 20;
                    this.keyword = '';
                    this.sort = 'userName';
                    this.ascending = true;
                    this.activeFilterSelected = '';
                default:
            }
        });
    }

    headerClicked(header: TableHeader) {
        if (header.field === this.sortedAscending) {
            this.sortedAscending = undefined;
            this.sortedDescending = header.field;
        } else {
            this.sortedDescending = undefined;
            this.sortedAscending = header.field;
        }
        this.sort = header.field;
        this.ascending = this.sortedAscending != undefined;
        this.emitTableReloadRequest();
    }

    loadMoreRequested() {
        this.size += 20;
        this.emitTableReloadRequest();
    }

    onSearchKeywordChanged() {
        this.size = 20;
        this.emitTableReloadRequest();
    }

    emitTableReloadRequest() {
       this.onTableReloadRequest.emit([{ page: this.page, size: this.size, keyword: this.keyword, sort: this.sort, ascending: this.ascending, activeFilter: this.activeFilterSelected } as TableReloadRequest]);
    }

    loadEditForm(id: string, viewOnly: boolean) {
        if (viewOnly) {
            this.router.navigate([`${this.domainFormLink}/${id}/view`]);
        } else {
            this.router.navigate([`${this.domainFormLink}/${id}`]);
        }
    }

    loadDeleteForm(row: TableRow) {
        const modalRef = this.modalService.open(DeleteModalComponent, { centered: true });
        modalRef.componentInstance.domainId = row.id;
        modalRef.componentInstance.title = `Delete ${this.domainName}`;
        modalRef.componentInstance.domainName = this.domainName.toLowerCase();
        modalRef.componentInstance.description = row.description;
        modalRef.componentInstance.onApproveDelete = this.onDeleteRow;
    }

    clearSearchField() {
        this.keyword = '';
        this.emitTableReloadRequest();
    }

    get hasView() {
        return this.allowView !== false;
    }

    get hasData() {
        return (this.rows || []).length > 0;
    }

    get hasDelete() {
        if (this.adminRole && !this.loginService.hasRole(this.adminRole)) {
            return false;
        }
        return this.allowDelete !== false;
    }

    get hasEdit() {
        if (this.adminRole && !this.loginService.hasRole(this.adminRole)) {
            return false;
        }
        return this.allowEdit !== false;
    }

    get tooFewRecords(): boolean {
        return this.rows.length < 20;
    }

    get hasAddButton() {
        return this.addButtonShown && (!this.adminRole || this.loginService.hasRole(this.adminRole));
    }

    get activeFilter() {
        return this.activeFilterSelected;
    }

    set activeFilter(activeFilter) {
        this.activeFilterSelected = activeFilter;
        this.onTableReloadRequest.emit([{ page: this.page, size: this.size, keyword: this.keyword, sort: this.sort, ascending: this.ascending, activeFilter: activeFilter } as TableReloadRequest]);
    }

}
