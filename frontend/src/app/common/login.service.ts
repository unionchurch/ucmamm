import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable, Observer, of, interval } from 'rxjs';
import { catchError, tap, map, filter } from 'rxjs/operators';

import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';

import * as jwtDecode from 'jwt-decode';

import { User } from './user';
import Keys from './common.keys';
import { Utils } from './utils';
import { AccessToken } from './access.token';
import { LoginEvent, ProtectedLinkClickedEvent } from './login.event';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    private loggedIn: boolean = false;
    private user: User;
    private roles: string[] = [];
    private protectedLinkClickedEventQueue: ProtectedLinkClickedEvent[] = [];

    loginEvents = new Observable<LoginEvent>((observer: Observer<LoginEvent>) => {

        const nextInterval = setInterval(() => {
            observer.next({ loggedIn: this.loggedIn, user: this.user } as LoginEvent);
        }, 1000);

        return { unsubscribe() { clearInterval(nextInterval); } };
    });

    protectedLinkClickedEvents = interval(1000).pipe(
        map(_ => this.protectedLinkClickedEventQueue.pop()),
        filter(event => event != undefined)
    );

    constructor(@Inject(SESSION_STORAGE) private storageService: StorageService, private http: HttpClient, private utils: Utils) { }

    getCurrentUser() {
        return this.http.get('user', this.utils.buildHttpOptions()).pipe(
            tap((user: User) => this.setUser(user)),
            catchError((error: any): Observable<User> => {
                if (error.status === 401) {
                    this.setLogin(false);
                }

                return of();
            })
        );
    }

    setLogin(loggedIn: boolean) {
        this.loggedIn = loggedIn;
        if (!loggedIn) {
            this.storageService.remove(Keys.TOKEN_KEY);
            this.storageService.remove(Keys.TOKEN_EXPIRY_KEY);
        }
    }

    isLoggedIn(): boolean {
        return this.loggedIn;
    }

    setUser(user: User) {
        this.user = user;
        if (user) {
            this.roles = user.roles;
        }
    }

    getUser(): User {
        return this.user;
    }

    getToken(username: string, password: string) {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
              'Authorization': `Basic ${btoa('ucmamm:ucmammsecret')}`
            }),
            params: new HttpParams().set('grant_type', 'password').set('username', username).set('password', password).set('scope', 'read')
        };
        return this.http.post<AccessToken>('oauth/token', {}, httpOptions).pipe(
           catchError(this.utils.handleError<AccessToken>(`Log-in`))
        );
    }

    hasRole(roleName: string): boolean {
        return (this.roles || []).indexOf(roleName) > -1;
    }

    isUserAdminOnly() {
        return (this.roles || []).length == 1 && this.hasRole('ROLE_ADMIN');
    }

    performRestOperation<T>(operation): Observable<T> {
        if (this.validateToken()) {
            return operation().pipe(
                catchError((error: any): Observable<T> => {
                    if (error.status === 401) {
                        this.setLogin(false);
                    }

                    return error.status >= 400 ? of(error) : of();
                })
            );
        }

        this.setLogin(false);
        this.setUser(undefined);

        return of();
    }

    validateToken() {
        try {
            const jwt = jwtDecode(this.storageService.get(Keys.TOKEN_KEY));
            this.setLogin(true);
            this.roles = jwt.authorities;
            return true;
        } catch (error) {
            return false;
        }
    }

    notifyNoLogin() {
        this.protectedLinkClickedEventQueue.push({ loggedIn: false } as ProtectedLinkClickedEvent);
    }

}
