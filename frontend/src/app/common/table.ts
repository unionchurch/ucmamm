export class TableHeader {
    name: string;
    field: string;
    sortable: boolean;
}

export class TableColumn {
    value: any;
}

export class TableRow {
    id: string;
    columns: TableColumn[];
    description: string;
    disabled: boolean;
    active: boolean;
}

export class TableReloadRequest {
    page: number;
    size: number;
    keyword: string;
    sort: string;
    ascending: boolean;
    activeFilter: string;
}

export const defaultTableReloadRequest = (sort: string) => {
    return { page: 0, size: 20, keyword: '', sort, ascending: true, activeFilter: '' } as TableReloadRequest;
};
