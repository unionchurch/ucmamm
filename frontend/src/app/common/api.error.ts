export class ApiError {
    field: string;
    name: string;
    message: string;
}