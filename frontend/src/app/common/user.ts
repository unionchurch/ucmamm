export class User {
    id: string;
    userName: string;
    firstName: string;
    middleName: string;
    lastName: string;
    deleted: boolean;
    active: boolean;
    version: number;
    roles: string[];
}

export class Role {
    roleName: string;
    title: string;
    description: string;
}
