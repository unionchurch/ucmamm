import { Injectable, Inject } from '@angular/core';
import { HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';

import { MessageService } from './message.service';

import Keys from './common.keys';

@Injectable({
  providedIn: 'root'
})
export class Utils {

    constructor(@Inject(SESSION_STORAGE) private storageService: StorageService, private messageService: MessageService) { }

    handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            console.error(error);

            this.messageService.add(`${operation} failed`);

            return of(result as T);
        };
    }

    extractBadRequestErrors(error: HttpErrorResponse | any) {
        return error && error.error && error.error.errors ? error.error.errors : null;
    }

    buildHttpParam(params = {}) {
        let httpParams = new HttpParams();
        Object.entries(params).forEach(entry => {
            httpParams = httpParams.set(entry[0], String(entry[1]));
        });
        return httpParams;
    }

    buildHttpHeadersWithAuthOnly() {
        const token = this.getToken();
        return new HttpHeaders({
            'Authorization': `Bearer ${token}`,
        });
    }
    
    buildHttpHeadersWithAuth() {
        const token = this.getToken();
        return new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
        });
    }

    buildHttpHeadersWithNoAuth() {
        return new HttpHeaders({
            'Content-Type': 'application/json',
        });
    }

    buildHttpOptions(params = {}) {
        const token = this.getToken();
        return {
            headers: this.buildHttpHeadersWithAuth(),
            params: this.buildHttpParam(params)
        };
    }

    buildHttpOptionsNoAuth(params = {}) {
        return {
            headers: this.buildHttpHeadersWithNoAuth(),
            params: this.buildHttpParam(params)
        };
    }

    getToken(): string {
        return this.storageService.get(Keys.TOKEN_KEY);
    }

} 